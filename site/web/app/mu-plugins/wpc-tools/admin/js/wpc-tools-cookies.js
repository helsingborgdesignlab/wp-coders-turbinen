(function ($) {
  'use strict';

  var wpcToolsAdminCookies = {
    init: function () {

      this.initColorPicker();

    },
    initColorPicker: function () {
      $('.color-picker').wpColorPicker();
    },
  };

  $(function () {
    wpcToolsAdminCookies.init();
  });

})(jQuery);
