(function ($) {
  'use strict';

  var wpcToolsAdmin = {
    init: function () {

      this.mediaUploader = null;

      this.setListeners();

    },
    setListeners: function () {
      var that = this;

      $(document).on("click", ".media-upload-button", function (e) {
        e.preventDefault();

        var inputId = $(this).data('input');

        that.manageMediaUploads(inputId, this);
      });

      $(document).on("click", ".media-remove-button", function (e) {
        e.preventDefault();

        var inputId = $(this).data('input');

        that.manageMediaRemove(inputId, this);
      });
    },
    manageMediaUploads: function (inputId, button) {
      var that = this;

      // If the uploader object has already been created, reopen the dialog
      if (that.mediaUploader) {
        that.mediaUploader.insertTo = inputId;
        that.mediaUploader.open();
        return;
      }
      // Extend the wp.media object
      that.mediaUploader = wp.media.frames.file_frame = wp.media({
        title: 'Choose Image',
        button: {
          text: 'Choose Image'
        },
        multiple: false,
        library: {
          type : ['image/jpeg', 'image/png']
        }
      });

      that.mediaUploader.insertTo = inputId;

      // When a file is selected, grab the URL and set it as the text field's value
      that.mediaUploader.on('select', function () {
        var attachment = that.mediaUploader.state().get('selection').first().toJSON();

        $('#' + that.mediaUploader.insertTo).val(attachment.id);

        $('#preview-' + that.mediaUploader.insertTo).empty().append(
          '<img src="' + attachment.url + '" title="' + attachment.title + '" alt="' + attachment.alt + '">'
        ).removeClass('hidden').parent().find('.media-remove-button').show();
      });
      // Open the uploader dialog
      that.mediaUploader.open();
    },
    manageMediaRemove: function(inputId, button) {
      $('#' + inputId).val("");
      $('#preview-' + inputId).empty();
      $(button).hide();
    }
  };

  $(function () {
    wpcToolsAdmin.init();
  });

})(jQuery);
