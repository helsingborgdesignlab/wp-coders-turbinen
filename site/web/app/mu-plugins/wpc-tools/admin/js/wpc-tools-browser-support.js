(function ($) {
  'use strict';

  var wpcToolsAdminBrowserSupport = {
    init: function () {

      this.initColorPicker();

    },
    initColorPicker: function () {
      $('.color-picker').wpColorPicker();
    },
  };

  $(function () {
    wpcToolsAdminBrowserSupport.init();
  });

})(jQuery);
