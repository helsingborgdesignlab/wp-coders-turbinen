<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://wpcoders.io
 * @since      1.0.0
 *
 * @package    WPC_Tools
 * @subpackage WPC_Tools/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WPC_Tools
 * @subpackage WPC_Tools/admin
 * @author     Robert Bokori <robert@wpcoders.io>
 */
class WPC_Tools_Admin {

  const MENU_SLUG = WPC_Tools::PLUGIN_NAME;

  /**
   * Admin modules array
   */
  private $modules = array();

  /**
   * The single instance of the class.
   */
  protected static $_instance = null;

  /**
   * WPC Tools Admin Instance.
   *
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * Initialize the class and set its properties.
   */
  public function __construct() {
    $this->load_modules();
  }

  /**
   * Load modules
   *
   * @TODO write an autoloader
   */
  public function load_modules() {

    // Abstract module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-abstract-module.php';

    // General module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-general.php';
    $this->set_module( WPC_Tools_Admin_Module_General::instance() );

    // Browser Support module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-browser-support.php';
    $this->set_module( WPC_Tools_Admin_Module_Browser_Support::instance() );

    // Cookies module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-cookies.php';
    $this->set_module( WPC_Tools_Admin_Module_Cookies::instance() );

    // Defaults module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-defaults.php';
    $this->set_module( WPC_Tools_Admin_Module_Defaults::instance() );

    // Emails module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-emails.php';
    $this->set_module( WPC_Tools_Admin_Module_Emails::instance() );

    // Gravity Forms module
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/modules/class-gravity-forms.php';
    $this->set_module( WPC_Tools_Admin_Module_Gravity_Forms::instance() );

    do_action('wpc_tools_add_admin_module');
  }

  /**
   * Add module to the modules array
   */
  public function set_module( WPC_Tools_Admin_Module $module ) {
    $this->modules[ $module::MODULE_NAME ] = $module;
  }
}
