<?php

class WPC_Tools_Admin_Module_Browser_Support extends WPC_Tools_Admin_Module {

  const MODULE_NAME = 'wpc-tools-browser-support';

  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Admin_Module_Browser_Support constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->set_defaults();
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'admin_init', $this, 'settings_init', 3 );
    WPC_Tools::loader()->add_action( 'admin_menu', $this, 'load_menu', 20 );
  }

  /**
   * Add module menu item on wp-admin
   */
  public function load_menu( $parent_id ) {

    $this->menu_id = add_submenu_page(
      WPC_Tools_Admin::MENU_SLUG,
      __( 'Browser Support', WPC_Tools::TEXT_DOMAIN ),
      __( 'Browser Support', WPC_Tools::TEXT_DOMAIN ),
      'manage_options',
      self::MODULE_NAME,
      [ $this, 'settings_html' ]
    );

    add_action( 'load-' . $this->menu_id, [ $this, 'load_assets' ] );

    global $submenu;
    if ( isset( $submenu[ WPC_Tools_Admin::MENU_SLUG ] ) && current_user_can( 'manage_options' ) ) {
      $submenu[ WPC_Tools_Admin::MENU_SLUG ][0][0] = __( 'General', WPC_Tools::TEXT_DOMAIN );
    }
  }

  /**
   * Register all assets required by the admin plugin.
   */
  public function load_assets() {
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
  }

  /**
   * Register the stylesheets for the admin area.
   */
  public function enqueue_styles() {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_style( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/css/' . self::MODULE_NAME . '.css', [], WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript for the admin area.
   */
  public function enqueue_scripts() {
    wp_enqueue_media();
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/js/' . self::MODULE_NAME . '.js', [ 'jquery' ], WPC_Tools::VERSION, false );
  }

  /**
   * Theme page callback
   */
  public function settings_html() {
    ?>
    <div class="wrap">
      <h1>
        WPC Tools: <?= __( 'Browser Support', WPC_Tools::TEXT_DOMAIN ); ?>
      </h1>

      <form method="post" action="options.php">
        <?php
        settings_fields( $this->settings_title );
        do_settings_sections( $this->settings_title );
        submit_button();
        ?>
      </form>
    </div>
    <?php
  }

  /**
   * Register setting fields
   */
  public function settings_init() {
    register_setting(
      $this->settings_title,
      $this->settings_title,
      [ $this, 'sanitize_settings' ]
    );

    add_settings_section(
      $this->settings_title . '_general_settings',
      __( 'General Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_enable_browser_warning',
      __( 'Enable Browser Warning', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[enable_browser_warning]',
        "field_id"      => $this->settings_title . '_enable_browser_warning',
        "setting_field" => 'enable_browser_warning',
        "field_class"   => ''
      ]
    );

    add_settings_section(
      $this->settings_title . '_browser_notification_settings',
      __( 'The following browsers will be notified', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_browser_v_ie_edge',
      __( 'Internet Explorer / Edge', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_dropdown' ],
      $this->settings_title,
      $this->settings_title . '_browser_notification_settings',
      [
        "setting_field" => 'browser_v_ie_edge',
        "field_name"    => $this->settings_title . '[browser_v_ie_edge]',
        "field_id"      => $this->settings_title . '_browser_v_ie_edge',
        "field_class"   => '',
        "field_options" => [
          '16'    => __( '<= 16', WPC_Tools::TEXT_DOMAIN ),
          '15'    => __( '<= 15', WPC_Tools::TEXT_DOMAIN ),
          '14'    => __( '<= 14', WPC_Tools::TEXT_DOMAIN ),
          '13'    => __( '<= 13', WPC_Tools::TEXT_DOMAIN ),
          '12'    => __( '<= 12', WPC_Tools::TEXT_DOMAIN ),
          '11'    => __( '<= 11', WPC_Tools::TEXT_DOMAIN ),
          '10'    => __( '<= 10', WPC_Tools::TEXT_DOMAIN ),
          '9'     => __( '<= 9', WPC_Tools::TEXT_DOMAIN ),
          '-6'    => __( 'more than 6 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-5'    => __( 'more than 5 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-4'    => __( 'more than 4 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-3'    => __( 'more than 3 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-2'    => __( 'more than 2 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-1'    => __( 'more than 1 versions behind', WPC_Tools::TEXT_DOMAIN ),
        ],
      ]
    );

    add_settings_field(
      $this->settings_title . '_browser_v_safari',
      __( 'Safari', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_dropdown' ],
      $this->settings_title,
      $this->settings_title . '_browser_notification_settings',
      [
        "setting_field" => 'browser_v_safari',
        "field_name"    => $this->settings_title . '[browser_v_safari]',
        "field_id"      => $this->settings_title . '_browser_v_safari',
        "field_class"   => '',
        "field_options" => [
          '-6'    => __( 'more than 6 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-5'    => __( 'more than 5 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-4'    => __( 'more than 4 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-3'    => __( 'more than 3 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-2'    => __( 'more than 2 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-1'    => __( 'more than 1 versions behind', WPC_Tools::TEXT_DOMAIN ),
        ],
      ]
    );

    add_settings_field(
      $this->settings_title . '_browser_v_chrome',
      __( 'Chrome', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_dropdown' ],
      $this->settings_title,
      $this->settings_title . '_browser_notification_settings',
      [
        "setting_field" => 'browser_v_chrome',
        "field_name"    => $this->settings_title . '[browser_v_chrome]',
        "field_id"      => $this->settings_title . '_browser_v_chrome',
        "field_class"   => '',
        "field_options" => [
          '-6'    => __( 'more than 6 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-5'    => __( 'more than 5 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-4'    => __( 'more than 4 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-3'    => __( 'more than 3 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-2'    => __( 'more than 2 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-1'    => __( 'more than 1 versions behind', WPC_Tools::TEXT_DOMAIN ),
        ],
      ]
    );

    add_settings_field(
      $this->settings_title . '_browser_v_firefox',
      __( 'Firefox', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_dropdown' ],
      $this->settings_title,
      $this->settings_title . '_browser_notification_settings',
      [
        "setting_field" => 'browser_v_firefox',
        "field_name"    => $this->settings_title . '[browser_v_firefox]',
        "field_id"      => $this->settings_title . '_browser_v_firefox',
        "field_class"   => '',
        "field_options" => [
          '-6'    => __( 'more than 6 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-5'    => __( 'more than 5 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-4'    => __( 'more than 4 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-3'    => __( 'more than 3 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-2'    => __( 'more than 2 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-1'    => __( 'more than 1 versions behind', WPC_Tools::TEXT_DOMAIN ),
        ],
      ]
    );

    add_settings_field(
      $this->settings_title . '_browser_v_opera',
      __( 'Opera', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_dropdown' ],
      $this->settings_title,
      $this->settings_title . '_browser_notification_settings',
      [
        "setting_field" => 'browser_v_opera',
        "field_name"    => $this->settings_title . '[browser_v_opera]',
        "field_id"      => $this->settings_title . '_browser_v_opera',
        "field_class"   => '',
        "field_options" => [
          '-6'    => __( 'more than 6 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-5'    => __( 'more than 5 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-4'    => __( 'more than 4 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-3'    => __( 'more than 3 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-2'    => __( 'more than 2 versions behind', WPC_Tools::TEXT_DOMAIN ),
          '-1'    => __( 'more than 1 versions behind', WPC_Tools::TEXT_DOMAIN ),
        ],
      ]
    );

    add_settings_section(
      $this->settings_title . '_modal_settings',
      __( 'Modal Template Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_modal_title',
      __( 'Title', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_title]',
        "field_id"      => $this->settings_title . '_modal_title',
        "setting_field" => 'modal_title',
        "field_class"   => 'regular-text'
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_headline',
      __( 'Headline', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_headline]',
        "field_id"      => $this->settings_title . '_modal_headline',
        "setting_field" => 'modal_headline',
        "field_class"   => 'regular-text'
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_text',
      __( 'Text', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_textarea' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_text]',
        "field_id"      => $this->settings_title . '_modal_text',
        "setting_field" => 'modal_text',
        "field_class"   => 'large-text',
        "field_rows"    => 3
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_remind_button_label',
      __( 'Remind Button Label', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_remind_button_label]',
        "field_id"      => $this->settings_title . '_modal_remind_button_label',
        "setting_field" => 'modal_remind_button_label',
        "field_class"   => 'regular-text'
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_ignore_button_label',
      __( 'Ignore Button Label', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_ignore_button_label]',
        "field_id"      => $this->settings_title . '_modal_ignore_button_label',
        "setting_field" => 'modal_ignore_button_label',
        "field_class"   => 'regular-text'
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_title_color',
      __( 'Title Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_title_color]',
        "field_id"      => $this->settings_title . '_modal_title_color',
        "setting_field" => 'modal_title_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_headline_color',
      __( 'Headline Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_headline_color]',
        "field_id"      => $this->settings_title . '_modal_headline_color',
        "setting_field" => 'modal_headline_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_text_color',
      __( 'Text Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_text_color]',
        "field_id"      => $this->settings_title . '_modal_text_color',
        "setting_field" => 'modal_text_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_remind_button_color',
      __( 'Remind Button Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_remind_button_color]',
        "field_id"      => $this->settings_title . '_modal_remind_button_color',
        "setting_field" => 'modal_remind_button_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_remind_button_background',
      __( 'Remind Button Background', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_remind_button_background]',
        "field_id"      => $this->settings_title . '_modal_remind_button_background',
        "setting_field" => 'modal_remind_button_background',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_ignore_button_color',
      __( 'Ignore Button Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_ignore_button_color]',
        "field_id"      => $this->settings_title . '_modal_ignore_button_color',
        "setting_field" => 'modal_ignore_button_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_modal_ignore_button_background',
      __( 'Ignore Button Background', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_modal_settings',
      [
        "field_name"    => $this->settings_title . '[modal_ignore_button_background]',
        "field_id"      => $this->settings_title . '_modal_ignore_button_background',
        "setting_field" => 'modal_ignore_button_background',
        "field_class"   => 'color-picker',
      ]
    );
  }

  /**
   * Sanitize input fields on save
   */
  public function sanitize_settings( $input_fields ) {

    $valid_fields = WPC_Tools::defaults()->get( $this->settings_title );

    foreach ( $input_fields as $key => $val ) {
      switch ( $key ) {
        case 'modal_title':
        case 'modal_headline':
        case 'modal_text':
        case 'modal_remind_button_label':
        case 'modal_ignore_button_label':
        case 'modal_title_color':
        case 'modal_headline_color':
        case 'modal_text_color':
        case 'modal_remind_button_color':
        case 'modal_remind_button_background':
        case 'modal_ignore_button_color':
        case 'modal_ignore_button_background':
        case 'browser_v_ie_edge':
        case 'browser_v_safari':
        case 'browser_v_chrome':
        case 'browser_v_firefox':
        case 'browser_v_opera':
          $valid_fields[ $key ] = strip_tags( stripslashes( trim( $val ) ) );
          break;
      }
    }

    foreach ( $valid_fields as $key => $val ) {
      switch ( $key ) {
        case 'enable_browser_warning':
          if ( isset( $input_fields[ $key ] ) ) {
            $valid_fields[ $key ] = absint( $val );
          } else {
            $valid_fields[ $key ] = 0;
          }
          break;
      }
    }

    return wp_parse_args( $input_fields, $valid_fields );
  }

  /**
   * Retrieve the default plugin settings
   */
  public function set_defaults() {
    WPC_Tools::defaults()->set( $this->settings_title, [
      'enable_browser_warning'         => 1,
      'modal_title'                    => __( 'Oops...', WPC_Tools::TEXT_DOMAIN ),
      'modal_headline'                 => __( 'It looks like you\'re using an outdated browser!', WPC_Tools::TEXT_DOMAIN ),
      'modal_text'                     => __( 'Our website has detected that you\'re using an outdated browser. This means that our website may not function at its best and certain features may be restricted in access. We suggest that you upgrade your browser for free by using the links below.', WPC_Tools::TEXT_DOMAIN ),
      'modal_remind_button_label'      => __( 'Remind me later', WPC_Tools::TEXT_DOMAIN ),
      'modal_ignore_button_label'      => __( 'No, do not remind me again', WPC_Tools::TEXT_DOMAIN ),
      'modal_title_color'              => '#1ecdff',
      'modal_headline_color'           => '#373838',
      'modal_text_color'               => '#373838',
      'modal_remind_button_color'      => '#ffffff',
      'modal_remind_button_background' => '#f0b535',
      'modal_ignore_button_color'      => '#ffffff',
      'modal_ignore_button_background' => '#d91f19',
      'browser_v_ie_edge'              => '10',
      'browser_v_safari'               => '-3',
      'browser_v_chrome'               => '-6',
      'browser_v_firefox'              => '-6',
      'browser_v_opera'                => '-5',
    ] );
  }
}



