<?php

class WPC_Tools_Admin_Module_Emails extends WPC_Tools_Admin_Module {

  const MODULE_NAME = 'wpc-tools-emails';

  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Emails constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->set_defaults();
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'admin_init', $this, 'settings_init', 3 );
    WPC_Tools::loader()->add_action( 'admin_menu', $this, 'load_menu', 20 );
  }

  /**
   * Add module menu item on wp-admin
   */
  public function load_menu( $parent_id ) {

    $this->menu_id = add_submenu_page(
      WPC_Tools_Admin::MENU_SLUG,
      __( 'Emails', WPC_Tools::TEXT_DOMAIN ),
      __( 'Emails', WPC_Tools::TEXT_DOMAIN ),
      'manage_options',
      self::MODULE_NAME,
      [ $this, 'settings_html' ]
    );

    add_action( 'load-' . $this->menu_id, [ $this, 'load_assets' ] );

    global $submenu;
    if ( isset( $submenu[ WPC_Tools_Admin::MENU_SLUG ] ) && current_user_can( 'manage_options' ) ) {
      $submenu[ WPC_Tools_Admin::MENU_SLUG ][0][0] = __( 'General', WPC_Tools::TEXT_DOMAIN );
    }
  }

  /**
   * Register all assets required by the admin plugin.
   */
  public function load_assets() {
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
  }

  /**
   * Register the stylesheets for the admin area.
   */
  public function enqueue_styles() {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_style( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/css/' . self::MODULE_NAME . '.css', [], WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript for the admin area.
   */
  public function enqueue_scripts() {
    wp_enqueue_media();
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/js/' . self::MODULE_NAME . '.js', [ 'jquery' ], WPC_Tools::VERSION, false );
  }

  /**
   * Theme page callback
   */
  public function settings_html() {
    ?>
    <div class="wrap">
      <h1>
        WPC Tools: <?= __( 'Emails', WPC_Tools::TEXT_DOMAIN ); ?>
      </h1>

      <form method="post" action="options.php">
        <?php
        settings_fields( $this->settings_title );
        do_settings_sections( $this->settings_title );
        submit_button();
        ?>
      </form>
    </div>
    <?php
  }

  /**
   * Register setting fields
   */
  public function settings_init() {
    register_setting(
      $this->settings_title,
      $this->settings_title,
      [ $this, 'sanitize_settings' ]
    );

    add_settings_section(
      $this->settings_title . '_template_settings',
      __( 'Email Template Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_header_image',
      __( 'Header Image', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_media_upload' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[header_image]',
        "field_id"      => $this->settings_title . '_header_image',
        "setting_field" => 'header_image',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_footer_text',
      __( 'Footer Text', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_textarea' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[footer_text]',
        "field_id"      => $this->settings_title . '_footer_text',
        "setting_field" => 'footer_text',
        "field_class"   => 'regular-text',
        "field_rows"    => 3
      ]
    );

    add_settings_field(
      $this->settings_title . '_background_color',
      __( 'Background Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[background_color]',
        "field_id"      => $this->settings_title . '_background_color',
        "setting_field" => 'background_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_heading_color',
      __( 'Heading Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[heading_color]',
        "field_id"      => $this->settings_title . '_heading_color',
        "setting_field" => 'heading_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_link_color',
      __( 'Link Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[link_color]',
        "field_id"      => $this->settings_title . '_link_color',
        "setting_field" => 'link_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_header_background_color',
      __( 'Header Background Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[header_background_color]',
        "field_id"      => $this->settings_title . '_header_background_color',
        "setting_field" => 'header_background_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_header_text_color',
      __( 'Header Text Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[header_text_color]',
        "field_id"      => $this->settings_title . '_header_text_color',
        "setting_field" => 'header_text_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_body_background_color',
      __( 'Body Background Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[body_background_color]',
        "field_id"      => $this->settings_title . '_body_background_color',
        "setting_field" => 'body_background_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_body_text_color',
      __( 'Body Text Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[body_text_color]',
        "field_id"      => $this->settings_title . '_body_text_color',
        "setting_field" => 'body_text_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_footer_text_color',
      __( 'Footer Text Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_template_settings',
      [
        "field_name"    => $this->settings_title . '[footer_text_color]',
        "field_id"      => $this->settings_title . '_footer_text_color',
        "setting_field" => 'footer_text_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_section(
      $this->settings_title . '_wp_mail_override_settings',
      __( 'WP Mail Override Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_enable_wp_mail_override',
      __( 'Enable WP Mail Override', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_wp_mail_override_settings',
      [
        "field_name"    => $this->settings_title . '[enable_wp_mail_override]',
        "field_id"      => $this->settings_title . '_enable_wp_mail_override',
        "setting_field" => 'enable_wp_mail_override',
        "field_class"   => ''
      ]
    );

    if ( wpc_is_plugin_active( 'gravityforms' ) ) {

      add_settings_section(
        $this->settings_title . '_gravity_override_settings',
        __( 'Gravity Forms Mail Override Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title
      );

      add_settings_field(
        $this->settings_title . '_enable_gravity_override',
        __( 'Enable Gravity Forms Mail Override', WPC_Tools::TEXT_DOMAIN ),
        [ $this, 'control_checkbox' ],
        $this->settings_title,
        $this->settings_title . '_gravity_override_settings',
        [
          "field_name"    => $this->settings_title . '[enable_gravity_override]',
          "field_id"      => $this->settings_title . '_enable_gravity_override',
          "setting_field" => 'enable_gravity_override',
          "field_class"   => ''
        ]
      );
    }
  }

  /**
   * Sanitize input fields on save
   */
  public function sanitize_settings( $input_fields ) {

    $valid_fields = WPC_Tools::defaults()->get( $this->settings_title );

    foreach ( $input_fields as $key => $val ) {
      switch ( $key ) {
        case 'header_image':
          if ( is_numeric( $val ) ) {
            $valid_fields[ $key ] = absint( $val );
          }
          break;
        case 'footer_text':
        case 'heading_color':
        case 'link_color':
        case 'background_color':
        case 'body_background_color':
        case 'header_background_color':
        case 'header_text_color':
        case 'body_text_color':
        case 'footer_text_color':
          $valid_fields[ $key ] = strip_tags( stripslashes( trim( $val ) ) );
          break;
      }
    }

    foreach ( $valid_fields as $key => $val ) {
      switch ( $key ) {
        case 'enable_gravity_override':
        case 'enable_wp_mail_override':
          if ( isset( $input_fields[ $key ] ) ) {
            $valid_fields[ $key ] = absint( $val );
          } else {
            $valid_fields[ $key ] = 0;
          }
          break;
      }
    }

    return wp_parse_args( $input_fields, $valid_fields );
  }

  public function set_defaults() {
    WPC_Tools::defaults()->set( $this->settings_title, [
      'header_image'            => '',
      'footer_text'             => get_bloginfo( 'name' ),
      'heading_color'           => '#1ecdff',
      'link_color'              => '#1ecdff',
      'background_color'        => '#f3f5f6',
      'body_background_color'   => '#ffffff',
      'header_background_color' => '#1ecdff',
      'header_text_color'       => '#ffffff',
      'body_text_color'         => '#373838',
      'footer_text_color'       => '#373838',
      'enable_wp_mail_override' => 1,
      'enable_gravity_override' => 1,
    ] );
  }
}


