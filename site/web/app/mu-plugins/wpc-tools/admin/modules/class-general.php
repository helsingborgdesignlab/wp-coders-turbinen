<?php

class WPC_Tools_Admin_Module_General extends WPC_Tools_Admin_Module {

  const MODULE_NAME = 'wpc-tools';

  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC_Tools_Module_General is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_General constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->set_defaults();
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'admin_init', $this, 'settings_init', 3 );
    WPC_Tools::loader()->add_action( 'admin_menu', $this, 'load_menu', 1 );
  }

  /**
   * Add module menu item on wp-admin
   */
  public function load_menu( $parent_id ) {
    $this->menu_id = add_menu_page(
      'WPC Tools: ' . __( 'General', WPC_Tools::TEXT_DOMAIN ),
      'WPC Tools',
      'manage_options',
      WPC_Tools_Admin::MENU_SLUG,
      array( $this, 'settings_html' ),
      'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHdpZHRoPSI1NTlweCIgaGVpZ2h0PSI1NTlweCIgdmlld0JveD0iMCAwIDU1OSA1NTkiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgICAgICA8dGl0bGU+V1BfTU9OT19NQVJRVUU8L3RpdGxlPiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4gICAgPGRlZnM+ICAgICAgICA8ZmlsdGVyIHg9Ii01MCUiIHk9Ii01MCUiIHdpZHRoPSIyMDAlIiBoZWlnaHQ9IjIwMCUiIGZpbHRlclVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgaWQ9ImZpbHRlci0xIj4gICAgICAgICAgICA8ZmVPZmZzZXQgZHg9IjAiIGR5PSIyIiBpbj0iU291cmNlQWxwaGEiIHJlc3VsdD0ic2hhZG93T2Zmc2V0T3V0ZXIxIj48L2ZlT2Zmc2V0PiAgICAgICAgICAgIDxmZUdhdXNzaWFuQmx1ciBzdGREZXZpYXRpb249IjIiIGluPSJzaGFkb3dPZmZzZXRPdXRlcjEiIHJlc3VsdD0ic2hhZG93Qmx1ck91dGVyMSI+PC9mZUdhdXNzaWFuQmx1cj4gICAgICAgICAgICA8ZmVDb2xvck1hdHJpeCB2YWx1ZXM9IjAgMCAwIDAgMCAgIDAgMCAwIDAgMCAgIDAgMCAwIDAgMCAgMCAwIDAgMC41IDAiIHR5cGU9Im1hdHJpeCIgaW49InNoYWRvd0JsdXJPdXRlcjEiIHJlc3VsdD0ic2hhZG93TWF0cml4T3V0ZXIxIj48L2ZlQ29sb3JNYXRyaXg+ICAgICAgICAgICAgPGZlTWVyZ2U+ICAgICAgICAgICAgICAgIDxmZU1lcmdlTm9kZSBpbj0ic2hhZG93TWF0cml4T3V0ZXIxIj48L2ZlTWVyZ2VOb2RlPiAgICAgICAgICAgICAgICA8ZmVNZXJnZU5vZGUgaW49IlNvdXJjZUdyYXBoaWMiPjwvZmVNZXJnZU5vZGU+ICAgICAgICAgICAgPC9mZU1lcmdlPiAgICAgICAgPC9maWx0ZXI+ICAgIDwvZGVmcz4gICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0iV1BfTU9OT19NQVJRVUUiIGZpbHRlcj0idXJsKCNmaWx0ZXItMSkiPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMjcuNDQyNTQ2LDExMy44NTE3IEwyNzkuOTY2NDM4LDExMy44NTE3IEMyOTUuNzk1NiwxMTMuODk1NTg1IDMxMC45ODQxNzUsMTA3LjYwMzcyMSAzMjIuMTQ1MzU3LDk2LjM3OTA5MTMgQzMyNi44NjU4NTIsOTEuNjYwOTA3MiAzMzAuNzY2MDU5LDg2LjE4ODE5MDYgMzMzLjY4NTM0LDgwLjE4NjM0NiBMMzMzLjY4NTM0LDIxOS44NjgyMiBMMjI3LjQ0OTYxNywyMTkuODY4MjIgTDIyNy40NDI1NDYsMTEzLjg1MTcgWiBNMjI3LjQ1NjY4OCwzMzkuMTkyNDg5IEwyMjcuNDQ5NjE3LDQ3OC44OTU1NzYgQzIzNy42MjkwNzcsNDU4LjkwNzIyNCAyNTguMTY0NjMyLDQ0Ni4zMjI1MDIgMjgwLjU5NTc2Myw0NDYuMzI2MjM3IEwzMzMuNzEzNjI0LDQ0Ni4zMjYyMzcgTDMzMy42NTcwNTYsMzM5LjE1NzEzNCBMMjI3LjQ1NjY4OCwzMzkuMTkyNDg5IFogTTU1OC4zOTY4MDQsMjc5LjUxMjY3NyBDNTU4LjQ3Nzg4NiwyNTUuMzA4MDU4IDU0Mi4wMTg0NjIsMjM0LjE3NzYzNyA1MTguNTMwMTIzLDIyOC4zMzIyODggTDUxNi44MTg5MjUsMjI3Ljk1MDQ1IEw1MTYuODg5NjM2LDIyNy42ODE3NSBDNTEzLjA3NTcwMSwyMjYuODMwMzY5IDUwOS4xNzk2NzcsMjI2LjQwMTE2NiA1MDUuMjcxODcxLDIyNi40MDE4ODYgTDU0LjEzNzc0NDksMjI2LjQwMTg4NiBDMjQuODE1NzU1MSwyMjYuNDM2MTI4IDEuMDU0MTI1MDUsMjUwLjE5Nzc1OCAxLjAxOTg4MzUxLDI3OS41MTk3NDggQzEuMDE5ODgzNTEsMjgwLjc1MDExMyAxLjA3NjQ1MjA1LDI4MS45NTIxOTUgMS4xNjEzMDQ4NiwyODMuMTgyNTYxIEwxLjI2MDI5OTgxLDI4NC4zMjgwNzQgQzEuMzM4MDgxNTYsMjg1LjIxMTk1NyAxLjQzNzA3NjUxLDI4Ni4wODg3NyAxLjU2NDM1NTczLDI4Ni45NTE0NCBMMS43MzQwNjEzNiwyODguMTExMDk1IEMxLjg4NDkxMDgsMjg5LjA1MzkwNCAyLjA2NDA0NDUyLDI4OS45ODcyODUgMi4yNzE0NjI1MSwyOTAuOTExMjM4IEwyLjUwNDgwNzc1LDI5MS45MjI0MDEgQzIuNzU5MzY2MTksMjkyLjk5NzIwMyAzLjA0MjIwODksMjk0LjA1Nzg2MyAzLjM2NzQ3ODAyLDI5NS4xMDQzODEgTDMuNjcxNTMzOTQsMjk2LjAwMjQwNyBDMy45NDczMDU1OCwyOTYuODI5NzIyIDQuMjMwMTQ4MjksMjk3LjY0OTk2NiA0LjU0MTI3NTI4LDI5OC40NTYwNjcgTDUuMDA3OTY1NzUsMjk5LjYxNTcyMiBDNS4zMTIwMjE2NywzMDAuMzQ0MDQyIDUuNjM3MjkwNzksMzAxLjA3OTQzMyA1Ljk2OTYzMDk4LDMwMS43OTM2MTEgTDYuNDc4NzQ3ODYsMzAyLjg4MjU1NiBDNi45MzEyOTYyLDMwMy44MDY1MDkgNy40MTIxMjg4MSwzMDQuNzE2MzE5IDcuOTIxMjQ1NjksMzA1LjYxMTk4OCBMOC4yMzk0NDM3NCwzMDYuMTcwNjAyIEw4LjQ5NDAwMjE4LDMwNi41OTQ4NjYgQzguOTM5NDc5NDYsMzA3LjMzNzMyOCA5LjM5MjAyNzgsMzA4LjA3MjcxOSA5Ljg3Mjg2MDQxLDMwOC43OTM5NjggQzEwLjEzNDQ4OTksMzA5LjE5NzAxOSAxMC40MTAyNjE2LDMwOS41ODU5MjggMTAuNjg2MDMzMiwzMDkuOTc0ODM3IEMxMS4wNzcyOTksMzEwLjUyNjM4IDExLjQ3NTYzNTgsMzExLjA3MDg1MiAxMS44ODEwNDM3LDMxMS42MDgyNTMgQzEyLjE4NTA5OTYsMzEyLjAxMTMwNCAxMi40ODkxNTU1LDMxMi40MTQzNTUgMTIuODE0NDI0NiwzMTIuODEwMzM1IEMxMy4yNTI4MzA4LDMxMy4zNjE4NzggMTMuNzA1Mzc5MiwzMTMuODg1MTM3IDE0LjE3OTE0MDcsMzE0LjQxNTQ2NyBMMTQuNTE4NTUyLDMxNC43NTQ4NzkgTDE1LjA5MTMwODUsMzE1LjQxMjQ4OCBMMTguNDIxNzgxNCwzMTguNzQyOTYxIEMxOC44NjAxODc2LDMxOS4xODEzNjcgMTkuMzU1MTYyMywzMTkuNTc3MzQ3IDE5LjgyODkyMzksMzE5Ljk4MDM5OCBMMjEuMTA4Nzg3MiwzMjEuMDMzOTg3IEMyMS43OTQ2ODA3LDMyMS41Nzg0NTkgMjIuNDg3NjQ1NCwzMjIuMTE1ODYgMjMuMTk0NzUyMiwzMjIuNjI0OTc3IEwyMy43NzQ1Nzk3LDMyMy4wMzUwOTkgQzI1LjY4NzM5OTUsMzI0LjM3MTI2NyAyNy42ODU3MjE5LDMyNS41ODA2NTQgMjkuNzU2NzAzMSwzMjYuNjU1NDg2IEwzMC4zOTMwOTkyLDMyNi45ODA3NTUgQzMxLjQ0NjY4ODMsMzI3LjUxMTA4NSAzMi41MTQ0MTk1LDMyOC4wMTMxMzEgMzMuNjAzMzY0LDMyOC40Nzk4MjEgQzQwLjEwNDI4MzYsMzMxLjIyMDc3OSA0Ny4wODk2OTg2LDMzMi42Mjc0OCA1NC4xNDQ4MTYsMzMyLjYxNjM5NiBMNTA1LjI3ODk0MiwzMzIuNjE2Mzk2IEM1MzQuNTk1NDE5LDMzMi41ODIxNzEgNTU4LjM1NDc3MywzMDguODI5MTQzIDU1OC4zOTY4MDQsMjc5LjUxMjY3NyBMNTU4LjM5NjgwNCwyNzkuNTEyNjc3IFogTTM5Ljg4MjQ3MjIsMjk0LjM0MDcwNiBDMzMuODU2NDk0NCwyODguMzE0NzI4IDMyLjA1MzgzNDcsMjc5LjI1MjE0NSAzNS4zMTUwNzAzLDI3MS4zNzg4MjYgQzM4LjU3NjMwNiwyNjMuNTA1NTA3IDQ2LjI1OTE5MjYsMjU4LjM3MTk2NiA1NC43ODEyMTIxLDI1OC4zNzE5NjYgQzYzLjMwMzIzMTYsMjU4LjM3MTk2NiA3MC45ODYxMTgyLDI2My41MDU1MDcgNzQuMjQ3MzUzOCwyNzEuMzc4ODI2IEM3Ny41MDg1ODk1LDI3OS4yNTIxNDUgNzUuNzA1OTI5NywyODguMzE0NzI4IDY5LjY3OTk1MiwyOTQuMzQwNzA2IEM2MS40NDQ3NjU3LDMwMi41NTI1MjQgNDguMTE3NjU4NCwzMDIuNTUyNTI0IDM5Ljg4MjQ3MjIsMjk0LjM0MDcwNiBaIE00OTIuODgzMzYxLDI5Mi42MTUzNjUgQzQ4NC42NTUwMTQsMjg0LjM4NzAxOCA0ODQuNjU1MDE0LDI3MS4wNDYyMzIgNDkyLjg4MzM2MSwyNjIuODE3ODg1IEM1MDEuMTExNzA3LDI1NC41ODk1MzkgNTE0LjQ1MjQ5MywyNTQuNTg5NTM5IDUyMi42ODA4NCwyNjIuODE3ODg1IEM1MzAuOTA5MTg3LDI3MS4wNDYyMzIgNTMwLjkwOTE4NywyODQuMzg3MDE4IDUyMi42ODA4NCwyOTIuNjE1MzY1IEM1MTQuNDQ1NjU0LDMwMC44MjcxODQgNTAxLjExODU0NywzMDAuODI3MTg0IDQ5Mi44ODMzNjEsMjkyLjYxNTM2NSBaIE0zMTcuNTI3OTUsOTEuNzYxNjg0IEMzMzIuNjg2MjY3LDc2LjU2MjcwNDQgMzM3LjIxMTYyNCw1My43MzY0NjYzIDMyOC45OTgzNzMsMzMuOTA0MDE2NSBDMzIwLjc4NTEyMiwxNC4wNzE1NjY3IDMwMS40NDY0MjYsMS4xMjgxOTU1NiAyNzkuOTgwNTgsMS4wOTY0NTI1NiBMNTguNzgzNDM2NSwxLjA2ODE2ODI5IEw1Ny44MzU5MTM0LDEuMDY4MTY4MjkgTDU1Ljk0MDg2NzIsMS4wNTQwMjYxNSBMNTUuOTQ3OTM4MywwLjkxOTY3NTg2NiBDNTUuMzM1MTEyNCwwLjg5MTM5MTU5NSA1NC43MTc1NzI1LDAuODc3MjQ5NDU5IDU0LjA5NTMxODUsMC44NzcyNDk0NTkgQzUyLjUyNTU0MTQsMC44NzcyNDk0NTkgNTAuOTc2OTc3NiwwLjk1NTAzMTIwNSA0OS40Mjg0MTM3LDEuMDg5MzgxNDkgTDQ2LjI2MDU3NTQsMS4wODkzODE0OSBMNDYuODMzMzMxOSwxLjM3OTI5NTI3IEMyMC41NTE4OTM4LDUuMDAyOTUzNDIgMC45NzU2ODAzNDEsMjcuNDY1MDM1OCAwLjk3NzQ1NzEsNTMuOTk1MTEwOSBMMC45ODQ1MjgxNjgsMjUyLjQxNjM0NSBDMTEuMTcwOTk3OCwyMzIuNDMzOTU3IDMxLjcwODc0ODgsMjE5Ljg1Nzc2MSA1NC4xMzc3NDQ5LDIxOS44NjgyMiBMMTA3LjI1NTYwNiwyMTkuODY4MjIgTDEwNy4yNTU2MDYsMTA3LjM1MzM4OSBMMjc5Ljk2NjQzOCwxMDcuMzE4MDMzIEMyOTQuMDYyMTA4LDEwNy4zNTczNDcgMzA3LjU4NzYzMywxMDEuNzU1NjYxIDMxNy41Mjc5NSw5MS43NjE2ODQgWiBNMjY1LjUyMDI0Niw2OC43MDI5MzE5IEMyNTkuNDk0MjY4LDYyLjY3Njk1NDEgMjU3LjY5MTYwOSw1My42MTQzNzE1IDI2MC45NTI4NDQsNDUuNzQxMDUyMSBDMjY0LjIxNDA4LDM3Ljg2NzczMjcgMjcxLjg5Njk2NiwzMi43MzQxOTIgMjgwLjQxODk4NiwzMi43MzQxOTIgQzI4OC45NDEwMDUsMzIuNzM0MTkyIDI5Ni42MjM4OTIsMzcuODY3NzMyNyAyOTkuODg1MTI4LDQ1Ljc0MTA1MjEgQzMwMy4xNDYzNjMsNTMuNjE0MzcxNSAzMDEuMzQzNzA0LDYyLjY3Njk1NDEgMjk1LjMxNzcyNiw2OC43MDI5MzE5IEMyODcuMDgyNTQsNzYuOTE0NzUwNCAyNzMuNzU1NDMyLDc2LjkxNDc1MDQgMjY1LjUyMDI0Niw2OC43MDI5MzE5IEwyNjUuNTIwMjQ2LDY4LjcwMjkzMTkgWiBNNTQzLjE1MTU4Miw1NDIuOTA5OTUzIEM1NTMuMTQ5MDUxLDUzMi45Nzg5NDYgNTU4Ljc1ODI5Miw1MTkuNDYxMjYyIDU1OC43MjkxNDQsNTA1LjM2OTY1NCBMNTU4LjcyOTE0NCwzMDUuOTY1NTQxIEM1NDguNzAzNTgzLDMyNi4zMTMzMzMgNTI3Ljk2OTUzLDMzOS4xNzk1MjkgNTA1LjI4NjAxMywzMzkuMTI4ODQ5IEw0NTIuNDY1MTM3LDMzOS4xNDI5OTEgTDQ1Mi40NjUxMzcsNDUyLjQ2MzkyNCBMMjgwLjY1MjMzMSw0NTIuNDc4MDY2IEMyNTEuMzMwMzQxLDQ1Mi41MTIzMDggMjI3LjU2ODcxMSw0NzYuMjczOTM4IDIyNy41MzQ0Nyw1MDUuNTk1OTI4IEMyMjcuNTM0NDcsNTA2LjgyNjI5NCAyMjcuNTkxMDM4LDUwOC4wMjgzNzUgMjI3LjY3NTg5MSw1MDkuMjU4NzQxIEwyMjcuNzc0ODg2LDUxMC40MDQyNTQgQzIyNy44NTI2NjgsNTExLjI4ODEzNyAyMjcuOTUxNjYzLDUxMi4xNjQ5NSAyMjguMDcxODcxLDUxMy4wMzQ2OTEgTDIyOC4yNDE1NzcsNTE0LjE5NDM0NiBDMjI4LjM5MjQyNiw1MTUuMTM3MTU1IDIyOC41NzE1Niw1MTYuMDcwNTM2IDIyOC43Nzg5NzgsNTE2Ljk5NDQ4OSBMMjI5LjAxMjMyMyw1MTguMDA1NjUyIEMyMjkuMjY2ODgxLDUxOS4wODA0NTQgMjI5LjU0OTcyNCw1MjAuMTQxMTE0IDIyOS44NzQ5OTMsNTIxLjE4NzYzMiBMMjMwLjE3OTA0OSw1MjIuMDg1NjU4IEMyMzAuNDU0ODIxLDUyMi45MTI5NzMgMjMwLjczNzY2NCw1MjMuNzMzMjE3IDIzMS4wNDg3OSw1MjQuNTM5MzE4IEwyMzEuNTE1NDgxLDUyNS42OTg5NzQgQzIzMS44MjY2MDgsNTI2LjQzNDM2NSAyMzIuMTQ0ODA2LDUyNy4xNjI2ODUgMjMyLjQ3NzE0Niw1MjcuODc2ODYyIEwyMzIuOTg2MjYzLDUyOC45NjU4MDcgQzIzMy40Mzg4MTEsNTI5Ljg4OTc2IDIzMy45MTk2NDQsNTMwLjc5OTU3IDIzNC40Mjg3NjEsNTMxLjY5NTIzOSBMMjM0Ljc0Njk1OSw1MzIuMjUzODUzIEwyMzUuMDAxNTE3LDUzMi42NzgxMTcgQzIzNS40NDY5OTUsNTMzLjQyMDU4IDIzNS44OTk1NDMsNTM0LjE1NTk3MSAyMzYuMzgwMzc2LDUzNC44NzcyMiBDMjM2LjY0MjAwNSw1MzUuMjgwMjcgMjM2LjkxNzc3Nyw1MzUuNjY5MTc5IDIzNy4xOTM1NDgsNTM2LjA1ODA4OCBDMjM3LjU4NDgxNCw1MzYuNjA5NjMxIDIzNy45ODMxNTEsNTM3LjE1NDEwMyAyMzguMzg4NTU5LDUzNy42OTE1MDUgQzIzOC42OTI2MTUsNTM4LjA5NDU1NSAyMzguOTk2NjcxLDUzOC40OTc2MDYgMjM5LjMyMTk0LDUzOC44OTM1ODYgQzIzOS43NjAzNDYsNTM5LjQ0NTEyOSAyNDAuMjEyODk0LDUzOS45NjgzODggMjQwLjY4NjY1Niw1NDAuNDk4NzE4IEwyNDEuMDI2MDY3LDU0MC44MzgxMyBMMjQxLjU5ODgyNCw1NDEuNDk1NzM5IEwyNDQuOTI5Mjk3LDU0NC44MjYyMTIgQzI0NS4zNjc3MDMsNTQ1LjI2NDYxOCAyNDUuODYyNjc4LDU0NS42NjA1OTggMjQ2LjMzNjQzOSw1NDYuMDYzNjQ5IEwyNDcuNjE2MzAyLDU0Ny4xMTcyMzggQzI0OC4zMDIxOTYsNTQ3LjY2MTcxIDI0OC45OTUxNjEsNTQ4LjE5OTExMSAyNDkuNzAyMjY3LDU0OC43MDgyMjggTDI1MC4yODIwOTUsNTQ5LjExODM1IEMyNTIuMTk0ODUyLDU1MC40NTQ2MTUgMjU0LjE5MzE4MSw1NTEuNjY0MDA1IDI1Ni4yNjQyMTgsNTUyLjczODczNyBMMjU2LjkwMDYxNCw1NTMuMDY0MDA2IEMyNTcuOTU0MjA0LDU1My41OTQzMzYgMjU5LjAyMTkzNSw1NTQuMDk2MzgyIDI2MC4xMTA4NzksNTU0LjU2MzA3MiBDMjY2LjYxMTc5OSw1NTcuMzA0MDMgMjczLjU5NzIxNCw1NTguNzEwNzMxIDI4MC42NTIzMzEsNTU4LjY5OTY0NyBMNTA0LjgzMzQ2NSw1NTguNzEzNzg5IEw1MDQuODU0Njc4LDU1OC40NjYzMDIgTDUwNS42MTgzNTQsNTU4LjQ4MDQ0NCBDNTE5LjcwNjQ5LDU1OC41MDk1NzIgNTMzLjIyMTEzNCw1NTIuOTAzMDgzIDU0My4xNTE1ODIsNTQyLjkwOTk1MyBaIE0yNjUuNTIwMjQ2LDUxOS45Nzg0OCBDMjU3LjI5MTg5OSw1MTEuNzUwMTMzIDI1Ny4yOTE4OTksNDk4LjQwOTM0NyAyNjUuNTIwMjQ2LDQ5MC4xODEgQzI3My43NDg1OTMsNDgxLjk1MjY1MyAyODcuMDg5Mzc5LDQ4MS45NTI2NTMgMjk1LjMxNzcyNiw0OTAuMTgxIEMzMDMuNTQ2MDczLDQ5OC40MDkzNDcgMzAzLjU0NjA3Myw1MTEuNzUwMTMzIDI5NS4zMTc3MjYsNTE5Ljk3ODQ4IEMyODcuMDgyNTQsNTI4LjE5MDI5OCAyNzMuNzU1NDMyLDUyOC4xOTAyOTggMjY1LjUyMDI0Niw1MTkuOTc4NDggWiIgaWQ9IkNvbWJpbmVkLVNoYXBlIj48L3BhdGg+ICAgICAgICA8L2c+ICAgIDwvZz48L3N2Zz4=',
      99
    );

    add_action( 'load-' . $this->menu_id, array( $this, 'load_assets' ) );
  }

  /**
   * Register all assets required by the admin plugin.
   */
  public function load_assets() {
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
  }

  /**
   * Register the stylesheets for the admin area.
   */
  public function enqueue_styles() {
    wp_enqueue_style( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/css/' . self::MODULE_NAME . '.css', array(), WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript for the admin area.
   */
  public function enqueue_scripts() {
    wp_enqueue_media();
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/js/' . self::MODULE_NAME . '.js', array( 'jquery' ), WPC_Tools::VERSION, false );
  }

  /**
   * Theme page callback
   */
  public function settings_html() {
    ?>
    <div class="wrap">
      <h1>
        WPC Tools: <?= __( 'General', WPC_Tools::TEXT_DOMAIN ); ?>
      </h1>

      <form method="post" action="options.php">
        <?php
        settings_fields( $this->settings_title );
        do_settings_sections( $this->settings_title );
        submit_button();
        ?>
      </form>
    </div>
    <?php
  }

  /**
   * Register setting fields
   */
  public function settings_init() {

    register_setting(
      $this->settings_title,
      $this->settings_title,
      array( $this, 'sanitize_settings' )
    );

    add_settings_section(
      $this->settings_title . '_general_settings',
      __( 'General Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_default_logo',
      __( 'Default Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[default_logo]',
        "field_id"      => $this->settings_title . '_default_logo',
        "setting_field" => 'default_logo',
        "field_class"   => ''
      )
    );

    add_settings_field(
      $this->settings_title . '_default_retina_logo',
      __( 'Default Retina Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[default_retina_logo]',
        "field_id"      => $this->settings_title . '_default_retina_logo',
        "setting_field" => 'default_retina_logo',
        "field_class"   => '',
        "description"   => __( 'Use the same dimension like the Default Logo', WPC_Tools::TEXT_DOMAIN )
      )
    );

    add_settings_field(
      $this->settings_title . '_default_svg_logo',
      __( 'Default SVG Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_textarea' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[default_svg_logo]',
        "field_id"      => $this->settings_title . '_default_svg_logo',
        "setting_field" => 'default_svg_logo',
        "field_class"   => 'large-text',
        "field_rows"    => 3
      )
    );

    add_settings_field(
      $this->settings_title . '_secondary_logo',
      __( 'Secondary Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[secondary_logo]',
        "field_id"      => $this->settings_title . '_secondary_logo',
        "setting_field" => 'secondary_logo',
        "field_class"   => ''
      )
    );

    add_settings_field(
      $this->settings_title . '_secondary_retina_logo',
      __( 'Secondary Retina Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[secondary_retina_logo]',
        "field_id"      => $this->settings_title . '_secondary_retina_logo',
        "setting_field" => 'secondary_retina_logo',
        "field_class"   => '',
        "description"   => __( 'Use the same dimension like the Secondary Logo', WPC_Tools::TEXT_DOMAIN )
      )
    );

    add_settings_field(
      $this->settings_title . '_secondary_svg_logo',
      __( 'Secondary SVG Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_textarea' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[secondary_svg_logo]',
        "field_id"      => $this->settings_title . '_secondary_svg_logo',
        "setting_field" => 'secondary_svg_logo',
        "field_class"   => 'large-text',
        "field_rows"    => 3
      )
    );

    add_settings_field(
      $this->settings_title . '_site_icon',
      __( 'Site Icon', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_general_settings',
      array(
        "field_name"    => $this->settings_title . '[site_icon]',
        "field_id"      => $this->settings_title . '_site_icon',
        "setting_field" => 'site_icon',
        "field_class"   => '',
        "description"   => 'Icon must be square, and at least 512 pixels wide and tall.'
      )
    );

    add_settings_section(
      $this->settings_title . '_login_settings',
      __( 'Login Page Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_default_login_logo',
      __( 'Login Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_login_settings',
      array(
        "field_name"    => $this->settings_title . '[default_login_logo]',
        "field_id"      => $this->settings_title . '_default_login_logo',
        "setting_field" => 'default_login_logo',
        "field_class"   => ''
      )
    );

    add_settings_field(
      $this->settings_title . '_default_retina_login_logo',
      __( 'Retina Login Logo', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_media_upload' ),
      $this->settings_title,
      $this->settings_title . '_login_settings',
      array(
        "field_name"    => $this->settings_title . '[default_retina_login_logo]',
        "field_id"      => $this->settings_title . '_default_retina_login_logo',
        "setting_field" => 'default_retina_login_logo',
        "field_class"   => '',
        "description"   => __( 'Use the same dimension like the Login Logo', WPC_Tools::TEXT_DOMAIN )
      )
    );

    add_settings_field(
      $this->settings_title . '_default_login_logo_title',
      __( 'Login Logo Title', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_text' ),
      $this->settings_title,
      $this->settings_title . '_login_settings',
      array(
        "field_name"    => $this->settings_title . '[default_login_logo_title]',
        "field_id"      => $this->settings_title . '_default_login_logo_title',
        "setting_field" => 'default_login_logo_title',
        "field_class"   => 'regular-text'
      )
    );

    add_settings_field(
      $this->settings_title . '_default_login_logo_link',
      __( 'Login Logo Link', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_text' ),
      $this->settings_title,
      $this->settings_title . '_login_settings',
      array(
        "field_name"    => $this->settings_title . '[default_login_logo_link]',
        "field_id"      => $this->settings_title . '_default_login_logo_link',
        "setting_field" => 'default_login_logo_link',
        "field_class"   => 'regular-text'
      )
    );

    add_settings_section(
      $this->settings_title . '_typekit_settings',
      __( 'Typekit Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_typekit_url',
      __( 'Typekit Url', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_text' ),
      $this->settings_title,
      $this->settings_title . '_typekit_settings',
      array(
        "field_name"    => $this->settings_title . '[typekit_url]',
        "field_id"      => $this->settings_title . '_typekit_url',
        "setting_field" => 'typekit_url',
        "field_class"   => 'regular-text'
      )
    );

    add_settings_field(
      $this->settings_title . '_typekit_async_load',
      __( 'Typekit Async Load', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_checkbox' ),
      $this->settings_title,
      $this->settings_title . '_typekit_settings',
      [
        "field_name"    => $this->settings_title . '[typekit_async_load]',
        "field_id"      => $this->settings_title . '_typekit_async_load',
        "setting_field" => 'typekit_async_load',
        "field_class"   => ''
      ]
    );

    add_settings_section(
      $this->settings_title . '_google_settings',
      __( 'Google Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_gfonts_url',
      __( 'Google Fonts Url', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_text' ),
      $this->settings_title,
      $this->settings_title . '_google_settings',
      array(
        "field_name"    => $this->settings_title . '[gfonts_url]',
        "field_id"      => $this->settings_title . '_gfonts_url',
        "setting_field" => 'gfonts_url',
        "field_class"   => 'regular-text'
      )
    );

    add_settings_field(
      $this->settings_title . '_gfonts_async_load',
      __( 'Google Fonts Async Load', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_checkbox' ),
      $this->settings_title,
      $this->settings_title . '_google_settings',
      [
        "field_name"    => $this->settings_title . '[gfonts_async_load]',
        "field_id"      => $this->settings_title . '_gfonts_async_load',
        "setting_field" => 'gfonts_async_load',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_gmap_api_key',
      __( 'Google Maps API Key', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_text' ),
      $this->settings_title,
      $this->settings_title . '_google_settings',
      array(
        "field_name"    => $this->settings_title . '[gmap_api_key]',
        "field_id"      => $this->settings_title . '_gmap_api_key',
        "setting_field" => 'gmap_api_key',
        "field_class"   => 'regular-text'
      )
    );

    add_settings_section(
      $this->settings_title . '_tfonts_settings',
      __( 'Typography.com Fonts Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_tfonts_url',
      __( 'Typography.com Fonts Url', WPC_Tools::TEXT_DOMAIN ),
      array( $this, 'control_text' ),
      $this->settings_title,
      $this->settings_title . '_tfonts_settings',
      array(
        "field_name"    => $this->settings_title . '[tfonts_url]',
        "field_id"      => $this->settings_title . '_tfonts_url',
        "setting_field" => 'tfonts_url',
        "field_class"   => 'regular-text'
      )
    );
  }

  /**
   * Sanitize input fields on save
   */
  public function sanitize_settings( $input_fields ) {

    $valid_fields = WPC_Tools::defaults()->get( $this->settings_title );

    foreach ( $input_fields as $key => $val ) {
      switch ( $key ) {
        case 'default_logo':
        case 'default_retina_logo':
        case 'secondary_logo':
        case 'secondary_retina_logo':
        case 'site_icon':
        case 'default_login_logo':
        case 'default_retina_login_logo':
          if ( is_numeric( $val ) ) {
            $valid_fields[ $key ] = absint( $val );
          }
          break;
        case 'default_svg_logo':
        case 'secondary_svg_logo':
          $valid_fields[ $key ] = $val;
          break;
        case 'default_login_logo_title':
        case 'default_login_logo_link':
        case 'typekit_url':
        case 'gfonts_url':
        case 'gmap_api_key':
        case 'tfonts_url':
          $valid_fields[ $key ] = strip_tags( stripslashes( trim( $val ) ) );
          break;
      }
    }

    foreach ( $valid_fields as $key => $val ) {
      switch ( $key ) {
        case 'gfonts_async_load':
        case 'typekit_async_load':
          if ( isset( $input_fields[ $key ] ) ) {
            $valid_fields[ $key ] = absint( $val );
          } else {
            $valid_fields[ $key ] = 0;
          }
          break;
      }
    }

    $fields = wp_parse_args( $input_fields, $valid_fields );

    $fields['site_icon'] = $this->generate_site_icons( $fields['site_icon'] );

    return $fields;
  }

  /**
   * Retrieve the default plugin settings
   */
  public function set_defaults() {
    WPC_Tools::defaults()->set( $this->settings_title, array(
      "default_logo"              => '',
      "default_retina_logo"       => '',
      "default_svg_logo"          => '',
      "secondary_logo"            => '',
      "secondary_retina_logo"     => '',
      "secondary_svg_logo"        => '',
      "site_icon"                 => '',
      "default_login_logo"        => '',
      "default_retina_login_logo" => '',
      "default_login_logo_title"  => get_bloginfo( 'name' ),
      "default_login_logo_link"   => home_url(),
      "typekit_url"               => '',
      "typekit_async_load"        => 1,
      "gfonts_url"                => '',
      "gfonts_async_load"         => 1,
      "gmap_api_key"              => '',
      "tfonts_url"                => '',
    ) );
  }

  /**
   * Generate site icons
   */
  public function generate_site_icons( $attachment_id ) {

    if ( ( '' === $attachment_id ) ) {
      return '';
    }

    if ( ( get_post_meta( $attachment_id, '_wp_attachment_context', true ) == 'site-icon' ) || ! wpc_get_image_attachment( $attachment_id ) ) {
      return $attachment_id;
    }

    $wp_site_icon = new WP_Site_Icon();
    $attachment = wpc_get_image_attachment( $attachment_id );

    $cropped = wp_crop_image( $attachment_id, 0, 0, $attachment['width'], $attachment['height'], $wp_site_icon->page_crop, $wp_site_icon->page_crop );

    if ( $cropped || ! is_wp_error( $cropped ) ) {

      $cropped = apply_filters( 'wp_create_file_in_uploads', $cropped, $attachment_id );
      $object  = $wp_site_icon->create_attachment_object( $cropped, $attachment_id );
      unset( $object['ID'] );

      add_filter( 'intermediate_image_sizes_advanced', array( $wp_site_icon, 'additional_sizes' ) );
      $attachment_id = $wp_site_icon->insert_attachment( $object, $cropped );
      remove_filter( 'intermediate_image_sizes_advanced', array( $wp_site_icon, 'additional_sizes' ) );
    }

    return $attachment_id;
  }
}
