<?php

class WPC_Tools_Admin_Module_Gravity_Forms extends WPC_Tools_Admin_Module {

  const MODULE_NAME = 'wpc-tools-gravity-forms';

  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Gravity_Forms constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->set_defaults();

    if ( wpc_is_plugin_active( 'gravityforms' ) ) {
      $this->module_hooks();
    }
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'admin_init', $this, 'settings_init', 3 );
    WPC_Tools::loader()->add_action( 'admin_menu', $this, 'load_menu', 20 );
  }

  /**
   * Add module menu item on wp-admin
   */
  public function load_menu( $parent_id ) {

    $this->menu_id = add_submenu_page(
      WPC_Tools_Admin::MENU_SLUG,
      __( 'Gravity Forms', WPC_Tools::TEXT_DOMAIN ),
      __( 'Gravity Forms', WPC_Tools::TEXT_DOMAIN ),
      'manage_options',
      self::MODULE_NAME,
      [ $this, 'settings_html' ]
    );

    add_action( 'load-' . $this->menu_id, [ $this, 'load_assets' ] );

    global $submenu;
    if ( isset( $submenu[ WPC_Tools_Admin::MENU_SLUG ] ) && current_user_can( 'manage_options' ) ) {
      $submenu[ WPC_Tools_Admin::MENU_SLUG ][0][0] = __( 'General', WPC_Tools::TEXT_DOMAIN );
    }
  }

  /**
   * Register all assets required by the admin plugin.
   */
  public function load_assets() {
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
  }

  /**
   * Register the stylesheets for the admin area.
   */
  public function enqueue_styles() {
    wp_enqueue_style( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/css/' . self::MODULE_NAME . '.css', [], WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript for the admin area.
   */
  public function enqueue_scripts() {
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/js/' . self::MODULE_NAME . '.js', [ 'jquery' ], WPC_Tools::VERSION, false );
  }

  /**
   * Theme page callback
   */
  public function settings_html() {
    ?>
    <div class="wrap">
      <h1>
        WPC Tools: <?= __( 'Gravity Forms', WPC_Tools::TEXT_DOMAIN ); ?>
      </h1>

      <form method="post" action="options.php">
        <?php
        settings_fields( $this->settings_title );
        do_settings_sections( $this->settings_title );
        submit_button();
        ?>
      </form>
    </div>
    <?php
  }

  /**
   * Register setting fields
   */
  public function settings_init() {
    register_setting(
      $this->settings_title,
      $this->settings_title,
      [ $this, 'sanitize_settings' ]
    );

    // General
    add_settings_section(
      $this->settings_title . '_general_settings',
      __( 'General Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_assets_before_wp_head',
      __( 'Enqueue Scripts In Head', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[assets_before_wp_head]',
        "field_id"      => $this->settings_title . '_assets_before_wp_head',
        "setting_field" => 'assets_before_wp_head',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_confirmation_anchor',
      __( 'Confirmation Anchor', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[confirmation_anchor]',
        "field_id"      => $this->settings_title . '_confirmation_anchor',
        "setting_field" => 'confirmation_anchor',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_disable_tabindex',
      __( 'Disable Tab Index', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[disable_tabindex]',
        "field_id"      => $this->settings_title . '_disable_tabindex',
        "setting_field" => 'disable_tabindex',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_disable_add_form_button',
      __( 'Hide "Add Form" Button From Editors', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[disable_add_form_button]',
        "field_id"      => $this->settings_title . '_disable_add_form_button',
        "setting_field" => 'disable_add_form_button',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_bootstrap_classes',
      __( 'Add Bootstrap Classes', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[enable_bootstrap_classes]',
        "field_id"      => $this->settings_title . '_enable_bootstrap_classes',
        "setting_field" => 'enable_bootstrap_classes',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_custom_control_classes',
      __( 'Add Bootstrap Custom Control Classes', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[enable_custom_control_classes]',
        "field_id"      => $this->settings_title . '_enable_custom_control_classes',
        "setting_field" => 'enable_custom_control_classes',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_field_type_classes',
      __( 'Add Field Type Classes', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[enable_field_type_classes]',
        "field_id"      => $this->settings_title . '_enable_field_type_classes',
        "setting_field" => 'enable_field_type_classes',
        "field_class"   => ''
      ]
    );

    // Form Layout
    add_settings_section(
      $this->settings_title . '_form_layout_settings',
      __( 'Form Layout Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_enable_style_settings',
      __( 'Add Style Form Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_form_layout_settings',
      [
        "field_name"    => $this->settings_title . '[enable_style_settings]',
        "field_id"      => $this->settings_title . '_enable_style_settings',
        "setting_field" => 'enable_style_settings',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_margin_settings',
      __( 'Add Margin Form Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_form_layout_settings',
      [
        "field_name"    => $this->settings_title . '[enable_margin_settings]',
        "field_id"      => $this->settings_title . '_enable_margin_settings',
        "setting_field" => 'enable_margin_settings',
        "field_class"   => ''
      ]
    );

    // Form Button
    add_settings_section(
      $this->settings_title . '_form_button_settings',
      __( 'Form Button Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_enable_submit_button_sizes',
      __( 'Add Submit Button Size Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_form_button_settings',
      [
        "field_name"    => $this->settings_title . '[enable_submit_button_sizes]',
        "field_id"      => $this->settings_title . '_enable_submit_button_sizes',
        "setting_field" => 'enable_submit_button_sizes',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_submit_button_styles',
      __( 'Add Submit Button Style Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_form_button_settings',
      [
        "field_name"    => $this->settings_title . '[enable_submit_button_styles]',
        "field_id"      => $this->settings_title . '_enable_submit_button_styles',
        "setting_field" => 'enable_submit_button_styles',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_submit_button_width',
      __( 'Add Submit Button Width Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_form_button_settings',
      [
        "field_name"    => $this->settings_title . '[enable_submit_button_width]',
        "field_id"      => $this->settings_title . '_enable_submit_button_width',
        "setting_field" => 'enable_submit_button_width',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_submit_button_placement',
      __( 'Add Submit Button Placement Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_form_button_settings',
      [
        "field_name"    => $this->settings_title . '[enable_submit_button_placement]',
        "field_id"      => $this->settings_title . '_enable_submit_button_placement',
        "setting_field" => 'enable_submit_button_placement',
        "field_class"   => ''
      ]
    );

    // Fields
    add_settings_section(
      $this->settings_title . '_fields_settings',
      __( 'Fields Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_enable_label_visibility',
      __( 'Enable Label Visibility', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_fields_settings',
      [
        "field_name"    => $this->settings_title . '[enable_label_visibility]',
        "field_id"      => $this->settings_title . '_enable_label_visibility',
        "setting_field" => 'enable_label_visibility',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_enable_field_widths',
      __( 'Add Field Width Option', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_fields_settings',
      [
        "field_name"    => $this->settings_title . '[enable_field_widths]',
        "field_id"      => $this->settings_title . '_enable_field_widths',
        "setting_field" => 'enable_field_widths',
        "field_class"   => ''
      ]
    );
  }

  /**
   * Sanitize input fields on save
   */
  public function sanitize_settings( $input_fields ) {

    $valid_fields = WPC_Tools::defaults()->get( $this->settings_title );

    foreach ( $valid_fields as $key => $val ) {
      switch ( $key ) {
        case 'assets_before_wp_head':
        case 'confirmation_anchor':
        case 'disable_tabindex':
        case 'disable_add_form_button':
        case 'enable_bootstrap_classes':
        case 'enable_custom_control_classes':
        case 'enable_field_type_classes':
        case 'enable_label_visibility':
        case 'enable_field_widths':
        case 'enable_style_settings':
        case 'enable_margin_settings':
        case 'enable_submit_button_sizes':
        case 'enable_submit_button_styles':
        case 'enable_submit_button_width':
        case 'enable_submit_button_placement':
          if ( isset( $input_fields[ $key ] ) ) {
            $valid_fields[ $key ] = absint( $val );
          } else {
            $valid_fields[ $key ] = 0;
          }
          break;
      }
    }

    return wp_parse_args( $input_fields, $valid_fields );
  }

  /**
   * Retrieve the default plugin settings
   */
  public function set_defaults() {
    WPC_Tools::defaults()->set( $this->settings_title, [
      'assets_before_wp_head'          => 1,
      'confirmation_anchor'            => 1,
      'disable_tabindex'               => 1,
      'disable_add_form_button'        => 1,
      'enable_bootstrap_classes'       => 1,
      'enable_custom_control_classes'  => 1,
      'enable_field_type_classes'      => 1,
      'enable_label_visibility'        => 1,
      'enable_field_widths'            => 1,
      'enable_style_settings'          => 1,
      'enable_margin_settings'         => 1,
      'enable_submit_button_sizes'     => 1,
      'enable_submit_button_styles'    => 1,
      'enable_submit_button_width'     => 1,
      'enable_submit_button_placement' => 1
    ] );
  }
}
