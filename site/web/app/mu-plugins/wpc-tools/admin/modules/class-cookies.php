<?php

class WPC_Tools_Admin_Module_Cookies extends WPC_Tools_Admin_Module {

  const MODULE_NAME = 'wpc-tools-cookies';

  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Admin_Module_Cookies constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->set_defaults();
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_filter( 'wpc_default_pages_wpc_pages', $this, 'get_cookies_pages_to_wpc_pages' );

    WPC_Tools::loader()->add_action( 'admin_init', $this, 'settings_init', 3 );
    WPC_Tools::loader()->add_action( 'admin_menu', $this, 'load_menu', 20 );
  }

  /**
   * Add module menu item on wp-admin
   */
  public function load_menu( $parent_id ) {

    $this->menu_id = add_submenu_page(
      WPC_Tools_Admin::MENU_SLUG,
      __( 'Cookies', WPC_Tools::TEXT_DOMAIN ),
      __( 'Cookies', WPC_Tools::TEXT_DOMAIN ),
      'manage_options',
      self::MODULE_NAME,
      [ $this, 'settings_html' ]
    );

    add_action( 'load-' . $this->menu_id, [ $this, 'load_assets' ] );

    global $submenu;
    if ( isset( $submenu[ WPC_Tools_Admin::MENU_SLUG ] ) && current_user_can( 'manage_options' ) ) {
      $submenu[ WPC_Tools_Admin::MENU_SLUG ][0][0] = __( 'General', WPC_Tools::TEXT_DOMAIN );
    }
  }

  /**
   * Register all assets required by the admin plugin.
   */
  public function load_assets() {
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
  }

  /**
   * Register the stylesheets for the admin area.
   */
  public function enqueue_styles() {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_style( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/css/' . self::MODULE_NAME . '.css', [], WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript for the admin area.
   */
  public function enqueue_scripts() {
    wp_enqueue_media();
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/js/' . self::MODULE_NAME . '.js', [ 'jquery' ], WPC_Tools::VERSION, false );
  }

  /**
   * Theme page callback
   */
  public function settings_html() {
    ?>
    <div class="wrap">
      <h1>
        WPC Tools: <?= __( 'Cookies', WPC_Tools::TEXT_DOMAIN ); ?>
      </h1>

      <form method="post" action="options.php">
        <?php
        settings_fields( $this->settings_title );
        do_settings_sections( $this->settings_title );
        submit_button();
        ?>
      </form>
    </div>
    <?php
  }

  /**
   * Register setting fields
   */
  public function settings_init() {
    register_setting(
      $this->settings_title,
      $this->settings_title,
      [ $this, 'sanitize_settings' ]
    );

    // General
    add_settings_section(
      $this->settings_title . '_general_settings',
      __( 'General Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_enable_cookie_warning',
      __( 'Enable Cookie Warning', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_checkbox' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"    => $this->settings_title . '[enable_cookie_warning]',
        "field_id"      => $this->settings_title . '_enable_cookie_warning',
        "setting_field" => 'enable_cookie_warning',
        "field_class"   => ''
      ]
    );

    add_settings_field(
      $this->settings_title . '_cookie_warning_expiry',
      __( 'Cookie Warning Expiry', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_number' ],
      $this->settings_title,
      $this->settings_title . '_general_settings',
      [
        "field_name"        => $this->settings_title . '[cookie_warning_expiry]',
        "field_id"          => $this->settings_title . '_cookie_warning_expiry',
        "field_class"       => '',
        "field_step"        => 1,
        "field_min"         => 1,
        "field_default"     => wpc_get_setting( 'cookie_warning_expiry', $this->settings_title ),
        "field_append"      => __( 'days', WPC_Tools::TEXT_DOMAIN ),
      ]
    );

    // Pages
    if ( ! empty( $cookies_pages = $this->get_cookies_pages() ) ) {

      add_settings_section(
        $this->settings_title . '_cookies_pages_settings',
        __( 'Pages Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title
      );

      foreach ( $cookies_pages as $name => $title ) {

        add_settings_field(
          'page_for_wpc_' . $name,
          $title,
          [ $this, 'control_dropdown_pages' ],
          $this->settings_title,
          $this->settings_title . '_cookies_pages_settings',
          [
            "field_name"              => 'page_for_wpc_' . $name,
            "field_id"                => 'page_for_wpc_' . $name,
            "field_class"             => '',
            "field_show_option_none"  => __( '&mdash; Select &mdash;' ),
            "field_option_none_value" => '0',
          ]
        );

        register_setting(
          $this->settings_title,
          'page_for_wpc_' . $name,
          [ $this, 'sanitize_settings' ]
        );
      }
    }

    // Warning template
    add_settings_section(
      $this->settings_title . '_warning_settings',
      __( 'Warning Template Settings', WPC_Tools::TEXT_DOMAIN ),
      '__return_false',
      $this->settings_title
    );

    add_settings_field(
      $this->settings_title . '_warning_text',
      __( 'Text', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_textarea' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_text]',
        "field_id"      => $this->settings_title . '_warning_text',
        "setting_field" => 'warning_text',
        "field_class"   => 'regular-text',
        "field_rows"    => 3
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_page_link_text',
      __( 'Page Link Text', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_page_link_text]',
        "field_id"      => $this->settings_title . '_warning_page_link_text',
        "setting_field" => 'warning_page_link_text',
        "field_class"   => 'regular-text'
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_close_button_label',
      __( 'Close Button Label', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_close_button_label]',
        "field_id"      => $this->settings_title . '_warning_close_button_label',
        "setting_field" => 'warning_close_button_label',
        "field_class"   => 'regular-text'
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_background_color',
      __( 'Background Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_background_color]',
        "field_id"      => $this->settings_title . '_warning_background_color',
        "setting_field" => 'warning_background_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_text_color',
      __( 'Text Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_text_color]',
        "field_id"      => $this->settings_title . '_warning_text_color',
        "setting_field" => 'warning_text_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_page_link_color',
      __( 'Page Link Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_page_link_color]',
        "field_id"      => $this->settings_title . '_warning_page_link_color',
        "setting_field" => 'warning_page_link_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_close_button_color',
      __( 'Close Button Color', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_close_button_color]',
        "field_id"      => $this->settings_title . '_warning_close_button_color',
        "setting_field" => 'warning_close_button_color',
        "field_class"   => 'color-picker',
      ]
    );

    add_settings_field(
      $this->settings_title . '_warning_close_button_background',
      __( 'Close Button Background', WPC_Tools::TEXT_DOMAIN ),
      [ $this, 'control_text' ],
      $this->settings_title,
      $this->settings_title . '_warning_settings',
      [
        "field_name"    => $this->settings_title . '[warning_close_button_background]',
        "field_id"      => $this->settings_title . '_warning_close_button_background',
        "setting_field" => 'warning_close_button_background',
        "field_class"   => 'color-picker',
      ]
    );
  }

  /**
   * Sanitize input fields on save
   */
  public function sanitize_settings( $input_fields ) {

    if ( is_array( $input_fields ) ) {
      $valid_fields = WPC_Tools::defaults()->get( $this->settings_title );

      foreach ( $input_fields as $key => $val ) {
        switch ( $key ) {
          case 'cookie_warning_expiry':
            $valid_fields[ $key ] = absint( $val );
            break;
        }
      }

      foreach ( $input_fields as $key => $val ) {
        switch ( $key ) {
          case 'warning_text':
          case 'warning_page_link_text':
          case 'warning_page_link_label':
          case 'warning_background_color':
          case 'warning_text_color':
          case 'warning_page_link_color':
          case 'warning_close_button_color':
          case 'warning_close_button_background':
            $valid_fields[ $key ] = strip_tags( stripslashes( trim( $val ) ) );
            break;
        }
      }

      foreach ( $valid_fields as $key => $val ) {
        switch ( $key ) {
          case 'enable_cookie_warning':
            if ( isset( $input_fields[ $key ] ) ) {
              $valid_fields[ $key ] = absint( $val );
            } else {
              $valid_fields[ $key ] = 0;
            }
            break;
        }
      }

      return wp_parse_args( $input_fields, $valid_fields );
    } else {
      return esc_attr( $input_fields );
    }
  }

  /**
   * Retrieve the default plugin settings
   */
  public function set_defaults() {
    WPC_Tools::defaults()->set( $this->settings_title, [
      'enable_cookie_warning'           => 0,
      'cookie_warning_expiry'                   => 365,
      'warning_text'                    => __( 'This website uses cookies to ensure you get the best experience on our website.', WPC_Tools::TEXT_DOMAIN ),
      'warning_page_link_text'          => __( 'More info', WPC_Tools::TEXT_DOMAIN ),
      'warning_close_button_label'      => __( 'Close', WPC_Tools::TEXT_DOMAIN ),
      'warning_background_color'        => '#373838',
      'warning_text_color'              => '#ffffff',
      'warning_page_link_color'         => '#1ecdff',
      'warning_close_button_color'      => '#ffffff',
      'warning_close_button_background' => '#f0b535',
    ] );
  }

  /**
   * Get cookies custom pages
   *
   * @return array
   */
  public function get_cookies_pages() {
    return [
      'cookie_policy' => __( 'Cookie Policy', WPC_Tools::TEXT_DOMAIN )
    ];
  }

  /**
   * Get cookies custom pages to Default Pages module
   *
   * @return array
   */
  public function get_cookies_pages_to_wpc_pages( $wpc_pages ) {
    $cookies_pages = $this->get_cookies_pages();

    return array_merge( $cookies_pages, $wpc_pages );
  }
}
