<?php

class WPC_Tools_Admin_Module_Defaults extends WPC_Tools_Admin_Module {

  const MODULE_NAME = 'wpc-tools-defaults';

  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Defaults constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->set_defaults();
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'admin_init', $this, 'settings_init', 3 );
    WPC_Tools::loader()->add_action( 'admin_menu', $this, 'load_menu', 20 );
    WPC_Tools::loader()->add_action( 'edit_form_after_title', $this, 'notice_to_default_pages' );
    WPC_Tools::loader()->add_action( 'display_post_states', $this, 'state_to_default_pages', 10, 2 );

    if ( wpc_is_plugin_active( 'acf' ) ) {
      WPC_Tools::loader()->add_filter( 'acf/location/rule_types', $this, 'acf_rule_types_to_default_pages' );
      WPC_Tools::loader()->add_filter( 'acf/get_field_group', $this, 'old_rule_types_fix', 30, 1 );

      WPC_Tools::loader()->add_filter( 'acf/location/rule_values/wpc_default_page', $this, 'wpc_default_page_acf_rule_values' );
      WPC_Tools::loader()->add_filter( 'acf/location/rule_match/wpc_default_page', $this, 'acf_rule_match_to_wpc_default_page', 10, 3 );

      WPC_Tools::loader()->add_filter( 'acf/location/rule_values/wpc_default_page_parent', $this, 'wpc_default_page_acf_rule_values' );
      WPC_Tools::loader()->add_filter( 'acf/location/rule_match/wpc_default_page_parent', $this, 'acf_rule_match_to_wpc_default_page_parent', 10, 3 );
    }
  }

  /**
   * Add module menu item on wp-admin
   */
  public function load_menu( $parent_id ) {

    if ( ! empty( $this->get_post_types() ) || ! empty( $this->get_taxonomies() || ! empty( $this->get_custom_pages() ) ) ) {

      $this->menu_id = add_submenu_page(
        WPC_Tools_Admin::MENU_SLUG,
        __( 'Defaults', WPC_Tools::TEXT_DOMAIN ),
        __( 'Defaults', WPC_Tools::TEXT_DOMAIN ),
        'manage_options',
        self::MODULE_NAME,
        [ $this, 'settings_html' ]
      );

      add_action( 'load-' . $this->menu_id, [ $this, 'load_assets' ] );

      global $submenu;
      if ( isset( $submenu[ WPC_Tools_Admin::MENU_SLUG ] ) && current_user_can( 'manage_options' ) ) {
        $submenu[ WPC_Tools_Admin::MENU_SLUG ][0][0] = __( 'General', WPC_Tools::TEXT_DOMAIN );
      }
    }
  }

  /**
   * Register all assets required by the admin plugin.
   */
  public function load_assets() {
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );
    add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
  }

  /**
   * Register the stylesheets for the admin area.
   */
  public function enqueue_styles() {
    wp_enqueue_style( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/css/' . self::MODULE_NAME . '.css', [], WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript for the admin area.
   */
  public function enqueue_scripts() {
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME, WPC_TOOLS_URL . 'admin/js/' . self::MODULE_NAME . '.js', [ 'jquery' ], WPC_Tools::VERSION, false );
  }

  /**
   * Theme page callback
   */
  public function settings_html() {
    ?>
    <div class="wrap">
      <h1>
        WPC Tools: <?= __( 'Defaults', WPC_Tools::TEXT_DOMAIN ); ?>
      </h1>

      <form method="post" action="options.php">
        <h2 class="nav-tab-wrapper">
          <a class="nav-tab <?= isset( $_GET['tab'] ) ? '' : 'nav-tab-active'; ?>"
             href="<?= admin_url( 'admin.php?page=' . self::MODULE_NAME ); ?>">

            <?php esc_html_e( 'Pages', WPC_Tools::TEXT_DOMAIN ); ?>
          </a>
          <a class="nav-tab <?= isset( $_GET['tab'] ) ? 'nav-tab-active' : ''; ?>"
             href="<?= esc_url( add_query_arg( array( 'tab' => 'terms' ), admin_url( 'admin.php?page=' . self::MODULE_NAME ) ) );; ?>">

            <?php esc_html_e( 'Terms', WPC_Tools::TEXT_DOMAIN ); ?>
          </a>
        </h2>

        <?php if ( ! isset( $_GET['tab'] ) ): ?>
          <?php settings_fields( $this->settings_title . '_pages' ); ?>
          <?php do_settings_sections( $this->settings_title . '_pages' ); ?>
        <?php else: ?>
          <?php settings_fields( $this->settings_title . '_terms' ); ?>
          <?php do_settings_sections( $this->settings_title . '_terms' ); ?>
        <?php endif; ?>

        <?php submit_button(); ?>
      </form>
    </div>
    <?php
  }

  /**
   * Register setting fields
   */
  public function settings_init() {

    // Custom Post Types Settings
    if ( ! empty( $post_types = $this->get_post_types() ) ) {

      add_settings_section(
        $this->settings_title . '_cpt_default_page_settings',
        __( 'Custom Post Types Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title . '_pages'
      );

      foreach ( $post_types as $post_type ) {

        if ( ! post_type_exists( $post_type ) ) {
          continue;
        }

        $post_type_obj = get_post_type_object( $post_type );

        add_settings_field(
          'page_for_' . $post_type,
          esc_html( $post_type_obj->labels->name ),
          [ $this, 'control_dropdown_pages' ],
          $this->settings_title . '_pages',
          $this->settings_title . '_cpt_default_page_settings',
          [
            "field_name"              => 'page_for_' . $post_type,
            "field_id"                => 'page_for_' . $post_type,
            "field_class"             => '',
            "field_show_option_none"  => __( '&mdash; Select &mdash;' ),
            "field_option_none_value" => '0',
          ]
        );

        register_setting(
          $this->settings_title . '_pages',
          'page_for_' . $post_type,
          [ $this, 'sanitize_settings' ]
        );

        add_settings_field(
          $post_type . '_per_page',
          sprintf( esc_html__( '%s Per Page', WPC_Tools::TEXT_DOMAIN ), $post_type_obj->labels->name ),
          [ $this, 'control_number' ],
          $this->settings_title . '_pages',
          $this->settings_title . '_cpt_default_page_settings',
          [
            "field_name"        => $post_type . '_per_page',
            "field_id"          => $post_type . '_per_page',
            "field_class"       => '',
            "field_step"        => 1,
            "field_min"         => - 1,
            "field_default"     => get_option( 'posts_per_page' ),
            "field_description" => sprintf( __( 'Use -1 to show all %s.', WPC_Tools::TEXT_DOMAIN ), $post_type_obj->labels->name )
          ]
        );

        register_setting(
          $this->settings_title . '_pages',
          $post_type . '_per_page',
          [ $this, 'sanitize_settings' ]
        );
      }
    }

    // Built-in & Custom Taxonomies Settings
    if ( ! empty( $taxonomies = $this->get_taxonomies() ) ) {

      add_settings_section(
        $this->settings_title . '_ct_default_page_settings',
        __( 'Built-in & Custom Taxonomies Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title . '_pages'
      );

      foreach ( $taxonomies as $taxonomy ) {

        if ( ! taxonomy_exists( $taxonomy ) ) {
          continue;
        }

        $taxonomy_obj = get_taxonomy( $taxonomy );

        add_settings_field(
          'page_for_' . $taxonomy,
          esc_html( $taxonomy_obj->labels->name ),
          [ $this, 'control_dropdown_pages' ],
          $this->settings_title . '_pages',
          $this->settings_title . '_ct_default_page_settings',
          [
            "field_name"              => 'page_for_' . $taxonomy,
            "field_id"                => 'page_for_' . $taxonomy,
            "field_class"             => '',
            "field_show_option_none"  => __( '&mdash; Select &mdash;' ),
            "field_option_none_value" => '0',
          ]
        );

        register_setting(
          $this->settings_title . '_pages',
          'page_for_' . $taxonomy,
          [ $this, 'sanitize_settings' ]
        );
      }
    }

    // Built-in & WPC Tools Pages Settings
    $wpc_pages     = $this->get_wpc_pages();
    $builtin_pages = $this->get_builtin_pages();

    if ( ! empty( $wpc_pages ) || ! empty( $builtin_pages ) ) {

      add_settings_section(
        $this->settings_title . '_wpc_page_settings',
        __( 'Built-in & WPC Pages Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title . '_pages'
      );

      if ( ! empty( $builtin_pages ) ) {

        foreach ( $builtin_pages as $name => $title ) {

          add_settings_field(
            $name,
            $title,
            [ $this, 'control_dropdown_pages' ],
            $this->settings_title . '_pages',
            $this->settings_title . '_wpc_page_settings',
            [
              "field_name"              => $name,
              "field_id"                => $name,
              "field_class"             => '',
              "field_show_option_none"  => __( '&mdash; Select &mdash;' ),
              "field_option_none_value" => '0',
            ]
          );

          register_setting(
            $this->settings_title . '_pages',
            $name,
            [ $this, 'sanitize_settings' ]
          );
        }
      }

      if ( ! empty( $wpc_pages ) ) {

        foreach ( $wpc_pages as $name => $title ) {

          add_settings_field(
            'page_for_wpc_' . $name,
            $title,
            [ $this, 'control_dropdown_pages' ],
            $this->settings_title . '_pages',
            $this->settings_title . '_wpc_page_settings',
            [
              "field_name"              => 'page_for_wpc_' . $name,
              "field_id"                => 'page_for_wpc_' . $name,
              "field_class"             => '',
              "field_show_option_none"  => __( '&mdash; Select &mdash;' ),
              "field_option_none_value" => '0',
            ]
          );

          register_setting(
            $this->settings_title . '_pages',
            'page_for_wpc_' . $name,
            [ $this, 'sanitize_settings' ]
          );
        }
      }
    }

    // Custom Default Pages Settings
    if ( ! empty( $custom_pages = $this->get_custom_pages() ) ) {

      add_settings_section(
        $this->settings_title . '_custom_default_page_settings',
        __( 'Custom Default Pages Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title . '_pages'
      );

      foreach ( $custom_pages as $name => $title ) {

        add_settings_field(
          'page_for_' . $name,
          $title,
          [ $this, 'control_dropdown_pages' ],
          $this->settings_title . '_pages',
          $this->settings_title . '_custom_default_page_settings',
          [
            "field_name"              => 'page_for_' . $name,
            "field_id"                => 'page_for_' . $name,
            "field_class"             => '',
            "field_show_option_none"  => __( '&mdash; Select &mdash;' ),
            "field_option_none_value" => '0',
          ]
        );

        register_setting(
          $this->settings_title . '_pages',
          'page_for_' . $name,
          [ $this, 'sanitize_settings' ]
        );
      }
    }


    // Default Taxonomy Terms Settings
    if ( ! empty( $taxonomies = $this->get_default_terms_taxonomies() ) ) {

      add_settings_section(
        $this->settings_title . '_default_taxonomy_term_settings',
        __( 'Default Taxonomy Terms Settings', WPC_Tools::TEXT_DOMAIN ),
        '__return_false',
        $this->settings_title . '_terms'
      );

      foreach ( $taxonomies as $taxonomy ) {

        if ( ! taxonomy_exists( $taxonomy ) ) {
          continue;
        }

        $taxonomy_obj = get_taxonomy( $taxonomy );

        $field_options = wp_list_pluck( get_terms( [
          'taxonomy'   => $taxonomy,
          'hide_empty' => false,
        ] ), 'name', 'term_id' );

        if ( $taxonomy !== 'category' ) {
          $field_options = [ 0 => __( '&mdash; Select &mdash;' ) ] + $field_options;
        }

        add_settings_field(
          'default_' . $taxonomy,
          esc_html( $taxonomy_obj->labels->singular_name ),
          [ $this, 'control_dropdown' ],
          $this->settings_title . '_terms',
          $this->settings_title . '_default_taxonomy_term_settings',
          [
            "field_name"    => 'default_' . $taxonomy,
            "field_id"      => 'default_' . $taxonomy,
            "field_class"   => '',
            'field_options' => $field_options,
          ]
        );

        register_setting(
          $this->settings_title . '_terms',
          'default_' . $taxonomy,
          [ $this, 'sanitize_settings' ]
        );
      }
    }
  }


  /**
   * Sanitize input fields on save
   */
  public function sanitize_settings( $input_fields ) {

    return esc_attr( $input_fields );
  }

  /**
   * Retrieve the default plugin settings
   */
  public function set_defaults() {

  }

  /**
   * Add notice to default pages
   */
  public function notice_to_default_pages() {
    global $post;

    if ( ! empty( $post_types = $this->get_post_types() ) ) {

      foreach ( $post_types as $post_type ) {
        $post_type_obj = get_post_type_object( $post_type );

        if ( wpc_icl_object_id( intval( get_option( 'page_for_' . $post_type ) ), 'page' ) == $post->ID ) {
          echo '<div class="notice notice-warning inline"><p>' . sprintf( __( 'You are currently editing the page that shows your %s.', WPC_Tools::TEXT_DOMAIN ), strtolower( $post_type_obj->labels->name ) ) . '</p></div>';
        }
      }
    }

    if ( ! empty( $taxonomies = $this->get_taxonomies() ) ) {

      foreach ( $taxonomies as $taxonomy ) {
        $taxonomy_obj = get_taxonomy( $taxonomy );

        if ( wpc_icl_object_id( intval( get_option( 'page_for_' . $taxonomy ) ), 'page' ) == $post->ID ) {
          echo '<div class="notice notice-warning inline"><p>' . sprintf( __( 'You are currently editing the page that shows your %s.', WPC_Tools::TEXT_DOMAIN ), strtolower( $taxonomy_obj->labels->name ) ) . '</p></div>';
        }
      }
    }

    if ( ! empty( $custom_pages = $this->get_custom_pages() ) ) {

      foreach ( $custom_pages as $name => $title ) {

        if ( wpc_icl_object_id( intval( get_option( 'page_for_' . $name ) ), 'page' ) == $post->ID ) {
          echo '<div class="notice notice-warning inline"><p>' . sprintf( __( 'You are currently editing the %s page.', WPC_Tools::TEXT_DOMAIN ), strtolower( $title ) ) . '</p></div>';
        }
      }
    }

    if ( ! empty( $wpc_pages = $this->get_wpc_pages() ) ) {

      foreach ( $wpc_pages as $name => $title ) {

        if ( wpc_icl_object_id( intval( get_option( 'page_for_wpc_' . $name ) ), 'page' ) == $post->ID ) {
          echo '<div class="notice notice-warning inline"><p>' . sprintf( __( 'You are currently editing the %s page.', WPC_Tools::TEXT_DOMAIN ), strtolower( $title ) ) . '</p></div>';
        }
      }
    }
  }

  /**
   * Add state to default pages
   *
   * @param $post_states
   * @param $post
   *
   * @return string
   */
  public function state_to_default_pages( $post_states, $post ) {

    if ( ! empty( $post_types = $this->get_post_types() ) ) {

      foreach ( $post_types as $post_type ) {
        $post_type_obj = get_post_type_object( $post_type );

        if ( wpc_icl_object_id( intval( get_option( 'page_for_' . $post_type ) ), 'page' ) === $post->ID ) {
          $post_states[ 'page_for_' . $post_type ] = sprintf( __( '%s Page', WPC_Tools::TEXT_DOMAIN ), $post_type_obj->labels->name );
        }
      }
    }

    if ( ! empty( $taxonomies = $this->get_taxonomies() ) ) {

      foreach ( $taxonomies as $taxonomy ) {
        $taxonomy_obj = get_taxonomy( $taxonomy );

        if ( wpc_icl_object_id( intval( get_option( 'page_for_' . $taxonomy ) ), 'page' ) === $post->ID ) {
          $post_states[ 'page_for_' . $taxonomy ] = sprintf( __( '%s Page', WPC_Tools::TEXT_DOMAIN ), $taxonomy_obj->labels->name );
        }
      }
    }

    if ( ! empty( $custom_pages = $this->get_custom_pages() ) ) {

      foreach ( $custom_pages as $name => $title ) {

        if ( wpc_icl_object_id( intval( get_option( 'page_for_' . $name ) ), 'page' ) === $post->ID ) {
          $post_states[ 'page_for_' . $name ] = sprintf( __( '%s Page', WPC_Tools::TEXT_DOMAIN ), esc_html( $title ) );
        }
      }
    }

    if ( ! empty( $wpc_pages = $this->get_wpc_pages() ) ) {

      foreach ( $wpc_pages as $name => $title ) {

        if ( wpc_icl_object_id( intval( get_option( 'page_for_wpc_' . $name ) ), 'page' ) === $post->ID ) {
          $post_states[ 'page_for_wpc_' . $name ] = sprintf( __( '%s Page', WPC_Tools::TEXT_DOMAIN ), esc_html( $title ) );
        }
      }
    }

    return $post_states;
  }

  /**
   * ACF Rule Types to default pages
   *
   * @param $choices
   *
   * @return mixed
   */
  public function acf_rule_types_to_default_pages( $choices ) {

    $category = __( "Default Pages", WPC_Tools::TEXT_DOMAIN );

    $choices[ $category ]['wpc_default_page']        = __( "Default Page", WPC_Tools::TEXT_DOMAIN );
    $choices[ $category ]['wpc_default_page_parent'] = __( "Default Page Parent", WPC_Tools::TEXT_DOMAIN );

    return $choices;
  }

  /**
   * Old rule types fix to previous plugin versions
   * ( wpc_dp_custom_post_type, wpc_dp_custom_taxonomy, wpc_dp_wpc_page, wpc_dp_custom_page )
   *
   * @since  1.7.11
   *
   * @param $field_group
   *
   * @return mixed
   */
  public function old_rule_types_fix( $field_group ) {
    if ( ! empty( $field_group['location'] ) ) {
      foreach ( $field_group['location'] as $g_key => $group ) {
        foreach ( $group as $r_key => $rule ) {
          foreach ( $rule as $option => $value ) {
            if ( $option === 'param' && array_key_exists( $value, $this->get_dp_acf_rule_value_categories() ) ) {
              $field_group['location'][ $g_key ][ $r_key ][ $option ] = 'wpc_default_page';
            }
          }
        }
      }
    }

    return $field_group;
  }

  /**
   * wpc_default_page ACF rule values
   *
   * @param $choices
   *
   * @return mixed
   */
  public function wpc_default_page_acf_rule_values() {

    $categories = $this->get_dp_acf_rule_value_categories();

    if ( ! empty( $post_types = $this->get_post_types() ) ) {

      foreach ( $post_types as $post_type ) {

        $post_type_obj = get_post_type_object( $post_type );

        $choices[ $categories['wpc_dp_custom_post_type'] ][ 'page_for_' . $post_type ] = $post_type_obj->labels->name;
      }
    }

    if ( ! empty( $taxonomies = $this->get_taxonomies() ) ) {

      foreach ( $taxonomies as $taxonomy ) {

        $taxonomy_obj = get_taxonomy( $taxonomy );

        $choices[ $categories['wpc_dp_custom_taxonomy'] ][ 'page_for_' . $taxonomy ] = $taxonomy_obj->labels->name;
      }
    }

    if ( ! empty( $builtin_pages = $this->get_builtin_pages() ) ) {

      foreach ( $builtin_pages as $name => $title ) {

        $choices[ $categories['wpc_dp_wpc_page'] ][ $name ] = $title;
      }
    }

    if ( ! empty( $wpc_pages = $this->get_wpc_pages() ) ) {

      foreach ( $wpc_pages as $name => $title ) {

        $choices[ $categories['wpc_dp_wpc_page'] ][ 'page_for_wpc_' . $name ] = $title;
      }
    }

    if ( ! empty( $custom_pages = $this->get_custom_pages() ) ) {

      foreach ( $custom_pages as $name => $title ) {

        $choices[ $categories['wpc_dp_custom_page'] ][ 'page_for_' . $name ] = $title;
      }
    }

    return $choices;
  }

  /**
   * ACF Rule Match to wpc_default_page rule
   *
   * @param $match
   * @param $rule
   * @param $options
   *
   * @return bool
   */
  public function acf_rule_match_to_wpc_default_page( $match, $rule, $options ) {

    if ( empty( $options['post_id'] ) ) {
      return $match;
    }

    $current_page_id  = $options['post_id'];
    $selected_page_id = wpc_icl_object_id( intval( get_option( $rule['value'] ) ), 'page' );

    if ( $rule['operator'] == "==" ) {

      $match = ( $current_page_id == $selected_page_id );
    } elseif ( $rule['operator'] == "!=" ) {

      $match = ( $current_page_id != $selected_page_id );
    }

    return $match;
  }

  /**
   * ACF Rule Match to wpc_default_page_parent rule
   *
   * @param $match
   * @param $rule
   * @param $options
   *
   * @return bool
   */
  public function acf_rule_match_to_wpc_default_page_parent( $match, $rule, $options ) {

    if ( empty( $options['post_id'] ) ) {
      return $match;
    }

    $selected_page_id = wpc_icl_object_id( intval( get_option( $rule['value'] ) ), 'page' );

    $current_page_id       = $options['post_id'];
    $current_page_object   = get_post( $current_page_id );
    $current_top_parent_id = false;

    if ( ! empty( $current_page_object->post_parent ) ) {
      $current_ancestors     = get_post_ancestors( $current_page_id );
      $current_top_parent_id = end( $current_ancestors );
    }

    if ( $rule['operator'] == "==" ) {

      $match = ( ! empty( $current_top_parent_id ) && ( $current_top_parent_id == $selected_page_id ) );
    } elseif ( $rule['operator'] == "!=" ) {

      $match = ( empty( $current_top_parent_id ) || $current_top_parent_id != $selected_page_id );
    }

    return $match;
  }

  /**
   * Get default pages ACF rule value's categories
   *
   * @return array
   */
  public function get_dp_acf_rule_value_categories() {
    return [
      'wpc_dp_custom_post_type' => __( "Custom Post Types", WPC_Tools::TEXT_DOMAIN ),
      'wpc_dp_custom_taxonomy'  => __( "Built-in & Custom Taxonomies", WPC_Tools::TEXT_DOMAIN ),
      'wpc_dp_wpc_page'         => __( "Built-in & WPC Pages", WPC_Tools::TEXT_DOMAIN ),
      'wpc_dp_custom_page'      => __( "Custom Default Pages", WPC_Tools::TEXT_DOMAIN ),
    ];
  }

  /**
   * Get post types to Default Pages
   *
   * @return array
   */
  public function get_post_types() {

    $args = [
      'public'   => true,
      '_builtin' => false,
    ];

    return apply_filters( 'wpc_default_pages_post_types', get_post_types( $args, 'names', 'and' ) );
  }

  /**
   * Get taxonomies to Default Pages
   *
   * @return array
   */
  public function get_taxonomies() {

    $args = [
      'public'  => true,
      'show_ui' => true,
    ];

    return apply_filters( 'wpc_default_pages_taxonomies', get_taxonomies( $args, 'names', 'and' ) );
  }

  /**
   * Get taxonomies to Default Terms
   *
   * @return array
   */
  public function get_default_terms_taxonomies() {

    $args = [
      'public'   => false,
      'show_ui'  => true,
      '_builtin' => false,
    ];

    return apply_filters( 'wpc_default_terms_taxonomies', array_merge( [
      'category',
      'post_tag'
    ], get_taxonomies( $args, 'names', 'and' ) ) );
  }

  /**
   * Get WPC Tools pages to Default Pages
   *
   * @return array
   */
  public function get_builtin_pages() {

    return apply_filters( 'wpc_default_pages_builtin_pages', [
      'wp_page_for_privacy_policy' => __( 'Privacy Policy', WPC_Tools::TEXT_DOMAIN ),
    ] );
  }

  /**
   * Get WPC Tools pages to Default Pages
   *
   * @return array
   */
  public function get_wpc_pages() {

    return apply_filters( 'wpc_default_pages_wpc_pages', [
      'error_404' => __( 'Error 404', WPC_Tools::TEXT_DOMAIN ),
      'search'    => __( 'Search', WPC_Tools::TEXT_DOMAIN )
    ] );
  }

  /**
   * Get custom pages to Default Pages
   *
   * @return array
   */
  public function get_custom_pages() {

    return apply_filters( 'wpc_default_pages_custom_pages', [] );
  }
}
