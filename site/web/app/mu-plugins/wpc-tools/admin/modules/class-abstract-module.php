<?php

abstract class WPC_Tools_Admin_Module {

  protected $settings_title;

  protected $menu_id;

  /**
   * Outputs the html for module settings
   */
  abstract public function settings_html();

  /**
   * Manage module settings
   */
  abstract public function settings_init();

  /**
   * Sanitize input fields
   */
  abstract public function sanitize_settings( $input_fields );

  /**
   * Stores the module defaults
   */
  public function set_defaults() {
    return array();
  }

  /**
   * Text field html
   */
  public function control_text( $args ) {

    $field_value = wpc_get_setting( $args['setting_field'], $this->settings_title );

    ?>
    <input type="text" class="<?php echo $args['field_class']; ?>" id="<?php echo $args['field_id']; ?>"
           name="<?php echo $args['field_name'] ?>" value="<?php echo esc_attr( $field_value ); ?>">
    <?php

    if ( isset( $args['description'] ) ):
      ?>
      <p class="description"><?= $args['description']; ?></p>
      <?php
    endif;
  }

  /**
   * Email field html
   */
  public function control_email( $args ) {

    $field_value = wpc_get_setting( $args['setting_field'], $this->settings_title );

    ?>
    <input type="email" class="<?php echo $args['field_class']; ?>" id="<?php echo $args['field_id']; ?>"
           name="<?php echo $args['field_name'] ?>" value="<?php echo esc_attr( $field_value ); ?>">
    <?php

    if ( isset( $args['description'] ) ):
      ?>
      <p class="description"><?= $args['description']; ?></p>
      <?php
    endif;
  }

  /**
   * Textarea field html
   */
  public function control_textarea( $args ) {

    $field_value = wpc_get_setting( $args['setting_field'], $this->settings_title );

    ?>
    <textarea name="<?php echo $args['field_name'] ?>" id="<?php echo $args['field_id']; ?>"
              class="<?php echo $args['field_class']; ?>"
              rows="<?php echo $args['field_rows']; ?>"><?php echo esc_attr( $field_value ); ?></textarea>
    <?php

    if ( isset( $args['description'] ) ):
      ?>
      <p class="description"><?= $args['description']; ?></p>
      <?php
    endif;
  }

  /**
   * Checkbox field html
   */
  public function control_checkbox( $args ) {

    $field_value = wpc_get_setting( $args['setting_field'], $this->settings_title );
    ?>
    <input type="checkbox" class="<?php echo $args['field_class']; ?>" id="<?php echo $args['field_id']; ?>"
           name="<?php echo $args['field_name'] ?>" value="1" <?php checked( '1', $field_value ); ?>>
    <?php

    if ( isset( $args['description'] ) ):
      ?>
      <p class="description"><?= $args['description']; ?></p>
      <?php
    endif;
  }

  /**
   * Media Upload field html
   */
  public function control_media_upload( $args ) {

    $image = array();
    $media_value = wpc_get_setting( $args['setting_field'], $this->settings_title );

    if ( '' !== $media_value ) {
      $image = wpc_get_image_attachment( $media_value, "medium" );
    }
    ?>

    <div id="preview-<?php echo $args['field_id']; ?>" class="wpc-tools-media-preview">
      <?php if ( empty( $image ) && ( $media_value !== '' ) ): ?>
        <i style="color: red"><?= __('this image has been deleted', WPC_Tools::TEXT_DOMAIN ); ?></i>
      <?php endif; ?>

      <?php if ( ! empty( $image ) ) : ?>
        <img src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>"
             alt="<?php echo $image['alt']; ?>">
      <?php endif; ?>
    </div>

    <input type="hidden" class="<?php echo $args['field_class']; ?>" id="<?php echo $args['field_id']; ?>"
           name="<?php echo $args['field_name'] ?>" value="<?php echo esc_attr( $media_value ); ?>">
    <input class="media-upload-button button" type="button" value="Upload Image"
           data-input="<?php echo $args['field_id']; ?>">
    <input class="media-remove-button button" type="button" value="Remove Image"
           data-input="<?php echo $args['field_id']; ?>" <?php if ( '' === $media_value ) : ?>style="display: none"<?php endif; ?>>

    <?php if ( isset( $args['description'] ) ):?>
      <p class="description"><?= $args['description']; ?></p>
    <?php endif;
  }

  /**
   * Dropdown field html
   */
  public function control_dropdown( $args ) {

    if ( isset( $args['setting_field'] ) ) {
      $field_value = wpc_get_setting( $args['setting_field'], $this->settings_title );
    } else {
      $field_value = get_option( $args['field_name'] );
    }

    if ( ! empty( $args['field_options'] ) ) :
      ?>
      <select class="<?php echo $args['field_class'] ?>" id="<?php echo $args['field_id'] ?>"
              name="<?php echo $args['field_name'] ?>">
        <?php foreach( $args['field_options'] as $option_value => $option_title ) : ?>
          <option value="<?= $option_value; ?>" <?= ($field_value == $option_value) ? 'selected' : '' ?>><?= $option_title; ?></option>
        <?php endforeach; ?>
      </select>
      <?php
    endif;
  }

  /**
   * WP Dropdown Pages field html
   */
  public function control_dropdown_pages( $args ) {

    wp_dropdown_pages( array(
      'name'              => $args['field_name'],
      'id'                => $args['field_id'],
      'class'             => $args['field_class'],
      'echo'              => 1,
      'show_option_none'  => $args['field_show_option_none'],
      'option_none_value' => $args['field_option_none_value'],
      'selected'          => wpc_icl_object_id( intval( get_option( $args['field_name'] ) ), 'page' ),
      'post_status'       => array( 'publish', 'private' ),
    ) );
  }

  /**
   * Number field html
   */
  public function control_number( $args ) {
    ?>
    <input type="number" class="small-text"
           id="<?= $args['field_id']; ?>" step="<?= $args['field_step']; ?>" min="<?= $args['field_min']; ?>"
           class="<?= $args['field_class']; ?>"
           name="<?= $args['field_name']; ?>"
           value="<?php echo esc_attr( get_option( $args['field_name'] ) ? get_option( $args['field_name'] ) : $args['field_default'] ); ?>">
    <?php

    if( isset( $args['field_append'] ) ) {
      echo ' ' . $args['field_append'];
    }

    if ( isset( $args['field_description'] ) ):
      ?>
      <p class="description"><?= $args['field_description']; ?></p>
      <?php
    endif;
  }
}
