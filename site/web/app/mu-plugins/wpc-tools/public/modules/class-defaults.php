<?php

class WPC_Tools_Public_Module_Defaults extends WPC_Tools_Public_Module {

  const MODULE_NAME = WPC_Tools_Admin_Module_Defaults::MODULE_NAME;

  protected static $_instance = null;

  /**
   * WPC Tools Admin Instance.
   *
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Defaults constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'pre_get_posts', $this, 'set_post_type_count', 90 );
    WPC_Tools::loader()->add_action( 'set_object_terms', $this, 'set_default_taxonomy_term', 10, 5 );
  }

  /**
   * Change posts_per_page based on default settings
   *
   * @param $query
   */
  public function set_post_type_count( $query ) {

    if ( $query->is_main_query() && ( ! is_admin() || ( is_admin() && ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) ) ) {

      $post_type = $query->get( 'post_type' );

      if ( is_tax() ) {
        $taxonomy     = $query->tax_query->queries[0]['taxonomy'];
        $taxonomy_obj = get_taxonomy( $taxonomy );
        $post_type    = $taxonomy_obj->object_type[0];
      }

      if ( is_array( $post_type ) ) {
        return;
      }

      $ppp = get_option( $post_type . '_per_page', false );

      if ( $ppp ) {
        $query->set( 'posts_per_page', intval( $ppp ) );
      }

    }

    return;
  }

  /**
   * Ensure default term gets set
   *
   * @param $object_id
   * @param $terms
   * @param $tt_ids
   * @param $taxonomy
   * @param $append
   */
  public function set_default_taxonomy_term( $object_id, $terms, $tt_ids, $taxonomy, $append ) {
    $default_term = absint( get_option( 'default_' . $taxonomy, 0 ) );

    if ( ! $append && $default_term && empty( $tt_ids ) ) {
      $tt_ids = array_map( 'absint', $tt_ids );

      if ( $default_term && ! in_array( $default_term, $tt_ids, true ) ) {
        wp_set_post_terms( $object_id, array( $default_term ), $taxonomy, true );
      }
    }
  }
}
