<?php

class WPC_Tools_Public_Module_General extends WPC_Tools_Public_Module {

  const MODULE_NAME = WPC_Tools_Admin_Module_General::MODULE_NAME;

  protected static $_instance = null;

  /**
   * WPC Tools Admin Instance.
   *
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_General constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {
    WPC_Tools::loader()->add_action( 'login_enqueue_scripts', $this, 'add_login_logo' );
    WPC_Tools::loader()->add_action( 'login_headertitle', $this, 'add_login_logo_title' );
    WPC_Tools::loader()->add_action( 'login_headerurl', $this, 'add_login_logo_link' );
    WPC_Tools::loader()->add_action( 'wp_head', $this, 'init_typekit' );
    WPC_Tools::loader()->add_action( 'wp_head', $this, 'init_gfonts' );
    WPC_Tools::loader()->add_action( 'mce_css', $this, 'init_gfonts_editor' );
    WPC_Tools::loader()->add_action( 'wp_head', $this, 'init_tfonts' );
    WPC_Tools::loader()->add_action( 'mce_css', $this, 'init_tfonts_editor' );
    WPC_Tools::loader()->add_action( 'wp_enqueue_scripts', $this, 'load_google_maps_script' );

    WPC_Tools::loader()->add_filter( 'get_site_icon_url', $this, 'add_site_icon', 99, 2 );
    WPC_Tools::loader()->add_filter( 'acf/fields/google_map/api', $this, 'register_acf_google_api_key' );
  }

  /**
   * Add site icon to the HEAD based on plugin settings
   */
  public function add_site_icon( $url, $size ) {

    $attachment_id = wpc_get_setting( 'site_icon', $this->settings_title );

    if ( '' === $attachment_id ) {
      return $url;
    }

    $image = wpc_get_image_attachment( $attachment_id, array( $size, $size ) );

    return $image["url"];
  }

  /**
   * Changes the login logo on wp-login page
   */
  public function add_login_logo() {

    $attachment_id = wpc_get_setting( 'default_login_logo', $this->settings_title );
    $retina_attachment_id = wpc_get_setting( 'default_retina_login_logo', $this->settings_title );

    if ( '' === $attachment_id ) {
      return;
    }

    $default_login_logo = wpc_get_image_attachment( $attachment_id );

    if ( $default_login_logo["width"] > 300 ) {
      $aspect_ratio = $default_login_logo["width"] / $default_login_logo["height"];
      $default_login_logo["width"] = 300;
      $default_login_logo["height"] = round( 300 / $aspect_ratio );
    }

    ?>
    <style type="text/css">
      body.login div#login h1 a {
        background-image: url(<?php echo $default_login_logo["url"]; ?>);
        background-position: center center;
        background-size: contain;
        width: <?php echo $default_login_logo["width"]; ?>px;
        height: <?php echo $default_login_logo["height"]; ?>px;
      }

      <?php
      if( '' !== $retina_attachment_id ):
      $default_retina_login_logo = wpc_get_image_attachment( $retina_attachment_id );
      ?>
        @media all and (-webkit-min-device-pixel-ratio : 1.5),
        all and (-o-min-device-pixel-ratio: 3/2),
        all and (min--moz-device-pixel-ratio: 1.5),
        all and (min-device-pixel-ratio: 1.5) {
          body.login div#login h1 a {
            background-image: url(<?php echo $default_retina_login_logo["url"]; ?>);
          }
        }
      <?php endif; ?>
    </style>
    <?php
  }

  /**
   * Changes the login logo title on on wp-login page
   */
  public function add_login_logo_title() {
    return wpc_get_setting( 'default_login_logo_title', $this->settings_title );
  }

  /**
   * Changes the login logo link on on wp-login page
   */
  public function add_login_logo_link() {
    return wpc_get_setting( 'default_login_logo_link', $this->settings_title );
  }

  /**
   * Load Typekit
   */
  public function init_typekit() {

    $typekit_url = wpc_get_setting( 'typekit_url', $this->settings_title );

    if ( '' === $typekit_url ) {
      return;
    }

    $typekit_url_parts = pathinfo( $typekit_url );

    if ( ! empty( $typekit_url_parts['filename'] ) ) {
      $typekit_id = $typekit_url_parts['filename'];

      if ( wpc_get_setting( 'typekit_async_load', $this->settings_title ) ) {

        echo '<script src="https://use.typekit.net/' . $typekit_id . '.js"></script>';
        echo '<script>try{Typekit.load({async:true,active:function(){jQuery("body").trigger("typekit-active")}})}catch(t){}</script>';
      } else {
        echo '<link href="https://use.typekit.net/' . $typekit_id . '.css" rel="stylesheet" type="text/css">';
      }
    }
  }

  /**
   * Load Google Fonts
   */
  public function init_gfonts() {

    $gfonts_url = wpc_get_setting( 'gfonts_url', $this->settings_title );

    if ( '' === $gfonts_url ) {
      return;
    }

    if ( wpc_get_setting( 'gfonts_async_load', $this->settings_title ) ) {
      $parts = parse_url( $gfonts_url );

      parse_str( $parts['query'], $query );

      if ( empty( $query['family'] ) ) {
        return;
      }

      $font_families = explode( '|', $query['family'] );

      foreach ( $font_families as &$font_family ) {
        $font_family = str_replace( '0i', '0italic', $font_family );
      }

      if ( ! empty( $query['subset'] ) ) {
        $font_families[0] .= ':latin,' . $query['subset'];
      }

      echo '<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>';
      echo '<script>WebFont.load({google:{families:' . json_encode( $font_families ) . '},active:function(){jQuery("body").trigger("gfonts-active")}})</script>';
    } else {
      echo '<link href="' . $gfonts_url . '" rel="stylesheet" type="text/css">';
    }
  }

  /**
   * Load Google Fonts in the editor
   */
  public function init_gfonts_editor( $mce_css ) {

    $gfonts_url = wpc_get_setting('gfonts_url', $this->settings_title);

    if ( '' === $gfonts_url ) {
      return $mce_css;
    }

    $heading_font_string = str_replace( ',', '%2C', $gfonts_url );
    $mce_css .= ', ' . $heading_font_string;

    return $mce_css;
  }

  /**
   * Load typography.com fonts
   */
  public function init_tfonts() {

    $tfonts_url = wpc_get_setting('tfonts_url', $this->settings_title);

    if ( '' === $tfonts_url ) {
      return;
    }

    echo '<link href="' . $tfonts_url . '" rel="stylesheet" type="text/css">';
  }

  /**
   * Load typography.com fonts in the editor
   */
  public function init_tfonts_editor( $mce_css ) {

    $tfonts_url = wpc_get_setting('tfonts_url', $this->settings_title);

    if ( '' === $tfonts_url ) {
      return $mce_css;
    }

    $heading_font_string = str_replace( ',', '%2C', $tfonts_url );
    $mce_css .= ', ' . $heading_font_string;

    return $mce_css;
  }

  /**
   * Load Google Maps script
   */
  public function load_google_maps_script() {

    if ( apply_filters( 'wpc_load_google_maps_script', true ) && wpc_get_setting( 'gmap_api_key', $this->settings_title ) ) {
      wp_enqueue_script( 'wpctools/googlemaps', 'https://maps.googleapis.com/maps/api/js?key=' . wpc_get_setting( 'gmap_api_key', $this->settings_title ), [ ], null, true );
    }
  }

  /**
   * Register Google API key
   */
  public function register_acf_google_api_key( $api ) {

    $api['key'] = wpc_get_setting( 'gmap_api_key', $this->settings_title );

    return $api;
  }
}
