<?php

class WPC_Tools_Public_Module_Gravity_Forms extends WPC_Tools_Public_Module {

  const MODULE_NAME = WPC_Tools_Admin_Module_Gravity_Forms::MODULE_NAME;

  protected static $_instance = null;

  /**
   * WPC Tools Admin Instance.
   *
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Gravity_Forms constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );

    if ( wpc_is_plugin_active( 'gravityforms' ) ) {
      $this->module_hooks();
    }
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {

    if ( boolval( wpc_get_setting( 'assets_before_wp_head', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_action( 'wp', $this, 'assets_before_wp_head' );
    }

    if ( boolval( wpc_get_setting( 'disable_tabindex', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_tabindex', $this, 'disable_tabindex' );
    }

    if ( ! boolval( wpc_get_setting( 'confirmation_anchor', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_confirmation_anchor', $this, 'confirmation_anchor' );
    }

    if ( boolval( wpc_get_setting( 'disable_add_form_button', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_display_add_form_button', $this, 'disable_add_form_button' );
    }

    if ( boolval( wpc_get_setting( 'enable_custom_control_classes', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_pre_render', $this, 'enable_custom_select_classes', 10 );
      WPC_Tools::loader()->add_filter( 'gform_field_choice_markup_pre_render', $this, 'enable_custom_choice_classes', 10, 4 );
    }

    if ( boolval( wpc_get_setting( 'enable_bootstrap_classes', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_action( 'gform_editor_js', $this, 'size_setting_to_fields' );
      WPC_Tools::loader()->add_action( 'wp_enqueue_scripts', $this, 'bootstrap_utility_enqueue_scripts' );

      WPC_Tools::loader()->add_filter( 'gform_pre_render', $this, 'enable_bootstrap_classes', 10 );
      WPC_Tools::loader()->add_filter( 'gform_field_css_class', $this, 'bootstrap_utility_classes', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_submit_button', $this, 'bootstrap_form_submit_button' );
    }

    if ( boolval( wpc_get_setting( 'enable_field_type_classes', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_field_css_class', $this, 'field_type_classes', 10, 2 );
    }

    if ( boolval( wpc_get_setting( 'enable_label_visibility', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_enable_field_label_visibility_settings', $this, 'enable_label_visibility' );
    }

    if ( boolval( wpc_get_setting( 'enable_field_widths', $this->settings_title ) ) && ! empty( $this->get_options()['field']['width'] ) ) {
      WPC_Tools::loader()->add_action( 'gform_field_appearance_settings', $this, 'enable_field_widths' );
      WPC_Tools::loader()->add_action( 'gform_editor_js', $this, 'enable_field_widths_scripts' );

      WPC_Tools::loader()->add_filter( 'gform_field_css_class', $this, 'add_field_width_class', 10, 3 );
    }

    if ( boolval( wpc_get_setting( 'enable_style_settings', $this->settings_title ) ) && ! empty( $this->get_options()['form']['style'] ) ) {
      WPC_Tools::loader()->add_filter( 'gform_form_settings', $this, 'enable_style_settings', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_pre_form_settings_save', $this, 'save_style_settings' );
      WPC_Tools::loader()->add_filter( 'gform_pre_render', $this, 'add_style_class', 10, 3 );
    }

    if ( boolval( wpc_get_setting( 'enable_margin_settings', $this->settings_title ) ) && ! empty( $this->get_options()['form']['margin'] ) ) {
      WPC_Tools::loader()->add_filter( 'gform_form_settings', $this, 'enable_margin_settings', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_pre_form_settings_save', $this, 'save_margin_settings' );
      WPC_Tools::loader()->add_filter( 'gform_field_css_class', $this, 'add_margin_class', 10, 3 );
    }

    if ( boolval( wpc_get_setting( 'enable_submit_button_sizes', $this->settings_title ) ) && ! empty( $this->get_options()['submit_button']['size'] ) ) {
      WPC_Tools::loader()->add_filter( 'gform_form_settings', $this, 'enable_submit_button_sizes', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_pre_form_settings_save', $this, 'save_submit_button_sizes' );
      WPC_Tools::loader()->add_filter( 'gform_submit_button', $this, 'add_submit_button_size_class', 99, 2 );
    }

    if ( boolval( wpc_get_setting( 'enable_submit_button_styles', $this->settings_title ) ) && ! empty( $this->get_options()['submit_button']['style'] ) ) {
      WPC_Tools::loader()->add_filter( 'gform_form_settings', $this, 'enable_submit_button_styles', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_pre_form_settings_save', $this, 'save_submit_button_styles' );
      WPC_Tools::loader()->add_filter( 'gform_submit_button', $this, 'add_submit_button_style_class', 99, 2 );
    }

    if ( boolval( wpc_get_setting( 'enable_submit_button_width', $this->settings_title ) ) && ! empty( $this->get_options()['submit_button']['width'] ) ) {
      WPC_Tools::loader()->add_filter( 'gform_form_settings', $this, 'enable_submit_button_width', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_pre_form_settings_save', $this, 'save_submit_button_width' );
      WPC_Tools::loader()->add_filter( 'gform_submit_button', $this, 'add_submit_button_width_class', 99, 2 );
    }

    if ( boolval( wpc_get_setting( 'enable_submit_button_placement', $this->settings_title ) ) && ! empty( $this->get_options()['submit_button']['placement'] ) ) {
      WPC_Tools::loader()->add_filter( 'gform_form_settings', $this, 'enable_submit_button_placement', 10, 2 );
      WPC_Tools::loader()->add_filter( 'gform_pre_form_settings_save', $this, 'save_submit_button_placement' );
      WPC_Tools::loader()->add_filter( 'gform_get_form_filter', $this, 'add_submit_button_placement_class', 99, 2 );
    }
  }

  /**
   * Load scripts before wp_head
   */
  public function assets_before_wp_head() {

    if ( ! class_exists( 'RGFormsModel' ) ) {
      return;
    }

    $forms = \RGFormsModel::get_forms( null, 'title' );
    foreach ( $forms as $form ):
      gravity_form_enqueue_scripts( $form->id, true );
    endforeach;
  }

  /**
   * Disable tab indexes
   */
  public function disable_tabindex() {
    return false;
  }

  /**
   * Confirmation anchor
   */
  public function confirmation_anchor() {
    return false;
  }

  /**
   * Disable Add Form button on editors
   */
  public function disable_add_form_button() {
    return false;
  }

  /**
   * Field Type Classes
   */
  public function field_type_classes( $classes, $field ) {

    if ( ! empty( $field->type ) ) {
      $classes .= ' gfield_type_' . $field->type;
    }

    return $classes;
  }

  /**
   * Custom Control Classes - select
   */
  public function enable_custom_select_classes( $form ) {

    if ( ! empty( $form['fields'] ) ) {
      foreach ( $form['fields'] as &$field ) {
        if ( $field->type == "select" || $field->type == "multiselect" ) {
          if ( $field->size == "small" ) {
            $field->size = 'custom-select custom-select-sm';
          }
          if ( $field->size == "medium" ) {
            $field->size = 'custom-select';
          }
          if ( $field->size == "large" ) {
            $field->size = 'custom-select custom-select-lg';
          }
        }
      }
    }

    return $form;
  }

  /**
   * Custom Control Classes - radio, checkbox
   */
  public function enable_custom_choice_classes( $choice_markup, $choice, $field, $value ) {

    if ( $field->get_input_type() == 'radio' ) {
      $choice_markup = str_replace( "li class='", "li class='custom-control custom-radio ", $choice_markup );
      $choice_markup = str_replace( "type='radio'", "class='custom-control-input' type='radio'", $choice_markup );
      $choice_markup = str_replace( "label for='", "label class='custom-control-label' for='", $choice_markup );
    }

    if ( $field->get_input_type() == 'checkbox' ) {
      $choice_markup = str_replace( "li class='", "li class='custom-control custom-checkbox ", $choice_markup );
      $choice_markup = str_replace( "type='checkbox'", "class='custom-control-input' type='checkbox'", $choice_markup );
      $choice_markup = str_replace( "label for='", "label class='custom-control-label' for='", $choice_markup );
    }

    return $choice_markup;
  }

  /**
   * Size setting to fields
   */
  public function size_setting_to_fields() {
    ?>
    <script type='text/javascript'>
      jQuery.each(fieldSettings, function (key, value) {
        if (jQuery.inArray(key, ["date"]) !== -1) {
          fieldSettings[key] += ", .size_setting";
        }
      });
    </script>
    <?php
  }

  /**
   * Bootstrap Classes
   */
  public function enable_bootstrap_classes( $form ) {

    if ( ! empty( $form['fields'] ) ) {
      foreach ( $form['fields'] as &$field ) {
        if ( $field->size == "small" ) {
          $field->size = 'form-control form-control-sm input-sm';
        }
        if ( $field->size == "medium" ) {
          $field->size = 'form-control';
        }
        if ( $field->size == "large" ) {
          $field->size = 'form-control form-control-lg input-lg';
        }
      }
    }

    return $form;
  }

  /**
   * Bootstrap utility classes
   */
  public function bootstrap_utility_classes( $classes, $field ) {

    if ( $field->disableMargins ) {
      $classes .= ' mb-0';
    }

    if ( $field->placeholder ) {
      $classes .= ' has-placeholder';
    }

    $classes .= ' form-group';

    return $classes;
  }

  /**
   * Bootstrap utility scripts
   */
  public function bootstrap_utility_enqueue_scripts() {
    wp_enqueue_script( self::MODULE_NAME . '_select_placeholder', WPC_TOOLS_URL . 'public/js/' . self::MODULE_NAME . '-select-placeholder.min.js', [ 'jquery' ], WPC_Tools::VERSION, false );
  }

  /**
   * Bootstrap style submit button
   */
  function bootstrap_form_submit_button( $button ) {
    $dom = new DOMDocument();
    $dom->loadHTML( $button );
    $input      = $dom->getElementsByTagName( 'input' )->item( 0 );
    $new_button = $dom->createElement( 'button' );
    $new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
    $input->removeAttribute( 'value' );

    foreach ( $input->attributes as $attribute ) {
      $new_button->setAttribute( $attribute->name, $attribute->value );
    }

    $new_button->setAttribute( 'class', 'btn btn-primary ' . str_replace( 'button', '', $new_button->getAttribute( 'class' ) ) );

    $input->parentNode->replaceChild( $new_button, $input );

    return $dom->saveHtml( $new_button );
  }

  /**
   * Show field label visibility option
   */
  public function enable_label_visibility() {
    return true;
  }

  /**
   * Add field width option to input fields
   */
  public function enable_field_widths( $position ) {

    if ( $position == 300 ) {
      ?>
      <li class="width_setting field_setting">
        <label for="field_admin_label">
          <?php _e( 'Field Width', WPC_Tools::TEXT_DOMAIN ); ?>
          <?php gform_tooltip( 'form_field_width_value' ) ?>
        </label>

        <select id="field_width_value" onchange="SetFieldProperty('widthField', this.value);">
          <?php foreach ( $this->get_options()['field']['width'] as $value => $name ): ?>
            <option value="<?= $value; ?>"
                    <?php if ( array_keys( $this->get_options()['field']['width'] )[0] === $value ): ?>selected<?php endif; ?>>
              <?= $name; ?>
            </option>
          <?php endforeach; ?>
        </select>
      </li>
      <?php
    }
  }

  /**
   * Editor scripts for field width option
   */
  public function enable_field_widths_scripts() {
    ?>
    <script type='text/javascript'>
      jQuery.each(fieldSettings, function (key, value) {
        if (jQuery.inArray(key, ["hidden", "hiddenproduct", "singleshipping"]) === -1) {
          fieldSettings[key] += ", .width_setting";
        }
      });

      jQuery(document).bind("gform_field_added", function (event, form, field) {
        field["widthField"] = "<?= array_keys( $this->get_options()['field']['width'] )[0]; ?>";
      });

      jQuery(document).bind("gform_load_field_settings", function (event, field, form) {
        jQuery("#field_width_value").val(field["widthField"]).change();
      });
    </script>
    <?php
  }

  /**
   * Add field width class
   */
  public function add_field_width_class( $classes, $field, $form ) {

    if ( ! isset( $field->widthField ) ) {
      return $classes;
    }

    $classes .= ' ' . $field->widthField;

    return $classes;
  }

  /**
   * Enable style settings
   */
  public function enable_style_settings( $settings, $form ) {

    $style_dd = '';

    foreach ( $this->get_options()['form']['style'] as $value => $label ) {
      $selected = rgar( $form, 'field_style' ) == $value ? 'selected="selected"' : '';

      $style_dd .= '<option value="' . $value . '" ' . $selected . '>' . $label . '</option>';
    }

    $settings['Form Layout']['field_style'] = '
    <tr id="field_style_setting">
      <th>
        ' . __( 'Style', WPC_Tools::TEXT_DOMAIN ) . '
      </th>
      <td>
        <select id="field_style" name="field_style">
          ' . $style_dd . '
        </select>
      </td>
    </tr>';

    return $settings;
  }

  /**
   * Save style settings
   */
  public function save_style_settings( $form ) {

    $form['field_style'] = rgpost( 'field_style' );

    return $form;
  }

  /**
   * Add style class to form fields
   */
  public function add_style_class( $form ) {

    if ( empty( $form['field_style'] ) ) {
      return $form;
    }

    if ( empty( $form['cssClass'] ) ) {
      $form['cssClass'] = $form['field_style'];
    } else {
      $form['cssClass'] .= ' ' . $form['field_style'];
    }

    return $form;
  }

  /**
   * Enable margin settings
   */
  public function enable_margin_settings( $settings, $form ) {

    $margin_dd = '';

    foreach ( $this->get_options()['form']['margin'] as $value => $label ) {
      $selected = rgar( $form, 'field_margin' ) == $value ? 'selected="selected"' : '';

      $margin_dd .= '<option value="' . $value . '" ' . $selected . '>' . $label . '</option>';
    }

    $settings['Form Layout']['field_margin'] = '
    <tr id="field_margin_setting">
      <th>
        ' . __( 'Margin size', WPC_Tools::TEXT_DOMAIN ) . '
      </th>
      <td>
        <select id="field_margin" name="field_margin">
          ' . $margin_dd . '
        </select>
      </td>
    </tr>';

    return $settings;
  }

  /**
   * Save margin settings
   */
  public function save_margin_settings( $form ) {

    $form['field_margin'] = rgpost( 'field_margin' );

    return $form;
  }

  /**
   * Add margin class to form fields
   */
  public function add_margin_class( $classes, $field, $form ) {

    if ( ! isset( $form['field_margin'] ) ) {
      return $classes;
    }

    return $classes . ' ' . $form['field_margin'];
  }

  /**
   * Add submit button size option
   */
  public function enable_submit_button_sizes( $settings, $form ) {

    $button_size_dd = '';

    foreach ( $this->get_options()['submit_button']['size'] as $value => $label ) {
      $selected = rgar( $form, 'submit_button_size' ) == $value ? 'selected="selected"' : '';

      $button_size_dd .= '<option value="' . $value . '" ' . $selected . '>' . $label . '</option>';
    }

    $settings['Form Button']['field_button_size'] = '
    <tr id="field_button_size_setting">
      <th>
        ' . __( 'Button size', WPC_Tools::TEXT_DOMAIN ) . '
      </th>
      <td>
        <select id="submit_button_size" name="submit_button_size">
          ' . $button_size_dd . '
        </select>
      </td>
    </tr>';

    return $settings;
  }

  /**
   * Save submit button size settings
   */
  public function save_submit_button_sizes( $form ) {

    $form['submit_button_size'] = rgpost( 'submit_button_size' );

    return $form;
  }

  /**
   * Add submit button size class
   */
  public function add_submit_button_size_class( $button, $form ) {

    if ( ! isset( $form['submit_button_size'] ) ) {
      return $button;
    }

    return str_replace( 'class="', 'class="' . $form['submit_button_size'] . ' ', $button );
  }

  /**
   * Add submit button style option
   */
  public function enable_submit_button_styles( $settings, $form ) {

    $button_style_dd = '';

    foreach ( $this->get_options()['submit_button']['style'] as $value => $label ) {
      $selected = rgar( $form, 'submit_button_style' ) == $value ? 'selected="selected"' : '';

      $button_style_dd .= '<option value="' . $value . '" ' . $selected . '>' . $label . '</option>';
    }

    $settings['Form Button']['field_button_style'] = '
    <tr id="field_button_style_setting">
      <th>
        ' . __( 'Button style', WPC_Tools::TEXT_DOMAIN ) . '
      </th>
      <td>
        <select id="submit_button_style" name="submit_button_style">
          ' . $button_style_dd . '
        </select>
      </td>
    </tr>';

    return $settings;
  }

  /**
   * Save submit button style settings
   */
  public function save_submit_button_styles( $form ) {

    $form['submit_button_style'] = rgpost( 'submit_button_style' );

    return $form;
  }

  /**
   * Add submit button style class
   */
  public function add_submit_button_style_class( $button, $form ) {

    if ( ! isset( $form['submit_button_style'] ) ) {
      return $button;
    }

    return str_replace( 'class="', 'class="' . $form['submit_button_style'] . ' ', str_replace( 'btn-primary', '', $button ) );
  }

  /**
   * Add submit button width option
   */
  public function enable_submit_button_width( $settings, $form ) {

    $button_style_dd = '';

    foreach ( $this->get_options()['submit_button']['width'] as $value => $label ) {
      $selected = rgar( $form, 'submit_button_width' ) == $value ? 'selected="selected"' : '';

      $button_style_dd .= '<option value="' . $value . '" ' . $selected . '>' . $label . '</option>';
    }

    $settings['Form Button']['field_button_width'] = '
    <tr id="field_button_style_setting">
      <th>
        ' . __( 'Button width', WPC_Tools::TEXT_DOMAIN ) . '
      </th>
      <td>
        <select id="submit_button_width" name="submit_button_width">
          ' . $button_style_dd . '
        </select>
      </td>
    </tr>';

    return $settings;
  }

  /**
   * Save submit button width settings
   */
  public function save_submit_button_width( $form ) {

    $form['submit_button_width'] = rgpost( 'submit_button_width' );

    return $form;
  }

  /**
   * Add submit button width class
   */
  public function add_submit_button_width_class( $button, $form ) {

    if ( ! isset( $form['submit_button_width'] ) ) {
      return $button;
    }

    return str_replace( 'class="', 'class="' . $form['submit_button_width'] . ' ', $button );
  }

  /**
   * Add submit button placement option
   */
  public function enable_submit_button_placement( $settings, $form ) {

    $button_placement_dd = '';

    foreach ( $this->get_options()['submit_button']['placement'] as $value => $label ) {
      $selected = rgar( $form, 'submit_button_placement' ) == $value ? 'selected="selected"' : '';

      $button_placement_dd .= '<option value="' . $value . '" ' . $selected . '>' . $label . '</option>';
    }

    $settings['Form Button']['field_button_placement'] = '
    <tr id="submit_button_placement_setting">
      <th>
        ' . __( 'Button placement', WPC_Tools::TEXT_DOMAIN ) . '
      </th>
      <td>
        <select id="submit_button_placement" name="submit_button_placement">
          ' . $button_placement_dd . '
        </select>
      </td>
    </tr>';

    return $settings;
  }

  /**
   * Save submit button placement settings
   */
  public function save_submit_button_placement( $form ) {

    $form['submit_button_placement'] = rgpost( 'submit_button_placement' );

    return $form;
  }

  /**
   * Add submit button placement class
   */
  public function add_submit_button_placement_class( $form_string, $form ) {

    if ( ! isset( $form['submit_button_placement'] ) ) {
      return $form_string;
    }

    return str_replace( 'gform_footer', 'gform_footer ' . $form['submit_button_placement'], $form_string );
  }

  /**
   * Get gravity options
   *
   * @return array
   */
  public function get_options() {

    return apply_filters( 'wpc_gravity_forms', [
      'form'          => [
        'style'  => [
          '' => __( 'Default', WPC_Tools::TEXT_DOMAIN ),
        ],
        'margin' => [
          ''     => __( 'Default', WPC_Tools::TEXT_DOMAIN ),
          'mb-0' => __( 'No Margin', WPC_Tools::TEXT_DOMAIN ),
          'mb-1' => __( 'Extra Small', WPC_Tools::TEXT_DOMAIN ),
          'mb-2' => __( 'Small', WPC_Tools::TEXT_DOMAIN ),
          'mb-4' => __( 'Medium', WPC_Tools::TEXT_DOMAIN ),
          'mb-5' => __( 'Large', WPC_Tools::TEXT_DOMAIN )
        ]
      ],
      'field'         => [
        'width' => [
          'col-sm-12' => __( '100%', WPC_Tools::TEXT_DOMAIN ),
          'col-sm-6'  => __( '50%', WPC_Tools::TEXT_DOMAIN )
        ]
      ],
      'submit_button' => [
        'size'      => [
          ''       => __( 'Default', WPC_Tools::TEXT_DOMAIN ),
          'btn-sm' => __( 'Small', WPC_Tools::TEXT_DOMAIN ),
          'btn-lg' => __( 'Large', WPC_Tools::TEXT_DOMAIN )
        ],
        'style'     => [
          'btn-primary'   => __( 'Primary', WPC_Tools::TEXT_DOMAIN ),
          'btn-secondary' => __( 'Secondary', WPC_Tools::TEXT_DOMAIN ),
          'btn-success'   => __( 'Success', WPC_Tools::TEXT_DOMAIN ),
          'btn-info'      => __( 'Info', WPC_Tools::TEXT_DOMAIN ),
          'btn-warning'   => __( 'Warning', WPC_Tools::TEXT_DOMAIN ),
          'btn-danger'    => __( 'Danger', WPC_Tools::TEXT_DOMAIN ),
          'btn-link'      => __( 'Link', WPC_Tools::TEXT_DOMAIN )
        ],
        'width'     => [
          ''          => __( 'Default', WPC_Tools::TEXT_DOMAIN ),
          'btn-block' => __( '100%', WPC_Tools::TEXT_DOMAIN ),
        ],
        'placement' => [
          'button-left'   => __( 'Left', WPC_Tools::TEXT_DOMAIN ),
          'button-center' => __( 'Center', WPC_Tools::TEXT_DOMAIN ),
          'button-right'  => __( 'Right', WPC_Tools::TEXT_DOMAIN )
        ],
      ]
    ] );
  }
}
