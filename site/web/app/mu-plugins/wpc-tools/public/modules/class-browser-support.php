<?php

class WPC_Tools_Public_Module_Browser_Support extends WPC_Tools_Public_Module {

  const MODULE_NAME = WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME;

  protected static $_instance = null;

  /**
   * WPC Tools Admin Instance.
   *
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Browser_Support constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {

    if ( boolval( wpc_get_setting( 'enable_browser_warning', $this->settings_title ) ) && ! isset( $_COOKIE['wpc_browser_support_warning'] ) ) {
      WPC_Tools::loader()->add_action( 'wp_enqueue_scripts', $this, 'enqueue_styles' );
      WPC_Tools::loader()->add_action( 'wp_enqueue_scripts', $this, 'enqueue_scripts' );
      WPC_Tools::loader()->add_action( 'wp_ajax_wpc_get_browser_support_modal', $this, 'get_browser_support_modal_html' );
      WPC_Tools::loader()->add_action( 'wp_ajax_nopriv_wpc_get_browser_support_modal', $this, 'get_browser_support_modal_html' );
    }
  }

  /**
   * Register the stylesheets
   */
  public function enqueue_styles() {
    wp_enqueue_style( self::MODULE_NAME, WPC_TOOLS_URL . 'public/css/' . self::MODULE_NAME . '.min.css', [], WPC_Tools::VERSION, 'all' );
  }

  /**
   * Register the JavaScript
   */
  public function enqueue_scripts() {
    wp_enqueue_script( WPC_Tools::PLUGIN_NAME . '_jquery_cookie', WPC_TOOLS_URL . 'public/js/vendor/jquery.cookie.min.js', [ 'jquery' ], '1.4.1', false );
    wp_register_script( self::MODULE_NAME, WPC_TOOLS_URL . 'public/js/' . self::MODULE_NAME . '.min.js', [ 'jquery' ], WPC_Tools::VERSION, false );
    wp_localize_script( self::MODULE_NAME, 'wpcToolsBrowserSupportGlobals', $this->get_js_globals() );
    wp_enqueue_script( self::MODULE_NAME );
  }

  /**
   * Returns the javascript globals
   *
   * @return array
   */
  public function get_js_globals() {
    return array(
      'ajax_url'  => admin_url( 'admin-ajax.php' ),
      'nonce'     => wp_create_nonce( 'wpc-tools-browser-support-ruVqkQqgQz0b' ),
      'v_ie_edge' => wpc_get_setting( 'browser_v_ie_edge', $this->settings_title ),
      'v_firefox' => wpc_get_setting( 'browser_v_firefox', $this->settings_title ),
      'v_opera'   => wpc_get_setting( 'browser_v_opera', $this->settings_title ),
      'v_safari'  => wpc_get_setting( 'browser_v_safari', $this->settings_title ),
      'v_chrome'  => wpc_get_setting( 'browser_v_chrome', $this->settings_title ),
    );
  }

  /**
   * Modal html on ajax callback
   */
  public function get_browser_support_modal_html() {
    check_ajax_referer( 'wpc-tools-browser-support-ruVqkQqgQz0b', 'nonce' );
    echo WPC_Tools::templates()->get_template_html( 'browser-support/modal' );
    exit;
  }
}

