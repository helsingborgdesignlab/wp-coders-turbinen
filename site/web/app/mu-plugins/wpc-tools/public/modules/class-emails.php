<?php

class WPC_Tools_Public_Module_Emails extends WPC_Tools_Public_Module {

  const MODULE_NAME = WPC_Tools_Admin_Module_Emails::MODULE_NAME;

  protected static $_instance = null;

  /**
   * WPC Tools Admin Instance.
   *
   * Ensures only one instance of WPC_Tools_Admin is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * WPC_Tools_Module_Emails constructor.
   */
  public function __construct() {
    $this->settings_title = str_replace( '-', '_', self::MODULE_NAME );
    $this->module_hooks();
  }

  /**
   * Load module hooks
   */
  private function module_hooks() {

    if ( boolval( wpc_get_setting( 'enable_wp_mail_override', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'wp_mail_content_type', $this, 'get_wp_mail_content_type' );
      WPC_Tools::loader()->add_filter( 'password_change_email', $this, 'get_password_change_email', 10, 3 );
      WPC_Tools::loader()->add_filter( 'email_change_email', $this, 'get_email_change_email', 10, 3 );
      WPC_Tools::loader()->add_filter( 'retrieve_password_message', $this, 'get_retrieve_password_message', 10, 4 );
      WPC_Tools::loader()->add_filter( 'new_user_email_content', $this, 'get_new_user_email_content', 10, 2 );
      WPC_Tools::loader()->add_filter( 'new_admin_email_content', $this, 'get_new_admin_email_content', 10, 2 );

      WPC_Tools::loader()->add_filter( 'wp_mail', $this, 'plain_text_urls_into_html', 100 );

      WPC_Tools::loader()->add_action( 'edit_user_profile_update', $this, 'before_send_wp_mail' );
      WPC_Tools::loader()->add_action( 'login_form_retrievepassword', $this, 'before_send_wp_mail' );
      WPC_Tools::loader()->add_action( 'login_form_lostpassword', $this, 'before_send_wp_mail' );
      WPC_Tools::loader()->add_action( 'lost_password', $this, 'after_send_wp_mail' );

      WPC_Tools::loader()->add_action( 'personal_options_update', $this, 'before_send_wp_mail', 9 );
      WPC_Tools::loader()->add_action( 'personal_options_update', $this, 'after_send_wp_mail', 11 );

      WPC_Tools::loader()->add_action( 'add_option_new_admin_email', $this, 'before_send_wp_mail', 9 );
      WPC_Tools::loader()->add_action( 'update_option_new_admin_email', $this, 'before_send_wp_mail', 9 );
      WPC_Tools::loader()->add_action( 'add_option_new_admin_email', $this, 'after_send_wp_mail', 11 );
      WPC_Tools::loader()->add_action( 'update_option_new_admin_email', $this, 'after_send_wp_mail', 11 );

      // Multisite
      if ( is_multisite() ) {
        WPC_Tools::loader()->add_filter( 'newuser_notify_siteadmin', $this, 'get_newuser_notify_siteadmin_message', 10, 2 );
        WPC_Tools::loader()->add_filter( 'newblog_notify_siteadmin', $this, 'get_newblog_notify_siteadmin_message', 10, 2 );
        WPC_Tools::loader()->add_filter( 'update_welcome_email', $this, 'get_wpmu_welcome_notification_message', 10, 6 );
        WPC_Tools::loader()->add_filter( 'update_welcome_user_email', $this, 'get_wpmu_welcome_user_notification_message', 10, 4 );
        WPC_Tools::loader()->add_filter( 'wpmu_signup_user_notification_email', $this, 'get_wpmu_signup_user_notification_message', 100, 5 );
        WPC_Tools::loader()->add_filter( 'wp_mail', $this, 'email_frame_to_new_site_created_email' );

        WPC_Tools::loader()->add_action( 'wpmu_new_user', $this, 'before_send_wp_network_mail', 9 );
        WPC_Tools::loader()->add_action( 'after_signup_site', $this, 'after_send_wp_network_mail', 11 );
      }
    }

    if ( boolval( wpc_get_setting( 'enable_gravity_override', $this->settings_title ) ) ) {
      WPC_Tools::loader()->add_filter( 'gform_pre_send_email', $this, 'get_gravity_email_template' );
    }
  }

  /**
   * Get WP Mail Content Type
   *
   * @param $content_type
   *
   * @return string
   */
  public function get_wp_mail_content_type( $content_type ) {

    return apply_filters( 'wpc_mail_content_type', 'text/html' );
  }

  /**
   * Convert plain text URLs into HTML hyperlinks
   *
   * @param $args
   *
   * @return array
   */
  public function plain_text_urls_into_html( $args ) {

    $link = wpc_emogrify( '<a href="###WPCURL###" target="_blank">###WPCURL###</a>', WPC_Tools::templates()->get_template_html( 'emails/email-styles' ) );
    $link = str_replace( '###WPCURL###', '$1', $link );

    $args['message'] = preg_replace( "/(?<!src=[\'\"])(?<!href=[\'\"])(?<!url\([\'\"])(((http)+(s)?:\/\/[^<>\s]+)>?)(?!(.*?)\s*<\/a>)/", $link, $args['message'] );

    return $args;
  }

  /**
   * Get WP Mail From Email Address
   *
   * @param $original_email_address
   *
   * @return string
   */
  public function get_wp_mail_from( $original_email_address ) {

    return get_option( 'admin_email', $original_email_address );
  }

  /**
   * Get WP Mail From Name
   *
   * @param $original_email_from
   *
   * @return string
   */
  public function get_wp_mail_from_name( $original_email_from ) {

    return get_option( 'blogname', $original_email_from );
  }

  /**
   * Get WP Mail From Email Address
   *
   * @param $original_email_address
   *
   * @return string
   */
  public function get_wp_network_mail_from( $original_email_address ) {

    return get_site_option( 'admin_email', $original_email_address );
  }

  /**
   * Get WP Mail From Name
   *
   * @param $original_email_from
   *
   * @return string
   */
  public function get_wp_network_mail_from_name( $original_email_from ) {

    return get_site_option( 'site_name', $original_email_from );
  }

  /**
   * Get password change notification
   *
   * @param $pass_change_mail
   *
   * @return array
   */
  public function get_password_change_email( $pass_change_email, $user, $userdata ) {

    $wpc_email_arr = apply_filters( 'wpc_email_password_change_email', [
      'subject' => $pass_change_email['subject'],
      'heading' => str_replace( '[%s] ', '', $pass_change_email['subject'] ),
      'message' => nl2br( $pass_change_email['message'] )
    ], $user, $userdata );

    $pass_change_email['subject'] = $wpc_email_arr['subject'];
    $pass_change_email['message'] = wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );

    return $pass_change_email;
  }

  /**
   * Get email change notification
   *
   * @param $email_change_mail
   *
   * @return array
   */
  public function get_email_change_email( $email_change_email, $user, $userdata ) {

    $wpc_email_arr = apply_filters( 'wpc_email_email_change_email', [
      'subject' => $email_change_email['subject'],
      'heading' => str_replace( '[%s] ', '', $email_change_email['subject'] ),
      'message' => nl2br( $email_change_email['message'] )
    ], $user, $userdata );

    $email_change_email['subject'] = $wpc_email_arr['subject'];
    $email_change_email['message'] = wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );

    return $email_change_email;
  }

  /**
   * Get retrieve password message
   *
   * @param $message
   * @param $key
   * @param $user_login
   * @param $user_data
   *
   * @return string
   */
  public function get_retrieve_password_message( $message, $key, $user_login, $user_data ) {

    $wpc_email_arr = apply_filters( 'wpc_email_retrieve_password_message', [
      'heading' => str_replace( '[%s] ', '', __( '[%s] Password Reset' ) ),
      'message' => nl2br( str_replace( [ '<', '>' ], '', $message ) )
    ], $key, $user_login, $user_data );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Get new email address content
   *
   * @param $email_text
   * @param $new_user_email
   *
   * @return string
   */
  public function get_new_user_email_content( $email_text, $new_user_email ) {

    $wpc_email_arr = apply_filters( 'wpc_email_new_user_email_content', [
      'heading' => str_replace( '[%s] ', '', __( '[%s] New Email Address' ) ),
      'message' => nl2br( $email_text )
    ], $new_user_email );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Get new admin email address content
   *
   * @param $email_text
   * @param $new_admin_email
   *
   * @return string
   */
  public function get_new_admin_email_content( $email_text, $new_admin_email ) {

    $wpc_email_arr = apply_filters( 'wpc_email_new_user_email_content', [
      'heading' => str_replace( '[%s] ', '', __( '[%s] New Admin Email Address' ) ),
      'message' => nl2br( $email_text )
    ], $new_admin_email );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Get new user network admin notification
   *
   * @param $message
   * @param $user
   *
   * @return string
   */
  public function get_newuser_notify_siteadmin_message( $message, $user ) {

    $wpc_email_arr = apply_filters( 'wpc_email_newuser_notify_siteadmin_message', [
      'heading' => str_replace( ': %s', '', __( 'New User Registration: %s' ) ),
      'message' => nl2br( $message )
    ], $user );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Get new blog network admin notification
   *
   * @param $message
   *
   * @return string
   */
  public function get_newblog_notify_siteadmin_message( $message ) {

    $wpc_email_arr = apply_filters( 'wpc_email_newblog_notify_siteadmin_message', [
      'heading' => str_replace( ': %s', '', __( 'New Site Registration: %s' ) ),
      'message' => nl2br( $message )
    ] );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Get site welcome email message
   *
   * @param $welcome_email
   * @param $blog_id
   * @param $user_id
   * @param $password
   * @param $title
   * @param $meta
   *
   * @return string|void
   */
  public function get_wpmu_welcome_notification_message( $welcome_email, $blog_id, $user_id, $password, $title, $meta ) {

    $wpc_email_arr = apply_filters( 'wpc_email_wpmu_welcome_notification_message', [
      'heading' => sprintf( str_replace( ': %2$s', '', __( 'New %1$s Site: %2$s' ) ), get_network()->site_name, wp_unslash( $title ) ),
      'message' => nl2br( $welcome_email )
    ], $blog_id, $user_id, $password, $title, $meta );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Get site welcome user email message
   *
   * @param $welcome_email
   * @param $blog_id
   * @param $user_id
   * @param $password
   * @param $title
   * @param $meta
   *
   * @return string|void
   */
  public function get_wpmu_welcome_user_notification_message( $welcome_email, $user_id, $password, $meta ) {

    $user = get_userdata( $user_id );

    $wpc_email_arr = apply_filters( 'wpc_email_wpmu_welcome_user_notification_message', [
      'heading' => sprintf( str_replace( ': %2$s', '', __( 'New %1$s User: %2$s' ) ), get_network()->site_name, wp_unslash( $user->user_login ) ),
      'message' => nl2br( $welcome_email )
    ], $user_id, $password, $meta );

    return wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
  }

  /**
   * Add email frame to new site created email notification
   *
   * @param $args
   *
   * @return array
   */
  public function email_frame_to_new_site_created_email( $args ) {

    if ( $args['subject'] === sprintf( __( '[%s] New Site Created' ), get_network()->site_name ) ) {

      $wpc_email_arr = apply_filters( 'wpc_email_new_site_created_message', [
        'heading' => str_replace( '[%s] ', '', __( '[%s] New Site Created' ) ),
        'message' => nl2br( $args['message'] )
      ], $args );

      $args['message'] = wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );
    }

    return $args;
  }

  /**
   * Add email frame to signup user notification email notification
   *
   * @param $user_login
   * @param $user_email
   * @param $key
   * @param $meta
   *
   * @return string
   */
  public function get_wpmu_signup_user_notification_message( $message, $user_login, $user_email, $key, $meta ) {

    $wpc_email_arr = apply_filters( 'wpc_wpmu_signup_user_notification_message', [
      'heading' => sprintf( str_replace( '%2$s', '%s', str_replace( '[%1$s] ', '', _x( '[%1$s] Activate %2$s', 'New user notification email subject' ) ) ), $user_login ),
      'message' => nl2br( $message )
    ], $user_login, $user_email, $key, $meta );

    $message = wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] );

    return preg_replace( '/%(?!s)/', '%%', $message );
  }

  /**
   * Get Gravity email template
   *
   * @param $email
   *
   * @return array
   */
  public function get_gravity_email_template( $email ) {

    $email['message'] = wpc_get_the_email_frame( $email['message'], [ 'email_heading' => $email['subject'] ] );

    return $email;
  }

  /**
   * Before send wp mail
   */
  public function before_send_wp_mail() {

    add_filter( 'wp_mail_from', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_mail_from' ) );
    add_filter( 'wp_mail_from_name', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_mail_from_name' ) );
  }

  /**
   * After send wp mail
   */
  public function after_send_wp_mail() {

    remove_filter( 'wp_mail_from', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_mail_from' ) );
    remove_filter( 'wp_mail_from_name', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_mail_from_name' ) );
  }

  /**
   * Before send wp network mail
   */
  public function before_send_wp_network_mail() {

    add_filter( 'wp_mail_from', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_network_mail_from' ) );
    add_filter( 'wp_mail_from_name', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_network_mail_from_name' ) );
  }

  /**
   * After send wp network mail
   */
  public function after_send_wp_network_mail() {

    remove_filter( 'wp_mail_from', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_network_mail_from' ) );
    remove_filter( 'wp_mail_from_name', array( WPC_Tools_Public_Module_Emails::instance(), 'get_wp_network_mail_from_name' ) );
  }
}
