(function( $ ) {
  'use strict';

  var wpcToolsGravitySelectPlaceholder = {
    init: function () {
      var that = this;

      this.manageGFormSelectPlaceholder();

      $(document).bind('gform_post_render', function(){
        that.manageGFormSelectPlaceholder();
      });
    },
    manageGFormSelectPlaceholder: function() {
      var selects = $('select', '.gform_wrapper .gfield');

      selects.each(function(){
        var gfield = $(this).closest('.gfield');
        if($(this).find('option:selected').attr('class') === "gf_placeholder") {
          gfield.addClass("has-placeholder");
        } else {
          gfield.removeClass("has-placeholder");
        }
      });

      selects.change(function(){
        var gfield = $(this).closest('.gfield');
        if($(this).find('option:selected').attr('class') === "gf_placeholder") {
          gfield.addClass("has-placeholder");
        } else {
          gfield.removeClass("has-placeholder");
        }
      });
    }
  };

  $(function () {
    wpcToolsGravitySelectPlaceholder.init();
  });

})( jQuery );
