(function( $ ) {
  'use strict';

  var wpcToolsCookieWarning = {
    init: function () {
      if (this.showWarning()) {
        this.getWarning();
      }
    },
    showWarning: function () {
      return (!$.cookie('wpc_cookie_warning'));
    },
    getWarning: function () {
      $.ajax({
        type: 'POST',
        url: wpcToolsCookiesGlobals.ajax_url,
        data: {
          action: 'wpc_get_cookie_warning',
          nonce: wpcToolsCookiesGlobals.nonce,
        },
        success: function (response) {
          $('body').append(response);

          var $warning = $('.wpc-cookie-warning');

          $warning.find('.wpc-close').on('click', function (e) {
            e.preventDefault();

            $.cookie('wpc_cookie_warning', 'yes', {expires: parseInt(wpcToolsCookiesGlobals.expires)});

            $warning.remove();
          });
        }
      });
    },
  };

  $(function () {
    wpcToolsCookieWarning.init();
  });

})( jQuery );
