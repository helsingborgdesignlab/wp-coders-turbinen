var wpcToolsBrowserSupportModal;

(function( $ ) {
  'use strict';

  wpcToolsBrowserSupportModal = {
    init: function () {
      if (this.showModal()) {
        this.getModal();
      }
    },
    showModal: function () {
      return (!$.cookie('wpc_browser_support_warning'));
    },
    getModal: function () {
      $.ajax({
        type: 'POST',
        url: wpcToolsBrowserSupportGlobals.ajax_url,
        data: {
          action: 'wpc_get_browser_support_modal',
          nonce: wpcToolsBrowserSupportGlobals.nonce
        },
        success: function (response) {
          $('html').addClass('wpc-overflow-hidden');

          $('body').append(response);

          var $modal = $('.wpc-browser-support-modal');

          $modal.find('.wpc-remind-later').on('click', function (e) {
            e.preventDefault();

            $.cookie('wpc_browser_support_warning', 'later', {expires: 1});

            $modal.remove();
            $('html').removeClass('wpc-overflow-hidden');
          });

          $modal.find('.wpc-no-reminder').on('click', function (e) {
            e.preventDefault();

            $.cookie('wpc_browser_support_warning', 'permanent');

            $modal.remove();
            $('html').removeClass('wpc-overflow-hidden');
          });
        }
      });
    },
  };

})( jQuery );

// http://browser-update.org
var $buoop = {
  notify: {
    i: Number(wpcToolsBrowserSupportGlobals.v_ie_edge),
    f: Number(wpcToolsBrowserSupportGlobals.v_firefox),
    o: Number(wpcToolsBrowserSupportGlobals.v_opera),
    s: Number(wpcToolsBrowserSupportGlobals.v_safari),
    c: Number(wpcToolsBrowserSupportGlobals.v_chrome)
  },
  onshow: function() {
    wpcToolsBrowserSupportModal.init();
  },
  unsecure: true,
  unsupported: false,
  // mobile: false,
  // test: true,
  reminder: 0,
  nomessage: true,
  api: 2018.06
};

function $buo_f(){
  var e = document.createElement("script");
  e.src = "//browser-update.org/update.min.js";
  document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
