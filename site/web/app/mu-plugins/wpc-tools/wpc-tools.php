<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @wordpress-plugin
 * Plugin Name:       WPC Tools
 * Plugin URI:        http://wpcoders.io
 * Description:       Plugin to add the default general theme options.
 * Version:           1.8.2
 * Author:            wpcoders.io
 * Author URI:        http://wpcoders.io
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpc-tools
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'WPC_TOOLS_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPC_TOOLS_URL', plugin_dir_url( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wpc-tools-activator.php
 */
function activate_wpc_tools() {
	require_once WPC_TOOLS_DIR . 'includes/class-wpc-tools-activator.php';
	WPC_Tools_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wpc-tools-deactivator.php
 */
function deactivate_wpc_tools() {
	require_once WPC_TOOLS_DIR . 'includes/class-wpc-tools-deactivator.php';
	WPC_Tools_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wpc_tools' );
register_deactivation_hook( __FILE__, 'deactivate_wpc_tools' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require WPC_TOOLS_DIR . 'includes/class-wpc-tools.php';

/**
 * Main instance of WPC_Tools.
 *
 * Returns the main instance of WPC_Tools to prevent the need to use globals.
 */
function WPC_Tools() {
	return WPC_Tools::instance();
}

$GLOBALS['wpc_tools'] = WPC_Tools();

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_wpc_tools() {

	WPC_Tools()->run();

}
run_wpc_tools();
