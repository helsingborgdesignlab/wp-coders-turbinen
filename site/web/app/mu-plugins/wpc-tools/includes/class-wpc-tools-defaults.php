<?php

class WPC_Tools_Defaults {

  private $defaults = array();

  protected static $_instance = null;


  /**
   * Ensures only one instance of WPC Tools is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  public function get($setting_title) {
    return $this->defaults[$setting_title];
  }

  public function set($setting_title, array $defaults) {
    $this->defaults[$setting_title] = $defaults;
  }
}
