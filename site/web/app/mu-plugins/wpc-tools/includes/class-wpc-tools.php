<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://wpcoders.io
 * @since      1.0.0
 *
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 * @author     Robert Bokori <robert@wpcoders.io>
 */
class WPC_Tools {

  /**
   * The unique identifier of this plugin.
   */
  const PLUGIN_NAME = 'wpc-tools';

  /**
   * Plugin text domain
   */
  const TEXT_DOMAIN = 'wpc-tools';

  /**
   * The current version of the plugin.
   */
  const VERSION = '1.8.2';

  /**
   * The single instance of the class.
   */
  protected static $_instance = null;

  /**
   * The loader that's responsible for maintaining and registering all hooks that power
   * the plugin.
   */
  protected static $loader;

  /**
   * Plugin defaults
   */
  protected static $defaults;

  /**
   * Plugin templates
   */
  protected static $templates;

  /**
   * i18n is responsible for loading plugin's language files
   */
  protected $i18n;

  /**
   * Admin instance.
   */
  public $admin_plugin = null;

  /**
   * Public instance.
   */
  public $public_plugin = null;

  /**
   * WPC Tools utility functions.
   */
  public $utils = null;

  /**
   * Ensures only one instance of WPC Tools is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * Define the core functionality of the plugin.
   *
   * Set the plugin name and the plugin version that can be used throughout the plugin.
   * Load the dependencies, define the locale, and set the hooks for the admin area and
   * the public-facing side of the site.
   */
  public function __construct() {
    $this->load_dependencies();
    $this->set_locale();
  }

  /**
   * Load the required dependencies for this plugin.
   *
   * Include the following files that make up the plugin:
   *
   * - WPC_Tools_Loader. Orchestrates the hooks of the plugin.
   * - WPC_Tools_i18n. Defines internationalization functionality.
   * - WPC_Tools_Templates. Defines template functionality.
   * - WPC_Tools_Admin. Defines all hooks for the admin area.
   * - WPC_Tools_Public. Defines all hooks for the public side of the site.
   *
   * Create an instance of the loader which will be used to register the hooks
   * with WordPress.
   */
  private function load_dependencies() {

    /**
     * Plugin's defaults
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wpc-tools-defaults.php';
    self::$defaults = WPC_Tools_Defaults::instance();

    /**
     * Plugin's WPML support functions
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wpc-tools-wpml.php';

    /**
     * Plugin's utility functions
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wpc-tools-utils.php';

    /**
     * This class provides functions for converting CSS styles into inline style attributes in your HTML code.
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-emogrifier.php';

    /**
     * The class responsible for orchestrating the actions and filters of the
     * core plugin.
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wpc-tools-loader.php';
    self::$loader = WPC_Tools_Loader::instance();

    /**
     * The class responsible for defining internationalization functionality
     * of the plugin.
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wpc-tools-i18n.php';
    $this->i18n = WPC_Tools_i18n::instance();

    /**
     * The class responsible for defining template functionality
     * of the plugin.
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wpc-tools-templates.php';
    self::$templates = WPC_Tools_Templates::instance();

    /**
     * The class responsible for defining all actions that occur in the admin area.
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wpc-tools-admin.php';
    $this->admin_plugin = WPC_Tools_Admin::instance();

    /**
     * The class responsible for defining all actions that occur in the public-facing
     * side of the site.
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wpc-tools-public.php';
    $this->public_plugin = WPC_Tools_Public::instance();

    /**
     * WP pluggable function overwrites
     */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wpc-tools-wp-pluggable.php';
  }

  /**
   * Define the locale for this plugin for internationalization.
   *
   * Uses the WPC_Tools_i18n class in order to set the domain and to register the hook
   * with WordPress.
   */
  private function set_locale() {
    $this->loader()->add_action( 'plugins_loaded', $this->i18n, 'load_plugin_textdomain' );
  }

  /**
   * Run the loader to execute all of the hooks with WordPress.
   */
  public function run() {
    $this->loader()->run();
  }

  /**
   * The reference to the class that orchestrates the hooks with the plugin.
   */
  public static function loader() {
    return self::$loader;
  }

  /**
   * Helper class to handle the defaults
   */
  public static function defaults() {
    return self::$defaults;
  }

  /**
   * Helper class to handle the emogrifier
   */
  public static function emogrifier() {
    return new Emogrifier();
  }

  /**
   * Helper class to handle the templates
   */
  public static function templates() {
    return self::$templates;
  }
}
