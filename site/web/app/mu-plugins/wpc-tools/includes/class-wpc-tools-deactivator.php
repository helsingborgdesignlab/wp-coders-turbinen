<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://wpcoders.io
 * @since      1.0.0
 *
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 * @author     Robert Bokori <robert@wpcoders.io>
 */
class WPC_Tools_Deactivator {

	public static function deactivate() {}

}
