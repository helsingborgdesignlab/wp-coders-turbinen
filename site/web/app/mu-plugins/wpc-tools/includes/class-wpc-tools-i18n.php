<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://wpcoders.io
 * @since      1.0.0
 *
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 * @author     Robert Bokori <robert@wpcoders.io>
 */
class WPC_Tools_i18n {

  /**
   * The single instance of the class.
   */
  protected static $_instance = null;


  /**
   * Ensures only one instance of WPC Tools is loaded or can be loaded.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * Load the plugin text domain for translation.
   */
  public function load_plugin_textdomain() {
    load_plugin_textdomain(
      WPC_Tools::TEXT_DOMAIN,
      false,
      dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
    );
  }
}
