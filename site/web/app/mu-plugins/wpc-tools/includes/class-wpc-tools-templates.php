<?php

class WPC_Tools_Templates {

  /**
   * The single instance of the class.
   */
  protected static $_instance = null;

  /**
   * Ensures only one instance of WPC Tools is loaded or can be loaded.
   */
  public static function instance() {

    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   * Returns the path to the WPC Tools templates directory
   *
   * @return string
   */
  public function get_templates_dir() {

    return WPC_TOOLS_DIR . 'templates';
  }

  /**
   * Returns the URL to the WPC Tools templates directory
   *
   * @return string
   */
  public function get_templates_url() {

    return WPC_TOOLS_URL . 'templates';
  }

  /**
   * Returns the URL to the theme's WPC Tools templates directory
   *
   * @return string
   */
  public function get_theme_template_dir_name() {

    return apply_filters( 'wpc_theme_template_dir_name', 'wpc-tools' );
  }

  /**
   * Retrieves a template part
   *
   * @param $slug
   * @param array $args
   *
   * @return string
   */
  public function get_template_part( $slug, $args = [ ] ) {

    if ( $args && is_array( $args ) ) {
      extract( $args );
    }

    $template = $slug . '.php';

    // Allow template parts to be filtered
    $template = apply_filters( 'wpc_get_template_part', $template, $slug, $args );

    $located = $this->locate_template( $template );

    // Return the part that is found
    if ( $located ) {
      include( $located );
    }
  }

  /**
   * Like get_template_part, but returns the HTML instead of outputting.
   *
   * @param $slug
   * @param array $args
   *
   * @return string
   */
  public function get_template_html( $slug, $args = [ ] ) {

    ob_start();
    $this->get_template_part( $slug, $args );

    return ob_get_clean();
  }

  /**
   * Retrieve the name of the highest priority template file that exists.
   *
   * Searches in the STYLESHEETPATH before TEMPLATEPATH so that themes which
   * inherit from a parent theme can just overload one file. If the template is
   * not found in either of those, it looks in the theme-compat folder last.
   *
   * @param $template_name
   *
   * @return bool|string
   */
  public function locate_template( $template_name ) {

    // No file found yet
    $located = false;

    // Trim off any slashes from the template name
    $template_name = ltrim( $template_name, '/' );

    // try locating this template file by looping through the template paths
    foreach ( $this->get_theme_template_paths() as $template_path ) {
      if ( file_exists( $template_path . $template_name ) ) {
        $located = $template_path . $template_name;
        break;
      }
    }

    return $located;
  }

  /**
   * Returns a list of paths to check for template locations
   *
   * @return mixed|void
   */
  public function get_theme_template_paths() {

    $template_dir = $this->get_theme_template_dir_name();

    $file_paths = [
      1   => trailingslashit( get_stylesheet_directory() ) . $template_dir,
      10  => trailingslashit( get_template_directory() ) . $template_dir,
      100 => $this->get_templates_dir()
    ];

    $file_paths = apply_filters( 'wpc_template_paths', $file_paths );

    // sort the file paths based on priority
    ksort( $file_paths, SORT_NUMERIC );

    return array_map( 'trailingslashit', $file_paths );
  }

}
