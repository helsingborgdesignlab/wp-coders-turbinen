<?php
/**
 * This is used to define the utility functions of the plugin
 */

/**
 * Get plugin settings mixed with defaults
 */
if ( ! function_exists( 'wpc_get_settings' ) ) {
  function wpc_get_settings( $settings_title ) {
    return wp_parse_args( (array) get_option( $settings_title ), WPC_Tools::defaults()->get( $settings_title ) );
  }
}

/**
 * Get plugin setting value
 */
if ( ! function_exists( 'wpc_get_setting' ) ) {
  function wpc_get_setting( $key, $settings_title ) {

    $settings_title = str_replace( '-', '_', $settings_title );

    $settings = wpc_get_settings( $settings_title );

    if ( isset( $settings[ $key ] ) ) {
      return $settings[ $key ];
    }

    return false;
  }
}

/**
 * Retrieve the attachment details from attachment id
 */
if ( ! function_exists( 'wpc_get_image_attachment' ) ) {
  function wpc_get_image_attachment( $id, $size = 'full' ) {

    if ( ! $post = get_post( $id ) ) {
      return false;
    }

    $image = wp_get_attachment_image_src( $id, $size );

    $attachment = [
      'title'  => $post->post_title,
      'alt'    => get_post_meta( $id, '_wp_attachment_image_alt', true ),
      'url'    => $image[0],
      'width'  => $image[1],
      'height' => $image[2]
    ];

    return $attachment;
  }
}

/**
 * Get the SVG logo
 */
if ( ! function_exists( 'wpc_get_the_logo' ) ) {
  function wpc_get_the_svg_logo( $type = 'default' ) {

    return wpc_get_setting( $type . '_svg_logo', WPC_Tools_Admin_Module_General::MODULE_NAME );
  }
}

/**
 * Get the logo object
 */
if ( ! function_exists( 'wpc_get_the_logo' ) ) {
  function wpc_get_the_logo( $type = 'default', $retina = false ) {

    if ( $retina ) {
      $attachment_id = wpc_get_setting( $type . '_retina_logo', WPC_Tools_Admin_Module_General::MODULE_NAME );
    } else {
      $attachment_id = wpc_get_setting( $type . '_logo', WPC_Tools_Admin_Module_General::MODULE_NAME );
    }

    if ( '' === $attachment_id ) {
      return;
    }

    $default_logo = wpc_get_image_attachment( $attachment_id );

    return $default_logo;
  }
}

/**
 * Get the logo url
 */
if ( ! function_exists( 'wpc_get_the_logo_url' ) ) {
  function wpc_get_the_logo_url( $type = 'default', $retina = false ) {
    $logo = wpc_get_the_logo( $type, $retina );

    if ( ! $logo ) {
      return false;
    }

    return $logo['url'];
  }
}

/**
 * Get the logo html
 */
if ( ! function_exists( 'wpc_get_the_logo_html' ) ) {
  function wpc_get_the_logo_html( $type = 'default', $classes = '' ) {

    ob_start();

    if ( ! empty( $svg_logo = wpc_get_the_svg_logo( $type ) ) ) {

      echo $svg_logo;

      return ob_get_clean();
    }

    if ( $logo = wpc_get_the_logo( $type ) ) : ?>
      <?php if ( $retina_logo = wpc_get_the_logo( $type, true ) ): ?>
        <img class="logo <?= $classes; ?>" src="<?= $logo['url']; ?>"
             title="<?= $logo['title']; ?>" alt="<?= $logo['alt']; ?>"
             srcset="<?= $retina_logo['url']; ?> 1.1x"
             width="<?= $logo['width']; ?>"
             height="<?= $logo['height']; ?>"/>
      <?php else: ?>
        <img class="logo <?= $classes; ?>" src="<?= $logo['url']; ?>"
             title="<?= $logo['title']; ?>" alt="<?= $logo['alt']; ?>"/>
      <?php endif; ?>
    <?php endif;

    return ob_get_clean();
  }
}

/**
 * Converts CSS styles into inline style attributes in your HTML code
 */
if ( ! function_exists( 'wpc_emogrify' ) ) {
  function wpc_emogrify( $html = '', $css = '', $disableStyleBlocksParsing = false ) {

    if ( '' === $html || '' === $css ) {
      return $html;
    }

    $emogrifier = WPC_Tools::emogrifier();

    if ( $disableStyleBlocksParsing ) {
      $emogrifier->disableStyleBlocksParsing();
    }

    $emogrifier->setHtml( $html );
    $emogrifier->setCss( $css );

    return $emogrifier->emogrify();
  }
}

/**
 * Get the email frame
 */
if ( ! function_exists( 'wpc_get_the_email_frame' ) ) {
  function wpc_get_the_email_frame( $message = "", $header_args = [] ) {

    if ( '' === $message ) {
      return;
    }

    $html = WPC_Tools::templates()->get_template_html( 'emails/email-header', $header_args );
    $html .= $message;
    $html .= WPC_Tools::templates()->get_template_html( 'emails/email-footer' );

    $css = WPC_Tools::templates()->get_template_html( 'emails/email-styles' );

    return wpc_emogrify( $html, $css, true );
  }
}

/**
 * Hex darker functions for colors.
 *
 * @param mixed $color
 * @param int $factor (default: 30)
 *
 * @return string
 */
if ( ! function_exists( 'wpc_hex_darker' ) ) {
  function wpc_hex_darker( $color, $factor = 30 ) {
    $base  = wpc_rgb_from_hex( $color );
    $color = '#';

    foreach ( $base as $k => $v ) {
      $amount      = $v / 100;
      $amount      = round( $amount * $factor );
      $new_decimal = $v - $amount;

      $new_hex_component = dechex( $new_decimal );
      if ( strlen( $new_hex_component ) < 2 ) {
        $new_hex_component = "0" . $new_hex_component;
      }
      $color .= $new_hex_component;
    }

    return $color;
  }
}

/**
 * Hex lighter functions for colors.
 *
 * @param mixed $color
 * @param int $factor (default: 30)
 *
 * @return string
 */
if ( ! function_exists( 'wpc_hex_lighter' ) ) {
  function wpc_hex_lighter( $color, $factor = 30 ) {
    $base  = wpc_rgb_from_hex( $color );
    $color = '#';

    foreach ( $base as $k => $v ) {
      $amount      = 255 - $v;
      $amount      = $amount / 100;
      $amount      = round( $amount * $factor );
      $new_decimal = $v + $amount;

      $new_hex_component = dechex( $new_decimal );
      if ( strlen( $new_hex_component ) < 2 ) {
        $new_hex_component = "0" . $new_hex_component;
      }
      $color .= $new_hex_component;
    }

    return $color;
  }
}

/**
 * RGB form HEX for colors.
 *
 * @param $color
 *
 * @return array
 */
if ( ! function_exists( 'wpc_rgb_from_hex' ) ) {
  function wpc_rgb_from_hex( $color ) {
    $color = str_replace( '#', '', $color );
    // Convert shorthand colors to full format, e.g. "FFF" -> "FFFFFF"
    $color = preg_replace( '~^(.)(.)(.)$~', '$1$1$2$2$3$3', $color );

    $rgb      = [];
    $rgb['R'] = hexdec( $color{0} . $color{1} );
    $rgb['G'] = hexdec( $color{2} . $color{3} );
    $rgb['B'] = hexdec( $color{4} . $color{5} );

    return $rgb;
  }
}

/**
 * Retrieve the default page object based on given post type
 *
 * @param $post_type
 * @param null $attr
 *
 * @return array|bool|mixed|null|\WP_Post
 */
if ( ! function_exists( 'wpc_get_default_page' ) ) {
  function wpc_get_default_page( $post_type, $attr = null ) {

    if ( 'post' === $post_type ) {
      $post_type = 'posts';
    }

    if ( ! $post_type || ! $post_id = apply_filters( 'wpc_get_default_page', get_option( 'page_for_' . $post_type ), $post_type ) ) {
      return false;
    }

    $post = get_post( wpc_icl_object_id( $post_id, 'page', true ) );

    if ( ! $post ) {
      return false;
    }

    if ( $attr && isset( $post->$attr ) ) {
      return $post->$attr;
    }

    return $post;
  }
}

/**
 * Get default page id for the given post type
 *
 * @param $post_type
 *
 * @return array|bool|mixed|null|\WP_Post
 */
if ( ! function_exists( 'wpc_get_default_page_id' ) ) {
  function wpc_get_default_page_id( $post_type ) {

    return wpc_get_default_page( $post_type, 'ID' );
  }
}

/**
 * Get per page for the given post type
 *
 * @param $post_type
 *
 * @return array|bool|mixed|null|\WP_Post
 */
if ( ! function_exists( 'wpc_get_per_page' ) ) {
  function wpc_get_per_page( $post_type = 'posts' ) {

    if ( ! post_type_exists( $post_type ) ) {
      return false;
    }

    return get_option( $post_type . '_per_page' );
  }
}

/**
 * Get default term ID for the given taxonomy
 *
 * @param $taxonomy
 *
 * @return bool|mixed|void
 */
if ( ! function_exists( 'wpc_get_default_term_id' ) ) {
  function wpc_get_default_term_id( $taxonomy = 'category' ) {

    if ( ! taxonomy_exists( $taxonomy ) ) {
      return false;
    }

    return get_option( 'default_' . $taxonomy );
  }
}

/**
 * Retrieve the actual post type
 *
 * @return string
 */
if ( ! function_exists( 'wpc_get_the_post_type' ) ) {
  function wpc_get_the_post_type() {

    $post_type = get_post_type();

    if ( empty( $post_type ) && isset( get_queried_object()->query_var ) ) {
      $post_type = get_queried_object()->query_var;
    }

    if ( empty( $post_type ) && ( is_category() || is_tag() || is_tax() ) && isset( get_queried_object()->taxonomy ) ) {
      $tax_object = get_taxonomy( get_queried_object()->taxonomy );

      if ( ! empty( $tax_object->object_type ) && count( $tax_object->object_type ) === 1 ) {
        $post_type = $tax_object->object_type[0];
      }
    }

    return apply_filters( 'wpc_get_the_post_type', $post_type );
  }
}

/**
 * Retrieve the actual post type label attribute
 *
 * @param string $attr
 * @param null $post_type
 *
 * @return string
 */
if ( ! function_exists( 'wpc_get_the_post_type_label' ) ) {
  function wpc_get_the_post_type_label( $attr = 'name', $post_type = null ) {

    if ( $post_type == null ) {
      $post_type = wpc_get_the_post_type();
    }

    $label = '';

    if ( isset( get_post_type_object( $post_type )->labels->$attr ) ) {
      $label = strtolower( get_post_type_object( $post_type )->labels->$attr );
    }

    return apply_filters( 'wpc_get_the_post_type_label', $label, $attr, $post_type );
  }
}


/**
 * Retrieve the actual post id
 *
 * @param boolean $single Return default page id on post type singles or not.
 *
 * @return int
 */
if ( ! function_exists( 'wpc_get_the_id' ) ) {
  function wpc_get_the_id( $single = false ) {

    if ( is_post_type_archive() || is_home() ) {
      $id = wpc_get_default_page_id( wpc_get_the_post_type() );
    } elseif ( is_tax() || is_category() || is_tag() ) {
      $id = wpc_get_default_page_id( get_queried_object()->taxonomy );
    } else {
      $id = get_the_ID();
    }

    if ( $single && is_single() ) {
      $id = wpc_get_default_page_id( wpc_get_the_post_type() );
    }

    if ( is_404() ) {
      $id = wpc_get_default_page_id( 'wpc_error_404' );
    }

    if ( is_search() ) {
      $id = wpc_get_default_page_id( 'wpc_search' );
    }

    return apply_filters( 'wpc_get_the_id', absint( $id ) );
  }
}

/**
 * Checks if a plugin is activated
 *
 * @param null $plugin
 *
 * @return bool
 */
if ( ! function_exists( 'wpc_is_plugin_active' ) ) {
  function wpc_is_plugin_active( $plugin_name = null, $directory = null ) {

    if ( $plugin_name === null ) {
      return false;
    }

    $active_plugins = get_option( 'active_plugins' );

    if ( is_multisite() ) {
      $active_sitewide_plugins = get_site_option( 'active_sitewide_plugins' );
      $active_plugins          = array_merge( $active_plugins, array_keys( $active_sitewide_plugins ) );
    }

    return ( strpos( implode( '', $active_plugins ), (string) $directory . '/' . (string) $plugin_name . '.php' ) !== false );
  }
}
