<?php

if ( ! function_exists( 'wp_new_user_notification' ) && boolval( wpc_get_setting( 'enable_wp_mail_override', WPC_Tools_Admin_Module_Emails::MODULE_NAME ) ) ) {
  /**
   * Email login credentials to a newly-registered user.
   *
   * A new user registration notification is also sent to admin email.
   *
   * @since 2.0.0
   * @since 4.3.0 The `$plaintext_pass` parameter was changed to `$notify`.
   * @since 4.3.1 The `$plaintext_pass` parameter was deprecated. `$notify` added as a third parameter.
   *
   * @global wpdb $wpdb WordPress database object for queries.
   * @global PasswordHash $wp_hasher Portable PHP password hashing framework instance.
   *
   * @param int $user_id User ID.
   * @param null $deprecated Not used (argument deprecated).
   * @param string $notify Optional. Type of notification that should happen. Accepts 'admin' or an empty
   *                           string (admin only), or 'both' (admin and user). Default empty.
   */
  function wp_new_user_notification( $user_id, $deprecated = null, $notify = '' ) {
    if ( $deprecated !== null ) {
      _deprecated_argument( __FUNCTION__, '4.3.1' );
    }

    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

    $message = sprintf( __( 'New user registration on your site %s:' ), $blogname ) . "<br><br>";
    $message .= sprintf( __( 'Username: %s' ), $user->user_login ) . "<br>";
    $message .= sprintf( __( 'Email: %s' ), $user->user_email );

    WPC_Tools_Public_Module_Emails::instance()->before_send_wp_mail();

    $new_user_reg_subject_base = __( '[%s] New User Registration' );

    $wpc_email_new_arr = apply_filters( 'wpc_email_new_user_registration', [
      'subject' => sprintf( $new_user_reg_subject_base, $blogname ),
      'heading' => str_replace( '[%s] ', '', $new_user_reg_subject_base ),
      'message' => $message
    ], $user );

    @wp_mail( get_option( 'admin_email' ), $wpc_email_new_arr['subject'], wpc_get_the_email_frame( $wpc_email_new_arr['message'], [ 'email_heading' => $wpc_email_new_arr['heading'] ] ) );

    // `$deprecated was pre-4.3 `$plaintext_pass`. An empty `$plaintext_pass` didn't sent a user notifcation.
    if ( 'admin' === $notify || ( empty( $deprecated ) && empty( $notify ) ) ) {
      return;
    }

    // Generate something random for a password reset key.
    $key = wp_generate_password( 20, false );

    /** This action is documented in wp-login.php */
    do_action( 'retrieve_password_key', $user->user_login, $key );

    // Now insert the key, hashed, into the DB.
    if ( empty( $wp_hasher ) ) {
      require_once ABSPATH . WPINC . '/class-phpass.php';
      $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
    $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

    $set_url = network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user->user_login ), 'login' );

    $message = sprintf( __( 'Username: %s' ), $user->user_login ) . "<br><br>";
    $message .= __( 'To set your password, visit the following address:' ) . "<br>";
    $message .= "<a href='" . $set_url . "' target='_blank'>" . $set_url . "</a><br><br>";
    $message .= __( 'Login Address (URL)' ) . ":<br>";
    $message .= "<a href='" . wp_login_url() . "' target='_blank'>" . wp_login_url() . "</a>";

    $your_user_pass_subject_base = __( '[%s] Your username and password info' );

    $wpc_email_your_arr = apply_filters( 'wpc_email_your_user_password_info', [
      'subject' => sprintf( $your_user_pass_subject_base, $blogname ),
      'heading' => str_replace( '[%s] ', '', $your_user_pass_subject_base ),
      'message' => $message
    ], $user, $key );

    wp_mail( $user->user_email, $wpc_email_your_arr['subject'], wpc_get_the_email_frame( $wpc_email_your_arr['message'], [ 'email_heading' => $wpc_email_your_arr['heading'] ] ) );

    WPC_Tools_Public_Module_Emails::instance()->after_send_wp_mail();
  }
}

if ( ! function_exists( 'wp_password_change_notification' ) && boolval( wpc_get_setting( 'enable_wp_mail_override', WPC_Tools_Admin_Module_Emails::MODULE_NAME ) ) ) {
  /**
   * Notify the blog admin of a user changing password, normally via email.
   *
   * @since 2.7.0
   *
   * @param WP_User $user User object.
   */
  function wp_password_change_notification( $user ) {
    // send a copy of password change notification to the admin
    // but check to see if it's the admin whose password we're changing, and skip this
    if ( 0 !== strcasecmp( $user->user_email, get_option( 'admin_email' ) ) ) {

      WPC_Tools_Public_Module_Emails::instance()->before_send_wp_mail();

      // The blogname option is escaped with esc_html on the way into the database in sanitize_option
      // we want to reverse this for the plain text arena of emails.
      $blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

      $subject_base = __( '[%s] Password Changed' );

      $wpc_email_arr = apply_filters( 'wpc_email_password_change_notification', [
        'subject' => sprintf( $subject_base, $blogname ),
        'heading' => str_replace( '[%s] ', '', $subject_base ),
        'message' => sprintf( __( 'Password changed for user: %s' ), $user->user_login )
      ], $user );

      wp_mail( get_option( 'admin_email' ), $wpc_email_arr['subject'], wpc_get_the_email_frame( $wpc_email_arr['message'], [ 'email_heading' => $wpc_email_arr['heading'] ] ) );

      WPC_Tools_Public_Module_Emails::instance()->after_send_wp_mail();
    }
  }
}
