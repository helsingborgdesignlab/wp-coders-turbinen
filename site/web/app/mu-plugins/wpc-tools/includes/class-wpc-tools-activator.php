<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wpcoders.io
 * @since      1.0.0
 *
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    WPC_Tools
 * @subpackage WPC_Tools/includes
 * @author     Robert Bokori <robert@wpcoders.io>
 */
class WPC_Tools_Activator {

	public static function activate() {}

}
