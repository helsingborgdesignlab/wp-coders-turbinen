<?php
/**
 * This is used to define the WPML support functions of the plugin
 */

/**
 * Adjust IDs for multilingual functionality
 */
if ( ! function_exists( 'wpc_icl_object_id' ) ) {
  function wpc_icl_object_id( $id, $post_type = 'post', $return_original_if_missing = false, $language_code = null ) {

    if ( function_exists( 'icl_object_id' ) ) {
      return icl_object_id( $id, $post_type, $return_original_if_missing, $language_code );
    }

    return $id;
  }
}
