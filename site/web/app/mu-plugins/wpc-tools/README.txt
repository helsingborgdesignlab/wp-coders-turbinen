=== Plugin Name ===
Contributors: wpcoders.io
Donate link: http://wpcoders.io
Tags: theme, settings, tools
Requires at least: 3.8.0
Tested up to: 5.0.3
Stable tag: 1.8.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

wpcoders.io team internal use only

== Description ==

Development helper plugin for wpcoders.io team

== Installation ==

1. Upload `wpc-tools` to the `/wp-content/mu-plugins/` directory

== Frequently Asked Questions ==

= Is it a must-use plugin only? =

No you can add it to the default plugins directory as well but recommended to install it as a must-use plugin.

== Changelog ==

= 1.8.2 =
* Ensure default term gets set

= 1.8.1 =
* Merge Default Terms and Default Pages modules to Defaults module
* pot file updated

= 1.8.0 =
* Default Terms module added
* New utility function
  - wpc_get_default_term_id

= 1.7.12.3 =
* btn-xs and btn-md submit button sizes removed from Gravity Forms module

= 1.7.12.2 =
* wpc_get_the_post_type() extend to give result on tax page

= 1.7.12.1 =
* Minor fix

= 1.7.12 =
* Replaces the gravity forms <input> buttons with <button> while maintaining attributes from original <input>
* Minor fixes
* pot file updated

= 1.7.11.1 =
* Minor fix

= 1.7.11 =
* ACF rule types merge to Default Page rule type
* Default Page Parent ACF rule type added
* pot file updated

= 1.7.10.2 =
* Minor fix

= 1.7.10.1 =
* Minor fix

= 1.7.10 =
* Email style fixes
* Emogrifier updated to version 2.0.0
* New utility function
  - wpc_emogrify

= 1.7.9 =
* Privacy Policy page added to to Default Pages module
* Browser Support updates

= 1.7.8.5 =
* Minor fix

= 1.7.8.4 =
* Minor fix

= 1.7.8.3 =
* wpc-email-frame class added to email template body

= 1.7.8.2 =
* Minor fix

= 1.7.8.1 =
* Size setting to gravity date field

= 1.7.8 =
* New admin email support to Email module

= 1.7.7.1 =
* Compressed public js and css

= 1.7.7 =
* Cookie expiry field added to Cookies module
* pot file updated

= 1.7.6.1 =
* Minor fix

= 1.7.6 =
* Confirmation Anchor option added to Gravity Forms module
* Load Typekit and Google Fonts changes

= 1.7.5.2 =
* Search page added to WPC Tools pages
* pot file updated

= 1.7.5.1 =
* wpc_templates_email_styles_after action

= 1.7.5 =
* Bootstrap Custom Control Classes to Gravity Forms module

= 1.7.4.5 =
* Emogrifier namespace removed

= 1.7.4.4 =
* Minor fixes

= 1.7.4.3 =
* Error 404 page added to WPC Tools pages
* pot file updated

= 1.7.4.2 =
* Minor fix

= 1.7.4.1 =
* Minor fix

= 1.7.4 =
* Convert plain text URLs into HTML hyperlinks in emails
* Minor fixes

= 1.7.3 =
* Signup user notification email support to Email module
* Emogrifier updated to version 1.2.0

= 1.7.2 =
* New email address confirmation mail support added

= 1.7.1.6 =
* JS trigger added to load Typekit

= 1.7.1.5 =
* Fix ACF CPT rule values

= 1.7.1.4 =
* Minor fix

= 1.7.1.3 =
* Minor fix

= 1.7.1.2 =
* Multisite support to wpc_is_plugin_active function

= 1.7.1.1 =
* Minor fix

= 1.7.1 =
* Multisite support to Email module
* pot file updated

= 1.7.0.1 =
* Param added to wpc_get_the_id function

= 1.7.0 =
* Cookies module added
* Button background fields added to Browser Support module
* WPC page type added to Default Pages module
* Minor fixes

= 1.6.0 =
* Browser Support module added
* Minor fixes

= 1.5.3.2 =
* Minor fix

= 1.5.3.1 =
* wpc_get_default_page filter

= 1.5.3 =
* category and tag taxonomies added to Default Pages module
* Add Field Type Classes option to Gravity Forms module
* Minor fixes

= 1.5.2 =
* New utility function
  - wpc_is_plugin_active

= 1.5.1.1 =
* wpc_get_the_post_type filter

= 1.5.1 =
* WP mail override fixes and new filters

= 1.5.0.5 =
* Minor fixes

= 1.5.0.4 =
* Minor fixes

= 1.5.0.3 =
* arguments added to wpc_get_the_post_type_label filter
* Site Icon generation fix

= 1.5.0.2 =
* Minor fixes

= 1.5.0.1 =
* New utility functions
  - wpc_get_the_post_type
  - wpc_get_the_post_type_label
  - wpc_get_the_id
* Max width added to Site Icon preview

= 1.5.0 =
* Renamed to WPC Tools
* New admin menu icon
* Media Upload field fix
* Gravity Forms plugin activated check
* Gravity Forms module filters merge
* Default Pages module filters merge
* General module changes:
  - Google Fonts Async Load field added
  - Favicon field removed
  - Site Icon field added
* Email module changes:
  - border radius option removed
  - box shadow option removed
  - base color removed
  - heading color added
  - link color added
  - header background color added
* Minor fixes

= 1.4.0.1 =
* Minor fixes

= 1.4.0 =
* WPML support to Default Pages module

= 1.3.9 =
* from email address and from name fixes
* retrieve password message overwrite added to Email module
* password changed message overwrite added to Email module

= 1.3.8.1 =
* Minor fixes

= 1.3.8 =
* Secondary Logo and Secondary Retina Logo fields added to General module
* Default SVG Logo and Secondary SVG Logo fields added to General module
* Default Default Favicon Logo renamed to Favicon

= 1.3.7 =
* Custom default pages filter added to Default Pages module
* ACF location rules added to Default Pages module

= 1.3.6 =
* Google Maps API Key field added to General module

= 1.3.5 =
* Field width filters added to Gravity Forms module

= 1.3.4.1 =
* editor_style fixes

= 1.3.4 =
* Custom states and notices filters added to Default Pages module

= 1.3.3 =
* Minor fixes

= 1.3.2 =
* Typography.com fonts input
* Load fonts in wp editors

= 1.3.1 =
* Minor fixes

= 1.3.0 =
* Default Pages module added

= 1.2.1 =
* Retina Logo support

= 1.2.0 =
* Email module added

= 1.1.1 =
* Typekit support
* Google Fonts support

= 1.1.0 =
* Modules added
* Gravity Forms support added

= 1.0.0 =
* Initial release
