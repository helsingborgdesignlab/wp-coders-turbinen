<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Load colors
$bg          = wpc_get_setting( 'background_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$body        = wpc_get_setting( 'body_background_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$link        = wpc_get_setting( 'link_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$heading     = wpc_get_setting( 'heading_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$header_bg   = wpc_get_setting( 'header_background_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$header_text = wpc_get_setting( 'header_text_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$text        = wpc_get_setting( 'body_text_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );
$footer_text = wpc_get_setting( 'footer_text_color', WPC_Tools_Admin_Module_Emails::MODULE_NAME );

$bg_darker_10    = wpc_hex_darker( $bg, 10 );
$body_darker_10  = wpc_hex_darker( $body, 10 );
$text_lighter_20 = wpc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
?>
body {
    padding: 0;
    background-color: <?php echo esc_attr( $bg ); ?>;
    min-width: 100%;
}

#wrapper {
    background-color: <?php echo esc_attr( $bg ); ?>;
    margin: 0;
    padding: 70px 0 70px 0;
    -webkit-text-size-adjust: none !important;
    width: 100%;
    min-width: 100%;
    text-align: center;
}

#template_container {
    margin: 0 5px;
    background-color: <?php echo esc_attr( $body ); ?>;
    border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
}

#template_header {
    background-color: <?php echo esc_attr( $header_bg ); ?>;
    color: <?php echo esc_attr( $header_text ); ?>;
    border-bottom: 0;
    font-weight: bold;
    line-height: 100%;
    vertical-align: middle;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
    color: <?php echo esc_attr( $header_text ); ?>;
}

#template_footer td {
    padding: 0;
}

#template_footer #credit {
    border:0;
    color: <?php echo esc_attr( $footer_text ); ?>;
    font-family: Arial;
    font-size: 12px;
    line-height: 125%;
    text-align: center;
    padding: 0 48px 48px 48px;
}

#body_content {
    background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
    padding: 48px;
}

#body_content table td td {
    padding: 12px;
}

#body_content table td th {
    padding: 12px;
}

#body_content td ul.wc-item-meta {
    font-size: small;
    margin: 1em 0 0;
    padding: 0;
    list-style: none;
}

#body_content td ul.wc-item-meta li {
    margin: 0.5em 0 0;
    padding: 0;
}

#body_content td ul.wc-item-meta li p {
    margin: 0;
}

#body_content p {
    margin: 0 0 16px;
}

#body_content_inner {
    color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 14px;
    line-height: 150%;
    text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
    color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    border: 1px solid <?php echo esc_attr( $body_darker_10 ); ?>;
}

.address {
    padding: 12px 12px 0;
    color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    border: 1px solid <?php echo esc_attr( $body_darker_10 ); ?>;
}

.text {
    color: <?php echo esc_attr( $text ); ?>;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
    color: <?php echo esc_attr( $link ); ?>;
}

#header_wrapper {
    padding: 36px 48px;
    display: block;
}

h1 {
    color: <?php echo esc_attr( $heading ); ?>;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 30px;
    font-weight: 300;
    line-height: 150%;
    margin: 0;
    text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
    -webkit-font-smoothing: antialiased;
}

h2 {
    color: <?php echo esc_attr( $heading ); ?>;
    display: block;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 18px;
    font-weight: bold;
    line-height: 130%;
    margin: 16px 0 8px;
    text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h3 {
    color: <?php echo esc_attr( $heading ); ?>;
    display: block;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 130%;
    margin: 16px 0 8px;
    text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
    color: <?php echo esc_attr( $link ); ?>;
    font-weight: normal;
    text-decoration: underline;
}

img {
    border: none;
    display: inline;
    font-size: 14px;
    font-weight: bold;
    height: auto;
    line-height: 100%;
    outline: none;
    text-decoration: none;
    text-transform: capitalize;
    vertical-align: middle;
}
<?php
/**
 * Attach custom styles to the template
 */
do_action( 'wpc_templates_email_styles_after', array(
  'background_color'                => $bg,
  'body_background_color'           => $body,
  'link_color'                      => $link,
  'heading_color'                   => $heading,
  'header_background_color'         => $header_bg,
  'header_text_color'               => $header_text,
  'body_text_color'                 => $text,
  'footer_text_color'               => $footer_text,
  'background_color_darker_10'      => $bg_darker_10,
  'body_background_color_darker_10' => $body_darker_10,
  'body_text_color_lighter_20'      => $text_lighter_20,
) );
