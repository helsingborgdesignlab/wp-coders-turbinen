<?php

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

?>
<!DOCTYPE html>
<html dir="<?php echo is_rtl() ? 'rtl' : 'ltr' ?>">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0"topmargin="0" marginheight="0" offset="0">
<div id="wrapper" class="wpc-email-frame" dir="<?php echo is_rtl() ? 'rtl' : 'ltr' ?>">
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tr>
      <td align="center" valign="top">
        <div id="template_header_image">
          <?php
          if ( $img = wpc_get_image_attachment( wpc_get_setting( 'header_image', WPC_Tools_Admin_Module_Emails::MODULE_NAME ) ) ) {
            if( isset( $img[ 'url' ] ) ) {
              $parsed_url = parse_url( $img[ 'url' ] );
              $home_url = is_multisite() ? network_home_url() : get_option( 'home' );
              echo '<p style="margin-top:0;"><img src="' . $home_url . $parsed_url[ 'path' ] . '" alt="' . get_bloginfo( 'name', 'display' ) . '" /></p>';
            }
          }
          ?>
        </div>
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
          <tr>
            <td align="center" valign="top">
              <!-- Header -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header">
                <tr>
                  <td id="header_wrapper">
                    <h1><?php echo $email_heading; ?></h1>
                  </td>
                </tr>
              </table>
              <!-- End Header -->
            </td>
          </tr>
          <tr>
            <td align="center" valign="top">
              <!-- Body -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                <tr>
                  <td valign="top" id="body_content">
                    <!-- Content -->
                    <table border="0" cellpadding="20" cellspacing="0" width="100%">
                      <tr>
                        <td valign="top">
                          <div id="body_content_inner">
