<?php

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

$page_id = wpc_get_default_page_id( 'wpc_cookie_policy' );

$text            = wpc_get_setting( 'warning_text', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );
$page_link_text  = wpc_get_setting( 'warning_page_link_text', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );
$close_btn_label = wpc_get_setting( 'warning_close_button_label', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );

$background_color = wpc_get_setting( 'warning_background_color', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );
$text_color       = wpc_get_setting( 'warning_text_color', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );
$page_link_color  = wpc_get_setting( 'warning_page_link_color', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );
$close_btn_color  = wpc_get_setting( 'warning_close_button_color', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );
$close_btn_bg     = wpc_get_setting( 'warning_close_button_background', WPC_Tools_Admin_Module_Cookies::MODULE_NAME );

$page_link_h_color = wpc_hex_darker( $page_link_color, 10 );
$close_btn_h_bg    = wpc_hex_darker( $close_btn_bg, 10 );
?>

<?php if ( ! empty( $text ) ): ?>
  <div class="wpc-cookie-warning" style="background-color: <?= $background_color; ?>;">
    <div class="wpc-container">
      <p class="wpc-warning-text" style="color: <?= $text_color; ?>;">
        <?= $text; ?>

        <?php if ( ! empty( $page_id ) && ! empty( $page_link_text ) ): ?>
          <a href="<?= get_the_permalink( $page_id ); ?>" title="<?= $page_link_text; ?>" class="wpc-info-link" style="color: <?= $page_link_h_color ?>;">
            <span style="color: <?= $page_link_color ?>;">
              <?= $page_link_text; ?>
            </span>
          </a>
        <?php endif; ?>
      </p>

      <?php if ( ! empty( $close_btn_label ) ): ?>
        <div class="wpc-buttons">
          <a class="wpc-close wpc-btn" href="#" title="<?= $close_btn_label; ?>" style="background-color: <?= $close_btn_h_bg; ?>; color: <?= $close_btn_color; ?>;">
            <span style="background-color: <?= $close_btn_bg; ?>">
              <?= $close_btn_label; ?>
            </span>
          </a>
        </div>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>

