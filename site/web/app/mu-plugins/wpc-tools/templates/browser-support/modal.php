<?php

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

$title            = wpc_get_setting( 'modal_title', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$headline         = wpc_get_setting( 'modal_headline', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$text             = wpc_get_setting( 'modal_text', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$remind_btn_label = wpc_get_setting( 'modal_remind_button_label', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$ignore_btn_label = wpc_get_setting( 'modal_ignore_button_label', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );

$title_color      = wpc_get_setting( 'modal_title_color', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$headline_color   = wpc_get_setting( 'modal_headline_color', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$text_color       = wpc_get_setting( 'modal_text_color', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$remind_btn_color = wpc_get_setting( 'modal_remind_button_color', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$remind_btn_bg    = wpc_get_setting( 'modal_remind_button_background', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$ignore_btn_color = wpc_get_setting( 'modal_ignore_button_color', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );
$ignore_btn_bg    = wpc_get_setting( 'modal_ignore_button_background', WPC_Tools_Admin_Module_Browser_Support::MODULE_NAME );

$remind_btn_h_bg = wpc_hex_darker( $remind_btn_bg, 10 );
$ignore_btn_h_bg = wpc_hex_darker( $ignore_btn_bg, 10 );
?>

<div class="wpc-browser-support-modal">
  <div class="wpc-container">
    <?php if ( ! empty( $title ) ): ?>
      <h1 class="wpc-modal-title" style="color: <?php echo $title_color; ?>;">
        <?php echo $title; ?>
      </h1>
    <?php endif; ?>

    <?php if ( ! empty( $headline ) ): ?>
      <h2 class="wpc-modal-headline" style="color: <?php echo $headline_color; ?>;">
        <?php echo $headline; ?>
      </h2>
    <?php endif; ?>

    <?php if ( ! empty( $text ) ): ?>
      <p class="wpc-modal-text" style="color: <?php echo $text_color; ?>;">
        <?php echo $text; ?>
      </p>
    <?php endif; ?>

    <div class="wpc-browsers" style="color: <?php echo $text_color; ?>;">
      <a class="wpc-browser wpc-chrome" href="https://www.google.com/chrome/browser" title="Google Chrome" target="_blank" rel="nofollow">
        <figure class="wpc-browser-logo">
          <img src="<?php echo WPC_TOOLS_URL . 'public/images/chrome.png'; ?>">
        </figure>
        <h4>Google Chrome</h4>
      </a>

      <a class="wpc-browser wpc-firefox" href="https://www.mozilla.org/firefox/new/" title="Mozilla Firefox" target="_blank" rel="nofollow">
        <figure class="wpc-browser-logo">
          <img src="<?php echo WPC_TOOLS_URL . 'public/images/firefox.png'; ?>">
        </figure>
        <h4>Mozilla Firefox</h4>
      </a>

      <div class="wpc-clearfix-xs"></div>

      <a class="wpc-browser wpc-opera" href="http://www.opera.com/download" title="Opera" target="_blank" rel="nofollow">
        <figure class="wpc-browser-logo">
          <img src="<?php echo WPC_TOOLS_URL . 'public/images/opera.png'; ?>">
        </figure>
        <h4>Opera</h4>
      </a>

      <a class="wpc-browser wpc-ie" href="https://support.microsoft.com/hu-hu/help/17621/internet-explorer-downloads" title="Internet Explorer" target="_blank" rel="nofollow">
        <figure class="wpc-browser-logo">
          <img src="<?php echo WPC_TOOLS_URL . 'public/images/internet_explorer.png'; ?>">
        </figure>
        <h4>Internet Explorer</h4>
      </a>

      <div class="wpc-clearfix-xs"></div>

      <a class="wpc-browser wpc-safari" href="https://support.apple.com/downloads/internet" title="Safari" target="_blank" rel="nofollow">
        <figure class="wpc-browser-logo">
          <img src="<?php echo WPC_TOOLS_URL . 'public/images/safari.png'; ?>">
        </figure>
        <h4>Safari</h4>
      </a>

      <div class="wpc-clearfix"></div>
    </div>

    <div class="wpc-buttons">
      <?php if ( ! empty( $remind_btn_label ) ): ?>
        <a class="wpc-remind-later wpc-btn" href="#" title="<?php echo $remind_btn_label; ?>" style="background-color: <?php echo $remind_btn_h_bg; ?>; color: <?php echo $remind_btn_color; ?>;">
          <span style="background-color: <?php echo $remind_btn_bg; ?>">
            <?php echo $remind_btn_label; ?>
          </span>
        </a>
      <?php endif; ?>

      <?php if ( ! empty( $ignore_btn_label ) ): ?>
        <a class="wpc-no-reminder wpc-btn" href="#" title="<?php echo $ignore_btn_label; ?>" style="background-color: <?php echo $ignore_btn_h_bg; ?>; color: <?php echo $ignore_btn_color; ?>;">
          <span style="background-color: <?php echo $ignore_btn_bg; ?>">
              <?php echo $ignore_btn_label; ?>
          </span>
        </a>
      <?php endif; ?>
    </div>
  </div>
</div>
