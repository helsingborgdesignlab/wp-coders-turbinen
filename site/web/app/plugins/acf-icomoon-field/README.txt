=== Advanced Custom Fields: IcoMoon Field ===
Contributors: Adam Fischer
Tags: acf, wordpress
Requires at least: 3.5
Tested up to: 4.9.8
Stable tag: 1.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

ACF field to select one IcoMoon icon.

== Description ==

ACF field to select one IcoMoon icon.

== Compatibility ==

This ACF field type is compatible with:
* ACF 5

== Installation ==

1. Copy the `acf-icomoon` folder into your `wp-content/plugins` folder
2. Activate the IcoMoon Field plugin via the plugins admin page
3. Create a new field via ACF and select the IcoMoon type
4. Please refer to the description for more info regarding the field type settings

== Changelog ==

= 1.2.1 =
* Minor fix

= 1.2.0 =
* Conditional Logic support

= 1.1.7 =
* Minor fix

= 1.1.6 =
* acf/load_field/type=icomoon filter removed
* fix export data

= 1.1.5 =
* Field type category changed to Choice

= 1.1.4 =
* Default Icon js error fix

= 1.1.3 =
* Select2 v4 support added

= 1.1.2 =
* Prevent CSS Caching option added

= 1.1.1 =
* Select2 AllowClear usage

= 1.1.0 =
* New options page

= 1.0.0 =
* Initial Release.
