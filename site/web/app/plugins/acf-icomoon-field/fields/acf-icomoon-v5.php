<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( ! class_exists( 'acf_field_icomoon' ) ) :


class acf_field_icomoon extends acf_field {


	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function __construct( $settings ) {

		$this->name = 'icomoon';
		$this->label = __( 'IcoMoon', 'acf-icomoon' );
		$this->category = 'choice';
		$this->defaults = array(
            'allow_null' 	=>	0,
            'save_format'	=>  'class',
            'default_value'	=>	'',
            'choices'		=>	array()
		);
		$this->l10n = array(
            'select_placeholder' => __( 'Select', 'acf-icomoon' )
		);
		$this->settings = $settings;
        $this->choices = $this->get_icomoon_icons();

		// do not delete!
    	parent::__construct();

	}


	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/

	function render_field_settings( $field ) {

		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		*/

        acf_render_field_setting( $field, array(
            'label'			=> __( 'Default Icon', 'acf-icomoon' ),
            'instructions'	=> '',
            'type'			=> 'select',
            'name'			=> 'default_value',
            'class'	  		=> 'icomoon',
            'placeholder'   => __( 'Select', 'acf-icomoon' ),
            'ui'            => 1,
            'choices'		=> array_merge( array( '' => '' ), $this->choices )
        ));

        acf_render_field_setting( $field, array(
            'label'			=> __( 'Return Value', 'acf-icomoon' ),
            'instructions'	=> __( 'Specify the returned value on front end', 'acf-icomoon' ),
            'type'			=> 'radio',
            'name'			=> 'save_format',
            'layout'        =>  'horizontal',
            'choices'	    =>	array(
                'class'		 =>	__( 'Class', 'acf-icomoon' ),
                'element'	 =>	__( 'Element', 'acf-icomoon' )
            )
        ));

        acf_render_field_setting( $field, array(
            'label'			=> __( 'Allow Null?', 'acf-icomoon' ),
            'instructions'	=> '',
            'type'			=> 'radio',
            'name'			=> 'allow_null',
            'layout'        => 'horizontal',
            'choices'	    =>	array(
                1	 =>	__( 'Yes', 'acf-icomoon' ),
                0	 =>	__( 'No', 'acf-icomoon' )
            )
        ));

	}


	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/

	function render_field( $field ) {

        // value must be array
        if ( ! is_array( $field['value'] ) ) {

            // perhaps this is a default value with new lines in it?
            if( strpos( $field['value'], "\n" ) !== false ) {
                // found multiple lines, explode it
                $field['value'] = explode( "\n", $field['value'] );
            } else {
                $field['value'] = array( $field['value'] );
            }
        }

        // trim value
        $field['value'] = array_map( 'trim', $field['value'] );

        // html
        echo '<div class="icomoon-field-wrapper">';

        echo '<select id="' . $field['id'] . '" class="' . $field['class'] . ' icomoon-select2-field" name="' . $field['name'] . '" data-allow_null="' . $field['allow_null'] . '" data-placeholder="' . __( 'Select', 'acf-icomoon' ) . '">';

        //null
        if ( $field['allow_null'] ) {
            echo '<option></option>';
        }

        // loop through values and add them as options
        if ( ! empty ( $this->choices ) ) {
            foreach( $this->choices as $key => $value ) {
                $selected = $this->find_selected( $key, $field['value'], $field['save_format'] );
                echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
            }
        }

        echo '</select>';
        echo '</div>';

	}


	/*
	* Find selected icon
	*
	* @param $needle
	* @param $haystack
	* @param $type
	* @param $choices
	*
	* @return string
	*/

    function find_selected( $needle, $haystack, $type ) {

        $string = '';

        switch( $type ) {
            case 'element':
                $search = array( '<i class="', '"></i>' );
                $string = str_replace( $search, '', $haystack[0] );
                break;

            case 'class':
                $string = $haystack[0];
                break;
        }

        if( $string == $needle ) {
            return 'selected="selected"';
        }

        return '';

    }


	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function input_admin_enqueue_scripts() {

		// vars
		$url = $this->settings['url'];
		$version = $this->settings['version'];
		$icomoon_url = $this->settings['icomoon_url'];


		// register & include JS
		wp_register_script( 'acf-input-icomoon', "{$url}assets/js/input.js", array('acf-input'), $version );
		wp_enqueue_script( 'acf-input-icomoon' );


		// register & include CSS
        if ( ! empty( $icomoon_url ) ) {
            wp_register_style( 'acf-icomoon', $icomoon_url, array('acf-input'), $version );
            wp_enqueue_style( 'acf-icomoon' );
        }

	}


	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add CSS + JavaScript to assist your render_field_options() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function field_group_admin_enqueue_scripts() {

        // vars
        $url = $this->settings['url'];
        $version = $this->settings['version'];
        $icomoon_url = $this->settings['icomoon_url'];

        $acf_icomoon_globals = [
            "icons_json" => json_encode($this->choices)
        ];


        // register & include JS
        wp_register_script( 'acf-field-group-icomoon', "{$url}assets/js/field-group.js", array('acf-field-group'), $version );
        wp_localize_script( 'acf-field-group-icomoon', 'acf_icomoon_globals', $acf_icomoon_globals);
        wp_enqueue_script( 'acf-field-group-icomoon' );


        // register & include CSS
        if ( $icomoon_url ) {
            wp_register_style( 'acf-icomoon', $icomoon_url, array('acf-field-group'), $version );
            wp_enqueue_style( 'acf-icomoon' );
        }

	}


	/*
	*  load_value()
	*
	*  This filter is applied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value found in the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*  @return	$value
	*/

	function load_value( $value, $post_id, $field ) {

        switch( $field['save_format'] ) {
            case 'element':
                $value = '<i class="' . $value . '" aria-hidden="true"></i>';
                break;
        }

        return $value;

	}


    /*
	*  load_field()
	*
	*  This filter is applied to the $field after it is loaded from the database
	*
	*  @type	filter
	*  @date	23/01/2013
	*  @since	3.6.0
	*
	*  @param	$field (array) the field array holding all the field options
	*  @return	$field
	*/

    function load_field( $field ) {

        $field['choices'] = array();

        return $field;

    }


    /*
    * Get IcoMoon field's choices
    *
    * @return array
    */

    function get_icomoon_icons() {

        $ico_arr = array();

        if( $this->settings['icomoon_url'] ) {

            $icomoon_data = wp_remote_get( $this->settings['icomoon_url'] );

            if( is_array($icomoon_data) && isset($icomoon_data['headers']['content-type'])) {

                if( $icomoon_data['headers']['content-type'] == 'text/css' ) {

                    //get class prefix
                    preg_match_all( '/^\[class\^="(.*?)\"/m', $icomoon_data['body'], $prefix );

                    //get classes
                    preg_match_all( '/^\.('.$prefix[1][0].'[\w\-]+)/m', $icomoon_data['body'], $icons );

                    foreach( $icons[1] as $icon ) {
                        $ico_arr[$icon] = $icon;
                    }
                }
            }
        }

        return $ico_arr;
    }

}


// initialize
new acf_field_icomoon( $this->settings );


// class_exists check
endif;

?>
