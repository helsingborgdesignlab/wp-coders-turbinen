<?php

/*
Plugin Name: Advanced Custom Fields: IcoMoon Field
Plugin URI: http://wpcoders.io
Description: ACF field to select one IcoMoon icon.
Version: 1.2.1
Author: Adam Fischer
Author URI: http://wpcoders.io
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( ! class_exists( 'acf_plugin_icomoon' ) ) :


class acf_plugin_icomoon {

	/*
	*  __construct
	*
	*  This function will setup the class functionality
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function __construct() {

		// vars
        $this->settings_title = 'acf_icomoon_settings';

		$this->settings = array(
			'version'	     => '1.2.1',
			'url'		     => plugin_dir_url( __FILE__ ),
			'path'		     => plugin_dir_path( __FILE__ ),
            'icomoon_url'    => $this->acf_icomoon_get_css_url()
		);

		// set text domain
		// https://codex.wordpress.org/Function_Reference/load_plugin_textdomain
		load_plugin_textdomain( 'acf-icomoon', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );

		// include field
		add_action( 'acf/include_field_types', 	array( $this, 'include_field_types' )); // v5

        // assets
        add_action( 'wp_head', array( $this, 'acf_icomoon_load_front_end_assets' ) );

        // options page
        add_action( 'admin_menu', array( $this, 'acf_icomoon_add_admin_menu' ) , 100);
        add_action( 'admin_init', array( $this, 'acf_icomoon_settings_init' ) );

	}


	/*
	*  include_field_types
	*
	*  This function will include the field type class
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	$version (int) major ACF version. Defaults to false
	*  @return	n/a
	*/

	function include_field_types( $version = false ) {

		// support empty $version
		if( !$version ) $version = 5;


		// include
		include_once( 'fields/acf-icomoon-v' . $version . '.php' );

	}


    /*
    * This function will load assets on the front end
    */

    function acf_icomoon_load_front_end_assets() {

        if ( ! $this->settings['icomoon_url'] ) {
            return;
        }

        if ( $this->acf_icomoon_get_setting('front_end_style') === 'disabled' ) {
            return;
        }

        echo '<link rel="stylesheet" href="' . $this->settings['icomoon_url'] . '">';

    }


    /*
	* This functions will create options page
	*/

    function acf_icomoon_add_admin_menu() {

        add_submenu_page(
            'edit.php?post_type=acf-field-group',
            __( 'IcoMoon Field Settings', 'acf-icomoon' ),
            __( 'IcoMoon', 'acf-icomoon' ),
            'manage_options',
            'acf-icomoon-settings',
            array( $this, 'acf_icomoon_options_page' )
        );

    }

    function acf_icomoon_settings_init() {

        register_setting(
            $this->settings_title,
            $this->settings_title,
            [ $this, 'acf_icomoon_sanitize_settings' ]
        );

        add_settings_section(
            $this->settings_title . '_general',
            '',
            '__return_false',
            $this->settings_title
        );

        add_settings_field(
            $this->settings_title . '_url',
            __( 'CSS File Url', 'acf-icomoon' ),
            array ( $this, 'acf_icomoon_text_field_render' ),
            $this->settings_title,
            $this->settings_title . '_general',
            array(
                "field_name"    => $this->settings_title . '[url]',
                "field_id"      => $this->settings_title . '_url',
                "setting_key"   => 'url',
                "field_class"   => 'regular-text',
                "default"       => ''
            )
        );

        add_settings_field(
            $this->settings_title . '_front_end_style',
            __( 'Enqueue Style On Front-End', 'acf-icomoon' ),
            array( $this,  'acf_icomoon_dropdown_render' ),
            $this->settings_title,
            $this->settings_title . '_general',
            array(
                "field_name"    => $this->settings_title . '[front_end_style]',
                "field_id"      => $this->settings_title . '_front_end_style',
                "setting_key"   => 'front_end_style',
                "field_class"   => '',
                "choices"       => array (
                    'enabled'  => __( 'Enabled', 'acf-icomoon' ),
                    'disabled' => __( 'Disabled', 'acf-icomoon' ),
                ),
                "default"       => 'enabled'
            )
        );

        add_settings_field(
            $this->settings_title . '_css_caching',
            __( 'Prevent CSS Caching', 'acf-icomoon' ),
            array( $this,  'acf_icomoon_dropdown_render' ),
            $this->settings_title,
            $this->settings_title . '_general',
            array(
                "field_name"    => $this->settings_title . '[css_caching]',
                "field_id"      => $this->settings_title . '_css_caching',
                "setting_key"   => 'css_caching',
                "field_class"   => '',
                "choices"       => array (
                    'enabled'  => __( 'Enabled', 'acf-icomoon' ),
                    'disabled' => __( 'Disabled', 'acf-icomoon' ),
                ),
                "default"       => 'disabled'
            )
        );

    }

    function acf_icomoon_sanitize_settings( $input_fields ) {

        $valid_fields = array();

        foreach ( $input_fields as $key => $val ) {
            $value                = trim( $val );
            $valid_fields[ $key ] = strip_tags( stripslashes( $value ) );
        }

        return $valid_fields;
    }

    function acf_icomoon_text_field_render( $args ) {

        $field_value = $this->acf_icomoon_get_setting($args['setting_key']);

        if ( ! $field_value ) {
            $field_value = $args['default'];
        }

        ?>
        <input type="text" class="<?php echo $args['field_class']; ?>" id="<?php echo $args['field_id']; ?>"
               name="<?php echo $args['field_name'] ?>" value="<?php echo esc_attr( $field_value ); ?>">
        <?php

    }

    function acf_icomoon_dropdown_render( $args ) {

        $field_value = $this->acf_icomoon_get_setting($args['setting_key']);

        if ( ! $field_value ) {
            $field_value = $args['default'];
        }

        ?>
        <select name="<?php echo $args['field_name'] ?>" id="<?php echo $args['field_id']; ?>" class="<?php echo $args['field_class']; ?>">
            <?php foreach ( $args['choices'] as $key => $value ): ?>
                <option class="level-0" value="<?php echo $key; ?>" <?php selected( $key, $field_value ); ?>>
                    <?php echo $value; ?>
                </option>
            <?php endforeach; ?>
        </select>
        <?php
    }

    function acf_icomoon_options_page() {

        ?>

        <div class="wrap">

            <h1><?php _e( 'IcoMoon Field Settings', 'acf-icomoon' ); ?></h1>

            <form action='options.php' method='post'>

                <?php
                settings_fields( $this->settings_title );
                do_settings_sections( $this->settings_title );
                submit_button();
                ?>

            </form>

        </div>

        <?php

    }


    /*
    * Get plugin setting value
    */

    function acf_icomoon_get_setting( $setting_key ) {

        $settings = get_option( $this->settings_title );

        if ( isset( $settings[ $setting_key ] ) ) {
            return $settings[ $setting_key ];
        }

        return false;
    }

    /*
    * Get CSS url
    */

    function acf_icomoon_get_css_url() {

        $css_url = $this->acf_icomoon_get_setting( 'url' );


        if ( $this->acf_icomoon_get_setting( 'css_caching' ) === 'enabled' ) {
            return strtok( $css_url, '?' ) . '?' . time();
        }

        return $css_url;
    }

}


// initialize
new acf_plugin_icomoon();


// class_exists check
endif;

?>
