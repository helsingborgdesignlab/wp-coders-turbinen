(function ($) {

  function initialize_field($el) {

    var l10n = acf._e('icomoon');

    if ($el.parent('.row-clone').length === 0 && $el.parents('.clones').length === 0) {
      $('select.icomoon-select2-field', $el).each(function () {
        $(this).select2({
          width: '100%',
          allowClear: $(this).data('allow_null'),
          placeholder: l10n.select_placeholder,
          formatResult: format,
          formatSelection: format,
          templateResult: format,
          templateSelection: format,
          escapeMarkup: function (m) {
            return m;
          }
        });
      });
    }
  }

  function format(state) {
    return "<i class='" + state.text + "'></i> " + state.text;
  }

  if (typeof acf.registerFieldType !== 'undefined') {

    var Field = acf.Field.extend({
      type: 'icomoon',
    });

    acf.registerFieldType(Field);

    acf.registerConditionForFieldType('hasValue', 'icomoon');
    acf.registerConditionForFieldType('hasNoValue', 'icomoon');
    acf.registerConditionForFieldType('EqualTo', 'icomoon');
    acf.registerConditionForFieldType('NotEqualTo', 'icomoon');
    acf.registerConditionForFieldType('Contains', 'icomoon');

  }

  if (typeof acf.add_action !== 'undefined') {

    acf.add_action('ready append', function ($el) {
      acf.get_fields({type: 'icomoon'}, $el).each(function () {
        initialize_field($(this));
      });
    });

  } else {

    $(document).live('acf/setup_fields', function (e, postbox) {
      $(postbox).find('.field[data-field_type="icomoon"], .sub_field[data-field_type="icomoon"]').each(function () {
        initialize_field($(this));
      });
    });

  }

})(jQuery);
