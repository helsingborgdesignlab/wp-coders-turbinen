(function ($) {

  function initSelect2(selects) {

    var l10n = acf._e('icomoon');

    selects.select2({
      width: '100%',
      allowClear: true,
      placeholder: l10n.select_placeholder,
      formatResult: format,
      formatSelection: format,
      templateResult: format,
      templateSelection: format,
      escapeMarkup: function (m) {
        return m;
      }
    });
  }

  function format(state) {
    return "<i class='" + state.text + "'></i> " + state.text;
  }

  acf.add_action('append', function ($el) {
    var $icomoon_select_form = $el.closest('.acf-field-object-icomoon');
    var $icomoon_select = $('select.icomoon', $icomoon_select_form);

    if ($icomoon_select.length) {
      var choices = JSON.parse(acf_icomoon_globals.icons_json);

      for (i in choices) {
        $icomoon_select.append($('<option>', {value: i, text: choices[i]}));
      }

      initSelect2($icomoon_select);
    }
  });

  acf.add_action('ready', function ($el) {
    var $selects = $('select.icomoon');

    initSelect2($selects);
  });

})(jQuery);
