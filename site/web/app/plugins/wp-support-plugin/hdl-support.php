<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://hbgdesignlab.io/
 * @since             1.0.0
 * @package           Hdl_Support
 *
 * @wordpress-plugin
 * Plugin Name:       HDL Support
 * Plugin URI:        https://hbgdesignlab.io/
 * Description:       The Helsingborg Design LAB Support Chat plugin is used for instant support questions and site support. This is a custom build plugin by the Helsingborg Design LAB support team.
 * Version:           1.0.0
 * Author:            Helsingborg Design LAB
 * Author URI:        https://hbgdesignlab.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       hdl-support
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-hdl-support-activator.php
 */
function activate_hdl_support() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hdl-support-activator.php';
	Hdl_Support_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-hdl-support-deactivator.php
 */
function deactivate_hdl_support() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hdl-support-deactivator.php';
	Hdl_Support_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_hdl_support' );
register_deactivation_hook( __FILE__, 'deactivate_hdl_support' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-hdl-support.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_hdl_support() {

	$plugin = new Hdl_Support();
	$plugin->run();

}
run_hdl_support();
