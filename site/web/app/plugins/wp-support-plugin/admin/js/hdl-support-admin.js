(function ($) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	var HDL = {
		admin: {
			init: function () {
				this.bind_upload_logo_button();
				this.bind_remove_logo_button();
			},
			
			// The "Upload" button
			bind_upload_logo_button: function () {
				jQuery('.upload_hdl_admin_logo_button').click(function () {
					var send_attachment_bkp = wp.media.editor.send.attachment;
					var button = jQuery(this);
					wp.media.editor.send.attachment = function (props, attachment) {
						jQuery(button).parent().prev().attr('src', attachment.url);
						jQuery(button).prev().val(attachment.id);
						wp.media.editor.send.attachment = send_attachment_bkp;
					}
					wp.media.editor.open(button);
					return false;
				});
			},

			// The "Remove" button (remove the value from input type='hidden')
			bind_remove_logo_button: function () {
				jQuery('.remove_hdl_admin_logo_button').click(function (event) {
					event.preventDefault();
					var answer = confirm('Are you sure?');
					if (answer == true) {
						var src = jQuery(this).parent().prev().attr('data-src');
						jQuery(this).parent().prev().attr('src', src);
						jQuery(this).prev().prev().val('');
					}
					return false;
				});
			},
		}
	};

	jQuery(function () {
		HDL.admin.init();
	});

})(jQuery);
