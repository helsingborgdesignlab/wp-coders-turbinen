<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://hbgdesignlab.com/
 * @since      1.0.0
 *
 * @package    Hdl_Support
 * @subpackage Hdl_Support/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Hdl_Support
 * @subpackage Hdl_Support/admin
 * @author     Anunay D. <anunay@hbgdesignlab.com>
 */
class Hdl_Support_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hdl_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hdl_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/hdl-support-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hdl_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hdl_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/hdl-support-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_media();

	}

	public function track_logged_in_user_for_intercom()
	{
		global $current_user;
		if (is_user_logged_in() && is_admin()) {
			$current_user = wp_get_current_user();
			$userhash = hash_hmac(
				'sha256', // hash function
				$current_user->user_email, // user's email address
				'9UPkv1PAgZQX6oLd2Ms3tONEYKLZlbbz0UKPO50M' // secret key (keep safe!)
			);
			?>
			<script>
					window.intercomSettings = {
						app_id: "jkpfhn5l",
						name: "<?php echo $current_user->user_firstname; ?> <?php echo $current_user->user_lastname;?>",
						firstname: "<?php echo $current_user->user_firstname; ?>",
						lastname: "<?php echo $current_user->user_lastname; ?>",
						email: "<?php echo $current_user->user_email; ?>", // Email address
						user_hash: "<?php echo $userhash; ?>" // HMAC using SHA-256

					};
					(function () { var w = window; var ic = w.Intercom; if (typeof ic === "function") { ic('reattach_activator'); ic('update', intercomSettings); } else { var d = document; var i = function () { i.c(arguments) }; i.q = []; i.c = function (args) { i.q.push(args) }; w.Intercom = i; function l() { var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/jkpfhn5l'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); } if (w.attachEvent) { w.attachEvent('onload', l); } else { w.addEventListener('load', l, false); } } })()
			</script>
		<?php
		}
	}

	/**
	 * Replace footer credits with HDL Hardcoded one. 
	 */
	public function update_footer_credits(){
		return "Thank you for hosting with Helsingborg Design LAB";
	}
	
	public function log_mailer_errors($wp_error){
		$fn = ABSPATH . '/mail.log'; // say you've got a mail.log file in your server root
		$fp = fopen($fn, 'a');
		fputs($fp, "Mailer Error: " . $wp_error->get_error_message() . "\n");
		fclose($fp);
	}	
}
