<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://hbgdesignlab.com/
 * @since      1.0.0
 *
 * @package    Hdl_Support
 * @subpackage Hdl_Support/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Hdl_Support
 * @subpackage Hdl_Support/includes
 * @author     Anunay D. <anunay@hbgdesignlab.com>
 */
class Hdl_Support_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
