<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://hbgdesignlab.se/
 * @since      1.0.0
 *
 * @package    WP_Support_Plugin
 * @subpackage WP_Support_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WP_Support_Plugin
 * @subpackage WP_Support_Plugin/admin
 * @author     Anunay D <anunay@hbgdesignlab.se>
 */
class WP_Support_Plugin_Settings
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string $plugin_name The name of this plugin.
	 * @param string $version     The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * HDL settings page
	 *
	 * @return void
	 */
	public function hdl_page()
	{
		?>
		<div class='wrap'>
		<?php settings_errors(); ?>
		<h1><?php echo __('HDL Panel', 'hdlplugin'); ?></h1>
		<?php
			$active_tab = 'api';
			if (isset($_GET['tab'])) {
				$active_tab = esc_attr($_GET['tab']);
			} // end if
		?>
		<h2 class="nav-tab-wrapper">
			<a href="?page=hdl&amp;tab=general" class="nav-tab <?php echo 'general' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php echo __('General', 'hdlplugin'); ?></a>
		</h2>
		<form method='post' action='options.php'>
			<?php
				if('general' === $active_tab) {
					settings_fields('hdl-general-section');
					do_settings_sections('hdl-general-options');
				} else {
					settings_fields('hdl-general-section');
					do_settings_sections('hdl-general-options');
				}
				submit_button();
			?>
		</form>
		</div>
	<?php

}


/**
 * HDL Email support page
 *
 * @return void
 */
public function hdl_email_support_page(){
	$sent_email_successfully = 0;	
	if( $_POST['sendsupportemail'] && $_POST['sendsupport'] == 1 ) {
		if( is_admin() ){
			$current_user = wp_get_current_user();
			$emaildata['to'] = HDL_SUPPORT_EMAIL;
			$emaildata['subject'] = SUPORT_EMAIL_SUBJECT;
			$emaildata['hdlmessage'] = $_POST['hdlmessage'];
			$emaildata['headers'][] = 'Content-Type: text/html; charset=UTF-8';
			if( !empty( $current_user->user_firstname ) ){
				$emaildata['headers'][] = ' From : ' . $current_user->user_firstname . ' ' . $current_user->user_lastname . ' <' . $current_user->user_email . '>';
			}else{
				$emaildata['headers'][] = ' From : ' . get_bloginfo('name') . ' <' . get_bloginfo('admin_email') . '>';
			}
			

			
			if( wp_mail($emaildata['to'], $emaildata['subject'], $emaildata['hdlmessage'], $emaildata['headers'] ) ){
				$sent_email_successfully = 1;
			}else{
				$sent_email_successfully = 2;
			}
		}
	}
	?>
	<h1><?php echo __("HDL Support Email","hdlplugin"); ?></h1>
	<?php if($sent_email_successfully > 0 ): ?>
		<?php if( $sent_email_successfully == 1 ): ?>
			<div class="success-notice-style">Message sent successfully</div>
		<?php else: ?>
			<div class="error-notice-style">There is error sending out an email</div>
		<?php endif; ?>
	<?php endif; ?>
	<form id="hdlsupportemail" method="post" action="<?php bloginfo("url"); ?>/wp-admin/admin.php?page=hdl_email_support">
		<input type="hidden" name="sendsupport" value="1">
		<textarea id="hdlmessage" name="hdlmessage"><?php echo $_POST['hdlmessage']; ?></textarea><br/>
		<input type="submit" id="sendsupportemail" name="sendsupportemail" value="Send Email" />
	</form>
<?php 
}


/**
 * Adds navigation tot the WordPress admin
 *
 * @return void
 */
public function add_hdl_menu_item()
{
	add_menu_page(__('HDL - Support', 'hdlplugin'), __('HDL - Support', 'hdlplugin'), 'manage_options', 'hdl', array($this, 'hdl_page'), plugins_url('wp-support-plugin/public/images/icon.png'), 2);
	add_submenu_page('hdl', __('Settings', 'hdlplugin'), __('Settings', 'hdlplugin'), 'manage_options', 'hdl', array($this, 'hdl_page'));
	add_submenu_page('hdl', __('Email Support ', 'hdlplugin'), __('Email Support ', 'hdlplugin'), 'manage_options', 'hdl_email_support', array($this, 'hdl_email_support_page'));
}


/**
 * Login logo
 *
 * @return void
 */
public function display_admin_login_logo_element()
{

		// Set variables.
	$options = get_option('hdl_admin_login_logo');
	$default_image = 'https://www.placehold.it/115x115';

	if (!empty($options)) {
		$image_attributes = wp_get_attachment_image_src($options, 'full');
		$src = $image_attributes[0];
		$value = $options;
	} else {
		$src = $default_image;
		$value = '';
	}
	?>
		<div class="upload" style="max-width:400px;">
			<img data-src="<?php echo esc_textarea($default_image); ?>" src="<?php echo esc_textarea($src); ?>" style="max-width:100%; height:auto;"/>
			<div>
				<input type="hidden" name="hdl_admin_login_logo" id="hdl_admin_login_logo" value="<?php echo esc_textarea($value); ?>" />
				<button type="button" class="upload_hdl_admin_logo_button button" title="Upload Image"><?php echo __('Upload', 'hdlplugin'); ?></button>
				<button type="button" class="remove_hdl_admin_logo_button button" title="Remove Image">&times;</button>
			</div>
		</div>
		<?php

}


/**
 * Display secret key option field
 *
 * @return void
 */
public function display_login_footer_first_link_label_element()
{
	?>
			<input type='text' style='width:500px' name='hdl_general_first_link_label' id='hdl_general_first_link_label' value='<?php echo esc_attr(get_option('hdl_general_first_link_label')); ?>' />
		<?php

}


/**
 * Display secret key option field
 *
 * @return void
 */
public function display_login_footer_first_link_element()
{
	?>
			<input type='text' style='width:500px' name='hdl_general_first_link_url' id='hdl_general_first_link_url' value='<?php echo esc_attr(get_option('hdl_general_first_link_url')); ?>' />
		<?php

}


/**
 * Display secret key option field
 *
 * @return void
 */
public function display_login_footer_second_link_label_element()
{
	?>
			<input type='text' style='width:500px' name='hdl_general_second_link_label' id='hdl_general_second_link_label' value='<?php echo esc_attr(get_option('hdl_general_second_link_label')); ?>' />
		<?php

}


/**
 * Display secret key option field
 *
 * @return void
 */
public function display_login_footer_second_link_element()
{
	?>
			<input type='text' style='width:500px' name='hdl_general_second_link_url' id='hdl_general_second_link_url' value='<?php echo esc_attr(get_option('hdl_general_second_link_url')); ?>' />
		<?php

}


/**
 * Display secret key option field
 *
 * @return void
 */
public function display_login_logo_height_element()
{
	?>
			<input type='text' style='width:500px' name='hdl_general_login_logo_height' id='hdl_general_login_logo_height' value='<?php echo esc_attr(get_option('hdl_general_login_logo_height')); ?>' />
		<?php

}

/**
 * Display all settings in settings page.
 *
 * @return void
 */
public function display_hdl_panel_fields()
{

		// Setting Sections.
	add_settings_section('hdl-general-section', __('General Settings', 'hdlplugin'), null, 'hdl-general-options');

		// GENERAL TAB
	add_settings_field('hdl_admin_login_logo', __('Login Logo URL', 'hdlplugin'), array($this, 'display_admin_login_logo_element'), 'hdl-general-options', 'hdl-general-section');
	add_settings_field('hdl_general_first_link_label', __('Login Footer 1st Label', 'hdlplugin'), array($this, 'display_login_footer_first_link_label_element'), 'hdl-general-options', 'hdl-general-section');
	add_settings_field('hdl_general_firt_link_url', __('Login Footer 1st Link', 'hdlplugin'), array($this, 'display_login_footer_first_link_element'), 'hdl-general-options', 'hdl-general-section');
	add_settings_field('hdl_general_second_link_label', __('Footer 2nd Label', 'hdlplugin'), array($this, 'display_login_footer_second_link_label_element'), 'hdl-general-options', 'hdl-general-section');
	add_settings_field('hdl_general_second_link_url', __('Footer 2nd Link', 'hdlplugin'), array($this, 'display_login_footer_second_link_element'), 'hdl-general-options', 'hdl-general-section');
	// add_settings_field('hdl_general_login_logo_height', __('Logo Height', 'hdlplugin'), array($this, 'display_login_logo_height_element'), 'hdl-general-options', 'hdl-general-section');


	register_setting('hdl-general-section', 'hdl_admin_login_logo');
	register_setting('hdl-general-section', 'hdl_general_first_link_label');
	register_setting('hdl-general-section', 'hdl_general_first_link_url');
	register_setting('hdl-general-section', 'hdl_general_second_link_label');
	register_setting('hdl-general-section', 'hdl_general_second_link_url');
	register_setting('hdl-general-section', 'hdl_general_login_logo_height');

}
}
