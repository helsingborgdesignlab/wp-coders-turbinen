<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://hbgdesignlab.com/
 * @since      1.0.0
 *
 * @package    Hdl_Support
 * @subpackage Hdl_Support/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Hdl_Support
 * @subpackage Hdl_Support/public
 * @author     Anunay D. <anunay@hbgdesignlab.com>
 */
class Hdl_Support_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hdl_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hdl_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/hdl-support-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hdl_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hdl_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/hdl-support-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/intercom.js', array( 'jquery' ), $this->version, false );

	}

	public function hdl_login_logo_url()
	{
		return 'https://hbgdesignlab.com';
	}
	

	public function hdl_login_logo_url_title()
	{
		return 'Helsingborg Design Lab';
	}
	
	public function hdl_login_logo()
	{
		$options = get_option('hdl_admin_login_logo', '');
		$src = "";
		$default_image = "";
		if (!empty($options)) {
			$image_attributes = wp_get_attachment_image_src($options, 'full');
			$src = $image_attributes[0];
			$value = $options;
		} else {
			$src = $default_image;
			$value = '';
		}		
		?>
		
		<?php if( $src != "" ): ?>
			<style type="text/css">
				#login h1 a, .login h1 a {
				background-image: url(<?php echo $src; ?>);
				height:<?php echo get_option("hdl_general_login_logo_height","60"); ?>px;
				width:100%;
				background-size: contain;
				background-repeat: no-repeat;
					padding-bottom: 30px;
				}
				.login-footer-container{
					width: 320px;
					padding: 0!important;
					margin: auto!important;
				}
				.custom-login-footer-text{
					padding: 0 24px!important;
				}
				.custom-login-footer-text a{
					text-decoration: none;
					font-size: 13px;
					color: #555d66;
				}

				.custom-login-footer-text a:hover{
					color: #00a0d2;
				}		
			</style>
		<?php endif; 
	}

	public function hdl_custom_footer()
	{

		echo '<div class="login-footer-container">';
			if( get_option( 'hdl_general_first_link_label', '' ) !="" ){
				echo '<p class="custom-login-footer-text"><a target="_blank" href="'. get_option( 'hdl_general_first_link_url', '#' ) .'">' . get_option('hdl_general_first_link_label', '') . '</a></p>';
			}

	if (get_option('hdl_general_second_link_label', '') != "") {
		echo '<p class="custom-login-footer-text"><a target="_blank" href="' . get_option('hdl_general_second_link_url', '#') . '">' . get_option('hdl_general_second_link_label', '') . '</a></p>';
	}

		echo '</div>';
	}	
}
