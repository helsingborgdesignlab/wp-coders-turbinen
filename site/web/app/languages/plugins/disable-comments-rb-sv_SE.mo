��          |      �             !     6     J  �   _     �     �            �     6   �  	   
  *       ?     Y     m  �   �     8     A     U     f  �   u  6   F  	   }              
                	                             Comments are closed. Disable Comments RB Disable all comments Easy tool to disable comments for your blog posts, pages. Admin can disable comments in just few clicks. Delete comments from blog post. Enable Rb Disable Comments Save Changes Settings The <em>Disable Comments</em> plugin is active, but isn't configured to do anything yet. Visit the <a href="%s">configuration page</a> to choose which post types to disable comments on. https://robosoft.co/wordpress-plugins/disable-comments rbPlugins PO-Revision-Date: 2017-12-11 00:17:17+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: sv_SE
Project-Id-Version: Plugins - Disable Comments - Stable (latest release)
 Kommentarer är stängda. Disable Comments RB Inaktivera alla kommentarer Lätt verktyg för att inaktivera kommentarer för dina blogginlägg och sidor. Admin kan inaktivera kommentarer med några få klick. Ta bort kommentarer från blogginlägg. Aktivera Rb Disable Comments Spara ändringar Inställningar Tillägget <em>Disable Comments</em> är aktivt men är inte konfigurerad att göra något än. Besök <a href="%s">konfigurationssidan</a> för att välja på vilka inläggstyper kommentarer ska inaktiveras. https://robosoft.co/wordpress-plugins/disable-comments rbPlugins 