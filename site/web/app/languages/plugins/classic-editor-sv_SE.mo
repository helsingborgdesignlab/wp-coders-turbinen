��          �      �        $   	     .  `   L  E   �  +   �          .  )   K  +   u     �     �     �  �   �     w     z     �     �  ?   �     �     
  ,     -   ;  (  i  ;   �     �  �   �  P   p  4   �     �  '     ,   -  -   Z  
   �     �     �  �   �     �	     �	     �	     �	  P   �	     
     9
  ,   <
  -   i
           	                      
                                                                       Allow site admins to change settings Allow users to switch editors By default the Block Editor is replaced with the Classic Editor and users cannot switch editors. Change the %1$sClassic Editor settings%2$s on your User Profile page. Change the %1$sClassic Editor settings%2$s. Classic Editor Default editor for all users Edit &#8220;%s&#8221; in the Block Editor Edit &#8220;%s&#8221; in the Classic Editor Editor Editor NameBlock Editor Editor NameClassic Editor Enables the WordPress classic editor and the old-style Edit Post screen with TinyMCE, Meta Boxes, etc. Supports the older plugins that extend this screen. No Settings Switch to Block Editor Switch to Classic Editor The Classic Editor plugin prevents use of the new Block Editor. WordPress Contributors Yes https://github.com/WordPress/classic-editor/ https://wordpress.org/plugins/classic-editor/ PO-Revision-Date: 2018-12-08 22:57:21+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: sv_SE
Project-Id-Version: Plugins - Classic Editor - Stable (latest release)
 Tillåt webbplatsadministratörer att ändra inställningar Låt användare byta redigerare Som standard ersätts blockredigeraren av den klassiska redigeraren och användarna kan inte växla mellan redigeringsmiljöerna. Ändra %1$sinställningarna för Classic Editor%2$s på din användarprofilsida. Ändra %1$sinställningarna för Classic Editor%2$s. Classic Editor Standardredigerare för alla användare Redigera &#8221;%s&#8221; i blockredigeraren Redigera ”%s” i den klassiska redigeraren Redigerare Blockredigerare Klassisk redigerare Aktiverar den klassiska redigeraren i WordPress och den tidigare layouten av sidan för redigering av inlägg, med TinyMCE, metafält m.m.. Stöder äldre WordPress-tillägg som utökar denna skärmbild. Nej Inställningar Växla till blockredigeraren Byt till Classic Editor Tillägget Classic Editor förhindrar användningen av den nya blockredigeraren. Bidragsgivare till WordPress Ja https://github.com/WordPress/classic-editor/ https://wordpress.org/plugins/classic-editor/ 