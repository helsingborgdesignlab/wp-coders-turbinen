import 'slick-carousel';

const heroSlider = () => {
  const $layouts = $('.layout-item.hero_slider');
  const globals = window.turbinenGlobals;

  $layouts.each((index, layout) => {
    const $slider = $('.hs-slides', layout);

    $slider.slick({
      rtl: true,
      arrows: true,
      dots: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      infinite: true,
      swipe: true,
      pauseOnHover: false,
      pauseOnFocus: false,
      draggable: true,
      autoplay: $slider.data('autoplay') === 'enabled',
      autoplaySpeed: $slider.data('interval'),
      prevArrow: '<button type="button" class="slick-prev"><i class="turbinen-icon-chevron-left" aria-hidden="true"></i> <span class="sr-only">' + globals.t_prev + '</span></button>',
      nextArrow: '<button type="button" class="slick-next"><i class="turbinen-icon-chevron-right" aria-hidden="true"></i> <span class="sr-only">' + globals.t_next + '</span></button>',
    });
  });
};

export default heroSlider;
