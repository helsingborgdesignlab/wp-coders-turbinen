import 'waypoints/lib/jquery.waypoints';
import manageImgCover from '../util/imgCover';

class housesManager {
  constructor(container) {
    this.$grid = $('.houses-row', container);
    this.$housesNav = $('.houses-nav', container);
    this.$termFilterNav = $('.houses-filters', container);
    this.$termFilterLinks = $('.houses-filters-select', this.$termFilterNav);

    this.globals = window.turbinenGlobals;

    this.data = {
      action: 'turbinen_load_houses',
      offset: 0,
      nonce: this.globals.nonce,
      terms: {},
      posts_per_page: this.$grid.data('posts-per-page'),
      no_results_message: this.$grid.data('no-results-message'),
      read_more_text: this.$grid.data('read-more-text'),
    };

    if( this.$grid.data('cat') !== '' ) {
      this.data.terms['turb_house_cat'] = this.$grid.data('cat');
    }

    if( this.$grid.data('area') !== '' ) {
      this.data.terms['turb_house_area'] = this.$grid.data('area');
    }

    if( this.$grid.data('phase') !== '' ) {
      this.data.terms['turb_house_phase'] = this.$grid.data('phase');
    }

    if( this.$grid.data('type') !== '' ) {
      this.data.terms['turb_house_type'] = this.$grid.data('type');
    }

    this.manageInfinityScroll();
    this.manageFilters();
  }

  // Send ajax request
  getPosts() {
    return $.ajax({
      method: 'POST',
      url: this.globals.ajax_url,
      dataType: 'json',
      data: this.data,
    });
  }

  // Handle responses
  handleResponse(response, slideDuration) {
    if (response.success) {
      const $gridItems = $('.hentry', this.$grid);
      const $elems = $(response.data.html).filter('.hentry').addClass('hentry-ajax');

      if (this.data.offset > 0) {
        // Load more
        $elems.insertAfter($gridItems.last()).hide().slideDown(slideDuration, () => {
          this.manageInfinityScroll();
        });
      } else {
        // Filter change
        if (parseInt(response.data.count, 10) > 0) {
          this.$grid.removeClass('empty');
        } else {
          this.$grid.addClass('empty');
        }

        this.$grid.html('');
        if (response.data.count === 0) {
          this.$grid.append(response.data.html).hide().slideDown(slideDuration);
        } else {
          this.$grid.append($elems).hide().slideDown(slideDuration);
        }
      }

      new manageImgCover({
        base: '.houses-row',
        selector: '.hentry-ajax .img-cover',
        minWidth: 0,
      });

      $('.hentry').removeClass('hentry-ajax');

      if (this.$housesNav.length) {
        if ($elems.length < this.houses_per_page || $('.hentry', this.$grid).length >= parseInt(response.data.count, 10)) {
          this.$housesNav.slideUp(slideDuration);
        } else {
          this.$housesNav.slideDown(slideDuration);
        }
      }
    } else {
      if (this.$housesNav.length) {
        this.$housesNav.slideUp(slideDuration);
      }
    }
  }

  // Handle infinity scroll
  manageInfinityScroll() {
    if (this.$housesNav.length) {
      window.Waypoint.destroyAll();

      this.$housesNav.waypoint({
        handler: () => {
          if (!this.$housesNav.hasClass('loading') && this.$housesNav.is(':visible')) {
            this.$housesNav.addClass('loading');
            this.data.offset = $('.hentry', this.$grid).length;

            const houses = this.getPosts();
            houses.success((response) => {
              this.handleResponse(response, 400);
              this.$housesNav.removeClass('loading');
              window.Waypoint.destroyAll();
            });
          }
        },
        offset: 'bottom-in-view',
      });
    }
  }

  // Handle filter changes
  manageFilters() {
    this.$termFilterLinks.on('change', (e) => {
      e.preventDefault();

      const $filterSelect = $(e.currentTarget);

      // Set link classes
      this.$termFilterLinks.attr('disabled', true);
      $filterSelect.parent().addClass('loading');

      // Set current term
      const taxonomy = $filterSelect.data('taxonomy');
      const term_id = $filterSelect.val();

      if (term_id === '') {
        delete this.data.terms[taxonomy];
      } else {
        this.data.terms[taxonomy] = term_id;
      }

      // Reset the offset because we change filters
      this.data.offset = 0;

      const houses = this.getPosts();
      houses.success((response) => {
        this.$termFilterLinks.attr('disabled', false).parent().removeClass('loading');
        this.handleResponse(response, 0);
        this.manageInfinityScroll();
      });
    });
  }
}

const houses = () => {
  $('.layout-item.houses').each((index, container) => {
    new housesManager(container);
  });
};

export default houses;
