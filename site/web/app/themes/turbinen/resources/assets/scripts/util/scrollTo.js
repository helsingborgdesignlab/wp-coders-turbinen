const scrollTo = () => {
  $('a').not('[data-toggle=collapse]').on('click', (e) => {
    if (/^#/.test($(e.currentTarget).attr('href')) || $(e.currentTarget).hasClass('scroll-to')) {
      e.preventDefault();

      const target = $(e.currentTarget).attr('href');
      let $target = $(target);

      if ($target.length) {
        const $header = $('.header-wrap');
        const headerHeight = $('.header', $header).outerHeight();
        const helperHeight = $('.header-helper', $header).outerHeight();
        let offset = headerHeight + helperHeight;

        if ($target.find('.layout-item-inner').length) {
          $target = $target.find('.layout-item-inner');
        }

        $('html, body').stop().animate({
          scrollTop: $target.offset().top - offset,
        }, 1250, 'easeInOutExpo');
      }
    }
  });
};

export default scrollTo;
