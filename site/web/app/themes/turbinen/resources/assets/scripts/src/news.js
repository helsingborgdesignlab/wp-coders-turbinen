import 'waypoints/lib/jquery.waypoints';
import manageImgCover from '../util/imgCover';

class newsManager {
  constructor(container) {
    this.$grid = $('.posts-row', container);
    this.$postsNav = $('.posts-nav', container);
    this.$categoryFilterNav = $('.posts-categories', container);
    this.$categoryFilterLinks = $('li a', this.$categoryFilterNav);

    this.globals = window.turbinenGlobals;

    this.data = {
      action: 'turbinen_load_news',
      offset: 0,
      nonce: this.globals.nonce,
      cat: this.$grid.data('cat'),
      posts_per_page: this.$grid.data('posts-per-page'),
      no_results_message: this.$grid.data('no-results-message'),
      read_more_text: this.$grid.data('read-more-text'),
    };

    this.manageInfinityScroll();
    this.manageFilters();
  }

  // Send ajax request
  getPosts() {
    return $.ajax({
      method: 'POST',
      url: this.globals.ajax_url,
      dataType: 'json',
      data: this.data,
    });
  }

  // Handle responses
  handleResponse(response, slideDuration) {
    if (response.success) {
      const $gridItems = $('.hentry', this.$grid);
      const $elems = $(response.data.html).filter('.hentry').addClass('hentry-ajax');

      if (this.data.offset > 0) {
        // Load more
        $elems.insertAfter($gridItems.last()).hide().slideDown(slideDuration, () => {
          this.manageInfinityScroll();
        });
      } else {
        // Filter change
        if (parseInt(response.data.count, 10) > 0) {
          this.$grid.removeClass('empty');
        } else {
          this.$grid.addClass('empty');
        }

        this.$grid.html('');
        if (response.data.count === 0) {
          this.$grid.append(response.data.html).hide().slideDown(slideDuration);
        } else {
          this.$grid.append($elems).hide().slideDown(slideDuration);
        }
      }

      new manageImgCover({
        base: '.posts-row',
        selector: '.hentry-ajax .img-cover',
        minWidth: 0,
      });

      $('.hentry').removeClass('hentry-ajax');

      if (this.$postsNav.length) {
        if ($elems.length < this.data.posts_per_page || $('.hentry', this.$grid).length >= parseInt(response.data.count, 10)) {
          this.$postsNav.slideUp(slideDuration);
        } else {
          this.$postsNav.slideDown(slideDuration);
        }
      }
    } else {
      if (this.$postsNav.length) {
        this.$postsNav.slideUp(slideDuration);
      }
    }
  }

  // Handle infinity scroll
  manageInfinityScroll() {
    if (this.$postsNav.length) {
      window.Waypoint.destroyAll();

      this.$postsNav.waypoint({
        handler: () => {
          if (!this.$postsNav.hasClass('loading') && this.$postsNav.is(':visible')) {
            this.$postsNav.addClass('loading');
            this.data.offset = $('.hentry', this.$grid).length;

            const posts = this.getPosts();
            posts.success((response) => {
              this.handleResponse(response, 400);
              this.$postsNav.removeClass('loading');
              window.Waypoint.destroyAll();
            });
          }
        },
        offset: 'bottom-in-view',
      });
    }
  }

  // Handle filter changes
  manageFilters() {
    this.$categoryFilterLinks.on('click', (e) => {
      e.preventDefault();

      const $filterBtn = $(e.currentTarget);

      if ($filterBtn.hasClass('active')) {
        return;
      }

      // Set link classes
      this.$categoryFilterLinks.removeClass('active').attr('disabled', true);
      $filterBtn.addClass('active').addClass('loading');

      // Set current category
      this.data.cat = $filterBtn.data('cat');

      // Reset the offset because we change filters
      this.data.offset = 0;

      const posts = this.getPosts();
      posts.success((response) => {
        this.$categoryFilterLinks.attr('disabled', false).removeClass('loading');
        this.handleResponse(response, 0);
        this.manageInfinityScroll();
      });
    });
  }
}

const news = () => {
  $('.layout-item.news').each((index, container) => {
    new newsManager(container);
  });
};

export default news;
