import 'slick-carousel';

const projectStatus = () => {
  const $layouts = $('.layout-item.project_status');

  $layouts.each((index, layout) => {
    const $carousel = $('.ps-items', layout);

    $carousel.slick({
      arrows: false,
      dots: true,
      autoplay: false,
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    });
  });
};

export default projectStatus;
