// Utils
import initScrollTo from '../util/scrollTo';
import manageImgCover from '../util/imgCover';

// Components
import initApartments from '../src/apartments';
import initButtons from '../src/buttons';
import initGallery from '../src/gallery';
import initGoogleMap from '../src/googleMap';
import initHeader from '../src/header';
import initHeroSlider from '../src/heroSlider';
import initHouses from '../src/houses';
import initNews from '../src/news';
import initOfficeSpaces from '../src/officeSpaces';
import initProjectStatus from '../src/projectStatus';
import initSplitContent from '../src/splitContent';

export default {
  init() {
    initApartments();
    initButtons();
    initGallery();
    initGoogleMap();
    initHeader();
    initHeroSlider();
    initHouses();
    initNews();
    initOfficeSpaces();
    initSplitContent();
    initProjectStatus();
    initScrollTo();

    new manageImgCover();
  },
};
