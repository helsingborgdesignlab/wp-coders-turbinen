import 'imagesloaded';
import manageImgCover from '../util/imgCover';

class galleryManager {
  constructor(container) {
    this.$grid = $('.layout-item-row', container);
    this.$galleryNav = $('.gallery-nav', container);
    this.$moreButton = $('.btn-load', this.$galleryNav);

    if (this.$galleryNav.length) {
      this.globals = window.turbinenGlobals;
      this.data = {
        action: 'turbinen_load_gallery',
        offset: 0,
        nonce: this.globals.nonce,
        images: this.$galleryNav.data('images'),
        images_per_page: parseInt(this.$galleryNav.data('images-per-page')),
      };

      this.manageLoadMore();
    }
  }

  // Send ajax request
  getImages() {
    return $.ajax({
      method: 'POST',
      url: this.globals.ajax_url,
      dataType: 'json',
      data: this.data,
    });
  }

  // Handle responses
  handleResponse(response, slideDuration) {
    if (response.success) {
      const $gridItems = $('.layout-item-col', this.$grid);
      const $elems = $(response.data.html).filter('.layout-item-col').addClass('layout-item-col-ajax');

      $elems.insertAfter($gridItems.last()).hide().imagesLoaded(() => {
        $elems.slideDown(slideDuration);

        new manageImgCover({
          base: '.layout-item.gallery',
          selector: '.layout-item-col-ajax .img-cover',
          minWidth: 0,
        });

        $('.layout-item-col').removeClass('.layout-item-col-ajax');
        this.$moreButton.removeClass('loading');

        if ($elems.length < this.data.images_per_page || $('.layout-item-col', this.$grid).length >= parseInt(response.data.count, 10)) {
          this.$galleryNav.slideUp(slideDuration);
        } else {
          this.$galleryNav.slideDown(slideDuration);
        }
      });
    } else {
      this.$moreButton.toggleClass('loading');
      this.$galleryNav.slideUp(slideDuration);
    }
  }

  // Handle load more items
  manageLoadMore() {
    this.$moreButton.on('click', (e) => {
      e.preventDefault();

      if (!this.$moreButton.hasClass('loading')) {
        this.$moreButton.toggleClass('loading');

        this.data.offset = $('.layout-item-col', this.$grid).length;

        const posts = this.getImages();
        posts.success((response) => {
          this.handleResponse(response, 400);
        });
      }
    });
  }
}

const gallery = () => {
  $('.layout-item.gallery').each((index, container) => {
    new galleryManager(container);
  });
};

export default gallery;
