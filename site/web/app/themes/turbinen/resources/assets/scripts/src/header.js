import bouncefix from 'bouncefix.js/dist/bouncefix';

class headerManager {
  constructor() {
    this.$html = $('html');
    this.$body = $('body');
    this.$header = $('.header-wrap');
    this.$dropdown = $('.header-dropdown', this.$header);

    this.initSearch();
    this.initDropdownNavigationToggle();
    this.manageStickyNav();

    $(window).on('scroll touchstart touchend swipe', () => {
      this.manageStickyNav();
    });
  }

  initSearch() {
    const $searchField = $('.search-form-field', this.$header);

    $('*[data-toggle=search-form]', this.$header).on('click', () => {
      this.$body.toggleClass('search-active');

      if(this.$body.hasClass('search-active')) {
        $searchField.focus();
      } else {
        $searchField.blur();
      }
    });
  }

  initDropdownNavigationToggle() {
    bouncefix.add('header-dropdown');

    $('.dropdown-nav-toggle', this.$header).on('click touchend', (e) => {
      e.preventDefault();
      this.$html.toggleClass('dropdown-nav-active');
    });
  }

  manageStickyNav() {
    if ($(window).scrollTop() > 30) {
      this.$body.addClass('sticky-nav-active');
    } else {
      this.$body.removeClass('sticky-nav-active');
    }
  }
}

const header = () => {
  new headerManager();
};

export default header;
