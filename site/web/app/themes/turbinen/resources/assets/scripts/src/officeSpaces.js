import 'waypoints/lib/jquery.waypoints';
import manageImgCover from '../util/imgCover';

class officeSpacesManager {
  constructor(container) {
    this.$grid = $('.offices-row', container);
    this.$officeSpacesNav = $('.offices-nav', container);
    this.$termFilterNav = $('.offices-filters', container);
    this.$termFilterLinks = $('.offices-filters-select', this.$termFilterNav);

    this.globals = window.turbinenGlobals;

    this.data = {
      action: 'turbinen_load_office_spaces',
      offset: 0,
      nonce: this.globals.nonce,
      terms: {},
      posts_per_page: this.$grid.data('posts-per-page'),
      no_results_message: this.$grid.data('no-results-message'),
      read_more_text: this.$grid.data('read-more-text'),
    };

    if( this.$grid.data('area') !== '' ) {
      this.data.terms['turb_office_space_area'] = this.$grid.data('area');
    }

    if( this.$grid.data('type') !== '' ) {
      this.data.terms['turb_office_space_type'] = this.$grid.data('type');
    }

    this.manageInfinityScroll();
    this.manageFilters();
  }

  // Send ajax request
  getPosts() {
    return $.ajax({
      method: 'POST',
      url: this.globals.ajax_url,
      dataType: 'json',
      data: this.data,
    });
  }

  // Handle responses
  handleResponse(response, slideDuration) {
    if (response.success) {
      const $gridItems = $('.hentry', this.$grid);
      const $elems = $(response.data.html).filter('.hentry').addClass('hentry-ajax');

      if (this.data.offset > 0) {
        // Load more
        $elems.insertAfter($gridItems.last()).hide().slideDown(slideDuration, () => {
          this.manageInfinityScroll();
        });
      } else {
        // Filter change
        if (parseInt(response.data.count, 10) > 0) {
          this.$grid.removeClass('empty');
        } else {
          this.$grid.addClass('empty');
        }

        this.$grid.html('');
        if (response.data.count === 0) {
          this.$grid.append(response.data.html).hide().slideDown(slideDuration);
        } else {
          this.$grid.append($elems).hide().slideDown(slideDuration);
        }
      }

      new manageImgCover({
        base: '.officeSpaces-row',
        selector: '.hentry-ajax .img-cover',
        minWidth: 0,
      });

      $('.hentry').removeClass('hentry-ajax');

      if (this.$officeSpacesNav.length) {
        if ($elems.length < this.officeSpaces_per_page || $('.hentry', this.$grid).length >= parseInt(response.data.count, 10)) {
          this.$officeSpacesNav.slideUp(slideDuration);
        } else {
          this.$officeSpacesNav.slideDown(slideDuration);
        }
      }
    } else {
      if (this.$officeSpacesNav.length) {
        this.$officeSpacesNav.slideUp(slideDuration);
      }
    }
  }

  // Handle infinity scroll
  manageInfinityScroll() {
    if (this.$officeSpacesNav.length) {
      window.Waypoint.destroyAll();

      this.$officeSpacesNav.waypoint({
        handler: () => {
          if (!this.$officeSpacesNav.hasClass('loading') && this.$officeSpacesNav.is(':visible')) {
            this.$officeSpacesNav.addClass('loading');
            this.data.offset = $('.hentry', this.$grid).length;

            const officeSpaces = this.getPosts();
            officeSpaces.success((response) => {
              this.handleResponse(response, 400);
              this.$officeSpacesNav.removeClass('loading');
              window.Waypoint.destroyAll();
            });
          }
        },
        offset: 'bottom-in-view',
      });
    }
  }

  // Handle filter changes
  manageFilters() {
    this.$termFilterLinks.on('change', (e) => {
      e.preventDefault();

      const $filterSelect = $(e.currentTarget);

      // Set link classes
      this.$termFilterLinks.attr('disabled', true);
      $filterSelect.parent().addClass('loading');

      // Set current term
      const taxonomy = $filterSelect.data('taxonomy');
      const term_id = $filterSelect.val();

      if (term_id === '') {
        delete this.data.terms[taxonomy];
      } else {
        this.data.terms[taxonomy] = term_id;
      }

      // Reset the offset because we change filters
      this.data.offset = 0;

      const officeSpaces = this.getPosts();
      officeSpaces.success((response) => {
        this.$termFilterLinks.attr('disabled', false).parent().removeClass('loading');
        this.handleResponse(response, 0);
        this.manageInfinityScroll();
      });
    });
  }
}

const officeSpaces = () => {
  $('.layout-item.office_spaces').each((index, container) => {
    new officeSpacesManager(container);
  });
};

export default officeSpaces;
