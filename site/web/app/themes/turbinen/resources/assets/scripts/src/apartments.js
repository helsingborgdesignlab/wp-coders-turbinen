import 'waypoints/lib/jquery.waypoints';
import manageImgCover from '../util/imgCover';

class apartmentsManager {
  constructor(container) {
    this.$grid = $('.apartments-row', container);
    this.$apartmentsNav = $('.apartments-nav', container);
    this.$termFilterNav = $('.apartments-filters', container);
    this.$termFilterLinks = $('.apartments-filters-select', this.$termFilterNav);

    this.globals = window.turbinenGlobals;

    this.data = {
      action: 'turbinen_load_apartments',
      offset: 0,
      nonce: this.globals.nonce,
      terms: {},
      posts_per_page: this.$grid.data('posts-per-page'),
      no_results_message: this.$grid.data('no-results-message'),
      custom_link: this.$grid.data('custom-link'),
    };

    if( this.$grid.data('municipality') !== '' ) {
      this.data.terms['turb_apartment_municipality'] = this.$grid.data('municipality');
    }

    this.manageInfinityScroll();
    this.manageFilters();
  }

  // Send ajax request
  getPosts() {
    return $.ajax({
      method: 'POST',
      url: this.globals.ajax_url,
      dataType: 'json',
      data: this.data,
    });
  }

  // Handle responses
  handleResponse(response, slideDuration) {
    if (response.success) {
      const $gridItems = $('.hentry', this.$grid);
      const $elems = $(response.data.html).filter('.hentry').addClass('hentry-ajax');

      if (this.data.offset > 0) {
        // Load more
        $elems.insertAfter($gridItems.last()).hide().slideDown(slideDuration, () => {
          this.manageInfinityScroll();
        });
      } else {
        // Filter change
        if (parseInt(response.data.count, 10) > 0) {
          this.$grid.removeClass('empty');
        } else {
          this.$grid.addClass('empty');
        }

        this.$grid.html('');
        if (response.data.count === 0) {
          this.$grid.append(response.data.html).hide().slideDown(slideDuration);
        } else {
          this.$grid.append($elems).hide().slideDown(slideDuration);
        }
      }

      new manageImgCover({
        base: '.apartments-row',
        selector: '.hentry-ajax .img-cover',
        minWidth: 0,
      });

      $('.hentry').removeClass('hentry-ajax');

      if (this.$apartmentsNav.length) {
        if ($elems.length < this.apartments_per_page || $('.hentry', this.$grid).length >= parseInt(response.data.count, 10)) {
          this.$apartmentsNav.slideUp(slideDuration);
        } else {
          this.$apartmentsNav.slideDown(slideDuration);
        }
      }
    } else {
      if (this.$apartmentsNav.length) {
        this.$apartmentsNav.slideUp(slideDuration);
      }
    }
  }

  // Handle infinity scroll
  manageInfinityScroll() {
    if (this.$apartmentsNav.length) {
      window.Waypoint.destroyAll();

      this.$apartmentsNav.waypoint({
        handler: () => {
          if (!this.$apartmentsNav.hasClass('loading') && this.$apartmentsNav.is(':visible')) {
            this.$apartmentsNav.addClass('loading');
            this.data.offset = $('.hentry', this.$grid).length;

            const apartments = this.getPosts();
            apartments.success((response) => {
              this.handleResponse(response, 400);
              this.$apartmentsNav.removeClass('loading');
              window.Waypoint.destroyAll();
            });
          }
        },
        offset: 'bottom-in-view',
      });
    }
  }

  // Handle filter changes
  manageFilters() {
    this.$termFilterLinks.on('change', (e) => {
      e.preventDefault();

      const $filterSelect = $(e.currentTarget);

      // Set link classes
      this.$termFilterLinks.attr('disabled', true);
      $filterSelect.parent().addClass('loading');

      // Set current term
      const taxonomy = $filterSelect.data('taxonomy');
      const term_id = $filterSelect.val();

      if (term_id === '') {
        delete this.data.terms[taxonomy];
      } else {
        this.data.terms[taxonomy] = term_id;
      }

      // Reset the offset because we change filters
      this.data.offset = 0;

      const apartments = this.getPosts();
      apartments.success((response) => {
        this.$termFilterLinks.attr('disabled', false).parent().removeClass('loading');
        this.handleResponse(response, 0);
        this.manageInfinityScroll();
      });
    });
  }
}

const apartments = () => {
  $('.layout-item.apartments').each((index, container) => {
    new apartmentsManager(container);
  });
};

export default apartments;
