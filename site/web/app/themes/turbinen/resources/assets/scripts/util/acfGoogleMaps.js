class ACFGoogleMaps {
  constructor($el, resize = true, params = {}) {
    const google = window.google;
    this.$el = $el;
    this.resize = resize;

    const defaults = {
      zoom: 16,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      draggable: false,
      zoomControl: false,
      scrollwheel: false,
      streetViewControl: false,
      disableDoubleClickZoom: true,
      rotateControl: false,
      fullscreenControl: false,
      styles: [],
    };

    this.args = {
      ...defaults,
      ...params,
    };
  }

  renderMaps() {
    this.$el.each((index, element) => {
      this.renderMap($(element));
    });
  }

  renderMap($el) {
    const google = window.google;
    const $markers = $el.find('.marker');

    const map = new google.maps.Map($el[0], this.args);

    map.markers = [];

    $markers.each((index, marker) => {
      ACFGoogleMaps.addMarker($(marker), map);
    });

    this.centerMap(map);

    if (this.resize) {
      let windowWidth = $(window).width();

      $(window).on('resize', () => {
        if ($(window).width() !== windowWidth) {
          windowWidth = $(window).width();

          this.centerMap(map);
        }
      });
    }

    return map;
  }

  centerMap(map) {
    const google = window.google;
    const bounds = new google.maps.LatLngBounds();

    $.each(map.markers, (index, marker) => {
      bounds.extend(marker.getPosition());
    });

    if (map.markers.length === 1) {
      map.setCenter(bounds.getCenter());
      map.setZoom(this.args.zoom);
    } else {
      map.fitBounds(bounds);
    }
  }

  static addMarker($marker, map) {
    const google = window.google;
    const latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
    const text = $marker.html();

    const marker = new google.maps.Marker({
      position: latlng,
      map: map,
    });

    map.markers.push(marker);

    if (text) {
      const infowindow = new google.maps.InfoWindow({
        content: text,
      });

      google.maps.event.addListener(marker, 'click', () => {
        infowindow.open(map, marker);
      });
    }
  }
}

export default ACFGoogleMaps;
