import 'imagesloaded';
import Modernizr from 'modernizr';
import _ from 'lodash';

export class imgCover {
  constructor(conf) {
    this.defaults = {
      base: 'body',
      selector: '.img-cover',
      minWidth: 0,
      resize: true,
    };

    this.config = {
      ...this.defaults,
      ...conf,
    };

    this.refreshIt = _.debounce(this.refresh, 200);

    this.refresh();

    if (this.config.resize) {
      let windowWidth = $(window).width();

      $(window).on('resize', () => {
        if ($(window).width() !== windowWidth) {
          windowWidth = $(window).width();

          this.refreshIt();
        }
      });
    }
  }

  refresh() {
    $(this.config.selector, this.config.base).each((index, item) => {
      const $container = $(item);
      const $img = $('img', $container);
      const minWidth = parseInt($(item).data('min'), 10) || this.config.minWidth;

      $container.removeClass('img-cover-active-width');
      $container.removeClass('img-cover-active-height');

      if (Modernizr.mq(`(min-width: ${minWidth}px)`)) {
        $container.imagesLoaded(() => {
          $container.addClass('img-cover-calculating');

          if ($img.outerHeight() < $container.outerHeight()) {
            $container.removeClass('img-cover-calculating');
            $container.addClass('img-cover-active-height');
          } else {
            $container.removeClass('img-cover-calculating');
            $container.addClass('img-cover-active-width');
          }
        });
      }
    });
  }
}

export default imgCover;
