class splitContentManager {
  constructor() {
    this.$layouts = $('.layout-item.split_content');

    this.initSlider();
  }

  initSlider() {
    const $items = $('.split-item.slider', this.$layouts);

    $items.each((index, item) => {
      const $slider = $('.s-s-slides', item);

      $slider.slick({
        arrows: false,
        dots: true,
        slidesToScroll: 1,
        slidesToShow: 1,
        infinite: true,
        swipe: true,
        pauseOnHover: false,
        pauseOnFocus: false,
        draggable: true,
        autoplay: $slider.data('autoplay') === 'enabled',
        autoplaySpeed: $slider.data('interval'),
      });
    });
  }
}

const splitContent = () => {
  new splitContentManager();
};

export default splitContent;
