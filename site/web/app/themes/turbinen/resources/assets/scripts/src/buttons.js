import 'owl.carousel';

const buttons = () => {
  const $layouts = $('.layout-item.buttons');
  const globals = window.turbinenGlobals;

  $layouts.each((index, layout) => {
    const $carousel = $('.b-buttons', layout);
    const $items = $('.b-button-item', $carousel);

    $('.b-button.active', $items).on('click', (e) => {
      e.preventDefault();
    });

    $carousel.owlCarousel({
      autoWidth: true,
      items: $items.length,
      dots: true,
      nav: true,
      navText: [
        '<i class="turbinen-icon-chevron-left" aria-hidden="true"></i> <span class="sr-only">' + globals.t_prev + '</span>',
        '<i class="turbinen-icon-chevron-right" aria-hidden="true"></i> <span class="sr-only">' + globals.t_next + '</span>',
      ],
    });
  });
};

export default buttons;
