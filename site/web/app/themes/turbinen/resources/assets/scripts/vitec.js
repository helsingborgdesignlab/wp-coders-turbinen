import $ from 'jquery';

const Vitec = {
  initImport: () => {
    const globals = window.vitecGlobals;

    $('.vitec-import-button').click((e) => {
      e.preventDefault();

      const $btn = $(e.currentTarget);

      if (confirm(globals.confirm)) {
        const btnValue = $btn.val();

        $btn.val(globals.loading).attr('disabled', true);

        $.ajax({
          type: 'POST',
          url: globals.ajax_url,
          data: {
            action: 'vitec_ajax_import',
            nonce: globals.ajax_nonce,
            post_type: $btn.data('post_type'),
          },
          success: (response) => {
            if (response.success) {
              $btn.val(btnValue).attr('disabled', false);
            }
          },
        });
      }
    });
  },
};

$(document).ready(() => {
  Vitec.initImport();
});
