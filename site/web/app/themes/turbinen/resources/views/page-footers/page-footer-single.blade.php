@php
  $id = apply_filters( 'post_footer_layouts_id', wpc_get_the_id( true ) );
  $field = apply_filters( 'post_footer_layouts_field', wpc_get_the_post_type() . '_footer_layouts', $id );
@endphp

@if( have_rows( $field, $id ) )
  <div class="page-footer">
    @while( have_rows( $field, $id ) ) @php the_row() @endphp
      @include( 'flexibles.' . get_row_layout() )
    @endwhile
  </div>
@endif
