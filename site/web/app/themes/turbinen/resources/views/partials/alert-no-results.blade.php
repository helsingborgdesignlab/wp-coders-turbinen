<div class="alert alert-warning mb-0">
  {{ empty( $fields['message'] ) ? __( 'Sorry, no results were found.', 'turbinen' ) : $message }}
</div>
