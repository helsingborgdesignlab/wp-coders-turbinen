<footer class="content-info">
  @if( ! empty( $overlay = get_field( 'footer_overlay', 'option' ) ) )
    <figure class="ci-overlay mb-0">
      <img src="{{ $overlay['url'] }}" alt="{{ $overlay['alt'] }}">
    </figure>
  @endif

  @if( have_rows( 'footer_widgets', 'option' ) )
    <div class="ci-container container-conditional">
      <div class="ci-widgets row">
        @while( have_rows( 'footer_widgets', 'option' ) ) @php the_row() @endphp
          @include( 'flexibles.footer-widgets.' . get_row_layout() )
        @endwhile
      </div>
    </div>
  @endif
</footer>
