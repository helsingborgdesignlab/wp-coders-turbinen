@php
  $id = apply_filters( 'post_content_layouts_id', wpc_get_the_id() );
  $field = apply_filters( 'post_content_layouts_field', wpc_get_the_post_type() . '_layouts', $id );
@endphp

@if( have_rows( $field, $id ) )
  <article @php post_class() @endphp>
    @while( have_rows( $field, $id ) ) @php the_row() @endphp
      @include( 'flexibles.' . get_row_layout() )
    @endwhile
  </article>
@endif
