@php
  $fields = [
    'thumbnail' => get_field( 'thumbnail' ),
    'location' => get_field( 'location' ),
    'status' => get_field( 'status' ),
    'excerpt' => get_field( 'excerpt' ),
    'details' => get_field( 'details' ),
    'custom_link' => ! empty( $custom_link ) ? $custom_link : get_field( 'custom_link' ),
    'read_more_text' => ! empty( $read_more_text ) ? $read_more_text : __( 'More information', 'turbinen' ),
  ];

  $classes = [
    'layout-item',
    'split_content',
    'order-reversed',
    'gutter-disabled',
    'min-height-default',
  ];

  $inner_classes = [
    'layout-item-inner',
    'layout-item-row',
    'row',
  ];

  $column_classes = [
    'layout-item-col',
    'col-md-6'
  ];
@endphp

<article @php post_class( implode( ' ', $classes ) ) @endphp>
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <div class="{{ implode( ' ', $column_classes ) }}">
      @include( 'flexibles.split-content-layouts.templates.building', [
        'fields' => [
          'layout_id' => '',
          'title' => get_the_title(),
          'sub_title' => $fields['location'],
          'lead' => $fields['status'],
          'text' => $fields['excerpt'],
          'details' => $fields['details'],
          'button' => [
            'link_type' => 'link',
            'link_link' => [
              'url' => ! empty( $fields['custom_link']['url'] ) ? $fields['custom_link']['url'] : get_the_permalink(),
              'title' => ! empty( $fields['custom_link']['title'] ) ? $fields['custom_link']['title'] : $fields['read_more_text'],
              'target' => ! empty( $fields['custom_link']['target'] ) ? $fields['custom_link']['target'] : '_self',
            ]
          ],
        ],
        'classes' => [
          'split-item',
          'building',
        ],
        'inner_classes' => [
          'split-item-inner',
          'bg-ns-light',
          'pt-default',
          'pb-default',
        ],
      ] )
    </div>

    <div class="{{ implode( ' ', $column_classes ) }}">
      @include( 'flexibles.split-content-layouts.templates.image', [
        'fields' => [
          'layout_id' => '',
          'image' => $fields['thumbnail'],
        ],
        'classes' => [
          'split-item',
          'image',
        ],
        'inner_classes' => [
          'split-item-inner',
          'mobile-min-height',
        ],
      ] )
    </div>
  </div>
</article>
