@php
  $fields = [
    'banner_title' => ! empty( get_field( 'banner_title' ) ) ? get_field( 'banner_title' ) : \App\title(),
    'banner_image' => ! empty( get_field( 'banner_image' ) ) ? get_field( 'banner_image' ) : get_field( 'thumbnail' ),
    'intro_sub_title' => get_field( 'intro_sub_title' ),
    'intro_text' => get_field( 'intro_text' ),
    'intro_facts' => get_field( 'intro_facts' ),
    'files_documents' => get_field( 'files_documents' ),
    'files_images' => get_field( 'files_images' ),
    'description_left_list' => get_field( 'description_left_list' ),
    'description_right_list' => get_field( 'description_right_list' ),
    'google_map_lat' => get_field( 'google_map_lat' ),
    'google_map_lng' => get_field( 'google_map_lng' ),
    'profile_picture' => get_field( 'profile_picture' ),
    'profile_name' => get_field( 'profile_name' ),
    'profile_position' => get_field( 'profile_position' ),
    'profile_phone' => get_field( 'profile_phone' ),
    'profile_email' => get_field( 'profile_email' ),
  ];

  $split_classes = [
    'layout-item',
    'split_content',
    'order-default',
    'gutter-disabled',
    'min-height-none',
  ];

  $split_inner_classes = [
    'layout-item-inner',
    'layout-item-row',
    'row',
  ];

  $split_column_classes = [
    'layout-item-col',
    'col-md-6'
  ];
@endphp

<article @php post_class() @endphp>

  @include( 'flexibles.templates.hero_slider', [
    'fields' => [
      'layout_id' => '',
      'slides' => [ [
        'title' => $fields['banner_title'],
        'image' => $fields['banner_image'],
        'style' => 'ns',
      ] ],
    ],
    'classes' => [
      'layout-item',
      'banner',
      'height-default',
      'full-screen-disabled',
    ],
    'inner_classes' => [
      'layout-item-inner',
    ],
  ] )

  @include( 'flexibles.templates.separator', [
    'fields' => [
      'layout_id' => '',
    ],
    'classes' => [
      'layout-item',
      'separator',
      'height-default',
    ],
    'inner_classes' => [
      'layout-item-inner',
    ],
  ] )

  <div class="{{ implode( ' ', $split_classes ) }}">
    <div class="{{ implode( ' ', $split_inner_classes ) }}">
      <div class="{{ implode( ' ', $split_column_classes ) }}">
        @include( 'flexibles.split-content-layouts.templates.building', [
          'fields' => [
            'layout_id' => '',
              'title' => 'Beskrivning',
              'sub_title' => $fields['intro_sub_title'],
              'text' => $fields['intro_text'],
          ],
          'classes' => [
            'split-item',
            'building',
          ],
          'inner_classes' => [
            'split-item-inner',
            'bg-ns-light',
            'pt-default',
            'pb-default',
          ],
        ] )
      </div>

      <div class="{{ implode( ' ', $split_column_classes ) }}">
        @php
        if ( ! empty( $fields['files_documents'] ) ) {
          foreach ( $fields['files_documents'] as $document ) {
            $fields['intro_facts'][] = [
              'title' => 'Dokument',
              'text' => '<a href="' . $document['url'] . '" target="_blank">' . $document['filename'] . '</a>',
            ];
          }
        }
        @endphp

        @include( 'flexibles.split-content-layouts.templates.building', [
          'fields' => [
            'layout_id' => '',
            'details_title' => 'Fakta',
            'details' => $fields['intro_facts'],
          ],
          'classes' => [
            'split-item',
            'building',
          ],
          'inner_classes' => [
            'split-item-inner',
            'bg-ns-light',
            'pt-md-6',
            'pb-default',
          ],
        ] )
      </div>
    </div>
  </div>

  @if( ! empty( $fields['files_images'] ) )
    @include( 'flexibles.templates.separator', [
      'fields' => [
        'layout_id' => '',
      ],
      'classes' => [
        'layout-item',
        'separator',
        'height-default',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ],
    ] )

    @include( 'flexibles.templates.content_block', [
      'fields' => [
        'layout_id' => '',
        'title' => 'Bilder',
        'max_width' => 1050,
      ],
      'classes' => [
        'layout-item',
        'content_block',
        'has-bg',
      ],
      'inner_classes' => [
        'layout-item-inner',
        'bg-ns',
        'pt-xs',
        'pb-xs',
      ],
    ] )

    @php
      $images = [];

      if ( ! empty( $fields['files_images'] ) ) {
        foreach ( $fields['files_images'] as $image ) {
          $images[] = [
            'image' => $image,
            'link' => [],
          ];
        }
      }
    @endphp

    @include( 'flexibles.templates.gallery', [
      'fields' => [
        'layout_id' => '',
        'images' => $images,
        'load_more_text' => __( 'Load More', 'turbinen' ),
        'images_per_page' => 4,
      ],
      'classes' => [
        'layout-item',
        'gallery',
        'split_content',
        'order-default',
        'gutter-enabled',
        'min-height-default',
      ],
      'inner_classes' => [
        'layout-item-inner',
        'layout-item-row',
        'row',
      ],
      'column_classes' => [
        'layout-item-col',
        'col-md-6'
      ],
    ] )
  @endif

  @if( ! empty( $fields['description_left_list'] ) || ( ! empty( 'description_right_list' ) ) )
    @include( 'flexibles.templates.separator', [
      'fields' => [
        'layout_id' => '',
      ],
      'classes' => [
        'layout-item',
        'separator',
        'height-default',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ],
    ] )

    @php
      $description_left_list = [];

      if( ! empty( $fields['description_left_list'] ) ) {
        foreach( $fields['description_left_list'] as $key => $list_item ) {
          $description_left_list[] = [
            'title' => $key === 0 ? '<div class="pb-2" style="font-weight: 500">Fastighetsbeskrivning</div>' : '',
            'text' => '<strong>' . $list_item['title'] . '</strong><br>' . $list_item['text'],
          ];
        }
      }

      $description_right_list = [];

      if( ! empty( $fields['description_right_list'] ) ) {
        foreach( $fields['description_right_list'] as $key => $list_item ) {
          $description_right_list[] = [
            'title' => $key === 0 ? '<div class="d-none d-md-block pb-2">&nbsp;</div>' : '',
            'text' => '<strong>' . $list_item['title'] . '</strong><br>' . $list_item['text'],
          ];
        }
      }
    @endphp

    <div class="{{ implode( ' ', $split_classes ) }}">
      <div class="{{ implode( ' ', $split_inner_classes ) }}">
        <div class="{{ implode( ' ', $split_column_classes ) }}">
          @include( 'flexibles.split-content-layouts.templates.content_blocks', [
            'fields' => [
              'layout_id' => '',
              'content_blocks' => $description_left_list,
            ],
            'classes' => [
              'split-item',
              'content_blocks',
              'vertical-position-top',
              'content-blocks-offset-small',
              'has-bg'
            ],
            'inner_classes' => [
              'split-item-inner',
              'bg-ns',
              'pt-default',
              'pb-md-6',
            ],
          ] )
        </div>

        <div class="{{ implode( ' ', $split_column_classes ) }}">
          @include( 'flexibles.split-content-layouts.templates.content_blocks', [
            'fields' => [
              'layout_id' => '',
              'content_blocks' => $description_right_list,
            ],
            'classes' => [
              'split-item',
              'content_blocks',
              'vertical-position-top',
              'content-blocks-offset-small',
              'has-bg'
            ],
            'inner_classes' => [
              'split-item-inner',
              'bg-ns',
              'pt-md-6',
              'pb-default',
            ],
          ] )
        </div>
      </div>
    </div>
  @endif

  @if( ! empty( $fields['google_map_lat'] ) && ! empty( $fields['google_map_lng'] ) )
    @include( 'flexibles.templates.separator', [
      'fields' => [
        'layout_id' => '',
      ],
      'classes' => [
        'layout-item',
        'separator',
        'height-default',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ],
    ] )

    @include( 'flexibles.templates.google_map', [
      'fields' => [
        'layout_id' => '',
        'markers' => [ [
          'marker' => [
            'lat' => $fields['google_map_lat'],
            'lng' => $fields['google_map_lng'],
          ],
        ] ],
      ],
      'classes' => [
        'layout-item',
        'google_map',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ]
    ] )
  @endif

  @if( ! empty( $fields['profile_picture'] ) || ! empty( $fields['profile_name'] ) || ! empty( $fields['profile_position'] ) || ! empty( $fields['profile_phone'] ) || ! empty( $fields['profile_email'] ) )
    @include( 'flexibles.templates.separator', [
      'fields' => [
        'layout_id' => '',
      ],
      'classes' => [
        'layout-item',
        'separator',
        'height-default',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ],
    ] )

    @include( 'flexibles.templates.profile', [
      'fields' => [
        'layout_id' => '',
        'picture' => $fields['profile_picture'],
        'name' => $fields['profile_name'],
        'position' => $fields['profile_position'],
        'phone' => $fields['profile_phone'],
        'email' => $fields['profile_email'],
      ],
      'classes' => [
        'layout-item',
        'profile',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ]
    ] )
  @endif

</article>
