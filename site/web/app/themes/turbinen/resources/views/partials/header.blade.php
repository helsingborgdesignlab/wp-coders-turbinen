@php
  $fields = [
    'social_media_links' => get_field( 'social_media_links', 'option' )
  ];
@endphp

<header class="header-wrap">
  <div class="header-dropdown-wrap">
    <div class="header-dropdown">
      <div class="header-dropdown-inner">
        @if ( has_nav_menu( 'primary_navigation' ) )
          <nav class="nav-header-dropdown">
            {!! wp_nav_menu( [
              'theme_location' => 'primary_navigation',
              'menu_class' => 'nav'
            ] ) !!}
          </nav>
        @endif

        <ul class="header-dropdown-links">
          <li>
            <a href="#" class="search-toggle" title="{{ __( 'Search', 'turbinen' ) }}" data-toggle="search-form">
              <i class="turbinen-icon-search" aria-hidden="true"></i>
              <span class="sr-only">{{ __( 'Search', 'turbinen' ) }}</span>
            </a>
          </li>

          @if( ! empty( $fields['social_media_links'] ) )
            @foreach( $fields['social_media_links'] as $social_media_link  )
              @if( ! empty( $social_media_link['url'] ) && ! empty( $social_media_link['icon'] ) )
                <li>
                  <a href="{{ $social_media_link['url'] }}" title="{{ $social_media_link['title'] }}" target="_blank" rel="nofollow">
                    <i class="{{ $social_media_link['icon'] }}" aria-hidden="true"></i>

                    @if( ! empty( $social_media_link['title'] ) )
                      <span class="sr-only">{{ $social_media_link['title'] }}</span>
                    @endif
                  </a>
                </li>
              @endif
            @endforeach
          @endif
        </ul>
      </div>
    </div>
  </div>

  <div class="header">
    <div class="header-helper"></div>
    <div class="header-inner">
      <a class="brand" href="{{ home_url( '/' ) }}" title="{{ get_bloginfo( 'name', 'display' ) }}">
        {!! wpc_get_the_svg_logo() !!}
      </a>

      @if ( has_nav_menu( 'primary_navigation' ) )
        <nav class="nav-primary">
          {!! wp_nav_menu( [
            'theme_location' => 'primary_navigation',
            'menu_class' => 'nav'
          ] ) !!}
        </nav>
      @endif

      <div class="header-right">
        <ul class="header-right-links">
          <li>
            <a href="#" class="search-toggle" title="{{ __( 'Search', 'turbinen' ) }}" data-toggle="search-form">
              <i class="turbinen-icon-search" aria-hidden="true"></i>
              <span class="sr-only">{{ __( 'Search', 'turbinen' ) }}</span>
            </a>
          </li>

          @if( ! empty( $fields['social_media_links'] ) )
            @foreach( $fields['social_media_links'] as $social_media_link  )
              @if( ! empty( $social_media_link['url'] ) && ! empty( $social_media_link['icon'] ) )
                <li>
                  <a href="{{ $social_media_link['url'] }}" title="{{ $social_media_link['title'] }}" target="_blank" rel="nofollow">
                    <i class="{{ $social_media_link['icon'] }}" aria-hidden="true"></i>

                    @if( ! empty( $social_media_link['title'] ) )
                      <span class="sr-only">{{ $social_media_link['title'] }}</span>
                    @endif
                  </a>
                </li>
              @endif
            @endforeach
          @endif
        </ul>

        @if ( has_nav_menu( 'secondary_navigation' ) )
          <nav class="nav-secondary">
            {!! wp_nav_menu( [
              'theme_location' => 'secondary_navigation',
              'menu_class' => 'nav'
            ] ) !!}
          </nav>
        @endif

        <a href="#" class="nav-toggle dropdown-nav-toggle" title="{{ __( 'Menu', 'turbinen' ) }}" data-toggle="header-dropdown">
          <i aria-hidden="true"></i>
          <span class="sr-only">{{ __( 'Menu', 'turbinen' ) }}</span>
        </a>
      </div>

      <form class="search-form form-inline" action="{{ esc_url( home_url( '/' ) ) }}" role="form" method="get">
        <label for="search-form-field" class="sr-only">{{ __( 'Search for...', 'turbinen' ) }}</label>

        <input type="search" class="search-form-field form-control" placeholder="{{ __( 'Search for...', 'turbinen' ) }}"
               value="{{ get_search_query() }}" name="s" id="search-form-field" required="required">

        <button type="button" class="search-form-close" data-toggle="search-form">
          <span class="sr-only">{{ __( 'Close search', 'turbinen' ) }}</span>
          <i class="turbinen-icon-cross"></i>
        </button>
      </form>
    </div>
  </div>
</header>
