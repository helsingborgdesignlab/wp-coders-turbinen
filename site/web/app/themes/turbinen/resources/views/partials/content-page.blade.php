@php
  $id = apply_filters( 'page_content_layouts_id', wpc_get_the_id() );
  $field = apply_filters( 'page_content_layouts_field', 'page_content_layouts', $id );
@endphp

@if( have_rows( $field, $id ) )
  @while( have_rows( $field, $id ) ) @php the_row() @endphp
    @include( 'flexibles.' . get_row_layout() )
  @endwhile
@endif
