@php
  global $post;

  $post_ids = get_posts( [
    'post_type' => wpc_get_the_post_type(),
    'posts_per_page' => -1,
    'fields' => 'ids',
  ] );

  $prev_next_ids   = [];
  $current         = array_search( $post->ID, $post_ids );
  $prev_next_ids[] = isset( $post_ids[ $current + 1 ] ) ? $post_ids[ $current + 1 ] : $post_ids[0];
  $prev_next_ids[] = isset( $post_ids[ $current - 1 ] ) ? $post_ids[ $current - 1 ] : $post_ids[ count( $post_ids ) - 1 ];

  $nav_items = get_posts( [
    'post_type' => wpc_get_the_post_type(),
    'post__in'  => $prev_next_ids,
    'posts_per_page' => 2,
  ] );
@endphp

@if( ! empty( $nav_items ) && ( count( $nav_items ) > 1 ) )
  <section class="layout-item post-navigation">
    <div class="layout-item-inner bg-ns-medium pt-xs pb-xs">
      <div class="container">
        <nav class="nav-single">
          <ul class="nav">
            <li>
              <a href="{{ get_the_permalink( $nav_items[0]->ID ) }}" title="{{ __( 'Previous', 'turbinen' ) }}"
                 class="btn btn-reversed btn-white">

                {{ __( 'Previous', 'turbinen' ) }}
              </a>
            </li>

            <li>
              <a href="{{ get_the_permalink( $nav_items[1]->ID ) }}" title="{{ __( 'Next', 'turbinen' ) }}"
                 class="btn btn-white">

                {{ __( 'Next', 'turbinen' ) }}
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </section>
@endif
