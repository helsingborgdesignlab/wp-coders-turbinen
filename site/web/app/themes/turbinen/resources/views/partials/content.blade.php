@php
  $fields = [
    'thumbnail' => get_field( 'thumbnail' ),
    'excerpt' => get_field( 'excerpt' ),
    'read_more_text' => ! empty( $read_more_text ) ? $read_more_text : __( 'Read more', 'turbinen' ),
  ];

  $classes = [
    'col-md-6',
    'col-xxl-4',
    empty( $fields['thumbnail'] ) ? 'no-thumbnail' : 'has-thumbnail'
  ];
@endphp

<article @php post_class( implode( ' ', $classes ) ) @endphp>
  @if( empty( $fields['thumbnail'] ) )
    <div class="entry-inner">
      <div class="entry-header">
        <h2 class="entry-title mb-0">
          <a href="{{ get_permalink() }}" title="{{ get_the_title() }}">
            {!! get_the_title() !!}
          </a>
        </h2>
      </div>

      @if( ! empty( $fields['excerpt'] ) )
        <div class="entry-summary rlpm">
          {!! $fields['excerpt'] !!}
        </div>
      @endif

      <div class="entry-footer">
        <time class="entry-updated" datetime="{{ get_post_time( 'c', true ) }}">{{ get_the_date() }}</time>

        <div class="entry-button">
          <a href="{{ get_the_permalink() }}" class="btn btn-sm btn-light" title="{{ $fields['read_more_text'] }}">
            {!! $fields['read_more_text'] !!}
          </a>
        </div>
      </div>
    </div>
  @else
    <div class="entry-inner">
      <div class="entry-header">
        <a href="{{ get_permalink() }}" class="entry-thumbnail-link" title="{{ get_the_title() }}">
          <figure class="entry-thumbnail img-cover mb-0">
            {!! \App\get_responsive_attachment( $fields['thumbnail']['id'], 'turbinen-thumbnail-md' ) !!}
          </figure>
        </a>

        <time class="entry-updated" datetime="{{ get_post_time( 'c', true ) }}">{{ get_the_date() }}</time>
      </div>

      <div class="entry-footer">
        <h2 class="entry-title mb-0">
          <a href="{{ get_permalink() }}" title="{{ get_the_title() }}">
            {!! get_the_title() !!}
          </a>
        </h2>

        <div class="entry-button">
          <a href="{{ get_the_permalink() }}" class="btn btn-light" title="{{ $fields['read_more_text'] }}">
            {!! $fields['read_more_text'] !!}
          </a>
        </div>
      </div>
    </div>
  @endif
</article>
