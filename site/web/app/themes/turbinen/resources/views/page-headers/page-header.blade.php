@php
  $id = apply_filters( 'header_layouts_id', wpc_get_the_id() );
  $field = apply_filters( 'header_layouts_field', 'page_header_layouts', $id );
@endphp

@if( have_rows( $field, $id ) )
  <div class="page-header">
    @while( have_rows( $field, $id ) ) @php the_row() @endphp
      @include( 'flexibles.' . get_row_layout() )
    @endwhile
  </div>
@endif
