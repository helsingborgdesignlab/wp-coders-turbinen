@if( have_rows( 'image' ) )
  @while( have_rows( 'image' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'image' . '-' ),
      'image' => get_sub_field( 'image' ),
      'link' => get_sub_field( 'link' ),
      'container' => ! empty( get_sub_field( 'container' ) ) ? get_sub_field( 'container' ) : 'enabled',
      'image_crop' => ! empty( get_sub_field( 'image_crop' ) ) ? get_sub_field( 'image_crop' ) : 'disabled',
      'height' => ! empty( get_sub_field( 'height' ) ) ? get_sub_field( 'height' ) : 'default',
    ];

    $classes = [
      'layout-item',
      'image',
      'crop-' . $fields['image_crop'],
      'height-' . $fields['height'],
    ];

    $inner_classes = [
      'layout-item-inner',
      $fields['container'] === 'enabled' ? 'container' : '',
    ];
  @endphp

  @include( 'flexibles.templates.image', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
