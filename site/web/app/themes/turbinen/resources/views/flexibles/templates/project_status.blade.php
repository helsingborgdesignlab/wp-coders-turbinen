@if( ! empty( $fields['items'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="ps-wrap container-conditional">
        <div class="ps-container">
          @if( ! empty( $fields['title'] ) )
            <h3 class="ps-title layout-title mb-0 pb-sm">
              {!! $fields['title'] !!}
            </h3>
          @endif

          <div class="ps-inner container">
            <ul class="ps-items slick-dots-ns">
              @foreach( $fields['items'] as $item )
                <li class="ps-item">
                <span class="ps-item-circle {{ $item['active'] ? 'bg-ns' : '' }}">
                  @if( $item['checked'] )
                    <i class="turbinen-icon-tick" aria-hidden="true"></i>
                  @endif
                </span>

                  @if( ! empty( $item['title'] ) )
                    <h5 class="ps-item-title mb-0">
                      {!! $item['title'] !!}
                    </h5>
                  @endif
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
@endif
