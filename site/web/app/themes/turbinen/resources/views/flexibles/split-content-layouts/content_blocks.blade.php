@if( have_rows( 'content_blocks' ) )
  @while( have_rows( 'content_blocks' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'content_blocks' . '-' ),
      'content_blocks' => get_sub_field( 'content_blocks' ),
      'vertical_position' => ! empty( get_sub_field( 'vertical_position' ) ) ? get_sub_field( 'vertical_position' ) : 'center',
      'padding' => get_sub_field( 'content_blocks_padding' ),
    ];

    $classes = [
      'split-item',
      'content_blocks',
      'vertical-position-' . $fields['vertical_position'],
      'content-blocks-offset-default',
    ];

    $inner_classes = [
      'split-item-inner',
      'bg-ns-light',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.split-content-layouts.templates.content_blocks', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
