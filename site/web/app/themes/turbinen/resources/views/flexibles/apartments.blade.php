@if( have_rows( 'apartments' ) )
  @while( have_rows( 'apartments' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'apartments' . '-' ),
      'municipalities_label' => ! empty( get_sub_field( 'municipalities_label' ) ) ? get_sub_field( 'municipalities_label' ) : __( 'Municipalities', 'turbinen' ),
      'read_more_link' => get_sub_field( 'read_more_link' ),
      'no_results_message' => get_sub_field( 'no_results_message' ),
      'preselected_municipality' => get_sub_field( 'preselected_municipality' ),
      'apartments_per_page' => ! empty( get_sub_field( 'apartments_per_page' ) ) ? get_sub_field( 'apartments_per_page' ) : 6,
      'infinity_scroll' => ! empty( get_sub_field( 'infinity_scroll' ) ) ? get_sub_field( 'infinity_scroll' ) : 'enabled',
    ];

    $classes = [
      'layout-item',
      'apartments',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.apartments', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
