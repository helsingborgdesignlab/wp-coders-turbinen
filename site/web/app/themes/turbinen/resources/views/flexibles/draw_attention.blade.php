@if( have_rows( 'draw_attention' ) )
  @while( have_rows( 'draw_attention' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'draw_attention' . '-' ),
      'image' => get_sub_field( 'image' ),
      'background' => ! empty( get_sub_field( 'background' ) ) ? get_sub_field( 'background' ) : 'transparent',
    ];

    $classes = [
      'layout-item',
      'draw_attention',
    ];

    if( \App\layout_has_bg( $fields['background'] ) ) {
      $classes[] = 'has-bg';
    }

    $inner_classes = [
      'layout-item-inner',
      'bg-' . $fields['background'],
    ];
  @endphp

  @include( 'flexibles.templates.draw_attention', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
