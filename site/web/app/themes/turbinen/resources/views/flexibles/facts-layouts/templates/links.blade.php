@if( ! empty( $fields['links'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      @if( ! empty( $fields['title'] ) )
        <h4 class="facts-item-title">
          {!! $fields['title'] !!}
        </h4>
      @endif

      <ul>
        @foreach( $fields['links'] as $link )
          @if( ! empty( $link['link'] ) )
            <li>
              <a href="{{ $link['link']['url'] }}" title="{{ $link['link']['title'] }}" target="{{ $link['link']['target'] }}">
                {!! $link['link']['title'] !!}
              </a>
            </li>
          @endif
        @endforeach
      </ul>
    </div>
  </section>
@endif
