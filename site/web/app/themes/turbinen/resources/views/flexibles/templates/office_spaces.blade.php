@php
  global $wp_query;

  $areas = get_terms( [
    'taxonomy' => 'turb_office_space_area',
    'hide_empty' => false,
  ] );

  $types = get_terms( [
    'taxonomy' => 'turb_office_space_type',
    'hide_empty' => false,
  ] );

  $args = [
    'post_type' => 'turb_office_space',
    'posts_per_page' => $fields['office_spaces_per_page'],
  ];

  if ( ! empty( $fields['preselected_area'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_office_space_area',
      'field'    => 'term_id',
      'terms'    => [ $fields['preselected_area'] ],
    ];
  }

  if ( ! empty( $fields['preselected_type'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_house_type',
      'field'    => 'term_id',
      'terms'    => [ $fields['turb_office_space_type'] ],
    ];
  }

  $wp_query = null;
	$wp_query = new \WP_Query( $args );
@endphp

<section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <form class="offices-filters bg-ns-medium pt-xs pb-xs">
      <div class="offices-filters-container container">
        <div class="offices-filters-row form-row">
          <div class="offices-filters-col col-md-6 col-lg-4">
            <label for="{{ $fields['layout_id'] }}-filter-areas">
              {!! $fields['areas_label'] !!}:
            </label>

            <div class="offices-filters-select-wrap custom-select-wrap">
              <select class="offices-filters-select custom-select" id="{{ $fields['layout_id'] }}-filter-areas"
                      data-taxonomy="turb_office_space_area">

                <option value="" {{ empty( $fields['preselected_area'] ) ? 'selected' : '' }}>
                  {{ __( 'All', 'turbinen' ) }}
                </option>

                @if( ! empty( $areas ) )
                  @foreach( $areas as $area )
                    <option value="{{ $area->term_id }}" {{ $fields['preselected_area'] == $area->term_id ? 'selected' : '' }}>
                      {!! $area->name !!}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>

          <div class="offices-filters-col col-md-6 col-lg-4">
            <label for="{{ $fields['layout_id'] }}-filter-types">
              {!! $fields['types_label'] !!}:
            </label>

            <div class="offices-filters-select-wrap custom-select-wrap">
              <select class="offices-filters-select custom-select" id="{{ $fields['layout_id'] }}-filter-types"
                      data-taxonomy="turb_office_space_type">

                <option value="" {{ empty( $fields['preselected_type'] ) ? 'selected' : '' }}>
                  {{ __( 'All', 'turbinen' ) }}
                </option>

                @if( ! empty( $types ) )
                  @foreach( $types as $type )
                    <option value="{{ $type->term_id }}" {{ $fields['preselected_type'] == $type->term_id ? 'selected' : '' }}>
                      {!! $type->name !!}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
      </div>
    </form>

    <div class="offices-container">
      <div class="offices-row {{ have_posts() ? '' : 'empty' }}" data-posts-per-page="{{ $fields['office_spaces_per_page'] }}"
           data-area="{{ empty( $fields['preselected_area'] ) ? '' : $fields['preselected_area'] }}"
           data-type="{{ empty( $fields['preselected_type'] ) ? '' : $fields['preselected_type'] }}"
           data-no-results-message="{{ $fields['no_results_message'] }}" data-read-more-text="{{ $fields['read_more_text'] }}">

        @if ( ! have_posts() )
          @include( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] )
        @else
          @while ( have_posts() ) @php the_post() @endphp
            @include( 'partials.content-' . get_post_type(), [ 'read_more_text' => $fields['read_more_text'] ] )
          @endwhile
        @endif
      </div>

      @if( $fields['infinity_scroll'] === 'enabled' )
        <div class="offices-nav pt-sm" {!! ( $wp_query->max_num_pages > 1 ) ? '' : 'style="display:none"' !!}>
          <i class="turbinen-icon-spinner" aria-hidden="true"></i>
        </div>
      @endif
    </div>
  </div>
</section>

@php
  wp_reset_query();
@endphp
