@if( have_rows( 'members' ) )
  @while( have_rows( 'members' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'members' . '-' ),
      'members' => get_sub_field( 'members' ),
      'padding' => get_sub_field( 'members_padding' ),
    ];

    $classes = [
      'layout-item',
      'members',
    ];

    $inner_classes = [
      'layout-item-inner',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.templates.members', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
