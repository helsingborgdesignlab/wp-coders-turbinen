@if( have_rows( 'gallery' ) )
  @while( have_rows( 'gallery' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'gallery' . '-' ),
      'images' => get_sub_field( 'images' ),
      'load_more_text' => ! empty( get_sub_field( 'load_more_text' ) ) ? get_sub_field( 'load_more_text' ) : __( 'Load More', 'turbinen' ),
      'images_per_page' => ! empty( get_sub_field( 'images_per_page' ) ) ? get_sub_field( 'images_per_page' ) : 4,
    ];

    $classes = [
      'layout-item',
      'gallery',
      'split_content',
      'order-default',
      'gutter-enabled',
      'min-height-default',
    ];

    $inner_classes = [
      'layout-item-inner',
      'layout-item-row',
      'row',
    ];

    $column_classes = [
      'layout-item-col',
      'col-md-6'
    ];
  @endphp

  @include( 'flexibles.templates.gallery', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
    'column_classes' => $column_classes,
  ] )

  @endwhile
@endif
