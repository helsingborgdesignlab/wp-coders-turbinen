@if( have_rows( 'google_map' ) )
  @while( have_rows( 'google_map' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'google_map' . '-' ),
      'markers' => get_sub_field( 'markers' ),
    ];

    $classes = [
      'layout-item',
      'google_map',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.google_map', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif

