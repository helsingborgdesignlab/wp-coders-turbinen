@if( have_rows( 'links' ) )
  @while( have_rows( 'links' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'links' . '-' ),
      'title' => get_sub_field( 'title' ),
      'links' => get_sub_field( 'links' ),
    ];

    $classes = [
      'facts-item',
      'links',
    ];

    $inner_classes = [
      'facts-item-inner',
    ];
  @endphp

  @include( 'flexibles.facts-layouts.templates.links', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
