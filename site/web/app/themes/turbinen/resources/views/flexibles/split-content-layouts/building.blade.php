@if( have_rows( 'building' ) )
  @while( have_rows( 'building' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'building' . '-' ),
      'title' => get_sub_field( 'title' ),
      'sub_title' => get_sub_field( 'sub_title' ),
      'lead' => get_sub_field( 'lead' ),
      'text' => get_sub_field( 'text' ),
      'details_title' => get_sub_field( 'details_title' ),
      'details' => get_sub_field( 'details' ),
      'button' => get_sub_field( 'building_button' ),
      'padding' => get_sub_field( 'building_padding' ),
    ];

    $classes = [
      'split-item',
      'building',
    ];

    $inner_classes = [
      'split-item-inner',
      'bg-ns-light',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.split-content-layouts.templates.building', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
