@if( have_rows( 'content_block' ) )
  @while( have_rows( 'content_block' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'content_block' . '-' ),
      'title' => get_sub_field( 'title' ),
      'text' => get_sub_field( 'text' ),
      'buttons' => get_sub_field( 'buttons' ),
      'max_width' => ! empty( get_sub_field( 'max_width' ) ) ? get_sub_field( 'max_width' ) : 1050,
      'background' => ! empty( get_sub_field( 'background' ) ) ? get_sub_field( 'background' ) : 'transparent',
      'padding' => get_sub_field( 'content_block_padding' ),
    ];

    $classes = [
      'layout-item',
      'content_block',
    ];

    if( \App\layout_has_bg( $fields['background'] ) ) {
      $classes[] = 'has-bg';
    }

    $inner_classes = [
      'layout-item-inner',
      'bg-' . $fields['background'],
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.templates.content_block', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif

