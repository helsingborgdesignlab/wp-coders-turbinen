@if( ! empty( $fields['lead'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="lead-text rlpm">
      {!! $fields['lead'] !!}
    </div>
  </section>
@endif
