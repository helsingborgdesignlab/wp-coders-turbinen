@if( have_rows( 'table' ) )
  @while( have_rows( 'table' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'table' . '-' ),
      'title' => get_sub_field( 'title' ),
      'table' => get_sub_field( 'table' ),
    ];

    $classes = [
      'facts-item',
      'table',
    ];

    $inner_classes = [
      'facts-item-inner',
    ];
  @endphp

  @include( 'flexibles.facts-layouts.templates.table', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
