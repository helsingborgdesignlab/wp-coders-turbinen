@if( ! empty( $fields['title'] ) || ! empty( $fields['text'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container">
        <div class="cb-inner" style="max-width: {{ absint( $fields['max_width'] ) }}px">
          @if( ! empty( $fields['title'] ) )
            <h2 class="cb-title layout-title mb-0">
              {!! $fields['title'] !!}
            </h2>
          @endif

          @if( ! empty( $fields['text'] ) )
            <div class="cb-text layout-text rlpm {{ empty( $fields['title'] ) ? '' : 'mt-3' }}">
              {!! $fields['text'] !!}
            </div>
          @endif

          @if( ! empty( $fields['buttons'] ) )
            <ul class="cb-buttons btn-troop {{ empty( $fields['title'] ) && empty( $fields['text'] ) ? '' : 'mt-4' }}">
              @foreach( $fields['buttons'] as $button )
                <li>
                  {!! \App\the_wpc_button( [
                    'data' => $button['button'],
                    'classes' => [ 'cb-button' ],
                  ] ) !!}
                </li>
              @endforeach
            </ul>
          @endif
        </div>
      </div>
    </div>
  </section>
@endif
