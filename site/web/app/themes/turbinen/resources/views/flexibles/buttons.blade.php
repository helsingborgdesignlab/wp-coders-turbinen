@if( have_rows( 'buttons' ) )
  @while( have_rows( 'buttons' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'buttons' . '-' ),
      'buttons' => get_sub_field( 'buttons' ),
    ];

    $classes = [
      'layout-item',
      'buttons',
    ];

    $inner_classes = [
      'layout-item-inner',
      'bg-ns-medium',
      'pt-xs',
      'pb-xs',
    ];
  @endphp

  @include( 'flexibles.templates.buttons', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif


