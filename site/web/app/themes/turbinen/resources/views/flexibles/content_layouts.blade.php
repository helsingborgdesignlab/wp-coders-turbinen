@if( have_rows( 'content_layouts' ) )
  @while( have_rows( 'content_layouts' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'content_layouts' . '-' ),
      'padding' => get_sub_field( 'content_layouts_padding' ),
    ];

    $classes = [
      'layout-item',
      'content_layouts',
    ];

    $inner_classes = [
      'layout-item-inner',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @if( have_rows( 'content_layouts_content_layouts' ) )
    <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
      <div class="{{ implode( ' ', $inner_classes ) }}">
        <div class="container">
          @while( have_rows( 'content_layouts_content_layouts' ) ) @php the_row() @endphp
            @include( 'flexibles.content-layouts.' . get_row_layout() )
          @endwhile
        </div>
      </div>
    </section>
  @endif

  @endwhile
@endif
