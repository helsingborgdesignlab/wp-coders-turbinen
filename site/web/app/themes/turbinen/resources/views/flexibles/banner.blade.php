@if( have_rows( 'banner' ) )
  @while( have_rows( 'banner' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'banner' . '-' ),
      'slides' => [ [
        'title' => ! empty( get_sub_field( 'title' ) ) ? get_sub_field( 'title' ) : \App\title(),
        'text' => get_sub_field( 'text' ),
        'buttons' => get_sub_field( 'buttons' ),
        'image' => get_sub_field( 'image' ),
        'style' => ! empty( get_sub_field( 'style' ) ) ? get_sub_field( 'style' ) : 'ns',
      ] ],
      'height' => ! empty( get_sub_field( 'height' ) ) ? get_sub_field( 'height' ) : 'default',
      'full_screen' => ! empty( get_sub_field( 'full_screen' ) ) ? get_sub_field( 'full_screen' ) : 'disabled',
    ];

    $classes = [
      'layout-item',
      'banner',
      'height-' . $fields['height'],
      'full-screen-' . $fields['full_screen'],
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.hero_slider', [
    'fields' => apply_filters( 'turbinen_layout_banner_fields', $fields ),
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
