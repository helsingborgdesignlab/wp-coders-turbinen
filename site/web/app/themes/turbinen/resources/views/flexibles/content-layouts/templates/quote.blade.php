@if( ! empty( $fields['quote'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <q class="quote-text">{!! $fields['quote'] !!}</q>
  </section>
@endif
