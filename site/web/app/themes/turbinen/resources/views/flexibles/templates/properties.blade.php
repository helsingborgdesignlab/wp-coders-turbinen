@if( ! empty( $fields['properties'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container-conditional">
        <div class="ps-row row">
          @foreach( $fields['properties'] as $property )
            <div class="ps-col col-md-6 col-xxl-4">
              <div class="ps-property">

                @if( ! empty( $property['link'] ) )
                  <a href="{{ $property['link']['url'] }}" title="{{ $property['link']['title'] }}"
                     target="{{ $property['link']['target'] }}" class="ps-property-image-link">
                @else
                  <div class="ps-property-image-wrap">
                @endif

                  @if( ! empty( $property['image'] ) )
                    <figure class="ps-property-image pos-abs-full img-cover mb-0">
                      {!! \App\get_responsive_attachment( $property['image']['id'], 'turbinen-thumbnail-md' ) !!}
                    </figure>
                  @endif

                @if( ! empty( $property['link'] ) )
                  </a>
                @else
                  </div>
                @endif

                @if( ! empty( $property['details'] ) )
                  <ul class="ps-property-details">
                    @foreach( $property['details'] as $item )
                      <li>
                        @if( ! empty( $item['title'] ) )
                          {!! $item['title'] !!}:
                        @endif

                        {!! $item['text'] !!}
                      </li>
                    @endforeach
                  </ul>
                @endif
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>
@endif
