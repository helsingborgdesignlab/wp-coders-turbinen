@if( have_rows( 'houses' ) )
  @while( have_rows( 'houses' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'houses' . '-' ),
      'areas_label' => ! empty( get_sub_field( 'areas_label' ) ) ? get_sub_field( 'areas_label' ) : __( 'Areas', 'turbinen' ),
      'phases_label' => ! empty( get_sub_field( 'phases_label' ) ) ? get_sub_field( 'phases_label' ) : __( 'Phases', 'turbinen' ),
      'types_label' => ! empty( get_sub_field( 'types_label' ) ) ? get_sub_field( 'types_label' ) : __( 'Type of Residence', 'turbinen' ),
      'read_more_text' => get_sub_field( 'read_more_text' ),
      'no_results_message' => get_sub_field( 'no_results_message' ),
      'preselected_category' => get_sub_field( 'preselected_category' ),
      'preselected_area' => get_sub_field( 'preselected_area' ),
      'preselected_phase' => get_sub_field( 'preselected_phase' ),
      'preselected_type' => get_sub_field( 'preselected_type' ),
      'houses_per_page' => ! empty( get_sub_field( 'houses_per_page' ) ) ? get_sub_field( 'houses_per_page' ) : 6,
      'infinity_scroll' => ! empty( get_sub_field( 'infinity_scroll' ) ) ? get_sub_field( 'infinity_scroll' ) : 'enabled',
    ];

    $classes = [
      'layout-item',
      'houses',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.houses', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
