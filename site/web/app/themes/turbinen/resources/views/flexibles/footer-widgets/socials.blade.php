@if( have_rows( 'socials' ) )
  @while( have_rows( 'socials' ) ) @php the_row() @endphp

  @php
    $fields = [
      'title' => get_sub_field( 'title' ),
      'social_media_links' => get_field( 'social_media_links', 'option' ),
      'classes' => get_sub_field( 'classes' ),
    ];

    $classes = [
      'ci-widget',
      'socials',
      $fields['classes']
    ];
  @endphp

  @if( ! empty( $fields['title'] ) || ! empty( $fields['social_media_links'] ) )
    <section class="{{ implode( ' ', $classes ) }}">
      @if( ! empty( $fields['title'] ) )
        <h3 class="ciw-title">
          {!! $fields['title'] !!}
        </h3>
      @endif

      @if( ! empty( $fields['social_media_links'] ) )
        <ul class="ciw-socials">
          @foreach( $fields['social_media_links'] as $social_media_link  )
            @if( ! empty( $social_media_link['url'] ) && ! empty( $social_media_link['icon'] ) )
              <li>
                <a href="{{ $social_media_link['url'] }}" title="{{ $social_media_link['title'] }}" target="_blank" rel="nofollow">
                  <i class="{{ $social_media_link['icon'] }}" aria-hidden="true"></i>

                  @if( ! empty( $social_media_link['title'] ) )
                    <span class="sr-only">{{ $social_media_link['title'] }}</span>
                  @endif
                </a>
              </li>
            @endif
          @endforeach
        </ul>
      @endif

      <a class="ciw-brand" href="{{ home_url( '/' ) }}" title="{{ get_bloginfo( 'name', 'display' ) }}">
        {!! wpc_get_the_svg_logo() !!}
      </a>
    </section>
  @endif

  @endwhile
@endif

