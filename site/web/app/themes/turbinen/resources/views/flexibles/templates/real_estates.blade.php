@if( empty( $fields['types'] ) )

  @include( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] )

@else

  @php
    $buttons = [];

    if( $fields['anchors'] === 'enabled' ) {
      foreach ( $fields['types'] as $type ) {
        $buttons[] = [
          'button' => [
            'link_type' => 'anchor',
            'label' => $type->name,
            'anchor_link' => '#' . $type->slug,
          ],
        ];
      }
    }
  @endphp

  @if( ! empty( $buttons ) )
    @include( 'flexibles.templates.buttons', [
      'fields' => [
        'layout_id' => '',
        'buttons' => $buttons,
      ],
      'classes' => [
        'layout-item',
        'buttons',
      ],
      'inner_classes' => [
        'layout-item-inner',
        'bg-ns-medium',
        'pt-xs',
        'pb-xs',
      ],
    ] )
  @endif

  @foreach( $fields['types'] as $type_key => $type )

    @include( 'flexibles.templates.content_block', [
      'fields' => [
        'layout_id' => $type->slug,
        'title' => $type->name,
        'max_width' => 1050,
      ],
      'classes' => [
        'layout-item',
        'content_block',
        'has-bg',
      ],
      'inner_classes' => [
        'layout-item-inner',
        'bg-ns',
        'pt-xs',
        'pb-xs',
      ],
    ] )

    @include( 'flexibles.templates.separator', [
      'fields' => [
        'layout_id' => '',
      ],
      'classes' => [
        'layout-item',
        'separator',
        'height-default',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ],
    ] )

    @php
      $real_estates = get_posts( [
        'post_type' => 'turb_real_estate',
        'posts_per_page' => -1,
        'tax_query' => [
          [
            'taxonomy' => 'turb_real_estate_type',
            'field' => 'slug',
            'terms' => [ $type->slug ],
          ]
        ]
      ] );

      $properties = [];

      foreach ( $real_estates as $real_estate ) {
        $properties[] = [
          'image' => get_field( 'thumbnail', $real_estate->ID ),
          'link' => [
            'title' => get_the_title( $real_estate->ID ),
            'url' => get_the_permalink( $real_estate->ID ),
            'target' => '_self',
          ],
          'details' => get_field( 'details', $real_estate->ID ),
        ];
      }
    @endphp

    @include( 'flexibles.templates.properties', [
      'fields' => [
        'layout_id' => '',
        'properties' => $properties,
      ],
      'classes' => [
        'layout-item',
        'properties',
      ],
      'inner_classes' => [
        'layout-item-inner',
      ],
    ] )

    @if( count( $fields['types'] ) !== ( $type_key + 1 ) )
      @include( 'flexibles.templates.separator', [
        'fields' => [
          'layout_id' => '',
        ],
        'classes' => [
          'layout-item',
          'separator',
          'height-default',
        ],
        'inner_classes' => [
         'layout-item-inner',
        ],
      ] )
    @endif

  @endforeach

@endif
