@if( ! empty( $fields['content'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="ce-content">
      {!! $fields['content'] !!}
    </div>
  </section>
@endif
