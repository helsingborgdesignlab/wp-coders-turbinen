@if( ! empty( $fields['slides'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="hs-slides"
        @if( ! empty( $fields['slider_settings'] ) )
           data-autoplay="{{ $fields['slider_settings']['autoplay'] }}"
           data-interval="{{ $fields['slider_settings']['interval'] * 1000 }}"
        @endif
      >

        @foreach( array_reverse( $fields['slides'] ) as $key => $slide )
          <div class="hs-slide">
            <div class="hs-slide-inner">
              @if( ! empty( $slide['image'] ) )
                <figure class="hs-slide-thumbnail pos-abs-full img-cover mb-0">
                  {!! \App\get_responsive_attachment( $slide['image']['id'], 'turbinen-thumbnail-lg' ) !!}
                </figure>
              @endif

              <i class="hs-slide-triangle bg-{{ $slide['style'] }}" aria-hidden="true"></i>

              <div class="container-fluid">
                <div class="hs-slide-content">
                  @if( ! empty( $slide['title'] ) )
                    @if( $key === 0 )
                      <h1 class="hs-slide-title {{ empty( $slide['title'] ) && empty( $slide['buttons'] ) ? 'mb-0' : 'mb-3' }}">
                        {!! $slide['title'] !!}
                      </h1>
                    @else
                      <h2 class="hs-slide-title {{ empty( $slide['title'] ) && empty( $slide['buttons'] ) ? 'mb-0' : 'mb-3' }}">
                        {!! $slide['title'] !!}
                      </h2>
                    @endif
                  @endif

                  @if( ! empty( $slide['text'] ) )
                    <div class="hs-slide-text rlpm">
                      {!! $slide['text'] !!}
                    </div>
                  @endif

                  @if( ! empty( $slide['buttons'] ) )
                    <ul class="hs-slide-buttons mb-0 {{ empty( $slide['title'] ) && empty( $slide['text'] ) ? '' : 'mt-2'  }}">
                      @foreach( $slide['buttons'] as $button )
                        @if( \App\is_wpc_button_data( $button['button'] ) )
                          <li>
                            {!! \App\the_wpc_button( [
                              'data' => $button['button'],
                              'style' => 'link',
                            ] ) !!}
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  @endif
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
