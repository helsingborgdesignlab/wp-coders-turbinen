@if( have_rows( 'hero_benefit' ) )
  @while( have_rows( 'hero_benefit' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'hero_benefit' . '-' ),
      'title' => get_sub_field( 'title' ),
      'text' => get_sub_field( 'text' ),
      'image' => get_sub_field( 'image' ),
      'horizontal_position' => ! empty( get_sub_field( 'horizontal_position' ) ) ? get_sub_field( 'horizontal_position' ) : 'left',
      'max_width' => ! empty( get_sub_field( 'max_width' ) ) ? get_sub_field( 'max_width' ) : 450,
    ];

    $classes = [
      'layout-item',
      'hero_benefit',
      'horizontal-position-' . $fields['horizontal_position']
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.hero_benefit', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif


