@if( ! empty( $fields['table'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      @if( ! empty( $fields['title'] ) )
        <h4 class="facts-item-title">
          {!! $fields['title'] !!}
        </h4>
      @endif

      <div class="table-responsive">
        <table class="table">
          @if( ! empty( $fields['table']['header'] ) )
            <thead>
            <tr>
              @foreach( $fields['table']['header'] as $th )
                <th>{!! $th['c'] !!}</th>
              @endforeach
            </tr>
            </thead>
          @endif

          @if( ! empty( $fields['table']['body'] ) )
            <tbody>
            @foreach( $fields['table']['body'] as $tr )
              <tr>
                @foreach( $tr as $td )
                  <td>{!! $td['c'] !!}</td>
                @endforeach
              </tr>
            @endforeach
            </tbody>
          @endif
        </table>
      </div>
    </div>
  </section>
@endif
