@if( ! empty( $fields['markers'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="turbinen-gmap">
        @foreach( $fields['markers'] as $marker )
          @if( isset( $marker['marker']['lat'] ) && isset( $marker['marker']['lng'] ) )
            <div class="marker" data-lat="{{ $marker['marker']['lat'] }}" data-lng="{{ $marker['marker']['lng'] }}">
              {!! isset( $marker['marker']['address'] ) ? $marker['marker']['address'] : '' !!}
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </section>
@endif
