@php
  $images_nav_data = [];

  if ( ( $fields['images_per_page'] == -1 ) || ( absint( $fields['images_per_page'] ) > count( $fields['images'] ) ) ) {
    $fields['images_per_page'] = count( $fields['images'] );
  } else {
    foreach ( $fields['images'] as $image ) {
      if ( ! empty( $image['image'] ) ) {
        $images_nav_data[] = [
          'image_id' => $image['image']['id'],
          'link' => $image['link'],
        ];
      }
    }
  }
@endphp

@if( ! empty( $fields['images'] ) && $fields['images_per_page'] !== 0 )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      @for ( $i = 0; $i < $fields['images_per_page']; $i++ )
        <div class="{{ implode( ' ', $column_classes ) }}">
          @if( ! empty( $fields['images'][$i]['link'] ) )
            @include( 'flexibles.split-content-layouts.templates.page_link', [
              'fields' => [
                'layout_id' => '',
                'image' => $fields['images'][$i]['image'],
                'button' => [
                  'link_type' => 'link',
                  'link_link' => [
                    'url' => $fields['images'][$i]['link']['url'],
                    'title' => $fields['images'][$i]['link']['title'],
                    'target' => $fields['images'][$i]['link']['target'],
                  ]
                ],
                'image_only' => 'enabled',
              ],
              'classes' => [
                'split-item',
                'page_link',
              ],
              'inner_classes' => [
                'split-item-inner',
                'mobile-min-height',
              ],
            ] )
          @else
            @include( 'flexibles.split-content-layouts.templates.image', [
              'fields' => [
                'layout_id' => '',
                'image' => $fields['images'][$i]['image'],
              ],
              'classes' => [
                'split-item',
                'image',
              ],
              'inner_classes' => [
                'split-item-inner',
                'mobile-min-height',
              ],
            ] )
          @endif
        </div>
      @endfor

      @if( ! empty( $images_nav_data ) )
        <div class="gallery-nav" data-images-per-page="{{ $fields['images_per_page'] }}"
             data-images="{{ json_encode( $images_nav_data ) }}">

          <div class="gallery-nav-inner container">
            <a href="#" class="btn btn-ns btn-load" title="{{ $fields['load_more_text'] }}">
              <span>{!! $fields['load_more_text'] !!}</span>
            </a>
          </div>
        </div>
      @endif
    </div>
  </section>
@endif
