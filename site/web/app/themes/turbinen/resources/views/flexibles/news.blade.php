@php $overwrite_defaults = get_sub_field( 'overwrite_defaults' ) @endphp

@if( have_rows( 'news' ) )
  @while( have_rows( 'news' ) ) @php the_row() @endphp

  @php
    $fields_option = get_field( 'news_defaults', 'option' );
    $fields = reset( $fields_option );

    if ( 'yes' === $overwrite_defaults ) {
      $fields = get_sub_field( 'news' );
    }

    if ( empty( $fields['layout_id'] ) ) {
      $fields['layout_id'] = uniqid( 'news' . '-' );
    }

    if ( empty( $fields['posts_per_page'] ) ) {
      $fields['posts_per_page'] = 6;
    }

    $classes = [
      'layout-item',
      'news',
    ];

    $inner_classes = [
      'layout-item-inner',
      'bg-ns-light',
      'pt-' . $fields['news_padding']['top'],
      'pb-' . $fields['news_padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.templates.news', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
