@if( have_rows( 'project_status' ) )
  @while( have_rows( 'project_status' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'project_status' . '-' ),
      'title' => get_sub_field( 'title' ),
      'items' => get_sub_field( 'items' ),
    ];

    $classes = [
      'layout-item',
      'project_status',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.project_status', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
