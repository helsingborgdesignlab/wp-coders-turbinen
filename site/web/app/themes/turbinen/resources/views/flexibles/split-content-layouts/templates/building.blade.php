<section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <div class="s-build-wrap container-split">
      @if( ! empty( $fields['title'] ) )
        <h3 class="s-build-title layout-title mb-0">
          {!! $fields['title'] !!}
        </h3>
      @endif

      @if( ! empty( $fields['sub_title'] ) )
        <p class="s-build-sub-title layout-text mb-0 {{ empty( $fields['title'] ) ? '' : 'mt-2' }}">
          {!! $fields['sub_title'] !!}
        </p>
      @endif

      @if( ! empty( $fields['lead'] ) )
        <p class="s-build-lead layout-text mb-0 {{ empty( $fields['title'] ) && empty( $fields['sub_title'] ) ? '' : 'mt-3' }}">
          {!! $fields['lead'] !!}
        </p>
      @endif

      @if( ! empty( $fields['text'] ) )
        <div class="s-build-text layout-text rlpm {{ empty( $fields['lead'] ) && empty( $fields['sub_title'] ) ? empty( $fields['title'] ) ? '' : 'mt-3' : empty( $fields['lead'] ) ? 'mt-3' : 'mt-1' }}">
          {!! $fields['text'] !!}
        </div>
      @endif

      @if( ! empty( $fields['details'] ) )
        <div class="s-build-details-wrap {{ empty( $fields['title'] ) && empty( $fields['sub_title'] ) && empty( $fields['lead'] ) && empty( $fields['text'] ) ? 'mt-0' : 'mt-4' }}">
          <div class="s-build-details bg-ns {{ ! empty( $fields['button'] ) && \App\is_wpc_button_data( $fields['button'] ) ? 'pb-0' : '' }}">
            @if( ! empty( $fields['details_title'] ) )
              <h4 class="s-build-details-title layout-title mb-0">
                {!! $fields['details_title'] !!}
              </h4>
            @endif

            <ul class="s-build-details-list layout-text {{ empty( $fields['details_title'] ) ? '' : 'mt-3' }}">
              @foreach( $fields['details'] as $item )
                <li>
                  @if( ! empty( $item['title'] ) )
                    <span>{!! $item['title'] !!}:</span>
                  @endif

                  @if( ! empty( $item['text'] ) )
                    <span>{!! $item['text'] !!}</span>
                  @endif
                </li>
              @endforeach
            </ul>

            @if( ! empty( $fields['button'] ) )
              <div class="s-build-button-wrap">
                {!! \App\the_wpc_button( [
                  'data' => $fields['button'],
                  'classes' => [ 's-build-button', 'btn-no-arrow', 'mt-2' ],
                  'style' => 'darker'
                ] ) !!}
              </div>
            @endif
          </div>
        </div>
      @else
        @if( ! empty( $fields['button'] ) )
          <div class="s-build-button-wrap">
            {!! \App\the_wpc_button( [
              'data' => $fields['button'],
              'classes' => [ 's-build-button', empty( $fields['title'] ) && empty( $fields['sub_title'] ) && empty( $fields['lead'] ) && empty( $fields['text'] ) ? '' : 'mt-3' ],
              'style' => 'darker'
            ] ) !!}
          </div>
        @endif
      @endif
    </div>
  </div>
</section>
