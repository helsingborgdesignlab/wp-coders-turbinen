@if( have_rows( 'navigation' ) )
  @while( have_rows( 'navigation' ) ) @php the_row() @endphp

  @php
    $fields = [
      'title' => get_sub_field( 'title' ),
      'menu' => get_sub_field( 'menu' ),
      'classes' => get_sub_field( 'classes' ),
    ];

    $classes = [
      'ci-widget',
      'navigation',
      $fields['classes']
    ];
  @endphp

  @if( ! empty( $fields['title'] ) || ! empty( $fields['menu'] ) )
    <section class="{{ implode( ' ', $classes ) }}">
      @if( ! empty( $fields['title'] ) )
        <h3 class="ciw-title">
          {!! $fields['title'] !!}
        </h3>
      @endif

      <nav class="ciw-nav">
        {!! wp_nav_menu( [ 'menu' => $fields['menu'], 'menu_class' => 'nav' ] ) !!}
      </nav>
    </section>
  @endif

  @endwhile
@endif
