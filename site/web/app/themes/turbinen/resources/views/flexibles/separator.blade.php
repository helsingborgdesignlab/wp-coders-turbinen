@if( have_rows( 'separator' ) )
  @while( have_rows( 'separator' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'separator' . '-' ),
      'height' => ! empty( get_sub_field( 'height' ) ) ? get_sub_field( 'height' ) : 'default',
      'background' => ! empty( get_sub_field( 'background' ) ) ? get_sub_field( 'background' ) : 'transparent',
    ];

    $classes = [
      'layout-item',
      'separator',
      'height-' . $fields['height'],
    ];
    
    if( \App\layout_has_bg( $fields['background'] ) ) {
      $classes[] = 'has-bg';
    }

    $inner_classes = [
      'layout-item-inner',
      'bg-' . $fields['background'],
    ];
  @endphp

  @include( 'flexibles.templates.separator', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif


