<section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <div class="container">
      <div class="profile-row row">
        <div class="profile-col col-md-5 col-xl-4 col-xxxl-3">
          @if( ! empty( $fields['picture'] ) )
            <div class="profile-picture-wrap">
              <figure class="profile-picture pos-abs-full img-cover mb-0">
                {!! \App\get_responsive_attachment( $fields['picture']['id'], 'turbinen-thumbnail' ) !!}
              </figure>
            </div>
          @endif
        </div>

        <div class="profile-col col-md-7 col-xl-8 col-xxxl-9 pt-default pb-default">
          <div class="profile-content">
            @if( ! empty( $fields['title'] ) )
              <h4 class="profile-title mb-0">
                {!! $fields['title'] !!}
              </h4>
            @endif

            @if( ! empty( $fields['text'] ) )
              <div class="profile-text rlpm {{ empty( $fields['title'] ) ? '' : 'mt-3' }}">
                {!! $fields['text'] !!}
              </div>
            @endif

            <div class="profile-details {{ empty( $fields['title'] ) && empty( $fields['text'] ) ? '' : 'mt-4' }}">
              @if( ! empty( $fields['name'] ) )
                <p class="profile-name mb-0">
                  {!! $fields['name'] !!}
                </p>
              @endif

              @if( ! empty( $fields['position'] ) )
                <p class="profile-position mb-0">
                  {!! $fields['position'] !!}
                </p>
              @endif

              @if( ! empty( $fields['phone'] ) || ! empty( $fields['email'] ) )
                <div class="profile-contact {{ empty( $fields['name'] ) && empty( $fields['position'] ) ? '' : 'mt-2' }}">
                  @if( ! empty( $fields['phone'] ) )
                    <p class="profile-phone mb-0">
                      <span>{{ __( 'Tel', 'turbinen' ) }}:</span>
                      <a href="tel:{{ $fields['phone'] }}">
                        {{ $fields['phone'] }}
                      </a>
                    </p>
                  @endif

                  @if( ! empty( $fields['email'] ) )
                    <p class="profile-email mb-0">
                      <span>{{ __( 'E-mail', 'turbinen' ) }}:</span>
                      <a href="mailto:{!! antispambot( $fields['email'] ) !!}">
                        {!! antispambot( $fields['email'] ) !!}
                      </a>
                    </p>
                  @endif
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
