@if( have_rows( 'office_spaces' ) )
  @while( have_rows( 'office_spaces' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'office_spaces' . '-' ),
      'areas_label' => ! empty( get_sub_field( 'areas_label' ) ) ? get_sub_field( 'areas_label' ) : __( 'Areas', 'turbinen' ),
      'types_label' => ! empty( get_sub_field( 'types_label' ) ) ? get_sub_field( 'types_label' ) : __( 'Type of Office Space', 'turbinen' ),
      'read_more_text' => get_sub_field( 'read_more_text' ),
      'no_results_message' => get_sub_field( 'no_results_message' ),
      'preselected_area' => get_sub_field( 'preselected_area' ),
      'preselected_type' => get_sub_field( 'preselected_type' ),
      'office_spaces_per_page' => ! empty( get_sub_field( 'office_spaces_per_page' ) ) ? get_sub_field( 'office_spaces_per_page' ) : 6,
      'infinity_scroll' => ! empty( get_sub_field( 'infinity_scroll' ) ) ? get_sub_field( 'infinity_scroll' ) : 'enabled',
    ];

    $classes = [
      'layout-item',
      'office_spaces',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.office_spaces', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
