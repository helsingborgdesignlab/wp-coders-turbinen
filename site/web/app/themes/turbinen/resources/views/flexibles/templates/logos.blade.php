@if( ! empty( $fields['logos'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container">
        @if( ! empty( $fields['title'] ) )
          <h3 class="l-title layout-title mb-4">
            {!! $fields['title'] !!}
          </h3>
        @endif

        <ul class="l-logos">
          @foreach( $fields['logos'] as $logo )
            <li class="l-logo-wrap" style="max-width: {{ empty( $logo['max_width'] ) ? 120 : $logo['max_width'] + 20 }}px">
              <figure class="l-logo mb-0">
                {!! \App\get_responsive_attachment( $logo['logo']['id'], 'turbinen-thumbnail-sm', 'img-fluid' ) !!}
              </figure>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </section>
@endif


