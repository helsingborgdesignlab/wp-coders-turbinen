@if( have_rows( 'content_editor' ) )
  @while( have_rows( 'content_editor' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'cl-content_editor-' ),
      'content' => get_sub_field( 'content' ),
      'columns' => get_sub_field( 'columns' ) ? get_sub_field( 'columns' ) : 'one',
    ];

    $classes = [
      'content-layout-item',
      'content_editor',
      'content-columns-' . $fields['columns']
    ];
  @endphp

  @include( 'flexibles.content-layouts.templates.content_editor', [
    'fields' => $fields,
    'classes' => $classes,
  ] )

  @endwhile
@endif
