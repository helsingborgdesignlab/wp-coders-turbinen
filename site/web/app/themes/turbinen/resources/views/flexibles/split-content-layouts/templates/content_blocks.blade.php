@if( ! empty( $fields['content_blocks'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="s-cbs-wrap container-split">
        @foreach( $fields['content_blocks'] as $cb )
          <div class="s-cbs-content">
            @if( ! empty( $cb['title'] ) )
              <h3 class="s-cbs-title layout-title mb-0">
                {!! $cb['title'] !!}
              </h3>
            @endif

            @if( ! empty( $cb['sub_title'] ) )
              <h4 class="s-cbs-sub-title layout-sub-title mb-0 {{ empty( $cb['title'] ) ? '' : 'mt-2' }}">
                {!! $cb['sub_title'] !!}
              </h4>
            @endif

            @if( ! empty( $cb['text'] ) )
              <div class="s-cbs-text layout-text rlpm {{ empty( $cb['sub_title'] ) ? ( empty( $cb['title'] ) ? '' : 'mt-3' ) : 'mt-2' }}">
                {!! $cb['text'] !!}
              </div>
            @endif

            @if( ! empty( $cb['button'] ) )
              {!! \App\the_wpc_button( [
                'data' => $cb['button'],
                'classes' => [ empty( $cb['title'] ) && empty( $cb['title'] ) && empty( $cb['sub_title'] ) ? '' : 'mt-3 mt-xl-4' ]
              ] ) !!}
            @endif
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
