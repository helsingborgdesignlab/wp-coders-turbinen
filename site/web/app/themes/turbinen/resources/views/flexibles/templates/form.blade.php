@if( ! empty( $fields['form']['id'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container">
        <div class="form-inner" style="max-width: {{ absint( $fields['max_width'] ) }}px">
          @php gravity_form( $fields['form']['id'], $fields['form_title'], $fields['form_description'], false, '', true, 1 ) @endphp
        </div>
      </div>
    </div>
  </section>
@endif
