@php
  global $wp_query;

  $categories = get_terms( [
    'taxonomy' => 'category',
    'hide_empty' => false,
  ] );

  $args = [
    'posts_per_page' => $fields['posts_per_page'],
  ];

  if ( is_category() ) {
    $fields['preselected_category'] = absint( get_query_var( 'cat', '' ) );
  }

  if ( empty( $fields['preselected_category'] ) && ! empty( $categories ) ) {
    $fields['preselected_category'] = $categories[0]->term_id;
  }

  if ( ! empty( $fields['preselected_category'] ) ) {
    $args['cat'] = $fields['preselected_category'];
  }

  $wp_query = null;
	$wp_query = new \WP_Query( $args );
@endphp

<section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <div class="posts-header container-conditional pb-sm">
      @if( ! empty( $fields['title'] ) )
        <h3 class="posts-title layout-title mb-0">
          {{ $fields['title'] }}
        </h3>
      @endif

      @if( ! empty( $categories ) )
        <ul class="posts-categories">
          @foreach( $categories as $category )
            <li>
              <a href="{{ get_term_link( $category ) }}" title="{{ $category->name }}" data-cat="{{ $category->term_id }}"
                 class="btn btn-load btn-white {{ $category->term_id === $fields['preselected_category'] ? 'active' : '' }}">
                <span>{{ $category->name }}</span>
              </a>
            </li>
          @endforeach
        </ul>
      @endif
    </div>

    <div class="posts-container container-conditional">
      <div class="posts-row row {{ have_posts() ? '' : 'empty' }}" data-posts-per-page="{{ $fields['posts_per_page'] }}"
           data-cat="{{ empty( $fields['preselected_category'] ) ? '' : $fields['preselected_category'] }}"
           data-no-results-message="{{ $fields['no_results_message'] }}" data-read-more-text="{{ $fields['read_more_text'] }}">

        @if ( ! have_posts() )
          @include( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] )
        @else
          @while ( have_posts() ) @php the_post() @endphp
            @include( 'partials.content-' . get_post_type(), [ 'read_more_text' => $fields['read_more_text'] ] )
          @endwhile
        @endif
      </div>

      @if( $fields['infinity_scroll'] === 'enabled' )
        <div class="posts-nav pt-sm" {!! ( $wp_query->max_num_pages > 1 ) ? '' : 'style="display:none"' !!}>
          <i class="turbinen-icon-spinner" aria-hidden="true"></i>
        </div>
      @endif
    </div>
  </div>
</section>

@php
  wp_reset_query();
@endphp
