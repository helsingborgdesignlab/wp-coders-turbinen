@if( have_rows( 'facts' ) )
  @while( have_rows( 'facts' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'facts' . '-' ),
      'padding' => get_sub_field( 'facts_padding' ),
    ];

    $classes = [
      'layout-item',
      'facts',
    ];

    $inner_classes = [
      'layout-item-inner',
      'bg-ns',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];

    $row_classes = [
      'layout-item-row',
      'row',
    ];

    $column_classes = [
      'layout-item-col',
      'col-md-6 col-xl-4'
    ];
  @endphp

  @if( have_rows( 'facts_content_fact_layouts' ) )
    <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
      <div class="{{ implode( ' ', $inner_classes ) }}">
        <div class="container">
          <div class="{{ implode( ' ', $row_classes ) }}">
            @while( have_rows( 'facts_content_fact_layouts' ) ) @php the_row() @endphp
              <div class="{{ implode( ' ', $column_classes ) }}">
                @include( 'flexibles.facts-layouts.' . get_row_layout() )
              </div>
            @endwhile
          </div>
        </div>
      </div>
    </section>
  @endif

  @endwhile
@endif
