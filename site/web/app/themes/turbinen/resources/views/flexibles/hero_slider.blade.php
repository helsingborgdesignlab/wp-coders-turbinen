@if( have_rows( 'hero_slider' ) )
  @while( have_rows( 'hero_slider' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'hero_slider' . '-' ),
      'slides' => get_sub_field( 'slides' ),
      'height' => ! empty( get_sub_field( 'height' ) ) ? get_sub_field( 'height' ) : 'default',
      'full_screen' => ! empty( get_sub_field( 'full_screen' ) ) ? get_sub_field( 'full_screen' ) : 'disabled',
      'slider_settings' => get_sub_field( 'hero_slider_slider_settings' ),
    ];

    $classes = [
      'layout-item',
      'hero_slider',
      'height-' . $fields['height'],
      'full-screen-' . $fields['full_screen'],
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.hero_slider', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
