@if( have_rows( 'price_list' ) )
  @while( have_rows( 'price_list' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'price_list' . '-' ),
      'table_head' => get_sub_field( 'table_head' ),
      'table_body' => get_sub_field( 'table_body' ),
      'max_width' => ! empty( get_sub_field( 'max_width' ) ) ? get_sub_field( 'max_width' ) : 1250,
      'padding' => get_sub_field( 'price_list_padding' ),
    ];

    $classes = [
      'layout-item',
      'price_list',
    ];

    $inner_classes = [
      'layout-item-inner',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.templates.price_list', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif

