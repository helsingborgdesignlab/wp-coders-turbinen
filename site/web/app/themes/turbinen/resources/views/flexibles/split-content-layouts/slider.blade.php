@if( have_rows( 'slider' ) )
  @while( have_rows( 'slider' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'slider' . '-' ),
      'slides' => get_sub_field( 'slides' ),
      'slider_settings' => get_sub_field( 'slider_slider_settings' ),
    ];

    $classes = [
      'split-item',
      'slider',
    ];

    $inner_classes = [
      'split-item-inner',
      'mobile-min-height',
    ];
  @endphp

  @include( 'flexibles.split-content-layouts.templates.slider', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif

