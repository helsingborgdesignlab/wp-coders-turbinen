@if( ! empty( $fields['image'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <figure class="s-i-image pos-abs-full img-cover mb-0">
        {!! \App\get_responsive_attachment( $fields['image']['id'], 'turbinen-thumbnail-md' ) !!}
      </figure>
    </div>
  </section>
@endif
