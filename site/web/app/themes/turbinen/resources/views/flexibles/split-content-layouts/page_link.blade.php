@if( have_rows( 'page_link' ) )
  @while( have_rows( 'page_link' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'page_link' . '-' ),
      'image' => get_sub_field( 'image' ),
      'overlay' => get_sub_field( 'overlay' ),
      'button' => get_sub_field( 'page_link_button' ),
      'image_only' => ! empty( get_sub_field( 'image_only' ) ) ? get_sub_field( 'image_only' ) : 'disabled',
    ];

    $classes = [
      'split-item',
      'page_link',
    ];

    $inner_classes = [
      'split-item-inner',
      'mobile-min-height'
    ];
  @endphp

  @include( 'flexibles.split-content-layouts.templates.page_link', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
