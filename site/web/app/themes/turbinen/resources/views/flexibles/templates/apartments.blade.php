@php
  global $wp_query;

  $municipalities = get_terms( [
    'taxonomy' => 'turb_apartment_municipality',
    'hide_empty' => false,
  ] );

  $args = [
    'post_type' => 'turb_apartment',
    'posts_per_page' => $fields['apartments_per_page'],
  ];

  if ( ! empty( $fields['preselected_municipality'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_apartment_municipality',
      'field'    => 'term_id',
      'terms'    => [ $fields['preselected_municipality'] ],
    ];
  }

  $wp_query = null;
	$wp_query = new \WP_Query( $args );
@endphp

<section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <form class="apartments-filters bg-ns-medium pt-xs pb-xs">
      <div class="apartments-filters-container container">
        <div class="apartments-filters-row form-row">
          <div class="apartments-filters-col col-md-6 col-lg-4">
            <label for="{{ $fields['layout_id'] }}-filter-municipalities">
              {!! $fields['municipalities_label'] !!}:
            </label>

            <div class="apartments-filters-select-wrap custom-select-wrap">
              <select class="apartments-filters-select custom-select" id="{{ $fields['layout_id'] }}-filter-municipalities"
                      data-taxonomy="turb_apartment_municipality">

                <option value="" {{ empty( $fields['preselected_municipality'] ) ? 'selected' : '' }}>
                  {{ __( 'All', 'turbinen' ) }}
                </option>

                @if( ! empty( $municipalities ) )
                  @foreach( $municipalities as $municipality )
                    <option value="{{ $municipality->term_id }}" {{ $fields['preselected_municipality'] == $municipality->term_id ? 'selected' : '' }}>
                      {!! $municipality->name !!}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
      </div>
    </form>

    <div class="apartments-container">
      <div class="apartments-row {{ have_posts() ? '' : 'empty' }}" data-posts-per-page="{{ $fields['apartments_per_page'] }}"
           data-municipality="{{ empty( $fields['preselected_municipality'] ) ? '' : $fields['preselected_municipality'] }}"
           data-no-results-message="{{ $fields['no_results_message'] }}" data-custom-link="{{ json_encode( $fields['read_more_link'] ) }}">

        @if ( ! have_posts() )
          @include( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] )
        @else
          @while ( have_posts() ) @php the_post() @endphp
            @include( 'partials.content-' . get_post_type(), [ 'custom_link' => $fields['read_more_link'] ] )
          @endwhile
        @endif
      </div>

      @if( $fields['infinity_scroll'] === 'enabled' )
        <div class="apartments-nav pt-sm" {!! ( $wp_query->max_num_pages > 1 ) ? '' : 'style="display:none"' !!}>
          <i class="turbinen-icon-spinner" aria-hidden="true"></i>
        </div>
      @endif
    </div>
  </div>
</section>

@php
  wp_reset_query();
@endphp
