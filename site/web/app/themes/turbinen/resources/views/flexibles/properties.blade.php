@if( have_rows( 'properties' ) )
  @while( have_rows( 'properties' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'properties' . '-' ),
      'properties' => get_sub_field( 'properties' ),
    ];

    $classes = [
      'layout-item',
      'properties',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.properties', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
