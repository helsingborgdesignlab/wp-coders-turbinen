@if( ! empty( $fields['image'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      @if( ! empty( $fields['link'] ) )
        <a href="{{ $fields['link']['url'] }}" title="{{ $fields['link']['title'] }}" class="i-image-link"
           target="{{ $fields['link']['target'] }}">
      @endif

      <figure class="i-image mb-0 {{ $fields['image_crop'] === 'enabled' ? 'img-cover' : '' }}">
        {!! \App\get_responsive_attachment( $fields['image']['id'], 'turbinen-thumbnail-md', ( $fields['image_crop'] === 'disabled' ? 'img-fluid' : '' ) ) !!}
      </figure>

      @if( ! empty( $fields['link'] ) )
        </a>
      @endif
    </div>
  </section>
@endif
