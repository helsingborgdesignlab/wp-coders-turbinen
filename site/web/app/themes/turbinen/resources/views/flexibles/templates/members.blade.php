@if( ! empty( $fields['members'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container">
        <div class="ms-row row">
          @foreach( $fields['members'] as $member )
            <div class="ms-col col-md-6 col-lg-4">
              <div class="ms-member bg-ns-light">
                <div class="ms-member-picture-wrap">
                  @if( ! empty( $member['picture'] ) )
                    <figure class="ms-member-picture pos-abs-full img-cover mb-0">
                      {!! \App\get_responsive_attachment( $member['picture']['id'], 'turbinen-thumbnail-md' ) !!}
                    </figure>
                  @endif
                </div>

                <div class="ms-member-inner">
                  @if( ! empty( $member['name'] ) )
                    <h4 class="ms-member-name mb-0">
                      {!! $member['name'] !!}
                    </h4>
                  @endif

                  @if( ! empty( $member['position'] ) )
                    <p class="ms-member-position mb-0 {{ empty( $member['name'] ) ? '' : 'mt-1' }}">
                      {!! $member['position'] !!}
                    </p>
                  @endif

                  @if( ! empty( $member['phone'] ) || ! empty( $member['mobile'] ) || ! empty( $member['email'] ) )
                    <div class="ms-member-contact {{ empty( $member['name'] ) || empty( $member['position'] ) ? '' : 'pt-3' }}">
                      @if( ! empty( $member['phone'] ) )
                        <p class="ms-member-phone mb-0">
                          <span>{{ __( 'Tel', 'turbinen' ) }}:</span>
                          <a href="tel: {{ $member['phone'] }}">
                            {!! $member['phone'] !!}
                          </a>
                        </p>
                      @endif

                      @if( ! empty( $member['mobile'] ) )
                        <p class="ms-member-mobile mb-0">
                          <span>{{ __( 'Mobile', 'turbinen' ) }}:</span>
                          <a href="tel:{{ $member['mobile'] }}">
                            {!! $member['mobile'] !!}
                          </a>
                        </p>
                      @endif

                      @if( ! empty( $member['email'] ) )
                        <p class="ms-member-email mb-0">
                          <span>{{ __( 'E-mail', 'turbinen' ) }}:</span>
                          <a href="mailto:{!! antispambot( $member['email'] ) !!}">
                            {!! antispambot( $member['email'] ) !!}
                          </a>
                        </p>
                      @endif
                    </div>
                  @endif
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>
@endif
