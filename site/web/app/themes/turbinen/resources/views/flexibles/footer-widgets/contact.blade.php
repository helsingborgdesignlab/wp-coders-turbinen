@if( have_rows( 'contact' ) )
  @while( have_rows( 'contact' ) ) @php the_row() @endphp

  @php
    $fields = [
      'title' => get_sub_field( 'title' ),
      'address' => get_sub_field( 'address' ),
      'phone' => get_sub_field( 'phone' ),
      'email' => get_sub_field( 'email' ),
      'classes' => get_sub_field( 'classes' ),
    ];

    $classes = [
      'ci-widget',
      'contact',
      $fields['classes']
    ];
  @endphp

  @if( ! empty( $fields['title'] ) || ! empty( $fields['address'] )|| ! empty( $fields['phone'] ) || ! empty( $fields['email'] ) )
    <section class="{{ implode( ' ', $classes ) }}">
      @if( ! empty( $fields['title'] ) )
        <h3 class="ciw-title">
          {!! $fields['title'] !!}
        </h3>
      @endif

      @if( ! empty( $fields['address'] ) )
        <p class="ciw-address">
          <i class="turbinen-icon-marker" aria-hidden="true"></i>
          <span>{!! $fields['address'] !!}</span>
        </p>
      @endif

      @if( ! empty( $fields['phone'] ) )
        <p class="ciw-phone">
          <i class="turbinen-icon-phone" aria-hidden="true"></i>
          <a href="tel:{{ $fields['phone'] }}" title="{{ $fields['phone'] }}">
            {!! $fields['phone'] !!}
          </a>
        </p>
      @endif

      @if( ! empty( $fields['email'] ) )
        <p class="ciw-email">
          <i class="turbinen-icon-email" aria-hidden="true"></i>
          <a href="mailto:{!! antispambot( $fields['email'] ) !!}">
            {!! antispambot( $fields['email'] ) !!}
          </a>
        </p>
      @endif
    </section>
  @endif

  @endwhile
@endif
