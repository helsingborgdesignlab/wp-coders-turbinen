@if( have_rows( 'quote' ) )
  @while( have_rows( 'quote' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'cl-quote-' ),
      'quote' => get_sub_field( 'quote' ),
    ];

    $classes = [
      'content-layout-item',
      'quote',
    ];
  @endphp

  @include( 'flexibles.content-layouts.templates.quote', [
    'fields' => $fields,
    'classes' => $classes,
  ] )

  @endwhile
@endif

