@if( have_rows( 'documents' ) )
  @while( have_rows( 'documents' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'documents' . '-' ),
      'title' => get_sub_field( 'title' ),
      'documents' => get_sub_field( 'documents' ),
    ];

    $classes = [
      'facts-item',
      'documents',
    ];

    $inner_classes = [
      'facts-item-inner',
    ];
  @endphp

  @include( 'flexibles.facts-layouts.templates.documents', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
