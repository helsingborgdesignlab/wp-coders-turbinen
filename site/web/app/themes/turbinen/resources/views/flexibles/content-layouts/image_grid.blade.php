@if( have_rows( 'image_grid' ) )
  @while( have_rows( 'image_grid' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'cl-image_grid-' ),
      'images_per_row' => get_sub_field( 'images_per_row' ) ? get_sub_field( 'images_per_row' ) : 'one',
    ];

    $sizes = [
      'one'   => 'turbinen-thumbnail-lg',
      'two'   => 'turbinen-thumbnail-md',
      'three' => 'turbinen-thumbnail',
    ];

    $classes = [
      'content-layout-item',
      'image_grid',
      'image-grid-' . $fields['images_per_row']
    ];
  @endphp

  @include( 'flexibles.content-layouts.templates.image_grid', [
    'fields' => $fields,
    'classes' => $classes,
    'sizes' => $sizes,
  ] )

  @endwhile
@endif
