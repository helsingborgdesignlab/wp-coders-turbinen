@if( ! empty( $fields['image'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <figure class="hb-image pos-abs-full img-cover mb-0">
        {!! \App\get_responsive_attachment( $fields['image']['id'], 'turbinen-thumbnail-lg' ) !!}
      </figure>

      @if( ! empty( $fields['title'] ) || ! empty( $fields['text'] ) )
        <div class="hb-container container pt-default pb-default">
          <div class="hb-inner" style="max-width: {{ absint( $fields['max_width'] ) }}px">
            <div class="hb-content">
              @if( ! empty( $fields['title'] ) )
                <h2 class="hb-title layout-title mb-0">
                  {!! $fields['title'] !!}
                </h2>
              @endif

              @if( ! empty( $fields['text'] ) )
                <div class="hb-text layout-text rlpm {{ empty( $fields['title'] ) ? '' : 'mt-2' }}">
                  {!! $fields['text'] !!}
                </div>
              @endif
            </div>
          </div>
        </div>
      @endif
    </div>
  </section>
@endif


