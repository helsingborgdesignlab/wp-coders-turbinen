@if( have_rows( 'image_grid_' . $fields['images_per_row'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    @while ( have_rows( 'image_grid_' . $fields['images_per_row'] ) ) @php the_row() @endphp
      @if ( ! empty( $img = get_sub_field( 'image' ) ) )
        <figure class="image-grid-item">
          {!! \App\get_responsive_attachment( $img['id'], $sizes[ $fields['images_per_row'] ], 'img-fluid' ) !!}

          @if( array_key_exists( 'caption', $img ) && $img['caption'] !== '' )
            <figcaption>
              {!! $img['caption'] !!}
            </figcaption>
          @endif
        </figure>
      @endif
    @endwhile
  </section>
@endif
