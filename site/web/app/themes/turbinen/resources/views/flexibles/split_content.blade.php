@if( have_rows( 'split_content' ) )
  @while( have_rows( 'split_content' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'split_content' . '-' ),
      'order' => ! empty( get_sub_field( 'order' ) ) ? get_sub_field( 'order' ) : 'default',
      'gutter' => ! empty( get_sub_field( 'gutter' ) ) ? get_sub_field( 'gutter' ) : 'disabled',
      'min_height' => ! empty( get_sub_field( 'min_height' ) ) ? get_sub_field( 'min_height' ) : 'default',
    ];

    $classes = [
      'layout-item',
      'split_content',
      'order-' . $fields['order'],
      'gutter-' . $fields['gutter'],
      'min-height-' . $fields['min_height'],
    ];

    $inner_classes = [
      'layout-item-inner',
      'layout-item-row',
      'row',
    ];

    $column_classes = [
      'layout-item-col',
      'col-md-6'
    ];
  @endphp

  @if( have_rows( 'split_content_split_layouts' ) )
    <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
      <div class="{{ implode( ' ', $inner_classes ) }}">
        @while( have_rows( 'split_content_split_layouts' ) ) @php the_row() @endphp
          <div class="{{ implode( ' ', $column_classes ) }}">
            @include( 'flexibles.split-content-layouts.' . get_row_layout() )
          </div>
        @endwhile
      </div>
    </section>
  @endif

  @endwhile
@endif
