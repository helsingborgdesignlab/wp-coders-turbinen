@if( ! empty( $fields['image'] ) && \App\is_wpc_button_data( $fields['button'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <a href="{{ \App\get_the_wpc_button_link( $fields['button'] ) }}" target="{{ \App\get_the_wpc_button_target( $fields['button'] ) }}"
       title="{{ \App\get_the_wpc_button_label( $fields['button'] ) }}" class="{{ implode( ' ', $inner_classes ) }}">

      <figure class="s-pl-image pos-abs-full img-cover mb-0">
        {!! \App\get_responsive_attachment( $fields['image']['id'], 'turbinen-thumbnail-md' ) !!}
      </figure>

      @if( ! empty( $fields['overlay'] ) )
        <figure class="s-pl-overlay mb-0">
          <img src="{{ $fields['overlay']['url'] }}" alt="{{ $fields['overlay']['alt'] }}">
        </figure>
      @endif

      @if( $fields['image_only'] === 'disabled' )
        <div class="s-pl-inner container-split pb-lg">
          <span class="s-pl-button btn btn-{{ \App\get_the_wpc_button_style( $fields['button'] )}}">
            <span class="s-pl-button-inner">
              {!! \App\get_the_wpc_button_label( $fields['button'] ) !!}
            </span>
          </span>
        </div>
      @endif
    </a>
  </section>
@endif
