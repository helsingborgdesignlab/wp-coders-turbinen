@if( ! empty( $fields['table_body'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container-conditional">
        <div class="table-wrap table-responsive" style="max-width: {{ absint( $fields['max_width'] ) }}px">
          <table class="table table-striped">
            @if( ! empty( $fields['table_head'] ) )
              <thead>
              @foreach( $fields['table_head'] as $row )
                <tr>
                  <th scope="col">{!! $row['first_column'] !!}</th>
                  <th scope="col">{!! $row['second_column'] !!}</th>
                  <th scope="col">{!! $row['third_column'] !!}</th>
                  <th scope="col">{!! $row['fourth_column'] !!}</th>
                  <th scope="col">{!! $row['fifth_column'] !!}</th>
                  <th scope="col">{!! $row['sixth_column'] !!}</th>
                </tr>
              @endforeach
              </thead>
            @endif

            @if( ! empty( $fields['table_head'] ) )
              <tbody>
              @foreach( $fields['table_body'] as $row )
                <tr>
                  <td>{!! $row['first_column'] !!}</td>
                  <td>{!! $row['second_column'] !!}</td>
                  <td>{!! $row['third_column'] !!}</td>
                  <td>{!! $row['fourth_column'] !!}</td>
                  <td>{!! $row['fifth_column'] !!}</td>
                  <td>
                    @if( ! empty( $row['sixth_column'] ) )
                      <a href="{{ $row['sixth_column']['url'] }}" title="{{ $row['sixth_column']['title'] }}" target="_blank">
                        <i class="turbinen-icon-file-download" aria-hidden="true"></i>
                        <span class="sr-only">{!! $row['sixth_column']['title'] !!}</span>
                      </a>
                    @endif
                  </td>
                </tr>
              @endforeach
              </tbody>
            @endif
          </table>
        </div>
      </div>
    </div>
  </section>
@endif
