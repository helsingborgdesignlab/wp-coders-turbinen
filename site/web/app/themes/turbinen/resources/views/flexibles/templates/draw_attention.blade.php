@if( ! empty( $fields['image']->ID ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="draw-attention-container container-conditional">
        <div class="table-responsive">
          <div class="draw-attention-image">
            {!! do_shortcode( '[drawattention ID="' . $fields['image']->ID . '"]' ) !!}
          </div>
        </div>
      </div>
    </div>
  </section>
@endif
