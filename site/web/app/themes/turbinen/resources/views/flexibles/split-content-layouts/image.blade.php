@if( have_rows( 'image' ) )
  @while( have_rows( 'image' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'image' . '-' ),
      'image' => get_sub_field( 'image' ),
    ];

    $classes = [
      'split-item',
      'image',
    ];

    $inner_classes = [
      'split-item-inner',
      'mobile-min-height',
    ];
  @endphp

  @include( 'flexibles.split-content-layouts.templates.image', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
