@if( have_rows( 'real_estates' ) )
  @while( have_rows( 'real_estates' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'real_estates' . '-' ),
      'no_results_message' => get_sub_field( 'no_results_message' ),
      'turb_real_estate_types' => get_sub_field( 'turb_real_estate_types' ),
      'anchors' => ! empty( get_sub_field( 'anchors' ) ) ? get_sub_field( 'anchors' ) : 'enabled',
    ];

    $args = [
      'taxonomy' => 'turb_real_estate_type',
      'parent' => 0,
      'hide_empty' => true,
      'exclude' => [ wpc_get_default_term_id( 'turb_real_estate_type' ) ]
    ];

    if ( ! empty( $fields['turb_real_estate_types'] ) ) {
      $args['include'] = wp_list_pluck( $fields['turb_real_estate_types'], 'term_id' );
    }

    $fields['types'] = get_terms( $args );

    $classes = [
      'layout-item',
      'real_estates',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.real_estates', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
