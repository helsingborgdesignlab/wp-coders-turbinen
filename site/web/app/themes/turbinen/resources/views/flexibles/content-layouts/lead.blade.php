@if( have_rows( 'lead' ) )
  @while( have_rows( 'lead' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'cl-lead-' ),
      'lead' => get_sub_field( 'lead' ),
    ];

    $classes = [
      'content-layout-item',
      'lead',
    ];
  @endphp

  @include( 'flexibles.content-layouts.templates.lead', [
    'fields' => $fields,
    'classes' => $classes,
  ] )

  @endwhile
@endif

