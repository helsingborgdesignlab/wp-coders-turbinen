@if( ! empty( $fields['documents'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      @if( ! empty( $fields['title'] ) )
        <h4 class="facts-item-title">
          {!! $fields['title'] !!}
        </h4>
      @endif

      <ul>
        @foreach( $fields['documents'] as $document )
          @if( ! empty( $document['file'] ) )
            <li>
              <a href="{{ $document['file']['url'] }}" title="{{ $document['title'] }}" target="_blank">
                <i class="turbinen-icon-file-download" aria-hidden="true"></i>
                <span>{!! $document['title'] !!}</span>
              </a>
            </li>
          @endif
        @endforeach
      </ul>
    </div>
  </section>
@endif
