@if( ! empty( $fields['buttons'] ) )

  @php
    $current_link = get_the_permalink( wpc_get_the_id() );

    if( is_tax() ) {
      $current_link = get_home_url() . get_term_link( get_queried_object()->term_id );
    }
  @endphp

  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="container-conditional">
        <ul class="b-buttons owl-carousel">
          @foreach( $fields['buttons'] as $button )
            <li class="b-button-item">
              {!! \App\the_wpc_button( [
                'data' => $button['button'],
                'style' => 'white',
                'classes' => [
                  'b-button',
                   $current_link === \App\get_the_wpc_button_link( $button['button'] ) ? 'active' : ''
                ],
              ] ) !!}
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </section>
@endif
