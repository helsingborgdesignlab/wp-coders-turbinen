@if( ! empty( $fields['slides'] ) )
  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="s-s-slides"
           data-autoplay="{{ $fields['slider_settings']['autoplay'] }}"
           data-interval="{{ $fields['slider_settings']['interval'] * 1000 }}">

        @foreach( $fields['slides'] as $slide )
          <div class="s-s-slide">
            <div class="s-s-slide-inner">
              @if( ! empty( $slide['image'] ) )
                <figure class="s-s-slide-image pos-abs-full img-cover mb-0">
                  {!! \App\get_responsive_attachment( $slide['image']['id'], 'turbinen-thumbnail-md' ) !!}
                </figure>
              @endif

              @if( ! empty( $slide['title'] ) || ! empty( $slide['text'] ) )
                <div class="s-s-slide-content pt-xs pb-xs style-{{ $slide['style'] }}">
                  <div class="container-split">
                    @if( ! empty( $slide['title'] ) )
                      <h3 class="s-s-slide-title layout-title mb-0">
                        {!! $slide['title'] !!}
                      </h3>
                    @endif

                    @if( ! empty( $slide['text'] ) )
                      <div class="s-s-slide-text layout-text rlpm {{ empty( $slide['title'] ) ? '' : 'mt-2' }}">
                        {!! $slide['text'] !!}
                      </div>
                    @endif
                  </div>
                </div>
              @endif
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
