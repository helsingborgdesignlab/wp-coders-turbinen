@php $overwrite_defaults = get_sub_field( 'overwrite_defaults' ) @endphp

@if( have_rows( 'logos' ) )
  @while( have_rows( 'logos' ) ) @php the_row() @endphp

  @php
    $fields_option = get_field( 'logos_defaults', 'option' );
    $fields = reset( $fields_option );

    if ( 'yes' === $overwrite_defaults ) {
      $fields = get_sub_field( 'logos' );
    }

    if ( empty( $fields['layout_id'] ) ) {
      $fields['layout_id'] = uniqid( 'logos' . '-' );
    }

    $classes = [
      'layout-item',
      'logos',
    ];

    $inner_classes = [
      'layout-item-inner',
      'pt-' . $fields['logos_padding']['top'],
      'pb-' . $fields['logos_padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.templates.logos', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
