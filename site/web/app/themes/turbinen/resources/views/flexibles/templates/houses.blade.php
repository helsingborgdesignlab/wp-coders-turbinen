@php
  global $wp_query;

  $areas = get_terms( [
    'taxonomy' => 'turb_house_area',
    'hide_empty' => false,
  ] );

  $phases = get_terms( [
    'taxonomy' => 'turb_house_phase',
    'hide_empty' => false,
  ] );

  $types = get_terms( [
    'taxonomy' => 'turb_house_type',
    'hide_empty' => false,
  ] );

  $args = [
    'post_type' => 'turb_house',
    'posts_per_page' => $fields['houses_per_page'],
  ];

  if ( ! empty( $fields['preselected_category'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_house_cat',
      'field'    => 'term_id',
      'terms'    => [ $fields['preselected_category'] ],
    ];
  }

  if ( ! empty( $fields['preselected_area'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_house_area',
      'field'    => 'term_id',
      'terms'    => [ $fields['preselected_area'] ],
    ];
  }

  if ( ! empty( $fields['preselected_phase'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_house_phase',
      'field'    => 'term_id',
      'terms'    => [ $fields['preselected_phase'] ],
    ];
  }

  if ( ! empty( $fields['preselected_type'] ) ) {
    $args['tax_query'][] = [
      'taxonomy' => 'turb_house_type',
      'field'    => 'term_id',
      'terms'    => [ $fields['preselected_type'] ],
    ];
  }

  $wp_query = null;
	$wp_query = new \WP_Query( $args );
@endphp

<section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
  <div class="{{ implode( ' ', $inner_classes ) }}">
    <form class="houses-filters bg-ns-medium pt-xs pb-xs">
      <div class="houses-filters-container container">
        <div class="houses-filters-row form-row">
          <div class="houses-filters-col col-md-6 col-lg-4">
            <label for="{{ $fields['layout_id'] }}-filter-areas">
              {!! $fields['areas_label'] !!}:
            </label>

            <div class="houses-filters-select-wrap custom-select-wrap">
              <select class="houses-filters-select custom-select" id="{{ $fields['layout_id'] }}-filter-areas"
                      data-taxonomy="turb_house_area">

                <option value="" {{ empty( $fields['preselected_area'] ) ? 'selected' : '' }}>
                  {{ __( 'All', 'turbinen' ) }}
                </option>

                @if( ! empty( $areas ) )
                  @foreach( $areas as $area )
                    <option value="{{ $area->term_id }}" {{ $fields['preselected_area'] == $area->term_id ? 'selected' : '' }}>
                      {!! $area->name !!}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>

          <div class="houses-filters-col col-md-6 col-lg-4">
            <label for="{{ $fields['layout_id'] }}-filter-phases">
              {!! $fields['phases_label'] !!}:
            </label>

            <div class="houses-filters-select-wrap custom-select-wrap">
              <select class="houses-filters-select custom-select" id="{{ $fields['layout_id'] }}-filter-phases"
                      data-taxonomy="turb_house_phase">

                <option value="" {{ empty( $fields['preselected_phase'] ) ? 'selected' : '' }}>
                  {{ __( 'All', 'turbinen' ) }}
                </option>

                @if( ! empty( $phases ) )
                  @foreach( $phases as $phase )
                    <option value="{{ $phase->term_id }}" {{ $fields['preselected_phase'] == $phase->term_id ? 'selected' : '' }}>
                      {!! $phase->name !!}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>

          <div class="houses-filters-col col-md-6 col-lg-4">
            <label for="{{ $fields['layout_id'] }}-filter-types">
              {!! $fields['types_label'] !!}:
            </label>

            <div class="houses-filters-select-wrap custom-select-wrap">
              <select class="houses-filters-select custom-select" id="{{ $fields['layout_id'] }}-filter-types"
                      data-taxonomy="turb_house_type">

                <option value="" {{ empty( $fields['preselected_type'] ) ? 'selected' : '' }}>
                  {{ __( 'All', 'turbinen' ) }}
                </option>

                @if( ! empty( $types ) )
                  @foreach( $types as $type )
                    <option value="{{ $type->term_id }}" {{ $fields['preselected_type'] == $type->term_id ? 'selected' : '' }}>
                      {!! $type->name !!}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
      </div>
    </form>

    <div class="houses-container">
      <div class="houses-row {{ have_posts() ? '' : 'empty' }}" data-posts-per-page="{{ $fields['houses_per_page'] }}"
           data-cat="{{ empty( $fields['preselected_category'] ) ? '' : $fields['preselected_category'] }}"
           data-area="{{ empty( $fields['preselected_area'] ) ? '' : $fields['preselected_area'] }}"
           data-phase="{{ empty( $fields['preselected_phase'] ) ? '' : $fields['preselected_phase'] }}"
           data-type="{{ empty( $fields['preselected_type'] ) ? '' : $fields['preselected_type'] }}"
           data-no-results-message="{{ $fields['no_results_message'] }}" data-read-more-text="{{ $fields['read_more_text'] }}">

        @if ( ! have_posts() )
          @include( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] )
        @else
          @while ( have_posts() ) @php the_post() @endphp
            @include( 'partials.content-' . get_post_type(), [ 'read_more_text' => $fields['read_more_text'] ] )
          @endwhile
        @endif
      </div>

      @if( $fields['infinity_scroll'] === 'enabled' )
        <div class="houses-nav pt-sm" {!! ( $wp_query->max_num_pages > 1 ) ? '' : 'style="display:none"' !!}>
          <i class="turbinen-icon-spinner" aria-hidden="true"></i>
        </div>
      @endif
    </div>
  </div>
</section>

@php
  wp_reset_query();
@endphp
