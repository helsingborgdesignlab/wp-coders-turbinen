@if( have_rows( 'form' ) )
  @while( have_rows( 'form' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'form' . '-' ),
      'form' => get_sub_field( 'form' ),
      'form_title' => get_sub_field( 'form_title' ) === 'enabled',
      'form_description' => get_sub_field( 'form_description' ) === 'enabled',
      'max_width' => ! empty( get_sub_field( 'max_width' ) ) ? get_sub_field( 'max_width' ) : 600,
      'padding' => get_sub_field( 'form_padding' ),
    ];

    $classes = [
      'layout-item',
      'form',
      $fields['form_title'] ? 'has-title' : '',
      $fields['form_description'] ? 'has-description' : '',
    ];

    $inner_classes = [
      'layout-item-inner',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  @include( 'flexibles.templates.form', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif
