@if( have_rows( 'profile' ) )
  @while( have_rows( 'profile' ) ) @php the_row() @endphp

  @php
    $fields = [
      'layout_id' => get_sub_field( 'layout_id' ) ? get_sub_field( 'layout_id' ) : uniqid( 'profile' . '-' ),
      'title' => get_sub_field( 'title' ),
      'text' => get_sub_field( 'text' ),
      'picture' => get_sub_field( 'picture' ),
      'name' => get_sub_field( 'name' ),
      'position' => get_sub_field( 'position' ),
      'phone' => get_sub_field( 'phone' ),
      'email' => get_sub_field( 'email' ),
    ];

    $classes = [
      'layout-item',
      'profile',
    ];

    $inner_classes = [
      'layout-item-inner',
    ];
  @endphp

  @include( 'flexibles.templates.profile', [
    'fields' => $fields,
    'classes' => $classes,
    'inner_classes' => $inner_classes,
  ] )

  @endwhile
@endif

