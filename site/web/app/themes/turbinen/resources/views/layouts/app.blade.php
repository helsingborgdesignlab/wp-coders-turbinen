<!doctype html>
<html @php language_attributes() @endphp>

  @include('partials.head')

  <body @php body_class() @endphp>
    <div class="wrap-outer">
      @php do_action('get_header') @endphp
      @include('partials.header')
      @php do_action('get_header_after') @endphp

      <div class="wrap" role="document">
        <div class="wrap-inner">
          @php do_action('page_header_before') @endphp
          @yield('page_header')
          @php do_action('page_header_after') @endphp

          <div class="content" id="main-content">
            <main class="main">
              @php do_action('content_before') @endphp
              @yield('content')
              @php do_action('content_after') @endphp
            </main>

            @if ( App\display_sidebar() )
              <aside class="sidebar">
                @include('partials.sidebar')
              </aside>
            @endif
          </div>

          @php do_action('page_footer_before') @endphp
          @yield('page_footer')
          @php do_action('page_footer_after') @endphp
        </div>
      </div>

      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @php do_action('get_footer_after') @endphp
    </div>

    @php wp_footer() @endphp
  </body>
</html>
