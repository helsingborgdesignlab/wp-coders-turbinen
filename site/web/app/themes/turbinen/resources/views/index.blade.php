@extends('layouts.app')

@section('page_header')
  @include('page-headers.page-header')
@endsection

@section('content')
  @include('partials.content-page')
@endsection

@section('page_footer')
  @include('page-footers.page-footer')
@endsection
