@extends('layouts.app')

@section('page_header')
  @include('page-headers.page-header')
@endsection

@section('content')
  @php
    $id = wpc_get_the_id();

    $fields = [
      'layout_id' => get_field( 'layout_id', $id ) ? get_field( 'layout_id', $id ) : uniqid( 'search' . '-' ),
      'title' => get_field( 'search_title', $id ),
      'no_results_message' => get_field( 'search_no_results_message', $id ),
      'read_more_text' => get_field( 'search_read_more_text', $id ),
      'padding' => get_field( 'search_padding', $id ),
    ];

    $classes = [
      'layout-item',
      'search',
    ];

    $inner_classes = [
      'layout-item-inner',
      'bg-ns-light',
      'pt-' . $fields['padding']['top'],
      'pb-' . $fields['padding']['bottom'],
    ];
  @endphp

  <section id="{{ $fields['layout_id'] }}" class="{{ implode( ' ', $classes ) }}">
    <div class="{{ implode( ' ', $inner_classes ) }}">
      <div class="posts-container container-conditional">
        <div class="posts-row row {{ have_posts() ? '' : 'empty' }}">
          @if ( ! have_posts() )
            @include( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] )
          @else
            @while ( have_posts() ) @php the_post() @endphp
              @include( 'partials.content', [ 'read_more_text' => $fields['read_more_text'] ] )
            @endwhile
          @endif
        </div>
      </div>
    </div>
  </section>
@endsection

@section('page_footer')
  @include('page-footers.page-footer')
@endsection
