<?php namespace App\Turbinen\PostTypes\OfficeSpace;

/**
 * Register Office Space Post Type
 */
add_action( 'init', function () {

    $labels = [
        'name'                  => _x( 'Office Spaces', 'Post Type General Name', 'turbinen' ),
        'singular_name'         => _x( 'Office Space', 'Post Type Singular Name', 'turbinen' ),
        'menu_name'             => __( 'Office Spaces', 'turbinen' ),
        'name_admin_bar'        => __( 'Office Spaces', 'turbinen' ),
        'archives'              => __( 'Office Space Archives', 'turbinen' ),
        'attributes'            => __( 'Office Space Attributes', 'turbinen' ),
        'parent_item_colon'     => __( 'Parent Office Space:', 'turbinen' ),
        'all_items'             => __( 'All Office Spaces', 'turbinen' ),
        'add_new_item'          => __( 'Add New Office Space', 'turbinen' ),
        'add_new'               => __( 'Add New', 'turbinen' ),
        'new_item'              => __( 'New Office Space', 'turbinen' ),
        'edit_item'             => __( 'Edit Office Space', 'turbinen' ),
        'update_item'           => __( 'Update Office Space', 'turbinen' ),
        'view_item'             => __( 'View Office Space', 'turbinen' ),
        'view_items'            => __( 'View Office Spaces', 'turbinen' ),
        'search_items'          => __( 'Search Office Space', 'turbinen' ),
        'not_found'             => __( 'Not found', 'turbinen' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'turbinen' ),
        'featured_image'        => __( 'Office Spaced Image', 'turbinen' ),
        'set_featured_image'    => __( 'Set featured image', 'turbinen' ),
        'remove_featured_image' => __( 'Remove featured image', 'turbinen' ),
        'use_featured_image'    => __( 'Use as featured image', 'turbinen' ),
        'insert_into_item'      => __( 'Insert into office space', 'turbinen' ),
        'uploaded_to_this_item' => __( 'Uploaded to this office space', 'turbinen' ),
        'items_list'            => __( 'Office Spaces list', 'turbinen' ),
        'items_list_navigation' => __( 'Office Spaces list navigation', 'turbinen' ),
        'filter_items_list'     => __( 'Filter office spaces list', 'turbinen' ),
    ];

    $rewrite = [
        'slug'       => \App\Turbinen()->turb_office_space_slug,
        'with_front' => false,
        'pages'      => true,
        'feeds'      => false,
    ];

    $args = [
        'label'               => __( 'Office Space', 'turbinen' ),
        'description'         => __( 'Office Space post type for turbinen theme.', 'turbinen' ),
        'labels'              => $labels,
        'supports'            => [ 'title' ],
        'taxonomies'          => [],
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 25,
        'menu_icon'           => 'dashicons-building',
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'page',
        'show_in_rest'        => true,
    ];

    register_post_type( 'turb_office_space', $args );

}, 0 );

/**
 * Register Office Space Area Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'Office Space Areas', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'Office Space Area', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Areas', 'turbinen' ),
        'all_items'                  => __( 'All Areas', 'turbinen' ),
        'parent_item'                => __( 'Parent Area', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Area:', 'turbinen' ),
        'new_item_name'              => __( 'New Area Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Area', 'turbinen' ),
        'edit_item'                  => __( 'Edit Area', 'turbinen' ),
        'update_item'                => __( 'Update Area', 'turbinen' ),
        'view_item'                  => __( 'View Area', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate areas with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove areas', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Areas', 'turbinen' ),
        'search_items'               => __( 'Search Areas', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No areas', 'turbinen' ),
        'items_list'                 => __( 'Areas list', 'turbinen' ),
        'items_list_navigation'      => __( 'Areas list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => false,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_office_space_area', [ 'turb_office_space' ], $args );

}, 0 );

/**
 * Register Office Space Type Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'Office Space Types', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'Office Space Type', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Types', 'turbinen' ),
        'all_items'                  => __( 'All Types', 'turbinen' ),
        'parent_item'                => __( 'Parent Type', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Type:', 'turbinen' ),
        'new_item_name'              => __( 'New Type Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Type', 'turbinen' ),
        'edit_item'                  => __( 'Edit Type', 'turbinen' ),
        'update_item'                => __( 'Update Type', 'turbinen' ),
        'view_item'                  => __( 'View Type', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate types with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove types', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Types', 'turbinen' ),
        'search_items'               => __( 'Search Types', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No types', 'turbinen' ),
        'items_list'                 => __( 'Types list', 'turbinen' ),
        'items_list_navigation'      => __( 'Types list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => false,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_office_space_type', [ 'turb_office_space' ], $args );

}, 0 );

/**
 * Admin CT filters
 */
add_action( 'restrict_manage_posts', function ( $post_type ) {

    // Apply this only on a specific post type
    if ( 'turb_office_space' !== $post_type ) {
        return;
    }

    // A list of taxonomy slugs to filter by
    $taxonomies = [ 'turb_office_space_type', 'turb_office_space_area' ];

    foreach ( $taxonomies as $taxonomy_slug ) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );

        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'All %s', 'turbinen' ), $taxonomy_obj->labels->menu_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ( ( isset( $_GET[ $taxonomy_slug ] ) && ( $_GET[ $taxonomy_slug ] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }
} );

/**
 * ACF readonly fields
 */
add_filter( 'acf/load_field/name=vitec_guid', __NAMESPACE__ . '\\disable_acf_load_field' );
add_filter( 'acf/load_field/name=vitec_andringsdatum', __NAMESPACE__ . '\\disable_acf_load_field' );
function disable_acf_load_field( $field ) {

    if ( is_admin() && function_exists( 'get_current_screen' ) ) {
        $screen = get_current_screen();

        if ( isset( $screen->post_type ) && $screen->post_type === 'turb_office_space' ) {
            $field['disabled'] = 1;
        }
    }

    return $field;
}

/**
 * Remove quick edit and trash
 */
add_filter( 'post_row_actions', function ( $actions ) {

    if ( is_admin() && function_exists( 'get_current_screen' ) ) {
        $screen = get_current_screen();

        if ( isset( $screen->post_type ) && $screen->post_type === 'turb_office_space' ) {
            unset( $actions['trash'] );
            unset( $actions['inline hide-if-no-js'] );
        }
    }

    return $actions;
} );

/**
 * Remove Edit and Move to Trash bulk actions
 */
add_filter( 'bulk_actions-edit-turb_office_space', '__return_empty_array' );

/**
 * Remove publish metabox
 */
add_action( 'admin_menu', function () {
    remove_meta_box( 'submitdiv', 'turb_office_space', 'side' );
} );
