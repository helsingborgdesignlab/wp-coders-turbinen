<?php namespace App\Turbinen\PostTypes\House;

/**
 * Register House Post Type
 */
add_action( 'init', function () {

    $labels = [
        'name'                  => _x( 'Houses', 'Post Type General Name', 'turbinen' ),
        'singular_name'         => _x( 'House', 'Post Type Singular Name', 'turbinen' ),
        'menu_name'             => __( 'Houses', 'turbinen' ),
        'name_admin_bar'        => __( 'Houses', 'turbinen' ),
        'archives'              => __( 'House Archives', 'turbinen' ),
        'attributes'            => __( 'House Attributes', 'turbinen' ),
        'parent_item_colon'     => __( 'Parent House:', 'turbinen' ),
        'all_items'             => __( 'All Houses', 'turbinen' ),
        'add_new_item'          => __( 'Add New House', 'turbinen' ),
        'add_new'               => __( 'Add New', 'turbinen' ),
        'new_item'              => __( 'New House', 'turbinen' ),
        'edit_item'             => __( 'Edit House', 'turbinen' ),
        'update_item'           => __( 'Update House', 'turbinen' ),
        'view_item'             => __( 'View House', 'turbinen' ),
        'view_items'            => __( 'View Houses', 'turbinen' ),
        'search_items'          => __( 'Search House', 'turbinen' ),
        'not_found'             => __( 'Not found', 'turbinen' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'turbinen' ),
        'featured_image'        => __( 'Housed Image', 'turbinen' ),
        'set_featured_image'    => __( 'Set featured image', 'turbinen' ),
        'remove_featured_image' => __( 'Remove featured image', 'turbinen' ),
        'use_featured_image'    => __( 'Use as featured image', 'turbinen' ),
        'insert_into_item'      => __( 'Insert into house', 'turbinen' ),
        'uploaded_to_this_item' => __( 'Uploaded to this house', 'turbinen' ),
        'items_list'            => __( 'Houses list', 'turbinen' ),
        'items_list_navigation' => __( 'Houses list navigation', 'turbinen' ),
        'filter_items_list'     => __( 'Filter houses list', 'turbinen' ),
    ];

    $rewrite = [
        'slug'       => \App\Turbinen()->turb_house_slug,
        'with_front' => false,
        'pages'      => true,
        'feeds'      => false,
    ];

    $args = [
        'label'               => __( 'House', 'turbinen' ),
        'description'         => __( 'House post type for turbinen theme.', 'turbinen' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'author', 'revisions' ],
        'taxonomies'          => [],
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 25,
        'menu_icon'           => 'dashicons-admin-multisite',
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'page',
        'show_in_rest'        => true,
    ];

    register_post_type( 'turb_house', $args );

}, 0 );

/**
 * Register House Category Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'House Categories', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'House Category', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Categories', 'turbinen' ),
        'all_items'                  => __( 'All Categories', 'turbinen' ),
        'parent_item'                => __( 'Parent Category', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Category:', 'turbinen' ),
        'new_item_name'              => __( 'New Category Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Category', 'turbinen' ),
        'edit_item'                  => __( 'Edit Category', 'turbinen' ),
        'update_item'                => __( 'Update Category', 'turbinen' ),
        'view_item'                  => __( 'View Category', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate categories with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove categories', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Categories', 'turbinen' ),
        'search_items'               => __( 'Search Categories', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No categories', 'turbinen' ),
        'items_list'                 => __( 'Categories list', 'turbinen' ),
        'items_list_navigation'      => __( 'Categories list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_house_cat', [ 'turb_house' ], $args );

}, 0 );

/**
 * Register House Area Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'House Areas', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'House Area', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Areas', 'turbinen' ),
        'all_items'                  => __( 'All Areas', 'turbinen' ),
        'parent_item'                => __( 'Parent Area', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Area:', 'turbinen' ),
        'new_item_name'              => __( 'New Area Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Area', 'turbinen' ),
        'edit_item'                  => __( 'Edit Area', 'turbinen' ),
        'update_item'                => __( 'Update Area', 'turbinen' ),
        'view_item'                  => __( 'View Area', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate areas with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove areas', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Areas', 'turbinen' ),
        'search_items'               => __( 'Search Areas', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No areas', 'turbinen' ),
        'items_list'                 => __( 'Areas list', 'turbinen' ),
        'items_list_navigation'      => __( 'Areas list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_house_area', [ 'turb_house' ], $args );

}, 0 );

/**
 * Register House Phase Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'House Phases', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'House Phase', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Phases', 'turbinen' ),
        'all_items'                  => __( 'All Phases', 'turbinen' ),
        'parent_item'                => __( 'Parent Phase', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Phase:', 'turbinen' ),
        'new_item_name'              => __( 'New Phase Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Phase', 'turbinen' ),
        'edit_item'                  => __( 'Edit Phase', 'turbinen' ),
        'update_item'                => __( 'Update Phase', 'turbinen' ),
        'view_item'                  => __( 'View Phase', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate phases with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove phases', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Phases', 'turbinen' ),
        'search_items'               => __( 'Search Phases', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No phases', 'turbinen' ),
        'items_list'                 => __( 'Phases list', 'turbinen' ),
        'items_list_navigation'      => __( 'Phases list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_house_phase', [ 'turb_house' ], $args );

}, 0 );

/**
 * Register House Type Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'House Types', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'House Type', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Types', 'turbinen' ),
        'all_items'                  => __( 'All Types', 'turbinen' ),
        'parent_item'                => __( 'Parent Type', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Type:', 'turbinen' ),
        'new_item_name'              => __( 'New Type Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Type', 'turbinen' ),
        'edit_item'                  => __( 'Edit Type', 'turbinen' ),
        'update_item'                => __( 'Update Type', 'turbinen' ),
        'view_item'                  => __( 'View Type', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate types with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove types', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Types', 'turbinen' ),
        'search_items'               => __( 'Search Types', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No types', 'turbinen' ),
        'items_list'                 => __( 'Types list', 'turbinen' ),
        'items_list_navigation'      => __( 'Types list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_house_type', [ 'turb_house' ], $args );

}, 0 );

/**
 * Archive page children rewrite fix
 */
add_filter( 'generate_rewrite_rules', function () {

    global $wp_rewrite;

    $rules = [];

    $children = get_children( [
        'post_parent' => \App\Turbinen()->turb_house_id,
        'post_type'   => 'page',
        'numberposts' => - 1,
        'post_status' => 'any'
    ] );

    if ( ! empty( $children ) ) {

        foreach ( $children as $child ) {
            $rules[ \App\Turbinen()->turb_house_slug . '/' . $child->post_name . '/?$' ] = 'index.php?page_id=' . $child->ID;
        }

        $wp_rewrite->rules = $rules + $wp_rewrite->rules;
    }
} );

/**
 * Template redirect
 */
add_action( 'template_redirect', function () {
    if ( is_singular( 'turb_house' ) ) {
        $custom_link = get_field( 'custom_link' );

        if ( ! empty( $custom_link['url'] ) ) {
            wp_redirect( $custom_link['url'] );
            die;
        }
    }
} );

/**
 * Admin CT filters
 */
add_action( 'restrict_manage_posts', function ( $post_type ) {

    // Apply this only on a specific post type
    if ( 'turb_house' !== $post_type ) {
        return;
    }

    // A list of taxonomy slugs to filter by
    $taxonomies = [ 'turb_house_cat', 'turb_house_area', 'turb_house_phase', 'turb_house_type' ];

    foreach ( $taxonomies as $taxonomy_slug ) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );

        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'All %s', 'turbinen' ), $taxonomy_obj->labels->menu_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ( ( isset( $_GET[ $taxonomy_slug ] ) && ( $_GET[ $taxonomy_slug ] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }
} );
