<?php namespace App\Turbinen\PostTypes\Post;

/**
 * Banner layout fields overwrite
 */
add_filter( 'turbinen_layout_banner_fields', function ( $fields ) {

    if ( is_singular( 'post' ) ) {
        $fields['slides'][0]['text'] = get_the_date();
    }

    return $fields;
} );

/**
 * Before page footer
 */
add_action( 'page_footer_before', function () {

    if ( is_singular( 'post' ) ) {
        echo \App\template( 'partials.nav-post' );
    }
} );
