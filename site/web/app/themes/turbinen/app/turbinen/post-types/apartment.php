<?php namespace App\Turbinen\PostTypes\Apartment;

/**
 * Register Apartment Post Type
 */
add_action( 'init', function () {

    $labels = [
        'name'                  => _x( 'Apartments', 'Post Type General Name', 'turbinen' ),
        'singular_name'         => _x( 'Apartment', 'Post Type Singular Name', 'turbinen' ),
        'menu_name'             => __( 'Apartments', 'turbinen' ),
        'name_admin_bar'        => __( 'Apartments', 'turbinen' ),
        'archives'              => __( 'Apartment Archives', 'turbinen' ),
        'attributes'            => __( 'Apartment Attributes', 'turbinen' ),
        'parent_item_colon'     => __( 'Parent Apartment:', 'turbinen' ),
        'all_items'             => __( 'All Apartments', 'turbinen' ),
        'add_new_item'          => __( 'Add New Apartment', 'turbinen' ),
        'add_new'               => __( 'Add New', 'turbinen' ),
        'new_item'              => __( 'New Apartment', 'turbinen' ),
        'edit_item'             => __( 'Edit Apartment', 'turbinen' ),
        'update_item'           => __( 'Update Apartment', 'turbinen' ),
        'view_item'             => __( 'View Apartment', 'turbinen' ),
        'view_items'            => __( 'View Apartments', 'turbinen' ),
        'search_items'          => __( 'Search Apartment', 'turbinen' ),
        'not_found'             => __( 'Not found', 'turbinen' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'turbinen' ),
        'featured_image'        => __( 'Apartmentd Image', 'turbinen' ),
        'set_featured_image'    => __( 'Set featured image', 'turbinen' ),
        'remove_featured_image' => __( 'Remove featured image', 'turbinen' ),
        'use_featured_image'    => __( 'Use as featured image', 'turbinen' ),
        'insert_into_item'      => __( 'Insert into real estate', 'turbinen' ),
        'uploaded_to_this_item' => __( 'Uploaded to this real estate', 'turbinen' ),
        'items_list'            => __( 'Apartments list', 'turbinen' ),
        'items_list_navigation' => __( 'Apartments list navigation', 'turbinen' ),
        'filter_items_list'     => __( 'Filter apartments list', 'turbinen' ),
    ];

    $args = [
        'label'               => __( 'Apartment', 'turbinen' ),
        'description'         => __( 'Apartment post type for turbinen theme.', 'turbinen' ),
        'labels'              => $labels,
        'supports'            => [ 'title' ],
        'taxonomies'          => [],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 25,
        'menu_icon'           => 'dashicons-admin-multisite',
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'rewrite'             => false,
        'capability_type'     => 'page',
        'show_in_rest'        => true,
    ];

    register_post_type( 'turb_apartment', $args );

}, 0 );

/**
 * Register Apartment Municipality Taxonomy
 */
add_action( 'init', function () {

    $labels = [
        'name'                       => _x( 'Apartment Municipalities', 'Taxonomy General Name', 'turbinen' ),
        'singular_name'              => _x( 'Apartment Municipality', 'Taxonomy Singular Name', 'turbinen' ),
        'menu_name'                  => __( 'Municipalities', 'turbinen' ),
        'all_items'                  => __( 'All Municipalities', 'turbinen' ),
        'parent_item'                => __( 'Parent Municipality', 'turbinen' ),
        'parent_item_colon'          => __( 'Parent Municipality:', 'turbinen' ),
        'new_item_name'              => __( 'New Municipality Name', 'turbinen' ),
        'add_new_item'               => __( 'Add New Municipality', 'turbinen' ),
        'edit_item'                  => __( 'Edit Municipality', 'turbinen' ),
        'update_item'                => __( 'Update Municipality', 'turbinen' ),
        'view_item'                  => __( 'View Municipality', 'turbinen' ),
        'separate_items_with_commas' => __( 'Separate municipalities with commas', 'turbinen' ),
        'add_or_remove_items'        => __( 'Add or remove municipalities', 'turbinen' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'turbinen' ),
        'popular_items'              => __( 'Popular Municipalities', 'turbinen' ),
        'search_items'               => __( 'Search Municipalities', 'turbinen' ),
        'not_found'                  => __( 'Not Found', 'turbinen' ),
        'no_terms'                   => __( 'No municipalities', 'turbinen' ),
        'items_list'                 => __( 'Municipalities list', 'turbinen' ),
        'items_list_navigation'      => __( 'Municipalities list navigation', 'turbinen' ),
    ];

    $args = [
        'labels'            => $labels,
        'hierarchical'      => false,
        'public'            => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
        'meta_box_cb'       => false,
        'rewrite'           => false,
    ];

    register_taxonomy( 'turb_apartment_municipality', [ 'turb_apartment' ], $args );

}, 0 );

/**
 * Admin CT filters
 */
add_action( 'restrict_manage_posts', function ( $post_type ) {

    // Apply this only on a specific post type
    if ( 'turb_real_estate' !== $post_type ) {
        return;
    }

    // A list of taxonomy slugs to filter by
    $taxonomies = [ 'turb_apartment_municipality' ];

    foreach ( $taxonomies as $taxonomy_slug ) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );

        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'All %s', 'turbinen' ), $taxonomy_obj->labels->menu_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ( ( isset( $_GET[ $taxonomy_slug ] ) && ( $_GET[ $taxonomy_slug ] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }
} );

/**
 * ACF readonly fields
 */
add_filter( 'acf/load_field/name=vitec_guid', __NAMESPACE__ . '\\disable_acf_load_field' );
add_filter( 'acf/load_field/name=vitec_andringsdatum', __NAMESPACE__ . '\\disable_acf_load_field' );
function disable_acf_load_field( $field ) {

    if ( is_admin() && function_exists( 'get_current_screen' ) ) {
        $screen = get_current_screen();

        if ( isset( $screen->post_type ) && $screen->post_type === 'turb_apartment' ) {
            $field['disabled'] = 1;
        }
    }

    return $field;
}

/**
 * Remove quick edit and trash
 */
add_filter( 'post_row_actions', function ( $actions ) {

    if ( is_admin() && function_exists( 'get_current_screen' ) ) {
        $screen = get_current_screen();

        if ( isset( $screen->post_type ) && $screen->post_type === 'turb_apartment' ) {
            unset( $actions['trash'] );
            unset( $actions['inline hide-if-no-js'] );
        }
    }

    return $actions;
} );

/**
 * Remove Edit and Move to Trash bulk actions
 */
add_filter( 'bulk_actions-edit-turb_apartment', '__return_empty_array' );

/**
 * Remove publish metabox
 */
add_action( 'admin_menu', function () {
    remove_meta_box( 'submitdiv', 'turb_apartment', 'side' );
} );
