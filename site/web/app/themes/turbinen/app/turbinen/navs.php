<?php namespace App\Turbinen\Navs;

/**
 * Auto populate title attribute if is empty
 */
add_filter( 'wp_setup_nav_menu_item', function ( $item ) {
    if ( empty( $item->attr_title ) && ! empty( $item->title ) ) {
        $item->attr_title = $item->title;
    }

    return $item;
} );

/**
 * 404 and search page Active class fix
 */
add_filter( 'nav_menu_css_class', function ( $classes, $item ) {

    if ( is_404() || is_search() ) {
        foreach ( $classes as $key => $class ) {
            if ( $class === 'active' ) {
                unset( $classes[ $key ] );
            }
        }
    }

    return $classes;
}, 100, 2 );
