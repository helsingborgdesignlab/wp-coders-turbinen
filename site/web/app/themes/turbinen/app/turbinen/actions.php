<?php namespace App\Turbinen\Actions;

// Search query changes
add_action( 'pre_get_posts', function ( $query ) {

    if ( ! is_admin() && $query->is_search() ) {
        $query->set( 'posts_per_page', - 1 );
    }
} );
