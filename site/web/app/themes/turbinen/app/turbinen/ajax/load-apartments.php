<?php namespace App\Turbinen\Ajax\LoadApartments;

/**
 * Load apartments
 */
add_action( 'wp_ajax_turbinen_load_apartments', __NAMESPACE__ . '\\turbinen_load_apartments' );
add_action( 'wp_ajax_nopriv_turbinen_load_apartments', __NAMESPACE__ . '\\turbinen_load_apartments' );

function turbinen_load_apartments() {
    global $wp_query;

    if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'turbinen-nonce' ) ||
         ! isset( $_REQUEST['offset'] ) ||
         ! isset( $_REQUEST['posts_per_page'] )
    ) {
        wp_send_json_error();
        exit;
    }

    $post_type      = 'turb_apartment';
    $posts_per_page = $_REQUEST['posts_per_page'];
    $offset         = $_REQUEST['offset'];
    $terms          = isset( $_REQUEST['terms'] ) ? $_REQUEST['terms'] : [];

    $fields = [
        'no_results_message' => isset( $_REQUEST['no_results_message'] ) ? $_REQUEST['no_results_message'] : '',
        'custom_link'     => isset( $_REQUEST['custom_link'] ) ? $_REQUEST['custom_link'] : '',
    ];

    $args = [
        'post_type'      => $post_type,
        'offset'         => $offset,
        'posts_per_page' => $posts_per_page,
        'order'          => 'DESC',
        'orderby'        => 'date',
        'post_status'    => 'publish'
    ];

    if ( ! empty( $terms ) ) {
        $args['tax_query'] = [ 'relation' => 'AND' ];

        foreach ( $terms as $taxonomy => $term_slug ) {
            $args['tax_query'][0][] = [
                'taxonomy' => $taxonomy,
                'field'    => 'term_id',
                'terms'    => [ $term_slug ],
            ];
        }
    }

    $wp_query = null;
    $wp_query = new \WP_Query( $args );

    ob_start();

    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            echo \App\template( 'partials.content-' . get_post_type(), [ 'custom_link' => $fields['custom_link'] ] );
        }
    } else {
        echo \App\template( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] );
    }

    $html = ob_get_clean();

    $total_count = 0;
    $count_posts = wp_count_posts( $post_type );

    if ( isset( $count_posts->publish ) ) {
        $total_count = absint( $count_posts->publish );
    }

    wp_send_json_success( [
        'html'   => $html,
        'count'  => isset( $wp_query->found_posts ) ? $wp_query->found_posts : $total_count,
        'offset' => $offset,
    ] );

    exit;
}
