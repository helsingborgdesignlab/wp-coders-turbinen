<?php namespace App\Turbinen\Ajax\LoadGallery;

/**
 * Load gallery
 */
add_action( 'wp_ajax_turbinen_load_gallery', __NAMESPACE__ . '\\turbinen_load_gallery' );
add_action( 'wp_ajax_nopriv_turbinen_load_gallery', __NAMESPACE__ . '\\turbinen_load_gallery' );

function turbinen_load_gallery() {

    if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'turbinen-nonce' ) ||
         ! isset( $_REQUEST['offset'] ) ||
         ! isset( $_REQUEST['images'] ) ||
         ! isset( $_REQUEST['images_per_page'] )
    ) {
        wp_send_json_error();
        exit;
    }

    $count           = count( $_REQUEST['images'] );
    $images_per_page = absint( $_REQUEST['images_per_page'] );
    $offset          = absint( $_REQUEST['offset'] );
    $images          = array_splice( $_REQUEST['images'], $offset, $images_per_page, true );

    ob_start();

    if ( ! empty( $images ) ) {
        foreach ( $images as $image ) {
            echo '<div class="layout-item-col col-md-6">';
            if ( ! empty( $image['link'] ) ) {
                echo \App\template(
                    'flexibles.split-content-layouts.templates.page_link', [
                        'fields'        => [
                            'layout_id'  => '',
                            'image'      => [
                                'id' => $image['image_id'],
                            ],
                            'button'     => [
                                'link_type' => 'link',
                                'link_link' => [
                                    'url'    => $image['link']['url'],
                                    'title'  => $image['link']['title'],
                                    'target' => $image['link']['target'],
                                ]
                            ],
                            'image_only' => 'enabled',
                        ],
                        'classes'       => [
                            'split-item',
                            'page_link',
                        ],
                        'inner_classes' => [
                            'split-item-inner',
                            'mobile-min-height',
                        ],
                    ]
                );
            } else {
                echo \App\template(
                    'flexibles.split-content-layouts.templates.image', [
                        'fields'        => [
                            'layout_id' => '',
                            'image'     => [
                                'id' => $image['image_id'],
                            ],
                        ],
                        'classes'       => [
                            'split-item',
                            'image',
                        ],
                        'inner_classes' => [
                            'split-item-inner',
                            'mobile-min-height',
                        ],
                    ]
                );
            }
            echo "</div>";
        }
    }

    $html = ob_get_clean();

    wp_send_json_success( [
        'html'   => preg_replace( '/\v(?:[\v\h]+)/', '', $html ),
        'count'  => $count,
        'offset' => $offset,
        'images' => $images,
    ] );

    exit;
}
