<?php namespace App\Turbinen\Ajax\LoadNews;

/**
 * Load news
 */
add_action( 'wp_ajax_turbinen_load_news', __NAMESPACE__ . '\\turbinen_load_news' );
add_action( 'wp_ajax_nopriv_turbinen_load_news', __NAMESPACE__ . '\\turbinen_load_news' );

function turbinen_load_news() {
    global $wp_query;

    if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'turbinen-nonce' ) ||
         ! isset( $_REQUEST['offset'] ) ||
         ! isset( $_REQUEST['posts_per_page'] )
    ) {
        wp_send_json_error();
        exit;
    }

    $post_type      = 'post';
    $posts_per_page = $_REQUEST['posts_per_page'];
    $offset         = $_REQUEST['offset'];
    $cat            = isset( $_REQUEST['cat'] ) ? $_REQUEST['cat'] : '';

    $fields = [
        'no_results_message' => isset( $_REQUEST['no_results_message'] ) ? $_REQUEST['no_results_message'] : '',
        'read_more_text'     => isset( $_REQUEST['read_more_text'] ) ? $_REQUEST['read_more_text'] : '',
    ];

    $args = [
        'post_type'      => $post_type,
        'offset'         => $offset,
        'posts_per_page' => $posts_per_page,
        'order'          => 'DESC',
        'orderby'        => 'date',
        'post_status'    => 'publish'
    ];

    if ( ! empty( $cat ) ) {

        $args['cat'] = $cat;
    }

    $wp_query = null;
    $wp_query = new \WP_Query( $args );

    ob_start();

    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            echo \App\template( 'partials.content-' . get_post_type(), [ 'read_more_text' => $fields['read_more_text'] ] );
        }
    } else {
        echo \App\template( 'partials.alert-no-results', [ 'message' => $fields['no_results_message'] ] );
    }

    $html = ob_get_clean();

    $total_count = 0;
    $count_posts = wp_count_posts( $post_type );

    if ( isset( $count_posts->publish ) ) {
        $total_count = absint( $count_posts->publish );
    }

    wp_send_json_success( [
        'html'   => preg_replace( '/\v(?:[\v\h]+)/', '', $html ),
        'count'  => isset( $wp_query->found_posts ) ? $wp_query->found_posts : $total_count,
        'offset' => $offset,
    ] );

    exit;
}
