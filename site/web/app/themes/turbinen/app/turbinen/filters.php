<?php namespace App\Turbinen\Filters;

/**
 * Add <body> classes
 */
add_filter( 'body_class', function ( array $classes ) {

    /** Add page style class */
    if ( ! empty( $page_style = get_field( 'page_style', wpc_get_the_id( true ) ) ) ) {
        $classes[] = 'style-' . $page_style;
    } else {
        $classes[] = 'style-gray';
    }

    return array_filter( $classes );
} );
