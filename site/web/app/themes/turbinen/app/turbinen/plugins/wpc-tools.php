<?php namespace App\Trubinen\Plugins\WPCTools;

/**
 * Add custom Gravity Forms options
 */
add_filter( 'wpc_gravity_forms', function ( $options ) {

    $options['submit_button']['style'] = [
        'btn-ns'     => __( 'Page Style', 'turbinen' ),
        'btn-gray'   => __( 'Gray', 'turbinen' ),
        'btn-blue'   => __( 'Blue', 'turbinen' ),
        'btn-orange' => __( 'Orange', 'turbinen' ),
        'btn-yellow' => __( 'Yellow', 'turbinen' ),
        'btn-green'  => __( 'Green', 'turbinen' ),
    ];

    return $options;
} );
