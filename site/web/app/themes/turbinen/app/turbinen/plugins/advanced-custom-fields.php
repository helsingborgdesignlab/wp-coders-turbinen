<?php namespace App\Turbinen\Plugins\ACF;

/**
 * Register Options Pages
 */
if ( function_exists( 'acf_add_options_page' ) ) {
    $parent = acf_add_options_page( array(
        'page_title' => __( 'Theme Settings: General', 'turbinen' ),
        'menu_title' => __( 'Theme Settings', 'turbinen' ),
        'capability' => 'edit_posts',
        'redirect'   => false
    ) );

    acf_add_options_sub_page( array(
        'title'      => __( 'Theme Settings: Layout Defaults', 'turbinen' ),
        'menu'       => __( 'Layout Defaults', 'turbinen' ),
        'parent'     => $parent['menu_slug'],
        'capability' => 'edit_posts',
        'redirect'   => false
    ) );
}

/**
 * Select field fix
 */
add_filter( 'acf/load_value/type=select', function ( $value, $post_id, $field ) {

    if ( ! is_admin() && $field['return_format'] === 'value' && is_array( $value ) ) {

        if ( empty( $value[0] ) ) {
            $value = empty( $field['default_value'][0] ) ? '' : $field['default_value'][0];
        } else {
            $value = $value[0];
        }
    }

    return $value;
}, 10, 3 );

/**
 * ACF style changes
 */
add_action( 'admin_head', function () {
    $screen = get_current_screen();

    $screen_ids = [
        'page',
        'turb_house',
        'turb_office_space',
        'turb_real_estate'
    ];

    $default_names = [
        'news',
        'logos',
    ];

    if ( isset( $screen->id ) && in_array( $screen->id, $screen_ids ) ) {
        ?>
        <style>
            <?php // defaults ?>
            <?php foreach ( $default_names as $name ) { ?>
            .acf-field-clone[data-name="<?= $name; ?>"] {
                padding: 0;
            }

            .acf-field-clone[data-name="<?= $name; ?>"] > .acf-label {
                display: none;
            }

            .acf-field-clone[data-name="<?= $name; ?>"] > .acf-input > .acf-clone-fields {
                border: 0;
            }
            <?php } ?>
        </style>
        <?php
    }
} );
