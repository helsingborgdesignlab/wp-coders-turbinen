<?php namespace App\Turbinen\Plugins\DrawAttention;

/**
 * Force tooltip layout
 */
add_action( 'save_post', function ( $post_id ) {
    if ( 'da_image' === get_post_type( $post_id ) ) {
        update_post_meta( $post_id, '_da_map_layout', 'tooltip' );
        update_post_meta( $post_id, '_da_event_trigger', 'click' );
    }
}, 999 );

/**
 * Hide admin items
 */
add_action( 'admin_head', function () {
    if ( function_exists( 'get_current_screen' ) ) {
        $screen = get_current_screen();

        if ( ! empty( $screen->id ) && $screen->id === 'da_image' ) {
            ?>
            <style>
                #da_layout_metabox,
                .cmb-row.cmb-type-file {
                    display: none !important;
                }
            </style>
            <?php
        }
    }
} );

/**
 * Custom Color Schemes
 */
//add_filter( 'da_themes', function ( $themes ) {
//
//    $themes = [
//        'available' => [
//            'slug'   => 'green',
//            'name'   => 'Green',
//            'values' => [
//                'image_background_color' => '#ffffff',
//                'map_title_color'        => '#ffffff',
//                'map_text_color'         => '#ffffff',
//                'map_background_color'   => '#ffffff',
//
//                'map_highlight_color'   => '#a1cd07',
//                'map_highlight_opacity' => 0.6,
//
//                'map_border_color'   => '#a1cd07',
//                'map_border_opacity' => 0.6,
//                'map_border_width'   => 1,
//
//                'map_hover_color'   => '#a1cd07',
//                'map_hover_opacity' => 0.8,
//            ],
//        ],
//    ];
//
//    return $themes;
//} );
