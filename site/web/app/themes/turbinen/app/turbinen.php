<?php namespace App;

class Turbinen {

    protected static $_instance = null;

    public $turb_house_id;
    public $turb_real_estate_id;
    public $turb_office_space_id;

    public $turb_house_slug;
    public $turb_real_estate_slug;
    public $turb_office_space_slug;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {

        add_action( 'after_setup_theme', [ $this, 'load_defaults' ] );
    }

    public function load_defaults() {

        $this->turb_house_id        = wpc_get_default_page_id( 'turb_house' );
        $this->turb_real_estate_id  = wpc_get_default_page_id( 'turb_real_estate' );
        $this->turb_office_space_id = wpc_get_default_page_id( 'turb_office_space' );

        $this->turb_house_slug        = empty( $this->turb_house_id ) ? __( 'houses', 'turbinen' ) : get_page_uri( $this->turb_house_id );
        $this->turb_real_estate_slug  = empty( $this->turb_real_estate_id ) ? __( 'real-estates', 'turbinen' ) : get_page_uri( $this->turb_real_estate_id );
        $this->turb_office_space_slug = empty( $this->turb_office_space_id ) ? __( 'office-spaces', 'turbinen' ) : get_page_uri( $this->turb_office_space_id );
    }
}

function Turbinen() {
    return Turbinen::instance();
}

Turbinen();
