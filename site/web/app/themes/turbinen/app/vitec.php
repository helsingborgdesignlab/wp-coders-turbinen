<?php namespace App;

class Vitec {

    protected static $_instance = null;

    public $name;
    public $url_params;
    public $list_urls;
    public $object_urls;
    public $image_url;
    public $file_url;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {

        $this->load_defaults();
        $this->hooks();
    }

    /**
     * load_defaults
     */
    public function load_defaults() {

        $this->name = 'vitec';

        $this->url_params = [
            'licenseid'  => '840240',
            'licensekey' => 'a234feef-5b8d-e187-1c94-dbfd90906593'
        ];

        $this->list_urls = [
            'turb_real_estate'  => 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaListaFastigheter/json',
            'turb_office_space' => 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaListaLokaler/json',
            'turb_apartment'    => 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaListaHyresratt/json',
        ];

        $this->object_urls = [
            'turb_real_estate'  => 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaFastighet/json',
            'turb_office_space' => 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaLokal/json',
        ];

        $this->image_url = 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaBildData/json';

        $this->file_url = 'http://fastighet.export.capitex.se/gemensam/export.svc/HamtaFilData/json';
    }

    /**
     * hooks
     */
    public function hooks() {

        add_action( 'admin_init', [ $this, 'hide_attachments' ] );

        // Import Page
        add_action( 'admin_menu', [ $this, 'load_menu' ] );
        add_action( 'wp_ajax_vitec_ajax_import', [ $this, 'ajax_import' ] );

        // Cron
        foreach ( array_keys( $this->list_urls ) as $post_type ) {
            add_action( 'turbinen_vitec_import', [ $this, 'import_' . $post_type ] );
        }
    }

    /**
     * load_menu
     */
    public function load_menu() {

        foreach ( array_keys( $this->list_urls ) as $post_type ) {
            $menu_id = add_submenu_page(
                'edit.php?post_type=' . $post_type,
                __( 'Import', 'turbinen' ),
                __( 'Import', 'turbinen' ),
                'manage_options',
                'import_' . $post_type,
                [ $this, 'admin_page_callback' ]
            );

            add_action( 'load-' . $menu_id, [ $this, 'load_assets' ] );
        }
    }

    /**
     * admin_page_callback
     */
    public function admin_page_callback() {

        if ( ! current_user_can( 'manage_options' ) ) {
            wp_die( __( 'You do not have sufficient permissions to access this page.', 'turbinen' ) );
        }

        ?>
        <div class="wrap">
            <?php if ( isset( $_GET['post_type'] ) && post_type_exists( $_GET['post_type'] ) ): ?>

                <?php
                $post_type_obj = get_post_type_object( $_GET['post_type'] );
                ?>

                <h1><?= sprintf( __( 'Import %s', 'turbinen' ), $post_type_obj->labels->name ); ?></h1>
                <h2></h2>

                <input type="button" role="button" class="button button-primary vitec-import-button"
                       value="<?= __( 'Import', 'turbinen' ); ?>" data-post_type="<?= $_GET['post_type'] ?>">
            <?php endif; ?>
        </div>
        <?php
    }

    /**
     * load_assets
     */
    public function load_assets() {

        wp_register_script( 'turbinen/' . $this->name . '.js', \App\asset_path( 'scripts/' . $this->name . '.js' ), [ 'jquery' ], null, true );
        wp_localize_script( 'turbinen/' . $this->name . '.js', 'vitecGlobals', [
                'ajax_nonce' => wp_create_nonce( $this->name ),
                'ajax_url'   => admin_url( 'admin-ajax.php' ),
                'confirm'    => __( 'Are you sure?', 'turbinen' ),
                'loading'    => __( 'Loading', 'turbinen' ),
            ]
        );
        wp_enqueue_script( 'turbinen/' . $this->name . '.js' );
    }

    /**
     * ajax_import
     */
    public function ajax_import() {

        if ( ! wp_verify_nonce( $_REQUEST['nonce'], $this->name ) || ! isset( $_REQUEST['post_type'] ) ) {
            wp_send_json_error();
            exit;
        }

        call_user_func( [ $this, 'import_' . $_REQUEST['post_type'] ] );

        wp_send_json_success();
        exit;
    }

    /**
     * hide_attachments
     */
    public function hide_attachments() {

        add_action( 'pre_get_posts', function ( $query ) {
            if ( is_admin() && $query->is_main_query() && ( 'attachment' === $query->get( 'post_type' ) ) ) {
                $query->set( 'meta_query', [
                    [
                        'key'     => 'vitec_guid',
                        'compare' => 'NOT EXISTS',
                    ]
                ] );
            }
        } );

        add_filter( 'ajax_query_attachments_args', function ( $query ) {
            $post_id   = isset( $_REQUEST['post_id'] ) ? $_REQUEST['post_id'] : 0;
            $post_type = get_post_type( $post_id );

            if ( in_array( $post_type, array_keys( $this->list_urls ) ) ) {
                $query['meta_query'][] = [
                    'key'     => 'vitec_guid',
                    'compare' => 'EXISTS',
                ];
            } else {
                $query['meta_query'][] = [
                    'key'     => 'vitec_guid',
                    'compare' => 'NOT EXISTS',
                ];
            }

            return $query;
        } );
    }

    /**
     * remote_get
     *
     * @param $url
     * @param $params
     *
     * @return array|mixed|object|null
     */
    public function remote_get( $url, $params ) {

        $response = wp_remote_get( $url . '?' . http_build_query( $params ), [
            'timeout' => 20,
        ] );

        if ( is_array( $response ) ) {

            $body = json_decode( $response['body'], true );

            if ( is_array( $body ) ) {
                return $body;
            }
        }

        return null;
    }

    /**
     * get_remote_list
     *
     * @param $urls_key
     *
     * @return array|mixed|object|null
     */
    public function get_remote_list( $urls_key ) {

        if ( empty( $urls_key ) ) {
            return null;
        }

        return $this->remote_get( $this->list_urls[ $urls_key ], $this->url_params );
    }

    /**
     * get_remote_object
     *
     * @param $urls_key
     * @param $guid
     *
     * @return array|mixed|object|null
     */
    public function get_remote_object( $urls_key, $guid ) {

        if ( empty( $guid ) || empty( $urls_key ) ) {
            return null;
        }

        return $this->remote_get( $this->object_urls[ $urls_key ], array_merge( $this->url_params, [ 'guid' => $guid ] ) );
    }

    /**
     * get_remote_image
     *
     * @param $guid
     *
     * @return array|mixed|object|null
     */
    public function get_remote_image( $guid ) {

        if ( empty( $guid ) ) {
            return null;
        }

        return $this->remote_get( $this->image_url, array_merge( $this->url_params, [ 'guid' => $guid ] ) );
    }

    /**
     * get_remote_file
     *
     * @param $guid
     *
     * @return array|mixed|object|null
     */
    public function get_remote_file( $guid ) {

        if ( empty( $guid ) ) {
            return null;
        }

        return $this->remote_get( $this->file_url, array_merge( $this->url_params, [ 'guid' => $guid ] ) );
    }

    /**
     * get_post_id_by_guid
     *
     * @param $guid
     *
     * @return string|null
     */
    public function get_post_id_by_guid( $guid ) {

        if ( empty( $guid ) ) {
            return null;
        }

        global $wpdb;

        return $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'vitec_guid' AND meta_value = %s", $guid ) );
    }

    /**
     * add_file_to_library
     *
     * @param $file
     * @param $filename
     *
     * @return int|\WP_Error
     */
    public function add_file_to_library( $decoded_file, $post_id = 0 ) {

        if ( empty( $decoded_file['data'] ) || empty( $decoded_file['name'] ) ) {
            return null;
        }

        $upload_dir  = wp_upload_dir();
        $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;
        $filename    = $decoded_file['name'];

        file_put_contents( $upload_path . $decoded_file['name'], $decoded_file['data'] );

        if ( ! function_exists( 'wp_handle_sideload' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }

        if ( ! function_exists( 'wp_get_current_user' ) ) {
            require_once( ABSPATH . 'wp-includes/pluggable.php' );
        }

        $file             = [];
        $file['error']    = '';
        $file['tmp_name'] = $upload_path . $filename;
        $file['name']     = $filename;
        $file['size']     = filesize( $upload_path . $filename );

        $file_return = wp_handle_sideload( $file, [ 'test_form' => false ] );

        $filename   = $file_return['file'];
        $attachment = [
            'post_mime_type' => $file_return['type'],
            'post_title'     => pathinfo( $decoded_file['name'], PATHINFO_FILENAME ),
            'post_content'   => '',
            'post_status'    => 'inherit',
            'guid'           => $upload_dir['url'] . '/' . basename( $filename )
        ];
        $attach_id  = wp_insert_attachment( $attachment, $filename, $post_id );

        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
        wp_update_attachment_metadata( $attach_id, $attach_data );

        return $attach_id;
    }

    /**
     * add_image_to_library
     *
     * @param $guid
     *
     * @return int|\WP_Error
     */
    public function add_image_to_library( $guid, $post_id = 0 ) {

        if ( empty( $guid ) ) {
            return null;
        }

        $attach_id = $this->get_post_id_by_guid( $guid );

        if ( ! empty( $attach_id ) ) {
            wp_update_post( [
                'ID'          => $attach_id,
                'post_parent' => $post_id,
            ] );

            return $attach_id;
        }

        $image = $this->get_remote_image( $guid );

        if ( empty( $image['data'] ) ) {
            return null;
        }

        $mime_map = [
            'image/jpeg' => 'jpg',
            'image/png'  => 'png',
        ];

        $decoded_data = base64_decode( $image['data'] );
        $details      = getimagesizefromstring( $decoded_data );
        $file_type    = ! empty( $mime_map[ $details['mime'] ] ) ? $mime_map[ $details['mime'] ] : [];

        if ( empty( $file_type ) ) {
            return null;
        }

        $attach_id = $this->add_file_to_library( [
            'data' => $decoded_data,
            'name' => sanitize_title( $guid ) . '.' . $file_type,
        ], $post_id );

        add_post_meta( $attach_id, 'vitec_guid', $guid, true );

        return $attach_id;
    }

    /**
     * add_doc_to_library
     *
     * @param $args
     * @param int $post_id
     *
     * @return int|string|\WP_Error|null
     */
    public function add_doc_to_library( $args, $post_id = 0 ) {

        if ( empty( $args['guid'] ) ) {
            return null;
        }

        $attach_id = $this->get_post_id_by_guid( $args['guid'] );

        if ( ! empty( $attach_id ) ) {
            wp_update_post( [
                'ID'          => $attach_id,
                'post_parent' => $post_id,
            ] );

            return $attach_id;
        }

        if ( empty( $args['type'] ) ) {
            return null;
        }

        if ( empty( $args['name'] ) ) {
            $args['name'] = $args['guid'];
        }

        $doc = $this->get_remote_file( $args['guid'] );

        if ( empty( $doc['data'] ) ) {
            return null;
        }

        $attach_id = $this->add_file_to_library( [
            'data' => base64_decode( $doc['data'] ),
            'name' => $args['name'] . '.' . $args['type'],
        ], $post_id );

        add_post_meta( $attach_id, 'vitec_guid', $args['guid'], true );

        return $attach_id;
    }


    /**
     * import REAL ESTATES
     */
    public function import_turb_real_estate() {

        $active_post_ids = [];

        $post_type  = 'turb_real_estate';
        $taxonomies = [
            'objekttyp' => 'turb_real_estate_type'
        ];

        $list = $this->get_remote_list( $post_type );

        if ( ! empty( $list ) ) {
            foreach ( $list as $item ) {

                if ( empty( $item['andringsdatum'] ) ||
                     empty( $item['guid'] ) ||
                     empty( $item['fastighetsbeteckning'] ) ||
                     empty( $item['objekttyp'] ) ) {

                    continue;
                }

                // get vitec object
                $object = $this->get_remote_object( $post_type, $item['guid'] );

                if ( empty( $object ) ) {
                    continue;
                }

                // get post id by guid
                $post_id = $this->get_post_id_by_guid( $item['guid'] );

                // create or update post
                if ( empty( $post_id ) ) {
                    $post = [
                        'post_type'   => $post_type,
                        'post_status' => 'publish',
                        'post_title'  => $item['fastighetsbeteckning'],
                        'meta_input'  => [
                            'vitec_guid'          => $item['guid'],
                            'vitec_andringsdatum' => $item['andringsdatum'],
                        ]
                    ];

                    $post_id = wp_insert_post( $post );

                    $active_post_ids[] = $post_id;
                } else {
                    $active_post_ids[] = $post_id;

                    // remove taxonomy term relationships
                    wp_delete_object_term_relationships( $post_id, $taxonomies );
                }

                // set taxonomy term relationships
                foreach ( $taxonomies as $taxonomy_key => $taxonomy ) {
                    if ( ! empty( $item[ $taxonomy_key ] ) ) {
                        $term = term_exists( $item[ $taxonomy_key ], $taxonomy );

                        if ( 0 !== $term && null !== $term ) {
                            wp_set_object_terms( $post_id, [ $item[ $taxonomy_key ] ], $taxonomy, true );
                        } else {
                            $term = get_term_by( 'id', wpc_get_default_term_id( $taxonomy ), $taxonomy );

                            if ( ! empty( $term ) ) {
                                wp_set_object_terms( $post_id, $term->name, $taxonomy, true );
                            }
                        }
                    }
                }

                // update vitec_andringsdatum
                update_post_meta( $post_id, 'vitec_andringsdatum', $item['andringsdatum'] );

                // acf text fields
                $text_fields = [
                    'banner_title'     => ! empty( $object['fastighetsbeteckning'] ) ? $object['fastighetsbeteckning'] : '',
                    'intro_sub_title'  => ! empty( $object['adress']['ort'] ) ? $object['adress']['ort'] : '',
                    'intro_text'       => ! empty( $object['omgivning']['beskrivning'] ) ? $object['omgivning']['beskrivning'] : '',
                    'google_map_lat'   => ! empty( $object['vagbeskrivning']['latitud'] ) ? $object['vagbeskrivning']['latitud'] : '',
                    'google_map_lng'   => ! empty( $object['vagbeskrivning']['longitud'] ) ? $object['vagbeskrivning']['longitud'] : '',
                    'profile_name'     => ! empty( $object['huvudhandlaggare']['namn'] ) ? $object['huvudhandlaggare']['namn'] : '',
                    'profile_position' => ! empty( $object['huvudhandlaggare']['befattning'] ) ? $object['huvudhandlaggare']['befattning'] : '',
                    'profile_phone'    => ! empty( $object['huvudhandlaggare']['telefonDirekt'] ) ? $object['huvudhandlaggare']['telefonDirekt'] : '',
                    'profile_email'    => ! empty( $object['huvudhandlaggare']['epost'] ) ? $object['huvudhandlaggare']['epost'] : '',
                ];

                foreach ( $text_fields as $text_field_key => $text_field_value ) {
                    update_field( $text_field_key, $text_field_value, $post_id );
                }

                // acf image fields
                $image_fields = [
                    'thumbnail'       => ! empty( $item['bildGuid'] ) ? $item['bildGuid'] : '',
                    'banner_image'    => ! empty( $object['bilder'][0]['guid'] ) ? $object['bilder'][0]['guid'] : '',
                    'profile_picture' => ! empty( $object['huvudhandlaggare']['bildGuid'] ) ? $object['huvudhandlaggare']['bildGuid'] : '',
                ];

                foreach ( $image_fields as $image_field_key => $image_field_value ) {
                    $attach_id = $this->add_image_to_library( $image_field_value, $post_id );

                    if ( ! empty( $attach_id ) ) {
                        update_field( $image_field_key, $attach_id, $post_id );
                    }
                }

                // acf repeater fields (with title and text sub fields)
                $repeater_fields = [
                    'details'                => [
                        [
                            'title' => 'Område',
                            'text'  => ! empty( $item['omrade'] ) ? $item['omrade'] : '',
                        ],
                        [
                            'title' => 'Fastighetstyp',
                            'text'  => ! empty( $item['objekttyp'] ) ? $item['objekttyp'] : '',
                        ],
                        [
                            'title' => 'Fastighetsbeteckning',
                            'text'  => ! empty( $item['fastighetsbeteckning'] ) ? $item['fastighetsbeteckning'] : '',
                        ],
                    ],
                    'intro_facts'            => [
                        [
                            'title' => 'Fastighetstyp',
                            'text'  => ! empty( $item['objekttyp'] ) ? $item['objekttyp'] : '',
                        ],
                        [
                            'title' => 'Byggår',
                            'text'  => ! empty( $object['byggnadenkel']['byggar'] ) ? $object['byggnadenkel']['byggar'] : '',
                        ],
                        [
                            'title' => 'Internet',
                            'text'  => ! empty( $object['byggnadenkel']['internet']['sammanstallning'] ) ? $object['byggnadenkel']['internet']['sammanstallning'] : '',
                        ],
                        [
                            'title' => 'Övrigt',
                            'text'  => ! empty( $object['ovrigt']['text'] ) ? $object['ovrigt']['text'] : '',
                        ],
                        [
                            'title' => 'Antal bostäder',
                            'text'  => ! empty( $object['fastighetsekonomi']['antalBostader'] ) ? $object['fastighetsekonomi']['antalBostader'] : '',
                        ],
                    ],
                    'description_left_list'  => [
                        [
                            'title' => 'Fastighetsbeteckning',
                            'text'  => ! empty( $item['fastighetsbeteckning'] ) ? $item['fastighetsbeteckning'] : '',
                        ],
                        [
                            'title' => 'Kommun/Ort/Område',
                            'text'  => ( ! empty( $object['kommun'] ) ? $object['kommun'] : '' ) . ' / ' . ( ! empty( $object['adress']['ort'] ) ? $object['adress']['ort'] : '' ) . ' / ' . ( ! empty( $object['omrade'] ) ? $object['omrade'] : '' ),
                        ],
                        [
                            'title' => 'Gatuadress',
                            'text'  => ! empty( $object['adress']['gatuadress'] ) ? $object['adress']['gatuadress'] : '',
                        ],
                        [
                            'title' => 'Energiprestanda',
                            'text'  => ! empty( $object['energi']['energiprestanda'] ) ? $object['energi']['energiprestanda'] : '',
                        ],
                        [
                            'title' => 'Byggår',
                            'text'  => ! empty( $object['byggnadenkel']['byggar'] ) ? $object['byggnadenkel']['byggar'] : '',
                        ],
                    ],
                    'description_right_list' => [
                        [
                            'title' => 'Kommunikationer',
                            'text'  => ! empty( $object['omgivning']['kommunikationer'] ) ? $object['omgivning']['kommunikationer'] : '',
                        ],
                        [
                            'title' => 'Service',
                            'text'  => ! empty( $object['omgivning']['service'] ) ? $object['omgivning']['service'] : '',
                        ],
                        [
                            'title' => 'Parkering',
                            'text'  => ! empty( $object['bilplatser']['ovrigt'] ) ? $object['bilplatser']['ovrigt'] : '',
                        ],
                        [
                            'title' => 'Länkar',
                            'text'  => ! empty( $object['lankar']['url'] ) ? '<a href="' . $object['lankar']['url'] . '" target="_blank">' . $object['lankar']['url'] . '</a>' : '',
                        ],
                    ],
                ];

                foreach ( $repeater_fields as $repeater_field_key => $repeater_field_value ) {
                    update_field( $repeater_field_key, [], $post_id );

                    if ( ! empty( $repeater_field_value ) ) {
                        foreach ( $repeater_field_value as $row ) {
                            if ( ! empty( $row['text'] ) ) {
                                add_row( $repeater_field_key, $row, $post_id );
                            }
                        }
                    }
                }

                // acf files_images
                if ( ! empty( $object['bilder'] ) ) {

                    $images = [];

                    foreach ( $object['bilder'] as $image ) {
                        if ( ! empty( $image['guid'] ) ) {
                            $attach_id = $this->add_image_to_library( $image['guid'], $post_id );

                            if ( ! empty( $attach_id ) ) {
                                $images[] = $attach_id;
                            }
                        }
                    }

                    unset( $images[0] );

                    update_field( 'files_images', $images, $post_id );
                } else {
                    update_field( 'files_images', [], $post_id );
                }

                // acf files_documents
                if ( ! empty( $object['filer'] ) ) {

                    $documents = [];

                    foreach ( $object['filer'] as $file ) {
                        $attach_id = $this->add_doc_to_library( [
                            'guid' => ! empty( $file['guid'] ) ? $file['guid'] : '',
                            'type' => ! empty( $file['filTyp'] ) ? $file['filTyp'] : '',
                            'name' => ! empty( $file['namn'] ) ? $file['namn'] : '',
                        ], $post_id );

                        if ( ! empty( $attach_id ) ) {
                            $documents[] = $attach_id;
                        }
                    }

                    update_field( 'files_documents', $documents, $post_id );
                } else {
                    update_field( 'files_documents', [], $post_id );
                }
            }
        }

        $this->import_delete_helper(
            $post_type,
            [],
            $active_post_ids
        );
    }

    /**
     * import OFFICE SPACES
     */
    public function import_turb_office_space() {

        $active_post_ids = [];

        $post_type  = 'turb_office_space';
        $taxonomies = [
            'omrade'    => 'turb_office_space_area',
            'objekttyp' => 'turb_office_space_type',
        ];

        $list = $this->get_remote_list( $post_type );

        if ( ! empty( $list ) ) {
            foreach ( $list as $item ) {

                if ( empty( $item['andringsdatum'] ) ||
                     empty( $item['guid'] ) ||
                     empty( $item['gatuadress'] ) ) {
                    continue;
                }

                // get vitec object
                $object = $this->get_remote_object( $post_type, $item['guid'] );

                if ( empty( $object ) ) {
                    continue;
                }

                // get post id by guid
                $post_id = $this->get_post_id_by_guid( $item['guid'] );

                // create or update post
                if ( empty( $post_id ) ) {
                    $post = [
                        'post_type'   => $post_type,
                        'post_status' => 'publish',
                        'post_title'  => $item['gatuadress'],
                        'meta_input'  => [
                            'vitec_guid'          => $item['guid'],
                            'vitec_andringsdatum' => $item['andringsdatum'],
                        ]
                    ];

                    $post_id = wp_insert_post( $post );

                    $active_post_ids[] = $post_id;
                } else {
                    $active_post_ids[] = $post_id;

                    // remove taxonomy term relationships
                    wp_delete_object_term_relationships( $post_id, $taxonomies );
                }

                // set taxonomy term relationships
                foreach ( $taxonomies as $taxonomy_key => $taxonomy ) {
                    if ( ! empty( $item[ $taxonomy_key ] ) ) {
                        wp_set_object_terms( $post_id, [ $item[ $taxonomy_key ] ], $taxonomy, true );
                    }
                }

                // update vitec_andringsdatum
                update_post_meta( $post_id, 'vitec_andringsdatum', $item['andringsdatum'] );

                // acf text fields
                $text_fields = [
                    'location'         => ! empty( $item['kommun'] ) ? ( ! empty( $item['omrade'] ) ? $item['omrade'] . ', ' . $item['kommun'] : $item['kommun'] ) : ( ! empty( $item['omrade'] ) ? $item['omrade'] : '' ),
                    'banner_title'     => ! empty( $item['gatuadress'] ) ? $item['gatuadress'] : '',
                    'intro_sub_title'  => ! empty( $object['adress']['ort'] ) ? $object['adress']['ort'] : '',
                    'intro_text'       => ! empty( $object['omgivning']['beskrivning'] ) ? $object['omgivning']['beskrivning'] : '',
                    'google_map_lat'   => ! empty( $object['vagbeskrivning']['latitud'] ) ? $object['vagbeskrivning']['latitud'] : '',
                    'google_map_lng'   => ! empty( $object['vagbeskrivning']['longitud'] ) ? $object['vagbeskrivning']['longitud'] : '',
                    'profile_name'     => ! empty( $object['huvudhandlaggare']['namn'] ) ? $object['huvudhandlaggare']['namn'] : '',
                    'profile_position' => ! empty( $object['huvudhandlaggare']['befattning'] ) ? $object['huvudhandlaggare']['befattning'] : '',
                    'profile_phone'    => ! empty( $object['huvudhandlaggare']['telefonDirekt'] ) ? $object['huvudhandlaggare']['telefonDirekt'] : '',
                    'profile_email'    => ! empty( $object['huvudhandlaggare']['epost'] ) ? $object['huvudhandlaggare']['epost'] : '',
                ];

                foreach ( $text_fields as $text_field_key => $text_field_value ) {
                    update_field( $text_field_key, $text_field_value, $post_id );
                }

                // acf image fields
                $image_fields = [
                    'thumbnail'       => ! empty( $item['bildGuid'] ) ? $item['bildGuid'] : '',
                    'banner_image'    => ! empty( $object['bilder'][0]['guid'] ) ? $object['bilder'][0]['guid'] : '',
                    'profile_picture' => ! empty( $object['huvudhandlaggare']['bildGuid'] ) ? $object['huvudhandlaggare']['bildGuid'] : '',
                ];

                foreach ( $image_fields as $image_field_key => $image_field_value ) {
                    $attach_id = $this->add_image_to_library( $image_field_value, $post_id );

                    if ( ! empty( $attach_id ) ) {
                        update_field( $image_field_key, $attach_id, $post_id );
                    }
                }

                // acf repeater fields (with title and text sub fields)
                $repeater_fields = [
                    'details'                => [
                        [
                            'title' => 'Fastighetstyp',
                            'text'  => ! empty( $item['objekttyp'] ) ? $item['objekttyp'] : '',
                        ],
                        [
                            'title' => 'Status',
                            'text'  => ! empty( $item['status'] ) ? $item['status'] : '',
                        ],
                    ],
                    'intro_facts'            => [
                        [
                            'title' => 'Fastighetstyp',
                            'text'  => ! empty( $item['objekttyp'] ) ? $item['objekttyp'] : '',
                        ],
                        [
                            'title' => 'Byggår',
                            'text'  => ! empty( $object['byggnadenkel']['byggar'] ) ? $object['byggnadenkel']['byggar'] : '',
                        ],
                        [
                            'title' => 'Internet',
                            'text'  => ! empty( $object['byggnadenkel']['internet']['sammanstallning'] ) ? $object['byggnadenkel']['internet']['sammanstallning'] : '',
                        ],
                        [
                            'title' => 'Övrigt',
                            'text'  => ! empty( $object['ovrigt']['text'] ) ? $object['ovrigt']['text'] : '',
                        ],
                    ],
                    'description_left_list'  => [
                        [
                            'title' => 'Fastighetsbeteckning',
                            'text'  => ! empty( $item['fastighetsbeteckning'] ) ? $item['fastighetsbeteckning'] : '',
                        ],
                        [
                            'title' => 'Kommun/Ort/Område',
                            'text'  => ( ! empty( $object['kommun'] ) ? $object['kommun'] : '' ) . ' / ' . ( ! empty( $object['adress']['ort'] ) ? $object['adress']['ort'] : '' ) . ' / ' . ( ! empty( $object['omrade'] ) ? $object['omrade'] : '' ),
                        ],
                        [
                            'title' => 'Gatuadress',
                            'text'  => ! empty( $object['adress']['gatuadress'] ) ? $object['adress']['gatuadress'] : '',
                        ],
                        [
                            'title' => 'Energiprestanda',
                            'text'  => ! empty( $object['energi']['energiprestanda'] ) ? $object['energi']['energiprestanda'] : '',
                        ],
                        [
                            'title' => 'Byggår',
                            'text'  => ! empty( $object['byggnadenkel']['byggar'] ) ? $object['byggnadenkel']['byggar'] : '',
                        ],
                    ],
                    'description_right_list' => [
                        [
                            'title' => 'Kommunikationer',
                            'text'  => ! empty( $object['omgivning']['kommunikationer'] ) ? $object['omgivning']['kommunikationer'] : '',
                        ],
                        [
                            'title' => 'Service',
                            'text'  => ! empty( $object['omgivning']['service'] ) ? $object['omgivning']['service'] : '',
                        ],
                        [
                            'title' => 'Parkering',
                            'text'  => ! empty( $object['bilplatser']['ovrigt'] ) ? $object['bilplatser']['ovrigt'] : '',
                        ],
                        [
                            'title' => 'Länkar',
                            'text'  => ! empty( $object['lankar']['url'] ) ? '<a href="' . $object['lankar']['url'] . '" target="_blank">' . $object['lankar']['url'] . '</a>' : '',
                        ],
                    ],
                ];

                foreach ( $repeater_fields as $repeater_field_key => $repeater_field_value ) {
                    update_field( $repeater_field_key, [], $post_id );

                    if ( ! empty( $repeater_field_value ) ) {
                        foreach ( $repeater_field_value as $row ) {
                            if ( ! empty( $row['text'] ) ) {
                                add_row( $repeater_field_key, $row, $post_id );
                            }
                        }
                    }
                }

                // acf files_images
                if ( ! empty( $object['bilder'] ) ) {

                    $images = [];

                    foreach ( $object['bilder'] as $image ) {
                        if ( ! empty( $image['guid'] ) ) {
                            $attach_id = $this->add_image_to_library( $image['guid'], $post_id );

                            if ( ! empty( $attach_id ) ) {
                                $images[] = $attach_id;
                            }
                        }
                    }

                    unset( $images[0] );

                    update_field( 'files_images', $images, $post_id );
                } else {
                    update_field( 'files_images', [], $post_id );
                }

                // acf files_documents
                if ( ! empty( $object['filer'] ) ) {

                    $documents = [];

                    foreach ( $object['filer'] as $file ) {
                        $attach_id = $this->add_doc_to_library( [
                            'guid' => ! empty( $file['guid'] ) ? $file['guid'] : '',
                            'type' => ! empty( $file['filTyp'] ) ? $file['filTyp'] : '',
                            'name' => ! empty( $file['namn'] ) ? $file['namn'] : '',
                        ], $post_id );

                        if ( ! empty( $attach_id ) ) {
                            $documents[] = $attach_id;
                        }
                    }

                    update_field( 'files_documents', $documents, $post_id );
                } else {
                    update_field( 'files_documents', [], $post_id );
                }
            }
        }

        $this->import_delete_helper(
            $post_type,
            $taxonomies,
            $active_post_ids
        );
    }

    /**
     * import APARTMENTS
     */
    public function import_turb_apartment() {

        $active_post_ids = [];

        $post_type  = 'turb_apartment';
        $taxonomies = [
            'kommun' => 'turb_apartment_municipality'
        ];

        $list = $this->get_remote_list( $post_type );

        if ( ! empty( $list ) ) {
            foreach ( $list as $item ) {

                if ( empty( $item['andringsdatum'] ) ||
                     empty( $item['guid'] ) ||
                     empty( $item['gatuadress'] ) ) {
                    continue;
                }

                // get post id by guid
                $post_id = $this->get_post_id_by_guid( $item['guid'] );

                // create or update post
                if ( empty( $post_id ) ) {
                    $post = [
                        'post_type'   => $post_type,
                        'post_status' => 'publish',
                        'post_title'  => $item['gatuadress'],
                        'meta_input'  => [
                            'vitec_guid'          => $item['guid'],
                            'vitec_andringsdatum' => $item['andringsdatum'],
                        ]
                    ];

                    $post_id = wp_insert_post( $post );

                    $active_post_ids[] = $post_id;
                } else {
                    $active_post_ids[] = $post_id;

                    // remove taxonomy term relationships
                    wp_delete_object_term_relationships( $post_id, $taxonomies );
                }

                // set taxonomy term relationships
                foreach ( $taxonomies as $taxonomy_key => $taxonomy ) {
                    if ( ! empty( $item[ $taxonomy_key ] ) ) {
                        wp_set_object_terms( $post_id, [ $item[ $taxonomy_key ] ], $taxonomy, true );
                    }
                }

                // update vitec_andringsdatum
                update_post_meta( $post_id, 'vitec_andringsdatum', $item['andringsdatum'] );

                // acf text fields
                $text_fields = [
                    'location' => ! empty( $item['postadress'] ) ? $item['postadress'] : '',
                ];

                foreach ( $text_fields as $text_field_key => $text_field_value ) {
                    update_field( $text_field_key, $text_field_value, $post_id );
                }

                // acf image fields
                $image_fields = [
                    'thumbnail' => ! empty( $item['bildGuid'] ) ? $item['bildGuid'] : '',
                ];

                foreach ( $image_fields as $image_field_key => $image_field_value ) {
                    $attach_id = $this->add_image_to_library( $image_field_value, $post_id );

                    if ( ! empty( $attach_id ) ) {
                        update_field( $image_field_key, $attach_id, $post_id );
                    }
                }

                // acf repeater fields (with title and text sub fields)
                $repeater_fields = [
                    'details' => [
                        [
                            'title' => 'Boendetyp',
                            'text'  => 'Hyresrätt',
                        ],
                        [
                            'title' => 'Storlek',
                            'text'  => ( ! empty( $item['antalRum'] ) ? $item['antalRum'] : '' ) . ' / ' . ( ! empty( $item['bostadsarea'] ) ? $item['bostadsarea'] : '' ),
                        ],
                        [
                            'title' => 'Vaningsplan',
                            'text'  => ! empty( $item['vaningsplan'] ) ? $item['vaningsplan'] : '',
                        ],
                        [
                            'title' => 'Månadshyra',
                            'text'  => ! empty( $item['manadshyra'] ) ? $item['manadshyra'] : '',
                        ],
                        [
                            'title' => 'Inflyttning',
                            'text'  => ! empty( $item['tilltradeDatum'] ) ? $item['tilltradeDatum'] : '',
                        ],
                    ],
                ];

                foreach ( $repeater_fields as $repeater_field_key => $repeater_field_value ) {
                    update_field( $repeater_field_key, [], $post_id );

                    if ( ! empty( $repeater_field_value ) ) {
                        foreach ( $repeater_field_value as $row ) {
                            if ( ! empty( $row['text'] ) ) {
                                add_row( $repeater_field_key, $row, $post_id );
                            }
                        }
                    }
                }
            }
        }

        $this->import_delete_helper(
            $post_type,
            $taxonomies,
            $active_post_ids
        );
    }

    /**
     * import delete helper
     *
     * @param $post_types
     * @param $taxonomies
     * @param $active_post_ids
     */
    public function import_delete_helper( $post_type, $taxonomies, $active_post_ids ) {

        // delete inactive posts
        $inactive_post_ids = get_posts( [
            'post_type'      => $post_type,
            'posts_per_page' => - 1,
            'fields'         => 'ids',
            'post__not_in'   => $active_post_ids,
        ] );

        if ( ! empty( $inactive_post_ids ) ) {
            foreach ( $inactive_post_ids as $post_id ) {

                // delete attachments
                $attachments = get_attached_media( '', $post_id );

                if ( ! empty( $attachments ) ) {
                    foreach ( $attachments as $attachment ) {
                        wp_delete_attachment( $attachment->ID, 'true' );
                    }
                }

                // delete post
                wp_delete_post( $post_id, true );
            }
        }

        // delete empty taxonomy terms
        if ( ! empty( $taxonomies ) ) {
            foreach ( $taxonomies as $taxonomy ) {
                $terms = get_terms( [
                    'taxonomy'   => $taxonomy,
                    'hide_empty' => false,
                ] );

                foreach ( $terms as $term ) {
                    if ( isset( $term->count ) && ( absint( $term->count ) < 1 ) ) {
                        wp_delete_term( $term->term_id, $taxonomy );
                    }
                }
            }
        }
    }
}

function Vitec() {
    return Vitec::instance();
}

Vitec();
